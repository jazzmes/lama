import traceback
import socket
import logging
import collections
from os import path
from argparse import ArgumentParser, ArgumentTypeError
from datetime import datetime
from multiprocessing import Pool
from time import sleep
from functools import reduce
from paramiko import SSHClient, AutoAddPolicy, SSHException, AuthenticationException
from manage.configs import CollectdConfigGenerator

logging.getLogger("paramiko").setLevel(logging.WARNING)


class connection(object):

    def __init__(self, ip, max_retries=3):
        self._ip = ip
        self._max_retries = max_retries
        self._connection = None

    def __enter__(self):
        retries = 0
        self._connection = SSHClient()
        self._connection.set_missing_host_key_policy(AutoAddPolicy())
        while retries < self._max_retries:
            try:
                print("{} [{}] Login: attempt {}".format(datetime.now(), self._ip, retries + 1))
                self._connection.connect(self._ip, username='lama')
                return self._connection
            except (socket.error, AuthenticationException, SSHException, EOFError) as e:
                # print("%s [%s] Exc: %s, trace=%s" % (datetime.now(), self._ip, e, traceback.format_exc()))
                print("{} [{}] Failed login: {}".format(datetime.now(), self._ip, e))
                # self._connection.close()
                if retries < 3:
                    retries += 1
                    sleep(.5)
                    continue
                pass

        raise ConnectionError() from None

    # noinspection PyUnusedLocal
    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._connection:
            self._connection.close()


class ActionManager(object):

    def __init__(self, args, max_procs=100):
        self._args = args
        self._targets = []

        self.get_targets()

        # config
        self._max_procs = max_procs

    def process_command(self):
        method = getattr(self, self._args.action)
        if method:
            with Pool(min(len(self._targets), self._max_procs)) as p:
                p.map(method, self._targets)
        else:
            print("Subaction '%s' not implemented".format(self._args.action))

    def get_targets(self):
        if self._args.target_file:
            with open(self._args.target_file, 'r') as fp:
                self._targets = [tuple(line.split()) for line in fp.readlines() if not line.lstrip().startswith(";")]

        if self._args.host:
            self._targets.append(self._args.host)


class ThanatosActionManager(ActionManager):

    @staticmethod
    def start(target):
        print("{} [{}] Processing target of type '{}'".format(datetime.now(), *target))
        ip, agent_type = target
        try:
            with connection(ip) as conn:
                cmd = "ps cax | grep thanatos_server > /dev/null; if [[ $? -ne 0 ]]; then " \
                      "echo 'New process'; `nohup thanatos_server > /var/log/thanatos/thanatos_server.log 2>&1 &`; " \
                      "else echo 'Already Running'; " \
                      "fi"
                sin, sout, serr = conn.exec_command(cmd)
                print("{} [{}] Thanatos server started: {}.".format(
                    datetime.now(),
                    ip,
                    str(sout.read(), encoding='utf-8').strip(),
                    str(serr.read(), encoding='utf-8').strip()
                ))
        except ConnectionError:
            print("{} [{}] Unable to connect!".format(datetime.now(), ip))

    @staticmethod
    def stop(target):
        print("{} [{}] Processing target of type '{}'".format(datetime.now(), *target))

        ip, agent_type = target
        try:
            with connection(ip) as conn:
                cmd = "killall thanatos_server"
                sin, sout, serr = conn.exec_command(cmd)
                print("{} [{}] Thanatos server stopped: {} / {}.".format(
                    datetime.now(),
                    ip,
                    str(sout.read(), encoding='utf-8').strip(),
                    str(serr.read(), encoding='utf-8').strip()
                ))
        except ConnectionError:
            print("{} [{}] Unable to connect!".format(datetime.now(), ip))

    @staticmethod
    def restart(target):
        ThanatosActionManager.stop(target)
        ThanatosActionManager.start(target)


class CollectdActionManager(ActionManager):

    config_folder = '/etc/collectd'
    config_file = 'collectd.conf'

    def configure(self, target):
        print("{} [{}] Collectd:configure target of type '{}'".format(datetime.now(), *target))
        ip, agent_type = target

        # login
        try:
            original_backup = '/etc/collectd/collectd.conf.original'
            original_file = path.join(
                CollectdActionManager.config_folder,
                CollectdActionManager.config_file
            )
            tmp_file = '__tmp_collectd.conf'
            with connection(ip) as conn:
                # backup config file
                cmd = 'sudo cp {} {}'.format(
                    original_file,
                    path.join(
                        CollectdActionManager.config_folder,
                        '{}.bkp.last'.format(CollectdActionManager.config_file)
                        # '{}.bkp.{}'.format(CollectdActionManager.config_file, datetime.now().strftime("%Y%m%d%H%M%S"))
                    )
                )
                _, sout, serr = conn.exec_command(cmd)
                print("{} [{}] Backup config file: {}/{}.".format(
                    datetime.now(),
                    ip,
                    str(sout.read(), encoding='utf-8').strip(),
                    str(serr.read(), encoding='utf-8').strip()
                ))

                # save once the old collectd original file
                cmd = '[ -f {1} ] && echo "No copy" || sudo cp {0} {1}'.format(
                    original_file,
                    original_backup
                )
                _, sout, serr = conn.exec_command(cmd)
                if str(sout.read(), encoding='utf-8').strip() != "No copy":
                    print("{} [{}] Backed up original config file.".format(
                        datetime.now(),
                        ip,
                    ))

                # create configuration
                config_str = CollectdConfigGenerator(**self._args.attributes).generate_config()

                # write file
                sftp = conn.open_sftp()
                file = sftp.file(tmp_file, mode='w')
                file.write(config_str)
                file.flush()
                sftp.close()

                cmd = 'sudo mv {} {}'.format(
                    tmp_file,
                    original_file
                )
                _, sout, serr = conn.exec_command(cmd)
                print("{} [{}] Created new config file: {}/{}.".format(
                    datetime.now(),
                    ip,
                    str(sout.read(), encoding='utf-8').strip(),
                    str(serr.read(), encoding='utf-8').strip()
                ))

                # restart collectd
                cmd = 'sudo systemctl restart collectd'
                _, sout, serr = conn.exec_command(cmd)
                print("{} [{}] Restart collectd: {}.".format(
                    datetime.now(),
                    ip,
                    str(sout.read(), encoding='utf-8').strip(),
                    str(serr.read(), encoding='utf-8').strip()
                ))
        except Exception:
            print("Unable to configure collectd: {}".format(traceback.format_exc()))

    @staticmethod
    def attribute(val):

        def check_bool(val):
            if val.lower() == 'true':
                return True
            elif val.lower() == 'false':
                return False
            raise ArgumentTypeError()

        def parse_val(val):
            for _type in [int, float, check_bool]:
                try:
                    return _type(val)
                except:
                    print("Failed parsing '{}' with type '{}'".format(val, _type))
            return val

        # plugin_attributes
        attribute = {}
        try:
            attr_name, attr_val = val.split('=')

            attr_name_parts = attr_name.split('.')
            if len(attr_name_parts) > 1:
                attribute['plugin_attributes'] = {attr_name_parts[0]: {attr_name_parts[1]: parse_val(attr_val)}}
            else:
                attribute[attr_name] = parse_val(attr_val)
        except:
            raise ArgumentTypeError('Bad attribute: {}'.format(val))

        return attribute

    @staticmethod
    def update(base_dict, update_dict):
        for k, v in update_dict.items():
            if isinstance(v, collections.Mapping):
                base_dict[k] = CollectdActionManager.update(base_dict.get(k, {}), v)
            else:
                base_dict[k] = update_dict[k]
        return base_dict


def ip_type_pair(val):
    try:
        ip, ht = val.split(':')
        return ip, ht
    except Exception as e:
        print("Error: {} - Format for inline host is <ip>:<host_type>".format(e))

    raise ArgumentTypeError('Bad IP:type combination')

if __name__ == "__main__":

    ManageActions = {
        'thanatos': ThanatosActionManager,
        'collectd': CollectdActionManager
    }

    parser = ArgumentParser()
    parser.add_argument('-l', dest='log_level', help="Set debug level",
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], default='WARNING')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-f', type=str, dest='target_file', help="File containing list of targets.")
    group.add_argument('-s', type=ip_type_pair, dest='host', help="<IP>:<Type of host>")

    subparsers = parser.add_subparsers(title="framework", description="Framework to operate in", dest="framework")
    subparsers.required = True

    provider_subparser = subparsers.add_parser('provider', help="Operations on providers")
    provider_subparser.add_argument('action', help="Provider action to perform",
                                    choices=["add", "del", "list"])

    lama_subparser = subparsers.add_parser("lama", help="LAMA system actions")
    lama_subparser.add_argument('action', help="LAMA action to perform",
                                choices=["start", "stop", "reset", "restart", "status"])

    thanatos_subparser = subparsers.add_parser('thanatos', help="Thanatos Event Manager")
    thanatos_subparser.add_argument('action', help="Action to perform",
                                    choices=['start', 'stop', 'restart'])

    collectd_subparser = subparsers.add_parser('collectd', help="Collectd manager")
    # collect_subparser.add_argument('action', help="Collectd configuration action to perform",
    #                                choices=['configure'])
    collectd_action_subparsers = collectd_subparser.add_subparsers(
        title='action',
        description='Action to perform',
        dest='action'
    )
    collectd_config_subparser = collectd_action_subparsers.add_parser('configure', help='Collectd configurations')
    collectd_config_subparser.add_argument('-a', dest='attributes', help='Non-default options', nargs='+',
                                           type=CollectdActionManager.attribute)

    args = parser.parse_args()
    if args.framework == 'collectd':
        args.attributes = reduce(CollectdActionManager.update, args.attributes)
    print("Arguments: {}".format(args))

    man = ManageActions.get(args.framework)

    if not man:
        print("Action '{}' not implemented yet".format(args.framework))
        exit()

    man(args).process_command()
