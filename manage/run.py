#!/usr/bin/env python3
from argparse import ArgumentParser, ArgumentTypeError
import inspect
import logging
import sys
from lama.scripts.lama_interface import LamaAPI

__author__ = 'tiago'


class ActionHandler(object):

    def __init__(self, dispatcher):
        self.dispatcher = dispatcher

    def workload(self, app_name, config=None, workload_file=None):
        if not config and not workload_file:
            return "Please provide non-zero number of clients or a path workload-spec file"

        if config:
            LamaAPI(self.dispatcher, app_name).change_workload_clients(config)
        else:
            # workload file
            raise NotImplementedError()

    def start(self, app_name):
        LamaAPI(self.dispatcher, app_name).start_app()


def set_logger(level):
    log = logging.getLogger('lama::run')
    log.setLevel(level)
    ch = logging.StreamHandler(sys.stdout)
    # formatter = logging.Formatter('%(asctime)s - %(name)s %(levelname)s - %(message)s')
    formatter = logging.Formatter('%(asctime)s: %(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)
    logging.root = log
    return log


def dict_config(val):
    try:
        d = dict(entry.split("=") for entry in val.split(","))
    except:
        raise ArgumentTypeError("'%s' is not a valid list of attributes" % val)
    return d

if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument('-l', dest='log_level', help="Set debug level",
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], default='WARNING')
    parser.add_argument('-d', dest='dispatcher', help="Set dispatcher IP address",
                        default="10.1.1.12")
    subparsers = parser.add_subparsers(title="actions", description="Action to perform", dest="action")
    subparsers.required = True

    # workload
    parser_workload = subparsers.add_parser('workload', help='Set app workload')
    parser_workload.add_argument('app_name', type=str, help="Application name.")
    workload_group = parser_workload.add_mutually_exclusive_group(required=True)
    workload_group.add_argument(
        '-c', dest='config', type=dict_config, metavar="ATTR1=VAL1,ATTR2=VAL2 ...",
        help="Add a single client node with config ATTR1=VAL1,ATTR2=VAL2,... .")
    workload_group.add_argument(
        '-f', dest='workload_file', help="File specifiying complex workload (change app specific arguments).")

    # start
    parser_start = subparsers.add_parser('start', help='Set app workload')
    parser_start.add_argument('app_name', type=str, help="Application name.")

    # stop
    # parser_stop = subparsers.add_parser('stop', help='Stop application')
    # parser_stop.add_argument('app_name', type=str, help="Appplication name.")

    params = parser.parse_args()
    logger = set_logger(level=params.log_level)

    logger.info(params)
    ah = ActionHandler(params.dispatcher)
    try:
        method = getattr(ah, params.action)
        args = vars(params)
        try:
            args = {k: args[k] for k in inspect.getargspec(method).args if k != "self"}
        except KeyError as e:
            logger.error("Required parameter '%s' not set: ", e.args[0])
        else:
            method(**args)
    except AttributeError:
        logger.error("Action '%s' not yet implemented.", params.action)
        exit(-1)

