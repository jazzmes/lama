#!/usr/bin/env python3

from argparse import ArgumentParser
from sys import argv, stdout
from time import sleep
from pexpect.pxssh import ExceptionPxssh
from pexpect.pxssh import pxssh
from io import BytesIO
import os
import re
import traceback
import pexpect
import time
import threading
from datetime import datetime


class ManageLama(threading.Thread):

    def __init__(self, target, actions):
        super(ManageLama, self).__init__()

        self.target = target
        self.actions = actions
        self.result = False
        self.child = None

    def run(self):
        if not self.target:
            print("Bad target: %s" % self.target)
            return -1

        ip, ag = self.target
        self.name = "%s @ %s" % (ag, ip)

        for a in self.actions:
            if a(self, ip, ag) < 0:
                return -1
            else:
                self.result = True

            # self.child.close()

    def login(self, ip):
        retries = 0
        while True:
            try:
                print("%s [%s] Login: attempt %d" % (datetime.now(), ip, retries + 1))
                self.child = pxssh()
                self.child.login(ip, "lama")
                return 0
            except (ExceptionPxssh, AssertionError) as e:
                print("%s [%s] Exception: %s" % (datetime.now(), ip, traceback.format_exc()))
                print("%s [%s] Failed login" % (datetime.now(), ip))
                self.child.close()
                if retries < 3:
                    retries += 1
                    # sleep(5)
                    continue
                else:
                    return -1

    def stop_lama(self, ip, ag, reset=False):
        try:
            self.login(ip)
        except ExceptionPxssh:
            return -1

        if ag == "dispatcher":
            print("%s [%s] Stopping Web interface" % (datetime.now(), ip))
            self.child.sendline("sudo supervisorctl stop lama_gui")
            self.child.prompt()

            print("%s [%s] Stopping dispatcher" % (datetime.now(), ip))
            self.child.sendline("sudo supervisorctl stop lama_dispatcher")
            self.child.prompt()

            self.child.sendline("sudo pkill -f lama.ops.experiments --signal 9")
            self.child.prompt()

        elif ag == "provider":
            print("%s [%s] Stopping provider" % (datetime.now(), ip))
            self.child.sendline("sudo supervisorctl stop lama_provider")
            self.child.prompt()

            # Make sure
            self.child.sendline("sudo pkill -f run_provider")
            self.child.prompt()

            print("%s [%s] Stopping apps" % (datetime.now(), ip))
            self.child.sendline("sudo pkill -9 -f run_application")
            self.child.prompt()

        if reset:
            print("%s [%s] Reset" % (datetime.now(), ip))
            self.child.sendline("sudo rm -f /home/lama/lama/persistence/*")
            self.child.prompt()

            self.child.sendline("sudo rm -f /home/lama/lama/vm-images/*")
            self.child.prompt()

            print("%s [%s] Clear iptables" % (datetime.now(), ip))
            self.child.sendline("sudo /home/lama/lama/scripts/cleaript.sh")
            self.child.prompt()

            print("%s [%s] Remove logs" % (datetime.now(), ip))
            self.child.sendline("rm -f /var/log/lama/*")
            # "find /var/log/lama/* -type f -exec dd if=/dev/null of={} \;"
            self.child.prompt()

            if ag == "provider":
                print("%s [%s] Delete VMs" % (datetime.now(), ip))
                self.delete_vms(ip, ag)

                # stop dnsmasq
                print("%s [%s] Stopping dnsmasq" % (datetime.now(), ip))
                self.child.sendline("sudo pkill -f dnsmasq.*lama")
                self.child.prompt()

                self.child.sendline("sudo rm -rf /home/lama/lama/dnsmasq")
                self.child.prompt()

                print("%s [%s] Stopping ryu" % (datetime.now(), ip))
                self.child.sendline("sudo pkill -f ryu.*lama")
                self.child.prompt()

                print("%s [%s] Kill internal SSH and namespace processes" % (datetime.now(), ip))
                self.child.sendline("sudo pkill -9 -e -f ssh.*192.168.*")
                self.child.prompt()

                self.child.sendline("sudo pkill -9 -e -f 'sudo ip netns'")
                self.child.prompt()

                print("%s [%s] Remove bridges" % (datetime.now(), ip))
                self.child.sendline("sudo ovs-vsctl list-br")
                output = BytesIO()
                self.child.logfile = output
                self.child.prompt()
                result = output.getvalue().decode("UTF-8").splitlines()
                output.close()
                self.child.logfile = None
                for a in range(1, len(result) - 1):
                    if result[a].startswith("lbr-"):
                        print("%s [%s] Remove bridge: %s" % (datetime.now(), ip, result[a]))
                        self.child.sendline("sudo ovs-vsctl del-br %s" % result[a])
                        self.child.prompt()

                # delete bridges
                output = BytesIO()
                self.child.logfile = output
                self.child.sendline("ip link")
                self.child.prompt()
                result = output.getvalue().decode("UTF-8")
                pattern = re.compile(' (lgw-\S+):')
                m = re.findall(pattern, result)
                if m:
                    for br in m:
                        print("%s [%s] Delete bridge: %s" % (datetime.now(), ip, br))
                        self.child.sendline("sudo ip link del %s" % br)
                        self.child.prompt()

                print("%s [%s] Remove namespaces" % (datetime.now(), ip))
                self.child.sendline("ip netns")
                output = BytesIO()
                self.child.logfile = output
                self.child.prompt()
                result = output.getvalue().decode("UTF-8").splitlines()
                output.close()

                self.child.logfile = None
                for a in range(1, len(result) - 1):
                    namespace = result[a].split()[0]
                    print("%s [%s] Remove namespace: %s" % (datetime.now(), ip, namespace))
                    self.child.sendline("sudo ip netns delete %s" % namespace)
                    self.child.prompt()

        self.child.logout()
        return 0

    def deploy_lama(self, ip, ag):

        base_path = os.path.abspath(os.path.join(os.path.dirname(argv[0]), "../.."))
        # self.child = pxssh.pxssh()
        # self.child.logfile = sys.stdout
        print("%s [%s] Deploying LAMA core" % (datetime.now(), ip))
        pexpect.run("rsync -arvz --exclude=*.pyc --exclude=.settings --exclude=.* --exclude=doc "
                    "%s lama@%s:~/lama/" % (os.path.join(base_path, 'lama', ''), ip))
        if ag == "dispatcher":
            print("%s [%s] Deploying LAMA Web interface" % (datetime.now(), ip))
            pexpect.run("rsync -arvz --exclude=*.pyc --exclude=.settings --exclude=.* --exclude=*.sqlite3 "
                        "--exclude=*.log --exclude=doc --exclude='monitor/migrations' "
                        "%s lama@%s:~/lama_gui/" % (os.path.join(base_path, 'lama_gui', ''), ip))

            print("%s [%s] Deploying LAMA angular" % (datetime.now(), ip))
            cmd = "rsync -arvz --exclude=.* --exclude=*.log --exclude=doc " \
                  "%s lama@%s:~/lama_angular/" % (os.path.join(base_path, 'lama_angular', ''), ip)
            print(cmd)
            pexpect.run(cmd)

            try:
                self.login(ip)
            except ExceptionPxssh:
                return -1

            self.child.sendline("sudo rm -rf /usr/share/nginx/lama_angular")
            self.child.prompt()
            self.child.sendline("sudo cp -R /home/lama/lama_angular /usr/share/nginx/")
            self.child.prompt()
            self.child.sendline("sudo chown -R nginx.nginx /usr/share/nginx/lama_angular")
            self.child.prompt()

            self.child.logout()
        return 0

    def reset_core_lama(self, ip, ag):
        try:
            self.login(ip)
        except ExceptionPxssh:
            return -1

        print("%s [%s] Reseting LAMA core" % (datetime.now(), ip))
        self.child.sendline("rm -rf /home/lama/lama/lama/*")
        self.child.prompt()

        self.child.logout()
        return 0

    # def test(self, ip, ag):
    #     if self.login(ip) < 0:
    #         return -1
    #
    #     output = BytesIO()
    #     self.child.logfile = output
    #     self.child.sendline("virsh --connect=qemu:///system list --all")
    #     self.child.prompt()
    #
    #     result = output.getvalue().decode("UTF-8")
    #     # print(repr(result))
    #     pattern = re.compile('\s+(\d+|-)\s+(\S+)\s+(running|shut off)')
    #     m = re.findall(pattern, result)
    #     if m:
    #         for id, name, status in m:
    #             if status != "shut off":
    #                 print("%s [%s]\tDestroy:\t%d" % (datetime.now(), ip, int(id)))
    #                 # self.child.sendline("virsh --connect=qemu:///system destroy %s" % id)
    #                 # self.child.prompt()
    #             print("%s [%s]\tUndefine:\t%s" % (datetime.now(), ip, name))
    #             # self.child.sendline("virsh --connect=qemu:///system undefine %s" % name)
    #             # self.child.prompt()
    #
    #     self.child.logout()
    #     return 0

    def delete_vms(self, ip, ag):
        output = BytesIO()
        self.child.logfile = output

        # print("%s [%s]\tRestart libvirtd" % (datetime.now(), ip))
        # self.child.sendline("sudo systemctl restart libvirtd")
        # self.child.prompt()
        # print("prompted sudo systemctl restart libvirtd")

        self.child.sendline("virsh --connect=qemu:///system list --all")
        self.child.prompt()

        result = output.getvalue().decode("UTF-8")
        pattern = re.compile('\s+(\d+|-)\s+(\S+)\s+(running|shut off|paused)')
        m = re.findall(pattern, result)
        if m:
            for id, name, status in m:
                if name.startswith('lama_') or name.startswith('temp-'):
                    # if status != "shut off":
                    print("%s [%s]\tDestroy:\t%s" % (datetime.now(), ip, id))
                    self.child.sendline("virsh --connect=qemu:///system destroy %s" % id)
                    self.child.prompt()
                    print("%s [%s]\tUndefine:\t%s" % (datetime.now(), ip, name))
                    self.child.sendline("virsh --connect=qemu:///system undefine %s" % name)
                    self.child.prompt()
                # elif name == "site":
                #     if status != "shut off":
                #         print("%s [%s]\tShutdown:\t%d" % (datetime.now(), ip, int(id)))
                #         self.child.sendline("virsh --connect=qemu:///system destroy %s" % id)
                #         self.child.prompt()
                elif name.startswith("instance-"):
                    if status == "shut off":
                        print("%s [%s]\tUndefine openstack instance:\t%s" % (datetime.now(), ip, name))
                        self.child.sendline("virsh --connect=qemu:///system undefine %s" % name)
                        self.child.prompt()
                elif name.startswith("attackwatch"):
                    # if status != "shut off":
                    #     print("%s [%s]\tShutdown:\t%d" % (datetime.now(), ip, int(id)))
                    #     self.child.sendline("virsh --connect=qemu:///system destroy %s" % id)
                    #     self.child.prompt()
                    # print("Will eventually stop VMs")
                    pass
                else:
                    pass
                    # if status != "shut off":
                    #     print("%s [%s]\tShutdown:\t%d" % (datetime.now(), ip, int(id)))
                    #     self.child.sendline("virsh --connect=qemu:///system destroy %s" % id)
                    #     self.child.prompt()
                    # print("Will eventually stop VMs")

        self.child.logfile = None
        output.close()
        # delete image files
        self.child.sendline("rm -f /home/lama-pool/*")
        self.child.prompt()

        output = BytesIO()
        self.child.logfile = output
        self.child.sendline("virsh --connect=qemu:///system net-list --all")
        self.child.prompt()

        result = output.getvalue().decode("UTF-8").splitlines()
        self.child.logfile = None
        output.close()

        for line in result:
            try:
                netname = line.split()[0].strip()
                if netname.startswith("lama-"):
                    print("Removing network '%s'" % netname)
                    self.child.sendline("virsh --connect=qemu:///system net-destroy %s" % netname)
                    self.child.prompt()
                    self.child.sendline("virsh --connect=qemu:///system net-undefine %s" % netname)
                    self.child.prompt()
            except:
                continue

    def start_lama(self, ip, ag):
        # for i in 8 9 10 11; do echo "10.1.1.$i"; ssh lama@10.1.1.$i ps axu | grep python; done
        self.child.logfile = stdout

        if self.login(ip) < 0:
            return -1

        print("%s [%s] Agent type: %s" % (datetime.now(), ip, ag))
        if ag == "dispatcher":
            print("%s [%s] Starting Web interface" % (datetime.now(), ip))
            self.child.sendline("cd /home/lama/lama_gui/")
            self.child.prompt()

            self.child.sendline("source ~/lama_gui_env/bin/activate")
            self.child.prompt()

            self.child.sendline("./manage.py collectstatic")
            self.child.expect_exact("cancel: ")

            self.child.sendline("yes")
            self.child.prompt()

            # print("1: %s" % self.child.before.decode("UTF-8"))
            m = re.findall('(\d+ static files copied)', self.child.before.decode("UTF-8"))
            if m:
                print("%s [%s] %s" % (datetime.now(), ip, m[0]))

            self.child.sendline("sudo supervisorctl start lama_gui")
            self.child.prompt()

            print("%s [%s] Starting dispatcher" % (datetime.now(), ip))
            self.child.sendline("sudo supervisorctl start lama_dispatcher")
            self.child.prompt()

        elif ag == "provider":
            n = 15
            print("%s [%s] Waiting %d seconds before starting provider..." % (datetime.now(), ip, n))
            sleep(n)
            print("%s [%s] Starting provider" % (datetime.now(), ip))
            self.child.sendline("sudo systemctl daemon-reload")
            self.child.prompt()

            self.child.sendline("sudo supervisorctl start lama_provider")
            self.child.prompt()

            self.child.sendline("redis-cli flushdb")
            self.child.prompt()

            self.child.sendline("sudo systemctl restart redis.service")
            self.child.prompt()

        print("%s [%s] Restarting collectd configuration and daemon" % (datetime.now(), ip))
        self.child.sendline("sudo systemctl restart collectd.service")
        self.child.prompt()

        # self.child.sendline("sudo mkdir -p /etc/collectd; sudo cp ~/lama/resources/collectd.conf /etc/collectd/collectd.conf")
        # self.child.prompt()

        # version of collectd used seems to have a memory leak. Add daily restart (should avoid duplicates)
        self.child.sendline("sudo ~/lama/add_restart_collectd_to_cron.sh")
        self.child.prompt()
        print("%s [%s] Finished start lama" % (datetime.now(), ip))

        #     TODO: verify if services are running!

        self.child.logout()
        return 0

    def stop_reset_lama(self, ip, ag):
        return self.stop_lama(ip, ag, reset=True)


class LamaManagerActionError(Exception):
    pass


class LamaManager(object):

    class Action:
        RESTART = "restart"
        START = "start"
        STOP = "stop"
        RESET = "reset"
        DEPLOY = "deploy"
        TEST = "test"
        DEL_CORE = "reset_core"

    def __init__(self, targets, action, max_threads=50, alert=True):
        self.targets = targets
        self.action = action
        self.running_time = None
        self.failed = 0
        self.alert = alert
        self.max_threads = max_threads

    def run(self):
        # TODO: make providers progress depend on dispatcher

        threadlist = []
        starttime = time.time()
        num_of_threads = min(self.max_threads, len(self.targets))

        print("\n\n----------------------------------------\n")
        print("Starting %d threads\n" % num_of_threads)

        # load object to run in threads
        for i in range(num_of_threads):
            target = self.targets[i]
            obj = self.get_action_object(target)
            threadlist.append(obj)
            obj.start()

        for t in threadlist:
            t.join()
            print("Thread " + t.name + " finished - %s!" % ("SUCCESS" if t.result else "FAIL"))
            if not t.result:
                self.failed += 1

        finishtime = time.time()
        self.running_time = finishtime - starttime
        print("\nExecution lasted for " + str(self.running_time) + " seconds!")
        print("\n----------------------------------------\n")

        if self.alert:
            self.announce_termination()

    def get_action_object(self, target, deps=None):
        if self.action == LamaManager.Action.RESTART:
            actions = [
                ManageLama.stop_lama,
                ManageLama.deploy_lama,
                ManageLama.start_lama
            ]
        elif self.action == LamaManager.Action.RESET:
            actions = [
                ManageLama.stop_reset_lama,
                ManageLama.deploy_lama,
                ManageLama.start_lama
            ]
        elif self.action == LamaManager.Action.STOP:
            actions = [
                ManageLama.stop_lama
            ]
        elif self.action == LamaManager.Action.START:
            actions = [
                ManageLama.start_lama
            ]
        elif self.action == LamaManager.Action.DEPLOY:
            actions = [
                ManageLama.deploy_lama,
            ]
        elif self.action == LamaManager.Action.TEST:
            actions = [
                ManageLama.test
            ]
        elif self.action == LamaManager.Action.DEL_CORE:
            actions = [
                ManageLama.reset_core_lama,
                ManageLama.deploy_lama
            ]
        else:
            raise LamaManagerActionError("Bad action requested - %s" % self.action)

        return ManageLama(actions=actions, target=target)

    def announce_termination(self):
        voice = "Agnes"
        if self.failed:
            sentence = "Oh, no! %s of %s tasks failed!" % (self.failed, len(self.targets))
        else:
            sentence = "Hurray! I'm done"
        print("[%s] %s" % (voice, sentence))
        os.system('say "%s" -v "%s"' % (sentence, voice))


if __name__ == "__main__":
    parser = ArgumentParser(description="Run LAMA management action.")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-f', type=str, dest='target_file', help="File containing list of targets.")
    group.add_argument('-s', type=str, dest='host', help="<IP to restart>:<Type of host>")
    parser.add_argument('-a', type=str, dest='action', help="Action to apply to targets.",
                        choices=["start", "stop", "restart", "reset", "deploy", "test", "reset_core"], required=True)
    cmdargs = parser.parse_args()

    lama_agents = []
    if cmdargs.target_file:
        with open(cmdargs.target_file, 'r') as fp:
            lama_agents = [tuple(line.split()) for line in fp.readlines() if not line.lstrip().startswith(";")]

    if cmdargs.host:
        try:
            ip, ht = cmdargs.host.split(':')
            lama_agents.append((ip, ht))
        except Exception as e:
            print("Error: %s - Format for inline host is <ip>:<host_type>" % e)

    if not lama_agents:
        print("No targets specified")
        exit(-1)
    man = LamaManager(lama_agents, cmdargs.action)

    man.run()


    # voices = [
    #     "Agnes",
    # #     "Albert",
    # #     "Alex",
    # #     "Bad News",
    # #     "Bahh",
    # #     "Bells",
    # #     "Boing",
    # #     "Bruce",
    # #     "Bubbles",
    # #     "Cellos",
    # #     "Deranged",
    # #     "Fred",
    # #     "Good News",
    # #     "Hysterical",
    # #     "Junior",
    # #     "Kathy",
    # #     "Pipe Organ",
    # #     "Princess",
    # #     "Ralph",
    # #     "Trinoids",
    # #     "Vicki",
    # #     "Victoria",
    # #     "Whisper",
    # #     "Zarvox"
    # ]
    #
    # success_sentences = [
    #     "Hurray! I'm done",
    #     # "Okidoki!",
    #     # "yipikaye!",
    #     # "It's so fluffy!",
    #     # "Houston, we solved a problem!",
    #     # "SILENCE! I kill you!",
    #     # "I want the truth!",
    #     # "You can't handle the truth!"
    # ]
    #
    # fail_sentences = [
    #     "Oh, no! %s of %s tasks failed!" % (fail, len(threadlist))
    # ]
    #
    # sentences = fail_sentences if fail > 0 else success_sentences
    # s = random.sample(sentences, 1)[0]
    # v = random.sample(voices, 1)[0]
    # print("%s [%s] %s" % (v, s))
    # os.system('say "%s" -v "%s"' % (s, v))
