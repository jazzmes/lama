import importlib

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

TabSize = 2


def format_val(val):
    if isinstance(val, bool):
        return str(val).lower()
    elif isinstance(val, str):
        return '"{}"'.format(val.strip())

    return val


class CollectdPluginSection(object):

    default_attributes = {}

    def __init__(self, type_name, plugin_name, **attrs):
        self.type_name = type_name
        self.plugin_name = plugin_name
        self.attributes = self.default_attributes or {}
        self.sections = []
        self.attributes.update(attrs)

    def generate_config(self, prefix=''):
        config_str = ''
        if self.attributes or self.sections:
            config_str += '{}<{} "{}">\n'.format(prefix, self.type_name, self.plugin_name)
            for key, val in self.attributes.items():
                config_str += '{}{} {}\n'.format(prefix + ' ' * TabSize, key, format_val(val))

            for sub_section in self.sections:
                config_str += sub_section.generate_config(prefix=prefix + ' ' * TabSize)

            config_str += '{}</{}>\n'.format(prefix, self.type_name)
        return config_str


class CollectdPlugin(CollectdPluginSection):

    def __init__(self, **kwargs):
        super().__init__('Plugin', self.name, **kwargs)
        self._tab = 2

    @property
    def name(self):
        if self.plugin_name:
            return self.plugin_name
        else:
            raise NotImplementedError('Set plugin name!')


class RrdToolPlugin(CollectdPlugin):

    plugin_name = 'rrdtool'
    default_attributes = {
        'DataDir': '/var/lib/collectd/rrd',
        'CreateFilesAsync': False,
        'CacheTimeout': 120,
        'CacheFlush': 900,
        'WritesPerSecond': 50
    }


class LogFilePlugin(CollectdPlugin):

    plugin_name = 'logfile'
    default_attributes = {
        'LogLevel': 'info',
        'File': '/var/log/lama/collectd.log',
        'Timestamp': True,
        'PrintSeverity': True
    }


class CGroupsPlugin(CollectdPlugin):

    plugin_name = 'cgroups'
    default_attributes = {
        'CGroup': 'libvirt',
        'IgnoreSelected': False
    }


class CpuPlugin(CollectdPlugin):

    plugin_name = 'cpu'


class DiskFreePlugin(CollectdPlugin):

    plugin_name = 'df'


class DiskPlugin(CollectdPlugin):

    plugin_name = 'disk'


class InterfacePlugin(CollectdPlugin):

    plugin_name = 'interface'


class LibvirtPlugin(CollectdPlugin):

    plugin_name = 'libvirt'
    default_attributes = {
        'Connection': 'qemu:///system',
        'RefreshInterval': 10,
        'HostnameFormat': 'name',
        'InterfaceFormat': 'name'
    }


class LoadPlugin(CollectdPlugin):

    plugin_name = 'load'


class MemoryPlugin(CollectdPlugin):

    plugin_name = 'memory'


class NumaPlugin(CollectdPlugin):

    plugin_name = 'numa'


class ProcessesPlugin(CollectdPlugin):

    plugin_name = 'processes'
    default_attributes = {
        'Process': 'name'
    }


class SwapPlugin(CollectdPlugin):

    plugin_name = 'swap'
    default_attributes = {
        'ReportByDevice': False,
        'ReportBytes': True
    }


class ThermalPlugin(CollectdPlugin):

    plugin_name = 'thermal'


class VMemPlugin(CollectdPlugin):

    plugin_name = 'vmem'


class WriteRedisPlugin(CollectdPlugin):

    plugin_name = 'write_redis'

    def __init__(self):
        super().__init__()
        self.sections = [
            CollectdPluginSection(
                'Node', 'local_redis',
                Host='localhost',
                Port='6379',
                Timeout=1000,
                StoreRates=False,
                PubSub=True
            )
        ]


class CollectdConfigGenerator(object):

    def __init__(self, plugins=None, plugin_attributes=None, **attrs):
        if plugin_attributes is None:
            plugin_attributes = {}

        self.attributes = {
            'FQDNLookup': True,
            'Interval': 10
        }
        self.attributes.update(attrs)

        self._plugins = plugins or [
            LogFilePlugin(),
            CGroupsPlugin(),
            CpuPlugin(),
            DiskFreePlugin(),
            DiskPlugin(),
            InterfacePlugin(),
            LibvirtPlugin(),
            LoadPlugin(),
            MemoryPlugin(),
            NumaPlugin(),
            ProcessesPlugin(),
            RrdToolPlugin(),
            SwapPlugin(),
            ThermalPlugin(),
            VMemPlugin(),
            WriteRedisPlugin()
        ]

        for plugin, attrs in plugin_attributes.items():
            plugin_class = getattr(importlib.import_module(__name__), plugin)
            plugin_class.default_attributes.update(attrs)

    def generate_config(self):
        config_str = ''

        for key, val in self.attributes.items():
            config_str += '{} {}\n'.format(key, format_val(val))
        config_str += '\n'

        for plugin in self._plugins:
            config_str += 'LoadPlugin {}\n'.format(plugin.name)

        for plugin in self._plugins:
            plugin_config = plugin.generate_config()
            if plugin_config:
                config_str += '\n{}'.format(plugin_config)
        return config_str


def main():
    ccg = CollectdConfigGenerator(Interval=60, plugin_attributes={
        'LibvirtPlugin': {
            'RefreshInterval': 60
        }
    })
    print(ccg.generate_config())


if __name__ == '__main__':
    main()
