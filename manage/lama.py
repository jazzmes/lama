#!/usr/bin/env python3

import gevent.monkey as monkey
monkey.patch_all()
import gevent
from gevent.greenlet import Greenlet
from gevent.pool import Pool
from pexpect.pxssh import ExceptionPxssh
from pexpect.pxssh import pxssh
from io import BytesIO
import os
import re
import traceback
import pexpect
from argparse import ArgumentParser
from time import time
from sys import argv, stdout

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'


class LamaManagerActionError(Exception):
    pass


class ManageLama(Greenlet):

    def __init__(self, target, actions):
        super(ManageLama, self).__init__()

        self.target = target
        self.actions = actions
        self.result = False
        self.child = None
        self.name = None

    def _run(self):
        if not self.target:
            print("Bad target: %s" % self.target)
            return False

        ip, ag = self.target
        self.name = "%s @ %s" % (ag, ip)
        self.result = True
        try:
            for a in self.actions:
                if not a(self, ip, ag):
                    self.result = False
        except:
            print("Exception running action: %s" % traceback.format_exc())

        return self.result

    def login(self, ip):
        retries = 0
        while True:
            try:
                print("[%s]\tLogin: attempt %d" % (ip, retries + 1))
                self.child = pxssh()
                self.child.login(ip, "lama")
                return True
            except (ExceptionPxssh, AssertionError) as e:
                print("[%s]\tException: %s" % (ip, traceback.format_exc()))
                print("[%s]\tFailed login" % ip)
                self.child.close()
                if retries < 3:
                    retries += 1
                    # sleep(5)
                    continue
                else:
                    return False

    def stop_lama(self, ip, ag, reset=False):
        try:
            self.login(ip)
        except ExceptionPxssh:
            return False

        if ag == "dispatcher":
            print("[%s]\tStopping Web interface" % ip)
            self.child.sendline("sudo supervisorctl stop lama_gui")
            self.child.prompt()

            print("[%s]\tStopping dispatcher" % ip)
            self.child.sendline("sudo supervisorctl stop lama_dispatcher")
            self.child.prompt()

            self.child.sendline("sudo pkill -f lama.ops.experiments --signal 9")
            self.child.prompt()

        elif ag == "provider":
            print("[%s]\tStopping provider" % ip)
            self.child.sendline("sudo supervisorctl stop lama_provider")
            self.child.prompt()

            # Make sure
            self.child.sendline("sudo pkill -f run_provider")
            self.child.prompt()

            print("[%s]\tStopping apps" % ip)
            self.child.sendline("sudo pkill -9 -f run_application")
            self.child.prompt()

        if reset:
            print("[%s]\tReset" % ip)
            self.child.sendline("rm -f ~/lama/persistence/*")
            self.child.prompt()

            self.child.sendline("rm -f ~/lama/vm-images/*")
            self.child.prompt()

            print("[%s]\tClear iptables" % ip)
            self.child.sendline("sudo ./lama/scripts/cleaript.sh")
            self.child.prompt()

            print("[%s]\tRemove logs" % ip)
            self.child.sendline("rm -f /var/log/lama/*")
            # "find /var/log/lama/* -type f -exec dd if=/dev/null of={} \;"
            self.child.prompt()

            if ag == "provider":
                print("[%s]\tDelete VMs" % ip)
                self.delete_vms(ip, ag)

                # stop dnsmasq
                print("[%s]\tStopping dnsmasq" % ip)
                self.child.sendline("sudo pkill -f dnsmasq.*lama")
                self.child.prompt()

                self.child.sendline("sudo rm -rf /home/lama/lama/dnsmasq")
                self.child.prompt()

                print("[%s]\tStopping ryu" % ip)
                self.child.sendline("sudo pkill -f ryu.*lama")
                self.child.prompt()

                print("[%s]\tKill internal SSH and namespace processes" % ip)
                print("[%s]\tDISABLED\t** sudo pkill -9 -e -f ssh.*192.168.*" % ip)
                print("[%s]\tDISABLED\t** sudo pkill -9 -e -f 'sudo ip netns'" % ip)
                # self.child.sendline("sudo pkill -9 -e -f ssh.*192.168.*")
                # self.child.prompt()
                #
                # self.child.sendline("sudo pkill -9 -e -f 'sudo ip netns'")
                # self.child.prompt()

                print("[%s]\tRemove bridges" % ip)
                self.child.sendline("sudo ovs-vsctl list-br")
                output = BytesIO()
                self.child.logfile = output
                self.child.prompt()
                result = output.getvalue().decode("UTF-8").splitlines()
                output.close()
                self.child.logfile = None
                # noinspection PyArgumentList
                for a in range(1, len(result) - 1):
                    if result[a].startswith("lbr-"):
                        print("[%s]\tRemove bridge: %s" % (ip, result[a]))
                        self.child.sendline("sudo ovs-vsctl del-br %s" % result[a])
                        self.child.prompt()

                # delete bridges
                output = BytesIO()
                self.child.logfile = output
                self.child.sendline("ip link")
                self.child.prompt()
                result = output.getvalue().decode("UTF-8")
                pattern = re.compile(' (lgw-\S+):')
                m = re.findall(pattern, result)
                if m:
                    for br in m:
                        print("[%s]\tDelete bridge: %s" % (ip, br))
                        self.child.sendline("sudo ip link del %s" % br)
                        self.child.prompt()

                print("[%s]\tRemove namespaces" % ip)
                self.child.sendline("ip netns")
                output = BytesIO()
                self.child.logfile = output
                self.child.prompt()
                result = output.getvalue().decode("UTF-8").splitlines()
                output.close()

                self.child.logfile = None
                for a in range(1, len(result) - 1):
                    print("[%s]\tRemove namespace: %s" % (ip, result[a]))
                    print("[%s]\tDISABLED\t** sudo ip netns delete %s" % (ip, result[a]))
                    # self.child.sendline("sudo ip netns delete %s" % result[a])
                    # self.child.prompt()

        self.child.logout()
        return True

    def deploy_lama(self, ip, ag):

        base_path = os.path.abspath(os.path.join(os.path.dirname(argv[0]), "../.."))
        # self.child = pxssh.pxssh()
        # self.child.logfile = sys.stdout
        print("[%s]\tDeploying LAMA core" % ip)
        pexpect.run("rsync -arvz --exclude=*.pyc --exclude=.settings --exclude=.* --exclude=doc "
                    "%s lama@%s:~/lama/" % (os.path.join(base_path, 'lama', ''), ip))
        if ag == "dispatcher":
            print("[%s]\tDeploying LAMA Web interface" % ip)
            pexpect.run("rsync -arvz --exclude=*.pyc --exclude=.settings --exclude=.* --exclude=*.sqlite3 "
                        "--exclude=*.log --exclude=doc --exclude='monitor/migrations' "
                        "%s lama@%s:~/lama_gui/" % (os.path.join(base_path, 'lama_gui', ''), ip))

            print("[%s]\tDeploying LAMA angular" % ip)
            cmd = "rsync -arvz --exclude=.* --exclude=*.log --exclude=doc " \
                  "%s lama@%s:~/lama_angular/" % (os.path.join(base_path, 'lama_angular', ''), ip)
            print(cmd)
            pexpect.run(cmd)

            try:
                self.login(ip)
            except ExceptionPxssh:
                return False

            self.child.sendline("sudo rm -rf /usr/share/nginx/lama_angular")
            self.child.prompt()
            self.child.sendline("sudo cp -R /home/lama/lama_angular /usr/share/nginx/")
            self.child.prompt()
            self.child.sendline("sudo chown -R nginx.nginx /usr/share/nginx/lama_angular")
            self.child.prompt()

            self.child.logout()
        return True

    def reset_core_lama(self, ip, ag):
        try:
            self.login(ip)
        except ExceptionPxssh:
            return False

        print("[%s]\tReseting LAMA core" % ip)
        self.child.sendline("rm -rf /home/lama/lama/lama/*")
        self.child.prompt()

        self.child.logout()
        return True

    def test(self, ip, ag):
        print("[%s]\tTesting..." % ip)
        if not self.login(ip):
            print("returning false...")
            return False
        output = BytesIO()
        self.child.logfile = output
        self.child.sendline("sudo virsh --connect=qemu:///system list --all")
        self.child.prompt()

        result = output.getvalue().decode("UTF-8")
        # print(repr(result))
        pattern = re.compile('\s+(\d+|-)\s+(\S+)\s+(running|shut off)')
        m = re.findall(pattern, result)
        if m:
            for id, name, status in m:
                if status != "shut off":
                    print("[%s]\tDestroy:\t%d" % (ip, int(id)))
                    # self.child.sendline("virsh --connect=qemu:///system destroy %s" % id)
                    # self.child.prompt()
                print("[%s]\tUndefine:\t%s" % (ip, name))
                # self.child.sendline("virsh --connect=qemu:///system undefine %s" % name)
                # self.child.prompt()
        else:
            print("[%s]\tNo VMs running" % ip)

        self.child.logout()
        return True

    def delete_vms(self, ip, ag):
        output = BytesIO()
        previous = self.child.logfile
        self.child.logfile = output

        # print("[%s]\tRestart libvirtd" % ip)
        # self.child.sendline("sudo systemctl restart libvirtd")
        # self.child.prompt()
        # print("prompted sudo systemctl restart libvirtd")

        self.child.sendline("virsh --connect=qemu:///system list --all")
        self.child.prompt()

        result = output.getvalue().decode("UTF-8")
        pattern = re.compile('\s+(\d+|-)\s+(\S+)\s+(running|shut off)')
        m = re.findall(pattern, result)
        if m:
            for id, name, status in m:
                if status != "shut off":
                    print("[%s]\tDestroy:\t%d" % (ip, int(id)))
                    print("[%s]\tDISABLED\t** virsh --connect=qemu:///system destroy %s" % (ip, id))
                    # self.child.sendline("virsh --connect=qemu:///system destroy %s" % id)
                    # self.child.prompt()
                print("[%s]\tUndefine:\t%s" % (ip, name))
                print("[%s]\tDISABLED\t** virsh --connect=qemu:///system undefine %s" % (ip, name))
                # self.child.sendline("virsh --connect=qemu:///system undefine %s" % name)
                # self.child.prompt()
        self.child.logfile = previous
        output.close()
        # delete image files
        self.child.sendline("rm -f /home/lama-pool/*")
        self.child.prompt()

        output = BytesIO()
        self.child.logfile = output
        self.child.sendline("virsh --connect=qemu:///system net-list --all")
        self.child.prompt()

        result = output.getvalue().decode("UTF-8").splitlines()
        self.child.logfile = None
        output.close()

        for line in result:
            try:
                netname = line.split()[0].strip()
                if netname.startswith("lama-"):
                    print("Removing network '%s'" % netname)
                    print("[%s]\tDISABLED\t** virsh --connect=qemu:///system net-destroy %s" % (ip, netname))
                    # self.child.sendline("virsh --connect=qemu:///system net-destroy %s" % netname)
                    # self.child.prompt()
                    print("[%s]\tDISABLED\t** virsh --connect=qemu:///system net-undefine %s" % (ip, netname))
                    # self.child.sendline("virsh --connect=qemu:///system net-undefine %s" % netname)
                    # self.child.prompt()
            except:
                continue

    def start_lama(self, ip, ag):
        # for i in 8 9 10 11; do echo "10.1.1.$i"; ssh lama@10.1.1.$i ps axu | grep python; done
        self.child.logfile = stdout

        if not self.login(ip):
            return False

        print("[%s]\tAgent type: %s" % (ip, ag))
        if ag == "dispatcher":
            # print("[%s]\tStarting Web interface" % ip)
            # self.child.sendline("cd /home/lama/lama_gui/")
            # self.child.prompt()
            #
            # self.child.sendline("source ~/lama_gui_env/bin/activate")
            # self.child.prompt()
            #
            # self.child.sendline("./manage.py collectstatic")
            # self.child.expect_exact("cancel: ")
            #
            # self.child.sendline("yes")
            # self.child.prompt()
            #
            # # print("1: %s" % self.child.before.decode("UTF-8"))
            # m = re.findall('(\d+ static files copied)', self.child.before.decode("UTF-8"))
            # if m:
            #     print("[%s]\t%s" % (ip, m[0]))
            #
            # self.child.sendline("sudo supervisorctl start lama_gui")
            # self.child.prompt()

            print("[%s]\tStarting dispatcher" % ip)
            self.child.sendline("sudo supervisorctl start lama_dispatcher")
            self.child.prompt()

        elif ag == "provider":
            n = 15
            print("[%s]\tWaiting %d seconds before starting provider..." % (ip, n))

            print("[%s]\tStarting provider" % ip)
            self.child.sendline("sudo systemctl daemon-reload")
            self.child.prompt()

            self.child.sendline("sudo supervisorctl start lama_provider")
            self.child.prompt()

            self.child.sendline("redis-cli flushdb")
            self.child.prompt()

            self.child.sendline("sudo systemctl restart redis.service")
            self.child.prompt()

        print("[%s]\tRestarting collectd configuration and daemon" % ip)
        self.child.sendline("sudo systemctl restart collectd.service")
        self.child.prompt()

        self.child.sendline("sudo mkdir -p /etc/collectd")

        self.child.sendline("sudo cp ~/lama/resources/collectd.conf /etc/collectd/collectd.conf")
        self.child.prompt()

        # version of collectd used seems to have a memory leak. Add daily restart (should avoid duplicates)
        self.child.sendline("sudo ~/lama/add_restart_collectd_to_cron.sh")
        self.child.prompt()

        #     TODO: verify if services are running!

        self.child.logout()
        return True

    def stop_reset_lama(self, ip, ag):
        return self.stop_lama(ip, ag, reset=True)


class LamaManager(object):

    class Action:
        RESTART = "restart"
        START = "start"
        STOP = "stop"
        RESET = "reset"
        DEPLOY = "deploy"
        TEST = "test"

    def __init__(self, targets, action, alert=True):
        self.targets = targets
        self.action = action
        self.running_time = None
        self.alert = alert
        self._pool = Pool()
        self.failed = 0
        self.completed = 0

    def run(self):
        # TODO: make providers progress depend on dispatcher

        starttime = time()

        print("\n\n----------------------------------------\n")

        for target in self.targets:
            self._pool.apply_async(self.start_thread, args=(target,), callback=self.process_result)

        self._pool.join()

        finishtime = time()
        self.running_time = finishtime - starttime
        print("\nExecution lasted for " + str(self.running_time) + " seconds!")
        print("\n----------------------------------------\n")

        if self.alert:
            self.announce_termination()

    def process_result(self, result):
        self.completed += 1
        if not result:
            self.failed += 1
        print("Tasks completed: %s/%s/%s" % (self.completed - self.failed, self.completed, len(self.targets)))

    def start_thread(self, target):
        obj = self.get_action_object(target)
        obj.run()
        return obj.result

    def get_action_object(self, target, deps=None):
        if self.action == LamaManager.Action.RESTART:
            actions = [
                ManageLama.stop_lama,
                ManageLama.deploy_lama,
                ManageLama.start_lama
            ]
        elif self.action == LamaManager.Action.RESET:
            actions = [
                ManageLama.stop_reset_lama,
                ManageLama.deploy_lama,
                ManageLama.start_lama
            ]
        elif self.action == LamaManager.Action.STOP:
            actions = [
                ManageLama.stop_lama
            ]
        elif self.action == LamaManager.Action.START:
            actions = [
                ManageLama.start_lama
            ]
        elif self.action == LamaManager.Action.DEPLOY:
            actions = [
                ManageLama.deploy_lama,
            ]
        elif self.action == LamaManager.Action.TEST:
            actions = [
                ManageLama.test
            ]
        else:
            raise LamaManagerActionError("Bad action requested - %s" % self.action)

        return ManageLama(actions=actions, target=target)

    def announce_termination(self):
        voice = "Agnes"
        if self.failed:
            sentence = "Oh, no! %s of %s tasks failed!" % (self.failed, len(self.targets))
        else:
            sentence = "Hurray! I'm done"
        print("[%s]\t%s" % (voice, sentence))
        # os.system('say "%s" -v "%s"' % (sentence, voice))


def main():
    parser = ArgumentParser(description="Run LAMA management action.")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-f', type=str, dest='target_file', help="File containing list of targets.")
    group.add_argument('-s', type=str, dest='host', help="<IP to restart>:<Type of host>")
    parser.add_argument('-a', type=str, dest='action', help="Action to apply to targets.",
                        choices=["start", "stop", "restart", "reset", "deploy", "test", "reset_core"], required=True)
    cmdargs = parser.parse_args()

    lama_agents = []
    if cmdargs.target_file:
        with open(cmdargs.target_file, 'r') as fp:
            lama_agents = [tuple(line.split()) for line in fp.readlines() if not line.lstrip().startswith(";")]

    if cmdargs.host:
        try:
            ip, ht = cmdargs.host.split(':')
            lama_agents.append((ip, ht))
        except Exception as e:
            print("Error: %s - Format for inline host is <ip>:<host_type>" % e)

    if not lama_agents:
        print("No targets specified")
        exit(-1)
    man = LamaManager(lama_agents, cmdargs.action)

    man.run()


if __name__ == '__main__':
    main()