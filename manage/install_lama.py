#!/usr/bin/python
import getpass
import threading
import traceback
import pexpect
import time
import sys
import os
from argparse import ArgumentParser
from time import sleep
from io import BytesIO
from pexpect.pxssh import ExceptionPxssh
from pexpect import pxssh
from config.settings import PATH_LOG, PATH_IMAGE_POOL

maxthread = 50


class ManageLama(threading.Thread):

    def __init__(self, target, actions, verbose=False):
        super(ManageLama, self).__init__()

        self.target = target
        self.actions = actions
        self.result = True
        self.child = None
        self.verbose = verbose

        self.possible_commands = {
            'pip3': ['pip3', 'pip-python3'],
            'pip': ['pip2', 'pip-python', 'pip']
        }
        self.commands = {}

    def run(self):
        print("Running for target: %s" % str(self.target))
        if not self.target:
            # print("Bad target: %s" % self.target)
            return -1

        ip, ag = self.target
        self.name = "%s @ %s" % (ag, ip)

        for a in self.actions:
            if hasattr(self, a):
                if getattr(self, a)(ip, ag) < 0:
                    self.result = False
                    return -1
            else:
                print("No action: %s" % a)

        # self.child.close()

    def login(self, ip):
        retries = 0
        while True:
            try:
                self.child = pxssh.pxssh()
                self.child.login(ip, "lama")
                if self.verbose:
                    self.child.logfile = sys.stdout.buffer
                return 0
            except (ExceptionPxssh, AssertionError) as e:
                print("Failed login @ %s : %s" % (ip, e))
                if retries < 3:
                    retries += 1
                    print("Trying again in 3 sec...")
                    sleep(3)
                    continue
                else:
                    return -1

    def check_command(self, cmd):
        # print("Check command: %s" % cmd)
        output = BytesIO()
        previous = self.child.logfile
        self.child.sendline("command -v %s; echo result$?" % cmd)
        self.child.logfile = output
        self.child.prompt()
        result = output.getvalue().decode("UTF-8")
        self.child.logfile = previous
        return 'result0' in result

    def check_commands(self, ip):
        if self.commands:
            return

        self.login(ip)

        for name, commands in self.possible_commands.items():
            for cmd in commands:
                if self.check_command(cmd):
                    self.commands[name] = cmd
                    print("Command for '%s': %s" % (name, cmd))
                    break
            if name not in self.commands:
                self.install_base_dependency(name)
                for cmd in commands:
                    if self.check_command(cmd):
                        self.commands[name] = cmd
                        print("Command for '%s': %s" % (name, cmd))
                        break

        self.child.close()

    def install_base_dependency(self, name):
        if name == 'pip3':
            print("Installing pip3")
            self.child.sendline("sudo wget https://bootstrap.pypa.io/get-pip.py")
            self.child.prompt()

            self.child.sendline("sudo python3 get-pip.py")
            self.child.prompt()

            self.child.sendline("sudo rm -f get-pip.py*")
            self.child.prompt()

    def install_all(self, ip, ag):
        if self.install_user(ip, ag) < 0:
            return -1
        return self.install_lama(ip, ag)

    def install_lama(self, ip, ag):
        self.check_commands(ip)
        # self.install_core(ip, ag)
        if self.install_deps(ip, ag) < 0:
            return -1
        if self.install_twisted(ip, ag) < 0:
            return -1
        if self.install_core(ip, ag) < 0:
            return -1
        if self.install_redis(ip, ag) < 0:
            return -1
        if self.install_collectd(ip, ag) < 0:
            return -1
        # self.install_mongodb(ip, ag)
        return self.install_ryu(ip, ag)

    def install_user(self, ip, ag):

        print("[%s] Setting up user 'lama'" % ip)
        try:
            # first root login
            print("[%s] \tNeed root password" % ip)
            lama_password = "lA&maD3plo!y"
            password = getpass.getpass("Root password: ")
            rssh = pxssh.pxssh()
            # rssh.logfile = sys.stdout.buffer
            rssh.login(ip, "root", password)
            print("[%s] \tAdding user LAMA" % ip)
            rssh.sendline("adduser lama")
            rssh.prompt()
            rssh.sendline("passwd lama")
            rssh.expect("password: ")
            rssh.sendline(lama_password)
            rssh.expect("password: ")
            rssh.sendline(lama_password)
            rssh.prompt()
            print("[%s] \tSetup sudo" % ip)
            rssh.sendline("yum -y install sudo")
            rssh.prompt()
            rssh.sendline(
                "echo -e 'lama ALL=(ALL) NOPASSWD:ALL\n"
                "Cmnd_Alias NOTTYCMDS=/usr/sbin/dnsmasq,/usr/sbin/hdparm,/usr/sbin/dmsetup,/usr/bin/lama_exp\n"
                "Defaults!NOTTYCMDS !requiretty\n' > /etc/sudoers.d/lama")
                # "Cmnd_Alias NOTTYCMDS=/usr/sbin/dnsmasq,/usr/sbin/hdparm,/usr/sbin/dmsetup,/usr/sbin/dnsmasq,/bin/python3 -m lama.utils.gateway_helper *,/sbin/ip netns identify *\n"
                # "Cmnd_Alias NOTTYCMDS=/usr/sbin/dnsmasq\n"
            rssh.prompt()
            rssh.logout()
            rssh.close()
            lssh = pxssh.pxssh()
            # lssh.logfile = sys.stdout.buffer
            lssh.login(ip, "lama", lama_password)
            print("[%s] \tSetting up authorized keys" % ip)
            lssh.sendline("mkdir ~/.ssh")
            lssh.prompt()
            lssh.sendline("chmod 700 ~/.ssh")
            lssh.prompt()
            lssh.sendline("touch ~/.ssh/authorized_keys")
            lssh.prompt()
            lssh.sendline("chmod 600 ~/.ssh/authorized_keys")
            lssh.prompt()

            lssh.logout()
            lssh.close()

            pe = pexpect.spawn("ssh-copy-id -i manage/keys/lama_rsa.pub lama@%s" % ip)
            try:
                pe.expect("password")
                pe.sendline(lama_password)
            except:
                pass
            pe.expect(pexpect.EOF, timeout=30)
            pe.close()
        except:
            traceback.print_exc()
            return -1

        return 0

    def install_deps(self, ip, ag):

        print("[%s] Installing dependencies" % ip)
        try:
            lssh = pxssh.pxssh()
            if self.verbose:
                lssh.logfile = sys.stdout.buffer

            # lssh.logfile = sys.stdout.buffer
            lssh.login(ip, "lama")
            print("[%s] Installing core dependencies 1/3" % ip)
            lssh.sendline("sudo yum -y install python3 python3-pip python3-devel glibc libxml2-devel libxslt-devel")
            lssh.prompt(timeout=300)
            print("[%s] Installing core dependencies 2/3" % ip)
            lssh.sendline("sudo yum -y install ntp supervisor hdparm qemu-kvm libvirt libvirt-devel gcc libffi-devel openssl-devel")
            lssh.prompt(timeout=300)
            print("[%s] Installing core dependencies 3/3" % ip)
            lssh.sendline("sudo yum -y install lshw openvswitch redhat-rpm-config virt-install")
            lssh.prompt(timeout=300)

            output = BytesIO()
            previous = lssh.logfile
            lssh.logfile = output
            lssh.sendline("python -c 'import pip'; echo $?")
            lssh.prompt()
            result = output.getvalue().decode("UTF-8")
            lssh.logfile = previous

            print("Result of checking pip2: %s" % result)
            if result != "0":
                print("Installing pip")
                lssh.sendline("sudo wget https://bootstrap.pypa.io/get-pip.py")
                lssh.prompt()

                lssh.sendline("sudo python get-pip.py")
                lssh.prompt()

                lssh.sendline("sudo rm -f get-pip.py*")
                lssh.prompt()


            print("[%s] Setup ntpd" % ip)
            lssh.sendline("sudo systemctl enable ntpd")
            lssh.prompt()
            lssh.sendline("sudo systemctl start ntpd")
            lssh.prompt()
            print("[%s] Setup supervisord" % ip)
            lssh.sendline("sudo systemctl enable supervisord")
            lssh.prompt()
            lssh.sendline("sudo systemctl restart supervisord")
            lssh.prompt()
            # lssh.sendline("sudo rm -rf /tmp/pip_build_root/*")
            # lssh.prompt()

            lssh.logout()
            lssh.close()
        except:
            traceback.print_exc()
            return -1

        try:
            self.check_commands(ip)
            lssh = pxssh.pxssh()
            if self.verbose:
                lssh.logfile = sys.stdout.buffer

            # lssh.logfile = sys.stdout.buffer
            lssh.login(ip, "lama")

            print("[%s] Installing python packages with %s 1/3" % (ip, self.commands['pip3']))
            lssh.sendline("sudo %s install netaddr networkx netifaces python-dateutil numpy requests port-for" % self.commands['pip3'])
            lssh.prompt(timeout=300)
            print("[%s] Installing python packages with %s 2/3" % (ip, self.commands['pip3']))
            lssh.sendline("sudo %s install psutil libvirt-python 'paramiko>=1.16.0' pyparsing requests_toolbelt" % self.commands['pip3'])
            lssh.prompt(timeout=300)
            print("[%s] Installing python packages with %s 3/3" % (ip, self.commands['pip3']))
            lssh.sendline("sudo %s install rrdtool demjson decorator flask 'pyroute2==0.3.4' redis lxml" % self.commands['pip3'])
            lssh.prompt(timeout=300)

            print("[%s] Installing python (2.7) packages" % ip)
            lssh.sendline("sudo %s install 'netaddr>=0.7.13' 'redis>=2.10.3'" % self.commands['pip'])
            lssh.prompt(timeout=600)

            lssh.logout()
            lssh.close()

            self.install_python_iptables(ip, ag)
            # self.install_pyroute2(ip, ag)

        except:
            traceback.print_exc()
            return -1

        return 0

    def install_core(self, ip, ag):
        print("[%s] Installing LAMA" % ip)
        try:
            # pe = pexpect.spawn("rsync -arvz --exclude=*.pyc --exclude=.settings --exclude=.* --exclude=doc "
            #                    "/Users/tiago/Projects/Eclipse/Python/lama/ lama@%s:~/lama/" % ip)
            pexpect.run("rsync -arvz --exclude=*.pyc --exclude=.settings --exclude=.* --exclude=doc "
                        "../lama/ lama@%s:~/lama/" % ip)
            # pe.expect(pexpect.EOF, timeout=30)
            if ag == "dispatcher":
                print("[%s] Deploying LAMA Web interface" % ip)
                pexpect.run("rsync -arvz --exclude=*.pyc --exclude=.settings --exclude=.* --exclude=*.sqlite3 "
                            "--exclude=*.log --exclude=doc --exclude='monitor/migrations' "
                            "../lama_gui/ lama@%s:~/lama_gui/" % ip)
            # if ag.lower() == "dispatcher":
            #     pe = pexpect.spawn("rsync -arvz --exclude=*.pyc --exclude=.settings --exclude=.* --exclude=*.sqlite3 "
            #                        "--exclude=*.log --exclude=doc ../lama_gui/ "
            #                        "lama@%s:~/lama_gui/" % ip)
            #     pe.expect(pexpect.EOF, timeout=30)
            # pe.close()

            lssh = pxssh.pxssh()
            # lssh.logfile = sys.stdout.buffer
            lssh.login(ip, "lama")

            lssh.sendline("sudo mkdir %s" % PATH_LOG)
            lssh.prompt()

            lssh.sendline("sudo chown lama:lama %s" % PATH_LOG)
            lssh.prompt()

            lssh.sendline("sudo rm -f /etc/supervisord.d/lama_dispatcher.ini /etc/supervisord.d/lama_provider.ini "
                          "/etc/supervisord.d/lama_gui.ini")
            lssh.prompt()
            if ag.lower() == "dispatcher":
                print("[%s] Installing Nginx" % ip)
                lssh.sendline("sudo yum -y install nginx")
                lssh.prompt()
                lssh.sendline("sudo cp ~/lama_gui/config_files/lama.nginx.conf /etc/nginx/conf.d")
                lssh.prompt()

                print("[%s] Installing Django" % ip)
                lssh.sendline("virtualenv -p python3 ~/lama_gui_env")
                lssh.prompt()
                lssh.sendline("source ~/lama_gui_env/bin/activate")
                lssh.prompt()
                lssh.sendline("pip install django==1.6.4 gunicorn south"
                              " djangorestframework django-bootstrap3 django_extensions")
                lssh.prompt()
                lssh.sendline("cp ~/lama_gui/config_files/gunicorn_start ~/lama_gui_env/bin/")
                lssh.prompt()
                lssh.sendline("deactivate")
                lssh.prompt()
                lssh.sendline("sudo cp ~/lama_gui/supervisor/lama_gui.ini /etc/supervisord.d/")
                lssh.prompt()
                lssh.sendline("sudo cp ~/lama/supervisor/lama_dispatcher.ini /etc/supervisord.d/")
                lssh.prompt()
                lssh.sendline("sudo iptables -I INPUT -p udp --dport 20000 -j ACCEPT")
                lssh.prompt()
                lssh.sendline("sudo iptables -I INPUT -p tcp --dport 20001 -j ACCEPT")
                lssh.prompt()

                print("[%s] Restart supervisor" % ip)
                lssh.sendline("sudo systemctl restart supervisord")
                lssh.prompt()

                lssh.sendline("sudo systemctl restart nginx")
                lssh.prompt()

            elif ag.lower() == "provider":
                lssh.sendline("sudo cp lama/supervisor/lama_provider.ini /etc/supervisord.d/")
                lssh.prompt()
                lssh.sendline("sudo iptables -I INPUT -p udp --dport 20010 -j ACCEPT")
                lssh.prompt()
                lssh.sendline("sudo iptables -I INPUT -p tcp --dport 20010 -j ACCEPT")
                lssh.prompt()
                lssh.sendline("sudo iptables -I INPUT -p tcp --dport 20011 -j ACCEPT")
                lssh.prompt()
                lssh.sendline("sudo sysctl -w net.ipv4.ip_forward=1")
                lssh.prompt()
                lssh.sendline("sudo mkdir -p " + PATH_IMAGE_POOL)
                lssh.prompt()
                lssh.sendline("sudo groupadd lamagrp")
                lssh.prompt()
                lssh.sendline("sudo usermod -a -G lamagrp lama")
                lssh.prompt()
                lssh.sendline("sudo usermod -a -G lamagrp qemu")
                lssh.prompt()
                lssh.sendline("sudo chown lama.lamagrp " + PATH_IMAGE_POOL)
                lssh.prompt()
                lssh.sendline("sudo chmod 755 " + PATH_IMAGE_POOL)
                lssh.prompt()
                # configure access of lama to libvirt
                lssh.sendline("sudo groupadd libvirt")
                lssh.prompt()
                lssh.sendline("sudo gpasswd -a lama libvirt")
                lssh.prompt()
                lssh.sendline('for j in "listen_tls" "unix_sock_group" "unix_sock_ro_perms" "unix_sock_rw_perms"'
                              ' "unix_sock_dir" "auth_unix_ro" "auth_unix_rw"; do sudo sed -i'
                              ' "s/#$j/$j/g" /etc/libvirt/libvirtd.conf; done')
                lssh.prompt()
                lssh.sendline("sudo systemctl restart libvirtd")
                lssh.prompt()
                print("[%s] Set manager" % ip)
                lssh.sendline("sudo ovs-vsctl set-manager ptcp:6640")
                lssh.prompt()

                print("[%s] Restart supervisor" % ip)
                lssh.sendline("sudo systemctl restart supervisord")
                lssh.prompt(timeout=300)

            print("[%s] Logout" % ip)
            lssh.logout()
            lssh.close()


        except:
            traceback.print_exc()
            return -1

        return 0

    def reinstall_twisted(self, ip, ag):
        self.remove_twisted(ip, ag)
        self.install_twisted(ip, ag)

    def remove_twisted(self, ip, ag):
        print("Removing twisted @ %s... " % ip)
        self.login(ip)
        self.child.sendline("sudo %s uninstall -y twisted" % self.commands['pip3'])
        self.child.prompt()
        self.child.logout()

    def install_twisted(self, ip, ag):
        print("[%s] Installing Twisted" % ip)
        try:
            self.check_commands(ip)
            self.login(ip)
            cmd = "sudo %s install --upgrade twisted==16.1.1" % self.commands['pip3']
            self.child.sendline(cmd)
            self.child.prompt()
            self.child.logout()

        #     # print("Installing twisted @ %s... " % ip)
        #     # copy all files from twistedcrea
        #     # TODO: Fork twisted project and git clone
        #     cmd = "rsync -arvz ../twisted/ lama@%s:~/twisted/" % ip
        #     print("[%s] \tRunning: %s" % (ip, cmd))
        #     pe = pexpect.spawn(cmd)
        #     pe.expect(pexpect.EOF, timeout=300)
        #     n_lines = len(pe.before.splitlines())
        #     pe.close()
        #     # print("Number of lines: %s" % n_lines)
        #
        #     # if n_lines > 5:
        #     self.login(ip)
        #
        #     print("[%s] Installing Twisted dependencies" % ip)
        #     self.child.sendline("sudo %s install zope.interface" % self.commands['pip3'])
        #     self.child.prompt()
        #
        #     self.child.logfile = sys.stdout.buffer
        #     print("Installing twisted @ %s... changed! Re-install twisted!" % ip)
        #     self.child.sendline("cd /home/lama/twisted/")
        #     self.child.prompt()
        #
        #     self.child.sendline("sudo python3 setup3.py install")
        #     self.child.prompt()
        #     # else:
        #     #     print("Installing twisted @ %s... no changes!" % ip)
        #
        except:
            traceback.print_exc()
            if self.child:
                self.child.logout()
            return -1

        return 0

    def install_lama_monitor(self, ip, ag):
        print("[%s] Installing lama_monitor" % ip)
        try:
            self.check_commands(ip)

            # self.child.logfile = sys.stdout.buffer
            self.login(ip)

            self.child.sendline("sudo dnf -y install python3-devel")
            self.child.prompt()

            self.child.sendline("git clone https://bitbucket.org/jazzmes/lama_monitor")
            self.child.prompt()

            self.child.sendline("cd lama_monitor")
            self.child.prompt()

            self.child.sendline("git pull")
            self.child.prompt()

            self.child.sendline("sudo %s install -r requirements.txt" % self.commands['pip3'])
            self.child.prompt()

            self.child.sendline("sudo python3 setup.py install")
            self.child.prompt()

            self.child.sendline("cd")
            self.child.prompt()

            # self.child.sendline("sudo rm -rf lama_monitor")
            # self.child.prompt()

        except:
            traceback.print_exc()
            if self.child:
                self.child.logout()
            return -1

        self.child.logout()
        return 0

    def install_lama_exp(self, ip, ag):
        print("[%s] Installing dependency lama_monitor" % ip)
        self.install_lama_monitor(ip, ag)

        print("[%s] Installing lama_exp" % ip)

        try:
            self.check_commands(ip)

            self.login(ip)

            if self.verbose:
                self.child.logfile = sys.stdout.buffer#.buffer

            print("[%s] Cloning" % ip)
            self.child.sendline("git clone https://bitbucket.org/jazzmes/lama_exp")
            self.child.prompt()

            self.child.sendline("cd lama_exp")
            self.child.prompt()

            self.child.sendline("git pull")
            self.child.prompt()

            print("[%s] Installing dependencies" % ip)
            self.child.sendline("sudo %s install -r requirements.txt" % self.commands['pip3'])
            self.child.prompt()

            self.child.sendline("sudo firewall-cmd --add-port=58228/tcp")
            self.child.prompt()

            self.child.sendline("sudo firewall-cmd --add-port=58228/tcp --permanent")
            self.child.prompt()

            self.child.sendline("sudo %s install netifaces" % self.commands['pip3'])
            self.child.prompt()

            print("[%s] Installing lama experiments" % ip)
            self.child.sendline("sudo python3 setup.py install")
            self.child.prompt()

            self.child.sendline("cd")
            self.child.prompt()

            # self.child.sendline("sudo rm -rf lama_exp")
            # self.child.prompt()

        except:
            traceback.print_exc()
            return -1
        finally:
            if self.child:
                self.child.logout()
        return 0

    def install_lama_thanatos(self, ip, ag):
        print("[%s] Installing lama_failures" % ip)

        try:
            self.check_commands(ip)

            self.login(ip)

            if self.verbose:
                self.child.logfile = sys.stdout.buffer

            self.child.sendline("sudo %s uninstall -y sample lama_thanatos" % self.commands['pip3'])
            self.child.prompt()

            print("[%s] Cloning" % ip)
            self.child.sendline("git clone https://bitbucket.org/jazzmes/lama_thanatos")
            self.child.prompt()

            self.child.sendline("cd lama_thanatos")
            self.child.prompt()

            self.child.sendline("git reset --hard")
            self.child.prompt()

            self.child.sendline("git pull")
            self.child.prompt()

            print("[%s] Installing dependencies" % ip)
            self.child.sendline("sudo %s install -r requirements.txt" % self.commands['pip3'])
            self.child.prompt()

            self.child.sendline("sudo firewall-cmd --add-port=58229/tcp")
            self.child.prompt()

            self.child.sendline("sudo firewall-cmd --add-port=58229/tcp --permanent")
            self.child.prompt()

            self.child.sendline("sudo mkdir -p /var/log/thanatos")
            self.child.prompt()

            self.child.sendline("sudo chown lama.lama /var/log/thanatos")
            self.child.prompt()

            print("[%s] Installing lama thanatos" % ip)
            self.child.sendline("sudo python3 setup.py install")
            self.child.prompt()

            self.child.sendline("cd")
            self.child.prompt()

            # self.child.sendline("sudo rm -rf lama_exp")
            # self.child.prompt()

        except:
            traceback.print_exc()
            return -1
        finally:
            if self.child:
                self.child.logout()
        return 0

    # def install_anomaly_generator(self, ip, ag):
    #     print("[%s] Installing anomaly generator" % ip)
    #     try:
    #         self.check_commands(ip)
    #
    #         self.login(ip)
    #
    #         if self.verbose:
    #             self.child.logfile = sys.stdout.buffer # .buffer
    #
    #         print("[%s] Install dependencies" % ip)
    #         self.child.sendline("sudo yum -y install stress wondershaper")
    #         self.child.prompt()
    #
    #         print("[%s] Cloning" % ip)
    #         self.child.sendline("git clone https://github.com/ephemeral2eternity/anomalyBenchmark.git")
    #         self.child.prompt()
    #
    #         self.child.sendline("cd anomalyBenchmark")
    #         self.child.prompt()
    #
    #         self.child.sendline("git fetch")
    #         self.child.prompt()
    #
    #         self.child.sendline("git checkout adapting")
    #         self.child.prompt()
    #
    #         self.child.sendline("git pull")
    #         self.child.prompt()
    #
    #         # self.child.sendline("sudo rm -rf lama_exp")
    #         # self.child.prompt()
    #
    #     except:
    #         traceback.print_exc()
    #         return -1
    #     finally:
    #         if self.child:
    #             self.child.logout()
    #     return 0

    def install_python_iptables(self, ip, ag):
        print("[%s] Installing python-iptables" % ip)
        try:
            # self.child.logfile = sys.stdout.buffer
            self.login(ip)

            self.child.sendline("git clone -b libextension --single-branch https://github.com/jazzmes/python-iptables.git")
            self.child.prompt()

            self.child.sendline("cd python-iptables")
            self.child.prompt()

            self.child.sendline("sudo python3 setup.py install")
            self.child.prompt()

            self.child.sendline("cd")
            self.child.prompt()

            self.child.sendline("sudo rm -rf python-iptables")
            self.child.prompt()

        except:
            traceback.print_exc()
            if self.child:
                self.child.logout()
            return -1

        self.child.logout()
        return 0

    # def install_pyroute2(self, ip, ag):
    #     print("[%s] Installing pyroute2" % ip)
    #     try:
    #         # self.child.logfile = sys.stdout.buffer
    #         self.login(ip)
    #
    #         self.child.sendline("sudo yum -y install python-sphinx")
    #         self.child.prompt()
    #
    #         self.child.sendline("git clone -b master --single-branch https://github.com/jazzmes/pyroute2.git")
    #         self.child.prompt()
    #
    #         self.child.sendline("cd pyroute2")
    #         self.child.prompt()
    #
    #         self.child.sendline("make dist python=python3")
    #         self.child.prompt()
    #
    #         self.child.sendline("sudo make install python=python3")
    #         self.child.prompt()
    #
    #         self.child.sendline("make clean")
    #         self.child.prompt()
    #
    #         self.child.sendline("make dist")
    #         self.child.prompt()
    #
    #         self.child.sendline("sudo make install")
    #         self.child.prompt()
    #
    #         self.child.sendline("cd")
    #         self.child.prompt()
    #
    #         self.child.sendline("sudo rm -rf pyroute2")
    #         self.child.prompt()
    #
    #     except:
    #         traceback.print_exc()
    #         if self.child:
    #             self.child.logout()
    #         return -1
    #
    #     self.child.logout()
    #     return 0

    def install_collectd(self, ip, ag, force=True):
        print("[%s] Installing Collectd" % ip)
        try:
            self.check_commands(ip)

            # self.child.logfile = sys.stdout.buffer
            self.login(ip)

            # active = False
            # if not force:
            # self.child.sendline("command -v collectd | test -f; echo $?")
            # self.child.sendline("[ -x collectd ] && echo 1 || echo 0")
            self.child.sendline("command -v collectd > /dev/null && echo 1 || echo 0")
            self.child.prompt()
            active = int(self.child.before.splitlines()[-1]) == 1

            # print("Force: %s" % force)
            # print("Active: %s" % active)
            # print("not force and active: %s" % (not force and active))

            if not force and active:
                print("NOT Installing collectd - already installed @ %s... " % ip)
            else:
                print("Installing collectd @ %s... " % ip)

                # for a viewer, pip: sickmuse (simple option and can be started manually)
                # needs cairo-devel, pango-devel
                self.child.sendline("sudo yum -y install python-devel rrdtool-devel cairo-devel pango-devel "
                                    "perl-ExtUtils-MakeMaker libtool-ltdl-devel libgcrypt-devel glib2-devel git "
                                    "hiredis-devel")
                self.child.prompt()

                self.child.sendline("sudo yum -y install flex byacc bison autoconf automake libtool")
                self.child.prompt()

                self.child.sendline("git clone -b lama --single-branch https://github.com/jazzmes/collectd.git")
                self.child.prompt()

                self.child.sendline("cd collectd")
                self.child.prompt()

                self.child.sendline("./clean.sh")
                self.child.prompt()

                self.child.sendline("./build.sh")
                self.child.prompt()

                self.child.sendline("./configure")
                self.child.prompt()

                self.child.sendline("make")
                self.child.prompt()

                self.child.sendline("sudo make install")
                self.child.prompt()

                self.child.sendline("sudo ln -s /opt/collectd/sbin/collectd /usr/sbin/")
                self.child.prompt()

                self.child.sendline("cd")
                self.child.prompt()

                self.child.sendline("sudo cp ~/collectd/contrib/collectd.service /usr/lib/systemd/system/")
                self.child.prompt()

                self.child.sendline("sudo rm -rf collectd")
                self.child.prompt()

                print("Installing sickmuse (collectd GUI) @ %s... " % ip)
                self.child.sendline("sudo %s install tornado python-rrdtool sickmuse" % self.commands['pip'])
                self.child.prompt()

            print("Installing collectd configuration @ %s... " % ip)
            self.child.sendline("sudo mkdir -p /etc/collectd; sudo cp ~/lama/resources/collectd.conf /etc/collectd/collectd.conf")
            self.child.prompt()

            self.child.sendline("sudo systemctl enable collectd.service")
            self.child.prompt()

            self.child.sendline("sudo systemctl restart collectd.service")
            self.child.prompt()

            # used version of collectd seems to have a memory leak. Add daily restart
            self.child.sendline("sudo ~/lama/add_restart_collectd_to_cron.sh")
            self.child.prompt()

        except:
            traceback.print_exc()
            self.child.logout()
            return -1

        self.child.logout()
        return 0

    def install_mongodb(self, ip, ag):
        print("[%s] Installing MongoDB" % ip)
        if ag != "provider":
            return 0

        try:
            self.login(ip)
            # self.child.logfile = sys.stdout.buffer
            print("Installing mongodb @ %s... " % ip)
            # pe = pexpect.spawn("scp ../experiments/resources/redis.conf lama@%s:~/" % ip)
            # pe.expect(pexpect.EOF, timeout=300)
            # pe.close()

            self.child.sendline("sudo yum -y install mongodb")
            self.child.prompt()

            # self.child.sendline("sudo mv ~/redis.conf /etc/redis.conf")
            # self.child.prompt()

            self.child.sendline("sudo systemctl restart mongodb.service")
            self.child.prompt()

            #mongo
            self.child.sendline("wget https://github.com/mongodb/mongo-c-driver/releases/download/0.98.0/mongo-c-driver-0.98.0.tar.gz")
            self.child.prompt()

            self.child.sendline("tar -xzvf mongo-c-driver-0.98.0.tar.gz")
            self.child.prompt()

            self.child.sendline("rm mongo-c-driver-0.98.0.tar.gz")
            self.child.prompt()

            self.child.sendline("cd mongo-c-driver-0.98.0")
            self.child.prompt()

            self.child.sendline("./configure --prefix=/usr --libdir=/usr/lib64")
            self.child.prompt()

        except:
            traceback.print_exc()
            self.child.logout()
            return -1

        self.child.logout()
        return 0

    def install_redis(self, ip, ag):
        print("[%s] Installing REDIS" % ip)
        try:
            print("Installing redis @ %s... " % ip)
            pe = pexpect.spawn("scp manage/config/redis.conf lama@%s:~/" % ip)
            pe.expect(pexpect.EOF, timeout=300)
            pe.close()

            self.login(ip)
            self.child.logfile = sys.stdout.buffer

            self.child.sendline("sudo yum -y install redis")
            self.child.prompt()

            self.child.sendline("sudo %s install redis" % self.commands['pip3'])
            self.child.prompt()

            self.child.sendline("sudo mv ~/redis.conf /etc/redis.conf")
            self.child.prompt()

            self.child.sendline("sudo systemctl restart redis.service")
            self.child.prompt()

            self.child.sendline("sudo systemctl enable redis.service")
            self.child.prompt()

            # open redis port
            self.child.sendline("sudo iptables -I INPUT -p tcp --dport 6379 -j ACCEPT")
            self.child.prompt()

            # self.child.sendline("rm credis-0.2.3.tar.gz")
            # self.child.prompt()
            #
            # self.child.sendline("wget https://credis.googlecode.com/files/credis-0.2.3.tar.gz")
            # self.child.prompt()
            #
            # self.child.sendline("tar -xzvf credis-0.2.3.tar.gz")
            # self.child.prompt()
            #
            # self.child.sendline("rm credis-0.2.3.tar.gz")
            # self.child.prompt()
            #
            # self.child.sendline("cd credis-0.2.3")
            # self.child.prompt()
            #
            # self.child.sendline("make")
            # self.child.prompt()
            #
            # self.child.sendline("sudo cp *.h /usr/local/include/")
            # self.child.prompt()
            # self.child.sendline("sudo cp *.so *.a /usr/local/lib64/")
            # self.child.prompt()
            # self.child.sendline("sudo cp libcredis.so /usr/lib64/")
            # self.child.prompt()
            #
            # self.child.sendline("cd")
            # self.child.prompt()
            #
            # self.child.sendline("rm -rf credis-0.2.3")
            # self.child.prompt()

        except:
            traceback.print_exc()
            self.child.logout()
            return -1

        self.child.logout()
        return 0

    def install_ryu(self, ip, ag, force=True):
        if 'pip' not in self.commands:
            self.check_commands(ip)

        print("[%s] Installing RYU" % ip)
        if ag != "provider":
            return 0
        try:
            # self.child.logfile = sys.stdout.buffer
            self.login(ip)

            active = False
            if not force:
                # self.child.sendline("command -v collectd | test -f; echo $?")
                # self.child.sendline("[ -x ryu-manager ] && echo 1 || echo 0")
                self.child.sendline("command -v ryu-manager > /dev/null && echo 1 || echo 0")
                self.child.prompt()
                active = int(self.child.before.splitlines()[-1]) == 1

            # print("Force: %s" % force)
            # print("Active: %s" % active)
            # print("not force and active: %s" % (not force and active))

            if not force and active:
                print("NOT Installing ryu - already installed @ %s... " % ip)
            else:
                print("Installing ryu @ %s... " % ip)

                # dependencies
                # self.child.sendline("sudo yum -y install python-devel python-pip")
                # self.child.prompt()

                # install virtualenv
                print("[%s] Setting up Ryu VirtualEnv" % ip)
                self.child.sendline("sudo %s install virtualenv" % self.commands['pip'])
                self.child.prompt()

                self.child.sendline("mkdir -p ~/lama_envs")
                self.child.prompt()

                # self.child.sendline("sudo rm -rf ~/lama_envs/ryu")
                # self.child.prompt()

                self.child.sendline("virtualenv -p /usr/bin/python2.7 ~/lama_envs/ryu")
                self.child.prompt()

                self.child.sendline("source ~/lama_envs/ryu/bin/activate")
                self.child.prompt()

                print("[%s] Install Ryu dependencies" % ip)
                self.child.sendline("~/lama_envs/ryu/bin/pip install --upgrade eventlet==0.15.2 msgpack-python==0.4.6 "
                                    "netifaces==0.10.4 "
                                    "oslo.config==1.9.3 'paramiko>=1.16.0' pbr==0.11.0 redis==2.10.3 "
                                    "stevedore==1.1.0 testrepository==0.0.20 WebOb==1.4 Routes==1.13")
                self.child.prompt()

                self.child.sendline("rm -rf ryu")
                self.child.prompt()

                print("[%s] Download Ryu" % ip)
                self.child.sendline("git clone -b lama --single-branch https://github.com/jazzmes/ryu.git")
                self.child.prompt()

                self.child.sendline("cd ryu")
                self.child.prompt()

                print("[%s] Install Ryu" % ip)
                self.child.sendline("python setup.py install")
                self.child.prompt(timeout=900)

                self.child.sendline("cd")
                self.child.prompt()

                self.child.sendline("rm -rf ryu")
                self.child.prompt()

        except:
            traceback.print_exc()
            self.child.logout()
            return -1

        self.child.logout()
        return 0

    def start_sickmuse(self, ip, ag):
        raise NotImplementedError()


# if __name__ == "__main__":
#     parser = ArgumentParser(description="Install LAMA components.")
#     parser.add_argument('-f', type=str, dest='target_file', help="File containing list of targets.", required=True)
#     parser.add_argument('-a', type=str, dest='action', help="Action to apply to targets.",
#                         choices=["start", "stop", "restart", "reset", "deploy", "test"], required=True)
#     cmdargs = parser.parse_args()


class InstallManager(object):

    # class Action:
    #     ALL = "all"
    #     TWISTED = "twisted"

    def __init__(self, targets, action, max_threads=50, alert=True, verbose=False):
        self.targets = targets
        self.failed = 0
        self.action = action
        self.running_time = None
        self.max_threads = max_threads
        self.alert = alert
        self.verbose = verbose

    def run(self):

        threadlist = []
        starttime = time.time()
        num_of_threads = min(self.max_threads, len(self.targets))

        print("\n\n----------------------------------------\n")
        print("Starting %d threads\n" % num_of_threads)

        # load object to run in threads
        for i in range(num_of_threads):
            target = self.targets[i]
            obj = ManageLama(target=target, actions=["install_%s" % self.action], verbose=self.verbose)
            threadlist.append(obj)
            obj.start()

        for t in threadlist:
            t.join()
            print("Thread " + t.name + " finished - %s!" % ("SUCCESS" if t.result else "FAIL"))
            if not t.result:
                self.failed += 1

        finishtime = time.time()
        self.running_time = finishtime - starttime
        print("\nExecution lasted for " + str(self.running_time) + " seconds!")
        print("\n----------------------------------------\n")


        if self.alert:
            self.announce_termination()

        # voices = [
        #     "Agnes",
        #     # "Albert",
        #     # "Alex",
        #     # "Bad News",
        #     # "Bahh",
        #     # "Bells",
        #     # "Boing",
        #     # "Bruce",
        #     # "Bubbles",
        #     # "Cellos",
        #     # "Deranged",
        #     # "Fred",
        #     # "Good News",
        #     # "Hysterical",
        #     # "Junior",
        #     # "Kathy",
        #     # "Pipe Organ",
        #     # "Princess",
        #     # "Ralph",
        #     # "Trinoids",
        #     # "Vicki",
        #     # "Victoria",
        #     # "Whisper",
        #     # "Zarvox"
        # ]
        #
        # success_sentences = [
        #     "Done"
        #     # "Hurray! I'm done",
        #     # "Okidoki!",
        #     # "yipikaye!",
        #     # "It's so fluffy!",
        #     # "Houston, we solved a problem!",
        #     # "SILENCE! I kill you!",
        #     # "I want the truth!",
        #     # "You can't handle the truth!"
        # ]
        #
        # fail_sentences = [
        #     "Oh, no! %s of %s tasks failed!" % (fail, len(threadlist))
        # ]

    def announce_termination(self):
        voice = "Agnes"
        if self.failed:
            sentence = "Oh, no! %s of %s tasks failed!" % (self.failed, len(self.targets))
        else:
            sentence = "Hurray! I'm done"
        print("[%s] %s" % (voice, sentence))
        os.system('say "%s" -v "%s"' % (sentence, voice))

# install
# redis-py


# "echo -e 'lama ALL=(ALL) NOPASSWD:ALL\nCmnd_Alias NOTTYCMDS=/usr/sbin/dnsmasq,/usr/sbin/hdparm,/usr/sbin/dmsetup\nDefaults!NOTTYCMDS !requiretty\n' > /etc/sudoers.d/lama"

if __name__ == "__main__":
    parser = ArgumentParser(description="Run LAMA management action.")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-f', type=str, dest='target_file', help="File containing list of targets.")
    group.add_argument('-s', type=str, dest='host', help="<IP to restart>:<Type of host>")

    parser.add_argument('-a', type=str, dest='action', help="Action to apply to targets.",
                        choices=["all", "lama", "twisted", "core", "python_iptables", "deps",
                                 "collectd", "redis", "ryu", "user", "lama_monitor", "lama_exp",
                                 "anomaly_generator", "lama_thanatos"], required=True)
    parser.add_argument('-v', dest='verbose', action='store_true', help="Dump output of commands")
    cmdargs = parser.parse_args()

    lama_agents = []
    if cmdargs.target_file:
        with open(cmdargs.target_file, 'r') as fp:
            lama_agents = [tuple(line.split()) for line in fp.readlines() if not line.lstrip().startswith(";")]

    if cmdargs.host:
        try:
            ip, ht = cmdargs.host.split(':')
            lama_agents.append((ip, ht))
        except Exception as e:
            print("Error: %s - Format for inline host is <ip>:<host_type>" % e)

    if not lama_agents:
        print("No targets specified")
        exit(-1)
    man = InstallManager(lama_agents, cmdargs.action, verbose=cmdargs.verbose)

    man.run()