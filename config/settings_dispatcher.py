LOG_LEVEL = 'DEBUG'

API_URL = "http://127.0.0.1:80/monitor/api/"
USER = "dispatcher"
PASS = "x08euAon"

# this parameters should into the configuration
IP_PREFIX = 24
HOSTS_PER_CLUSTER = 2

PATH = "/home/lama/lama/"
MIDDLEWARE_SERVICES_FOLDER = "/home/lama/vm_images/support/"

DISPATCHER_EXPERIMENT_SERVER_PORT = 21000


# filters: for experiments only
# impose an agent to be chosen by the dispatcher
# FILTER_AGENT_PROVIDER = '10.1.1.42'
