# Example - default values - do not change
# FORCE_ALLOCATIONS = {
#     'rubbos-001': {
#         'provider': '10.1.1.42',
#         'allocations': {
#             'mysql-001': ('10.1.1.17',),
#             'apache-001': ('10.1.1.17',),
#             'lb-apache': ('10.1.1.3',)
#         }
#     }
# }
# FORCE_PEERS = {
#     '10.1.1.42': {'10.1.1.14'},
# }
# FILTER_NO_ALLOC_PROVIDERS = set() #  {'10.1.1.31', '10.1.1.32', '10.1.1.38', '10.1.1.41', '10.1.1.42', '10.1.1.50'}
# AVOID_DUPLICATE_PROVIDERS_SAME_ALLOCATION = True
# END DEFAULT

# Add custom options
FORCE_ALLOCATIONS = {
    # 'rubbos-000001': {
    #     'provider': '10.1.1.42',
    # },
    # 'demo-000001': {
    #     'provider': '10.1.1.42',
    # },
    # 'rubbos-001': {
    #     'provider': '10.1.1.42',
    #     'allocations': {
    #         'client-0001': ('10.1.1.42',),
    #         'img_client-0001': ('10.1.1.42',),
    #
    #         'apache-0001': ('10.1.1.50',),
    #         'apache-0002': ('10.1.1.18',),
    #         'img_apache-0001': ('10.1.1.31',),
    #         # 'apache-0002': ('10.1.1.1',),
    #         #
    #         'lb-apache-0001': ('10.1.1.19',),
    #         'img_lb-apache-0001': ('10.1.1.42',),
    #         #
    #         'mysql-0001': ('10.1.1.31',),
    #         # 'mysql-0002': ('10.1.1.50',),
    #         'img_mysql-0001': ('10.1.1.50',),
    #     }
    # },
    # 'rubbos-002': {
    #     'provider': '10.1.1.3',
    #     'allocations': {
    #         'client-0001': ('10.1.1.3',),
    #         'img_client-0001': ('10.1.1.3',),
    #
    #         'apache-0001': ('10.1.1.31',),
    #         'apache-0002': ('10.1.1.1',),
    #     }
    # },
    # 'rubbos-003': {
    #     'provider': '10.1.1.2',
    #     'allocations': {
    #         'client-0001': ('10.1.1.2',),
    #         'img_client-0001': ('10.1.1.2',),
    #
    #         'apache-0001': ('10.1.1.1',),
    #     }
    # },

    # 'DEFAULT': {
    #     'provider': '10.1.1.42'
    # }
}
FORCE_PEERS = {
    # '10.1.1.42': {
    #     #     '10.1.1.1',
    #     #     # '10.1.1.13',
    #     #     # '10.1.1.14',
    #     #     # '10.1.1.16',
    #     #     # '10.1.1.17',
    #     '10.1.1.19',
    #     '10.1.1.31',
    #     #     # '10.1.1.32',
    #     '10.1.1.50',
    # },
    # '10.1.1.3': {
    #     '10.1.1.1',
    #     '10.1.1.31',
    #     '10.1.1.50'
    # },
    # '10.1.1.2': {
    #     '10.1.1.1',
    #     '10.1.1.50'
    # }

}

FORCE_NO_ALLOC_PROVIDERS = set()
FORCE_AVOID_DUPLICATE_PROVIDERS_SAME_ALLOCATION = False

# Configure some workflow options
PROVIDER_USE_FAILURE_GRAPH = True
# # Multiple/Composite strategy example
# APP_DEFAULT_MONITORING_STRATEGY = 'fsm.HierarchicalFSMStrategy,fsm.OnOffStrategy'
# # Instance Crash Failure strategy
APP_DEFAULT_MONITORING_STRATEGY = 'fsm.OnOffStrategy,fsm.Hang100PerCentStrategy'
