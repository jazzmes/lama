# LOG_LEVEL = 5
LOG_LEVEL = 'DEBUG'

DISPATCHER_AG_PORT = 20000
DISPATCHER_AG_WEB_PORT = 20001
PROVIDER_AG_PORT = 20010
PROVIDER_AG_WEB_PORT = 20011

# message types
APP_GRAPH = 10

# supply demand
CPU_IDLE_COST = 10

# timeouts
TIMEOUT_APP_DEPLOY_PROVIDER_CONNECT = 1
TIMEOUT_APP_DEPLOY_REPLY = 3
TIMEOUT_APP_DEPLOY_DROP = 5
TIMEOUT_PROBES = 0.2
TIMEOUT_APP_LAUNCH = 1
# dispatcher
# catalog
INITIAL_CANDIDATES = 5

# peering
# * peers are only queried for new candidates at most once per
PEER_QUERY_TIMEOUT = 300
# * number of simultaneous peers to ask for new candidates on each round
PEER_QUERY_SIMUL_NUMBER = 5
# default constraint (number of providers in a shortlist)
DEFAULT_PROVIDER_NUM_SHORTLIST = 10
DEFAULT_PROVIDER_VCPUS_NUM_SHORTLIST = 10

# determine avillable resources
FORCE_RESOURCE_SCAN = False

# resource reservation
RESOURCE_RESV_TIMEOUT = 1

PATH = "/home/lama/lama/"

PATH_CONFIG = PATH + "config/"
PATH_TMP = PATH + "tmp/"
PATH_APP_SPEC_FILES = PATH_CONFIG + "spec_files/"
PATH_SCRIPTS_FOLDER = PATH + "lama/scripts/"
PATH_PERSISTENCE = PATH + "persistence/"
# PATH_IMAGES = "/var/lib/libvirt/images/lama/"
PATH_IMAGE_POOL = "/home/lama-pool"
PATH_IMAGE_STORAGE = PATH + "vm-images/"
# PATH_DNSMASQ = "/var/lib/libvirt/images/lama/dnsmasq/"
PATH_DNSMASQ = PATH + "dnsmasq/"
PATH_LOG = "/var/log/lama/"
PATH_RRD = "/var/lib/collectd/rrd"
PATH_GML = PATH + "lama/ops/app_gml"


# filters: providers that should not be used for resource allocation
