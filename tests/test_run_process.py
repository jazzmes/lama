from __future__ import absolute_import, division, print_function, unicode_literals
import logging
import sys
from twisted.internet.protocol import ProcessProtocol
from twisted.internet import reactor


logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)


class LamaProcessProtocol(ProcessProtocol):
    def __init__(self):
        super().__init__()
        logger.debug("On init process protocol")

    def connectionMade(self):
        logger.debug("On connectionMade process protocol")
        super().connectionMade()

    def outReceived(self, data):
        logger.debug("On outReceived process protocol: data=%s", data)
        super().outReceived(data)

    def errReceived(self, data):
        logger.debug("On errReceived process protocol: data=%s", data)
        super().errReceived(data)

    def inConnectionLost(self):
        logger.debug("On inConnectionLost process protocol")
        super().inConnectionLost()

    def outConnectionLost(self):
        logger.debug("On outConnectionLost process protocol")
        super().outConnectionLost()

    def errConnectionLost(self):
        logger.debug("On errConnectionLost process protocol")
        super().errConnectionLost()

    def processExited(self, reason):
        logger.debug("On processExited process protocol: reason=%s", reason)
        super().processExited(reason)

    def processEnded(self, reason):
        logger.debug("On processEnded process protocol: reason=%s", reason)
        super().processEnded(reason)


command = "ls -al"
command = "%s -m lama.agent.helpers.network_scripts" % sys.executable

command = command.split()
logger.info(command)
reactor.spawnProcess(LamaProcessProtocol(), command[0], args=command, uid=None, gid=None)
reactor.run()
