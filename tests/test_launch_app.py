import os
import subprocess
import  traceback
from multiprocessing import Process, Queue
from pwd import getpwnam
from grp import getgrnam
from config.settings import PATH_SCRIPTS_FOLDER
from lama.utils.temp_netns import setns

__author__ = 'tiago'


def set_user(user="lama", group=None):
    try:
        print("change_user")
        u = getpwnam(user)
        print("u: %s" % str(u))
        if group:
            g = getgrnam(group)
            print("u: %s" % str(g))
            os.setegid(g.gr_gid)
            print("gid: %s" % g.gr_gid)
        else:
            os.setegid(u.pw_gid)
            print("gid from user: %s" % u.pw_uid)
        os.seteuid(u.pw_uid)
        print("uid: %s" % u.pw_uid)
    except:
        print(traceback.format_exc())

def launch_app(q, ns_name, app_name):

    print("Starting app: %s @ %s" % (app_name, ns_name))
    setns(ns_name)

    exe = os.path.join(PATH_SCRIPTS_FOLDER, "run_application.py")
    args = [exe, '10', '/home/lama/lama/config/spec_files/test-000001.json', '0']
    print("Launching agent: %s" % args)
    cmd = None
    try:
        cmd = subprocess.Popen(args, close_fds=True, preexec_fn=lambda: set_user())
        # cmd = subprocess.Popen(args, close_fds=True)
    except:
        print("Error launching process (%s). Trace: %s" % ([app_name, args], traceback.format_exc()))
        # return None

    if cmd:
        q.put(cmd.pid)

    # if proc.pid:
    #     self.pid = proc.pid
    #     self.status = AppStatus.Launched
    #     logger.info("Launched application agent for App: '%s' at port %s => PID: %s",
    #                 self.app_name, self.port, self.pid)
    # else:
    #     return None
        # raise NotImplementedError("Application was not launched: no pid.")

if __name__ == "__main__":
    q = Queue()
    proc = Process(target=launch_app, args=(q, "ns-test-000001", "test-000001"))
    proc.start()
    pid = q.get()
    print("Process started with pid: %s" % pid)