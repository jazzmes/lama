#!/usr/bin/python3
import sys
import os
from lama.agent.app.connectors.app_connector import CmartAppConnector

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]), os.pardir)))

if __name__ == "__main__":

    ns_name = "gw-cmart-000001"
    ip = "192.168.1.1"
    user = "root"
    password = "Cm4rt!"

    # print("Connecting to %s" % [ns_name, ip, user, password])
    # connector = CmartAppConnector(ns_name, ip, user, password)
    # print(connector)
    # connector.add_config({
    #     "full_url1": "http://lb-0001:80/cmart-1",
    #     "run_length": 10,
    #     "rampup_time": 30
    # })
    # print(connector)
    # print(dir(connector))
    # print(connector.start)
    # connector.test()
    # connector.start()

    connector = CmartAppConnector(ip, user, password, ns=ns_name)
    connector.add_config({
        "full_url1": "http://lb-0001:80/cmart-1",
        "run_length": 3600,
        "rampup_time": 100
    })
    connector.start()
