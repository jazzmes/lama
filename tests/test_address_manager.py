from netaddr import IPAddress, IPNetwork

from lama.agent.helpers.address_manager import AddressManager, InvalidIPNetwork, DuplicateSubnet, IPRangeExhausted, \
    InvalidSubnet


__author__ = 'tiago'

import unittest


class TestAddressManager(unittest.TestCase):

    def test_create_address_manager(self):
        # experiment with creating invalid ip ranges
        self.assertRaises(InvalidIPNetwork, AddressManager, "test")
        self.assertRaises(InvalidIPNetwork, AddressManager, 3)

        for net, size in [("192.168.1.1", 1), ("192.168.1.0/30", 4)]:
            addrman = AddressManager(net)
            self.assertIsNotNone(addrman)
            self.assertEqual(addrman.size, size, "Wrong size")

        self.assertIsNotNone(AddressManager())

    def test_single_address(self):
        addrman = AddressManager("192.168.1.0/28")
        net = IPNetwork("192.168.1.0/28")
        ip = addrman.get_ip()
        self.assertIsNotNone(ip)
        self.assertIsInstance(ip, IPAddress)
        self.assertIn(ip, net)

    def test_unique_address(self):
        addrman = AddressManager("192.168.1.0/28")
        used = set()
        ip = addrman.get_ip()
        while ip:
            self.assertNotIn(ip, used)
            addrman.reserve(ip)
            used.add(ip)
            ip = addrman.get_ip()

        s = addrman.size
        self.assertIn(len(used), range(s-3, s-1))

    def test_reserve(self):
        addrman = AddressManager("192.168.0.0/16")
        self.assertFalse(addrman.reserve("192.168.1.1"))
        addrman.create_subnet("sn1")
        self.assertFalse(addrman.reserve("192.168.1.1"))
        addrman.create_subnet("sn2")
        self.assertTrue(addrman.reserve("192.168.1.1"))
        self.assertFalse(addrman.reserve("192.168.1.1"))
        self.assertFalse(addrman.reserve("192.168.2.1"))

    def test_subnet_add_remove_subnets(self):
        addrman = AddressManager("192.168.0.0/16", base_subnet_prefix=24)
        # default subnet
        self.assertIsNone(addrman.get_cidr())
        addrman.get_ip()
        self.assertEqual(addrman.get_cidr(), "192.168.0.0/24")
        addrman.get_ip()
        self.assertEqual(addrman.get_cidr(), "192.168.0.0/24")

        addrman = AddressManager("192.168.1.0/16", base_subnet_prefix=24)
        self.assertEqual(addrman.create_subnet("subnet1"), IPNetwork("192.168.0.0/24"))
        self.assertEqual(addrman.get_cidr("subnet1"), "192.168.0.0/24")
        self.assertEqual(addrman.subnet_count, 1)
        self.assertEqual(addrman.create_subnet("subnet2"), IPNetwork("192.168.1.0/24"))
        self.assertEqual(addrman.get_cidr("subnet2"), "192.168.1.0/24")
        self.assertEqual(addrman.subnet_count, 2)

        self.assertRaises(DuplicateSubnet, addrman.create_subnet, "subnet1")

        addrman = AddressManager("192.168.1.0/24", base_subnet_prefix=25)
        self.assertEqual(addrman.create_subnet("subnet1"), IPNetwork("192.168.1.1/25"))
        self.assertEqual(addrman.create_subnet("subnet2"), IPNetwork("192.168.1.128/25"))
        self.assertRaises(IPRangeExhausted, addrman.create_subnet, "subnet3")

        addrman.reserve(addrman.get_ip("subnet1"))
        addrman.delete_subnet("subnet1")
        self.assertEqual(addrman.subnet_count, 1)
        self.assertIsNone(addrman.get_cidr("subnet1"))
        for ip in IPNetwork("192.168.1.0/24"):
            self.assertFalse(addrman.reserved(ip), "%s was not deleted" % ip)

        self.assertEqual(addrman.create_subnet("subnet3"), IPNetwork("192.168.1.1/25"))
        self.assertEqual(addrman.get_cidr("subnet3"), "192.168.1.0/25")
        self.assertEqual(addrman.subnet_count, 2)

        # non-existent subnet
        ip_size_before = addrman.used_ip_count
        subnet_size_before = addrman.subnet_count
        self.assertRaises(InvalidSubnet, addrman.delete_subnet, "subnet1")
        self.assertEqual(addrman.subnet_count, subnet_size_before)
        self.assertEqual(addrman.used_ip_count, ip_size_before)

    def test_subnet_unique_address(self):
        addrman = AddressManager("192.168.0.0/16", base_subnet_prefix=24)
        net = IPNetwork("192.168.0.0/24")
        self.assertEqual(addrman.create_subnet("subnet1"), net)
        used = set()
        self.assertNotIn(addrman.get_ip(), net)

        ip = addrman.get_ip("subnet1")
        while ip:
            self.assertIn(ip, net)
            self.assertNotIn(ip, used, "%s was repeated")
            addrman.reserve(ip)
            used.add(ip)
            ip = addrman.get_ip("subnet1")

        s = IPNetwork(addrman.get_cidr("subnet1")).size
        self.assertIn(len(used), range(s-3, s-1))

    def test_create_default_no_subnet(self):
        addrman = AddressManager("192.168.0.0/16", base_subnet_prefix=24, default_subnet=True)
        self.assertEqual(addrman.subnet_count, 1)
        self.assertEqual(addrman.get_cidr(), "192.168.0.0/24")
