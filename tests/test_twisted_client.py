from twisted.internet import reactor, protocol
from datetime import datetime

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'


class EchoClient(protocol.Protocol):

    def connectionMade(self):
        print("[%s] connection made" % datetime.now())
        self.transport.write("hello. world!")

    def dataReceived(self, data):
        print("[%s] server said: %s" % (datetime.now(), data))
        self.transport.loseConnection()

    def connectionLost(self, reason):
        print("[%s] connection lost" % datetime.now())


class EchoFactory(protocol.ClientFactory):
    protocol = EchoClient

    def clientConnectionFailed(self, connector, reason):
        print("[%s] connection failed" % datetime.now())
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        print("[%s] connection lost" % datetime.now())
        reactor.stop()


def main(port=8000):
    print("[%s] start" % datetime.now())
    factory = EchoFactory()
    reactor.connectTCP('localhost', 8000, factory)
    reactor.run()
    print("[%s] connection done" % datetime.now())

if __name__ == '__main__':
    main()