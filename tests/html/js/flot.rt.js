/**
 * Created by tiago on 6/16/15.
 */


function RealTimeSeriesChart(containerSelector, getDataCb, updateInterval, maxPoints, xScale, yScale) {
    this.containerSelector = containerSelector;
    this.getDataCb = getDataCb;
    this.chart_data = {};

    // optional parameters
    this.maxPoints = maxPoints || 300;
    this.xScale = xScale || {'min': 0, 'max': 100};
    this.yScale = yScale || {'min': 0, 'max': 10};
//    this.updateInterval = updateInterval || 1000;

    // create plot
    this.plot = this.createPlot();

    this.range = 1000 * 60 * 10; // 10 minutes
}

RealTimeSeriesChart.prototype.formatter = function(val, axis) {
    if (val > 1000) {
        val = val.toExponential(2);
    } else {
        val = Math.round(val * 1000) / 1000;
    }
    return "<span>" +  val + "</span>";
};

RealTimeSeriesChart.prototype.createPlot = function() {
//    var d = this.loadData();
    var d = new Date();
    var maxX = d.getTime(); // - d.getTimezoneOffset() * 60 * 1000;
    var minX = maxX - this.range;
    return $.plot(this.containerSelector, [ [] ], {
        series: {
            shadowSize: 0,	// Drawing is faster without shadows
            lines: {
                show: true,
                fill: true
            }
        },
        yaxis: {
            tickFormatter: this.formatter
        },
        xaxis: {
            mode: "time",
            minTickSize: [2, "minute"]
//            min: minX,
//            max: maxX
        },
        legend: {
            show: true,
            noColumns: 10,
//            backgroundColor: "green"
            position: 'nw'
//            backgroundOpacity: 1
        }
    });
};

RealTimeSeriesChart.prototype.addData = function(data, debug) {
    var maxX = new Date().getTime();
    var minX = maxX - 1000 * 60 * 60; // 1hour
    var minTs = null;
    var maxTs = 0;

    var sortedSeries = Object.keys(data).sort();
//    console.log("data", data, Object.keys(data), sortedSeries);
    for (var j = 0; j < sortedSeries.length; j++) {
        var series = sortedSeries[j];
        if (data.hasOwnProperty(series)) {
//            var points = data[series];
//        }
//    }
////        console.log(series);
//    for (var series in data) {
//        if (data.hasOwnProperty(series)) {
            var sdata = null;
            var cnt = 0;

            if (this.chart_data.hasOwnProperty(series)){
                sdata = this.chart_data[series];
                if (debug) {
                    console.log("Data:", sdata, ", Min X: ", minX);
                    console.log("Current series has " + sdata.length + " points");
                }
                // remove old values
                while (sdata.length > 0 && sdata[0][0] < minX) {
                    sdata.shift();
                    cnt++;
                }
            } else {
                sdata = [];
                this.chart_data[series] = sdata;
            }
            if (debug) {
                console.log("Removed " + cnt + " points");
            }

            var points = data[series];
            cnt = 0;
            var lastTs = sdata.length ? sdata[sdata.length - 1][0] : null;
            for (var i = 0; i < points.length; i++) {
                var pt = points[i];
                if (debug) {
                    console.log("Last ts: ", lastTs, ", Point: ", pt);
                }
                if (pt[0] > minX && (!lastTs || pt[0] > lastTs)) {
                    sdata.push(pt);
                    cnt++;
                }
            }

            // update last timestamp
            lastTs = sdata.length ? sdata[sdata.length - 1][0] : null;
            if (!minTs || lastTs < minTs) {
                minTs = lastTs;
            }
            if (lastTs > maxTs) {
                maxTs = lastTs;
            }
//            console.log(sdata);
//            console.log("lastTs: ", lastTs);
//            console.log("minTs: ", minTs);

            if (debug) {
              console.log("Added " + cnt + " points");
            }
        }
    }

    var gdata = [];
    for (series in this.chart_data) {
        if (this.chart_data.hasOwnProperty(series)) {
            gdata.push({label: series, data: this.chart_data[series]});
        }
    }

    if (debug){
        console.log("Update plot with", gdata, gdata.length);
        var values = (((gdata || [])[0] || {}) || {}).data || [];
        for (var i = 0; i < values.length; i++) {
            var pt = values[i];
            console.log(pt[0], pt[1]);
        }
    }

    if (maxTs > 0) {
        var axes = this.plot.getAxes();
        axes.xaxis.options.max = maxTs;
        axes.xaxis.options.min = maxTs - this.range;
    }


    this.plot.setData(gdata);
    this.plot.setupGrid();
    this.plot.draw();
//    bang;
    return minTs
};

