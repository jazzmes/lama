/**
 * Created by tiago on 8/4/15.
 */

function StatusDashboard(containerSelector, alertSelector, args) {
    this.containerSelector = containerSelector;
    this.container = $(containerSelector);
    this.container_alerts = $(alertSelector);
    this.code = 65;

    this.params = {};
    this.paramControls = [];

    args = args  || {};
    this.server = args.server || "10.1.1.8";
    this.port = args.port || "20011";
    this.updateInterval = args.updateInterval || 10000;
    this.app_name = args.app_name || "rubbos-000001";

    this.svg = null;
    this.g = null;
    this.inner = null;
    this.zoom = null;
    this.render = null;
    this.intervalId = null;
    this.requests = lamaRequests || new LamaRequests([]);
}

StatusDashboard.prototype.setParam = function(name, val) {
    this.params[name] = val;
};

StatusDashboard.prototype.registerParam = function(selector, name) {
  this.paramControls[selector] = name;
};

StatusDashboard.prototype.start = function() {
    // Set up zoom support
    this.svg = d3.select(this.containerSelector + " svg");
    this.inner = this.svg.select("g");
    this.zoom = d3.behavior
            .zoom()
            .on("zoom", function () {
                this.inner.attr("transform", "translate(" + d3.event.translate + ")" + "scale(" + d3.event.scale + ")");
        }.bind(this));
    this.svg.call(this.zoom);

    console.log(dagreD3);
    this.render = new dagreD3.render();

    // Left-to-right layout
    this.g = new dagreD3.graphlib.Graph({compound:true});
    this.g.setGraph({
        nodesep: 70,
        ranksep: 50,
        rankdir: "LR",
        marginx: 20,
        marginy: 20
    });

    this.getArchitecture();
    this.restart();
//    this.createExpArch();
//    this.draw();

};

StatusDashboard.prototype.restart = function() {
    this.intervalId = setInterval(this.getArchitecture.bind(this), this.updateInterval, true);
};

StatusDashboard.prototype.pause = function() {
    clearInterval(this.intervalId);
};

StatusDashboard.prototype.getArchitecture = function(update) {
    var alert_p = $("p#alerts");
    alert_p.removeClass();
    alert_p.text("Trying to contact server");

    this.params = {};
    for (var selector in this.paramControls) {
        if (this.paramControls.hasOwnProperty(selector)) {
            this.params[this.paramControls[selector]] = $(selector).prop("checked");
//            console.log($(selector).value);
        }
    }

    var o =  $("#agent-address");
    if (o) {
        var address = o.val();
        if (address) {
            var result = address.split(":");
            if (result.length == 1) {
                this.server = address;
            } else if (result.length == 2) {
                this.server = result[0];
                this.port = result[1];
            }
        } else {
            o.val(this.server + ":" + this.port);
        }
    }

    this.requests.provider(
        this.server, this.port, "app_status_data", {
        data: {
            app_name: this.app_name,
            options: this.params
        },
        success: function (data) {
            console.log("Agent data: ", data);
            this.refreshPage(data, update);
        }.bind(this),
        error: function(){
            this.handleFail("server unreachable!");
        }.bind(this)
    });


////    console.log("Get request", this.params);
//    $.get("http://" + this.server + ":" + this.port + "/arch", this.params,
//        function(data, status, jqxhr){
////                console.log("Success:Data: ", jqxhr.responseText);
////            console.log("Success:Data Size", jqxhr.responseText.length);
//            alert_p.removeClass();
//            alert_p.addClass("success");
//            alert_p.text("Connection active - Last update: " + new Date());
////            console.log("data", data);
//            this.refreshPage(data, update);
//
//        }.bind(this),
//        "json"
//    ).fail(function(){
//        this.handleFail("server unreachable!");
//    }.bind(this));
};

StatusDashboard.prototype.refreshPage = function(data, update) {

    //if (!data.success
    //    || !data.hasOwnProperty("results")) {
    if (!data) {
        this.handleFail(data);
        return;
    }

    if (update)
        this.updateArchStatus(data);

    this.arch = data;
    this.draw(update);
};

StatusDashboard.prototype.updateArchStatus = function(newArch) {
    var oldArch = this.arch;
    if (newArch.hasOwnProperty("services")) {
        var services = newArch.services;
        var oldServices = oldArch ? oldArch.services : null;
        for (var serviceName in services) {
            if (services.hasOwnProperty(serviceName)) {
                var service = services[serviceName];
                var oldService = oldServices ? oldServices[serviceName] : null;
                var newStatus = false;
                if (!oldService) {
                    newStatus = true;
                } else {
                    service.id = oldService.id;
                }

                if (service.hasOwnProperty("instances")) {
                    for (var instanceName in service.instances) {
                        if (service.instances.hasOwnProperty(instanceName)) {
                            var instance = service.instances[instanceName];
                            var oldInstance = oldService ? oldService.instances[instanceName] : null;
                            if (newStatus || !oldInstance) {
                                instance.new = true;
                                console.log("New Instance: " + instanceName);
//                                setTimeout(this.clearNew.bind(this), 10000, instance);
                            } else {
                                instance.id = oldInstance.id;
                            }
                        }
                    }
                }
            }
        }
    }
};

StatusDashboard.prototype.handleFail = function(data) {
    console.log("handle failed");
    var alert_p = $("p#alerts");
    alert_p.removeClass();
    alert_p.toggleClass("fail");
    alert_p.text("Connection failed: " + (data.results || data) + " - " + new Date());
};

StatusDashboard.prototype.codifyServices = function() {
    if (this.arch.hasOwnProperty("services")) {
        for (var sname in this.arch.services) {
            if (this.arch.services.hasOwnProperty(sname)) {
                var s = this.arch.services[sname];
                if (!s.id) {
                    s["id"] = String.fromCharCode(this.code++);
                }
                s["counter"] = 1;
                if (s.hasOwnProperty("instances")) {
                    for (var iname in s.instances){
                        if (s.instances.hasOwnProperty(iname)) {
                            var i = s.instances[iname];
                            i["id"] = s.id + s.counter++;
//                            if (!i.id) {
//                                i["id"] = s.id + s.counter++;
//                            }
                        }
                    }
                }
            }
        }
    }
};

// for testing
//StatusDashboard.prototype.createExpArch = function() {
//    this.arch = {
//        "services": {
//            "client": {
//                name: "client",
//                links: ["lb"],
//                instances: {
//                    "client-0001": {
//                        state: "low",
//                        value: 50,
//                        ip: "192.168.1.1",
//                        host: "10.1.1.9"
//                    }
//                }
//            },
//            "lb": {
//                name: "lb",
//                links: ["apache"],
//                instances: {
//                    "lb-0001": {
//                        state: "low",
//                        value: 50,
//                        ip: "192.168.4.1",
//                        host: "10.1.1.9"
//                    }
//                }
//            },
//            "apache": {
//                name: "apache",
//                links: ["mysql"],
//                instances: {
//                    "apache-0001": {
//                        state: "normal",
//                        value: 20,
//                        ip: "192.168.2.1",
//                        host: "10.1.1.9"
//                    }
//                }
//            },
//            "mysql": {
//                name: "mysql",
//                links: [],
//                instances: {
//                    "mysql-0001": {
//                        state: "low",
//                        value: 80,
//                        ip: "192.168.3.1",
//                        host: "10.1.1.11"
//                    }
//                }
//            }
//        },
//        "agent": "10.1.1.8",
//        "hosts": {
//            "10.1.1.9": {
//                value: 50
//            }
//        }
//    };
//};

StatusDashboard.prototype.getFirstInstance = function(a, serviceName) {
    if (a.services) {
        var service = a.services[serviceName];
        var instances = service.instances;
        for (var instanceName in instances) {
            if (instances.hasOwnProperty(instanceName)) {
                return instanceName;
            }
        }
    }
};

StatusDashboard.prototype.getInstances = function(a, serviceName) {
    var instanceList = [];
    if (a.services) {
        var service = a.services[serviceName];
        var instances = service.instances;
        for (var instanceName in instances) {
            if (instances.hasOwnProperty(instanceName)) {
                instanceList.push(instanceName);
            }
        }
    }
    return instanceList;
};

StatusDashboard.prototype.clearNew = function(instance) {
    console.log("clear instance: ", instance);
    if (instance && instance.new) {
        delete instance.new;
    }
    this.draw(true);
};

StatusDashboard.prototype.draw = function(isUpdate) {
//        console.log("drawing");
    this.codifyServices();
    var services = this.arch["services"];
    var nodes = this.g.nodes();
    if (services) {
        for (var serviceName in services) {
            if (services.hasOwnProperty(serviceName)) {
                var service = services[serviceName];
                // TODO: process the service properties
                // TODO: add links between clusters
                this.g.setNode(service.name, {label: service.name + " (" + service.id + ")", clusterLabelPos: 'top', style: 'fill: #ddd'});
                nodes.splice( $.inArray(serviceName, nodes), 1 );
                if (service.links) {
                    for (var j = 0; j < service.links.length; j++) {
                        var peerService = service.links[j];
                        var selfInstances = this.getInstances(this.arch, service.name);
                        var peerInstances = this.getInstances(this.arch, peerService);

                        for (var k1 = 0; k1 < selfInstances.length; k1++) {
                            for (var k2 = 0; k2 < peerInstances.length; k2++) {
                                this.g.setEdge(selfInstances[k1], peerInstances[k2], {
                                    width: 40
                                });
                            }
                        }
//                            g.setEdge(selfInstance, peerInstance, {
//                                width: 40,
//                                ltail: service.name,
//                                lhead: peerService
//                            });
                    }
                }

                var instances = service["instances"];
                if (instances) {
                    for (var instanceName in instances) {
                        if (instances.hasOwnProperty(instanceName)) {
                            var instance = instances[instanceName];
                            var html = "<div>";
                            html += "<span class=status></span>";
                            html += "<span class=lbl>" + instance.id + "</span>";
                            html += "<span class=name>" + instanceName + "</span>";
                            html += "<span class=host>" + (instance.ip || "N/A") + " (@ " + instance.host + ")</span>";
                            html += "<span class=metrics>";
                            if (instance.values) {
                                for (var lbl in instance.values) {
                                    if (instance.values.hasOwnProperty(lbl)) {
                                        var val = instance.values[lbl];
                                        html += "<span class='counter " + lbl.toLowerCase() + "'>" + lbl + ": " + Math.round(val*100)/100 + "</span>";
                                    }
                                }
                            }

                            html += "</span>";
                            html += "</div>";

                            var state = instance.state ? instance.state.toLowerCase() : "";

                            this.g.setNode(instanceName, {
                                labelType: "html",
                                label: html,
                                rx: 5,
                                ry: 5,
                                padding: 0,
                                class: (state || "nostate") + (instance.new ? " warn" : "")
                            });
                            nodes.splice( $.inArray(instanceName, nodes), 1 );
                            // console.log((state || "nostate") + (instance.new ? " warn" : ""));
                            this.g.setParent(instanceName, service.name);
                        }
                    }
                }
            }
        }
    }

    // remove deleted nodes
    for (var i = 0; i < nodes.length; i++)
        this.g.removeNode(nodes[i]);

    this.inner.call(this.render, this.g);

    // Zoom and scale to fit
//    var zoomScale = zoom.scale();
    var graphWidth = this.g.graph().width + 80;
    var graphHeight = this.g.graph().height + 40;
    console.log(this.svg);

    var width = parseInt(this.svg.style("width").replace(/px/, ""));
    var height = parseInt(this.svg.style("height").replace(/px/, ""));
    var zoomScale = Math.min(width / graphWidth, height / graphHeight);
    var translate = [(width / 2) - ((graphWidth * zoomScale) / 2), (height / 2) - ((graphHeight * zoomScale) / 2)];
    this.zoom.translate(translate);
    this.zoom.scale(zoomScale);
    this.zoom.event(isUpdate ? this.svg.transition().duration(500) : d3.select("svg"));
};