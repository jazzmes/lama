/**
* Created by tiago on 6/19/15.
*/

function MonitorDashboard(containerSelector, alertSelector, args) {
    this.container =    $(containerSelector);
    this.container_alerts = $(alertSelector);
    args = args  || {};
    this.maxPoints = args.maxPoints || 100;
    this.server = args.server || "10.1.1.31";
    this.port = args.port || "8400";
    this.charts = {};
    this.updateInterval = args.updateInterval || 10000;
    this.paused = true;
    this.timeout = null;
    this.minMaxTs = null;
}

MonitorDashboard.prototype.metricOrder = [
    "response_time",
    "cpu",
    "disk",
    "interface",
    "virt_cpu_total",
    "memory",
    "disk_octets",
    "if_octets",
    "virt_vcpu",
    "disk_ops",
    "if_packets",
    "if_dropped",
    "if_errors"
];

MonitorDashboard.prototype.handleFail = function(msg) {
    var alert_p = $("p#alerts");
    alert_p.removeClass();
    alert_p.toggleClass("fail");
    alert_p.text("Connection failed: " + (msg || ""));
};

MonitorDashboard.prototype.getMonitorData = function() {
    // get minimum timestamp
//    var minTs = data.length > 0 ? data[data.length - 1][0] : 0;
    var params = {
        min_ts: this.minMaxTs || -1,
        max_points: this.maxPoints
    };

    var alert_p = $("p#alerts");
    alert_p.removeClass();
//    alert_p.removeClass("unknown");
    alert_p.text("Trying to contact server");

    console.log("get request with ts =", params.min_ts);
    var o =  $("#agent-address");
    if (o) {
        var address = o.val();
        if (address) {
            var result = address.split(":");
            if (result.length == 1) {
                this.server = address;
            } else if (result.length == 2) {
                this.server = result[0];
                this.port = result[1];
            }
        }
    }


    $.get("http://" + this.server + ":" + this.port + "/metrics", params,
            function(data, status, jqxhr){
//                console.log("Success:Data: ", jqxhr.responseText);
                console.log("Success:Data Size", jqxhr.responseText.length);
                alert_p.removeClass();
                alert_p.addClass("success");
                alert_p.text("Connection active - Last update: " + new Date());
                this.minMaxTs = -1;
                this.refreshPage(data);
            }.bind(this),
            "json"
    ).fail(function(){
            this.handleFail("server unreachable! Last attempt: " + new Date());
        }.bind(this));
    console.log("started request");
};

MonitorDashboard.prototype.updateMinMaxTs = function(ts) {
//    console.log("minMax = ", this.minMaxTs, ", ts = ", ts);
    this.minMaxTs = ts && (!this.minMaxTs || this.minMaxTs < 0 || ts < this.minMaxTs) ? ts : this.minMaxTs;
//    console.log("minMax = ", this.minMaxTs);
};

MonitorDashboard.prototype.refreshPage = function(data) {
    // for each service:
    console.log("Refresh page: ", data);
    if (!data.success) {
        this.handleFail(data.results);
        return;
    }

    var arch = null;
    var hosts = null;
    if (data.hasOwnProperty("results")) {
        arch = data["results"]["arch"];
        hosts = data["results"]["hosts"];
    }

    var arg = null;
    if (arch) {
        for (var serviceName in arch) {
            if (arch.hasOwnProperty(serviceName)) {
                var service = arch[serviceName];

                // verify if service div exists
                var serviceDiv = this.container.find('div.group-row#' + serviceName);
                if (!serviceDiv.length){
                    // if not create div.service
                    serviceDiv = $('<div/>', {
                        id: serviceName,
                        "class": "group-row"
                    });
                    serviceDiv.append($('<div/>', {
                        "class": "group-row-title"
                    }).text(serviceName));
                    this.container.append(serviceDiv);
                }

                for (var instanceName in service) {
                    if (service.hasOwnProperty(instanceName)) {
                        var instance = service[instanceName];
                        var instanceDiv = $(serviceDiv).find('div.instance#' + instanceName);
                        if (!instanceDiv.length){
                            // if not create div.instance
                            instanceDiv = $('<div/>', {
                                id: instanceName,
                                "class": "instance"
                            });
                            instanceDiv.append($('<div/>', {
                                "class": "instance-title rotate"
                            }).text(instanceName));
                            serviceDiv.append(instanceDiv);

                            this.charts[instanceName] = {};
                        }

                        // check individual charts
                        var metrics = ((((data || {}).results || {}).instances || {})[instanceName] || {});

//                        for (var metric in metrics) {
                        var metric = null;

                        for (var i = 0; i < MonitorDashboard.prototype.metricOrder.length; i++) {
                            metric = MonitorDashboard.prototype.metricOrder[i];
                            if (metrics.hasOwnProperty(metric)) {
                                for (arg in metrics[metric]) {
                                    if (metrics[metric].hasOwnProperty(arg)) {
                                        this.updateChart(instanceDiv, instanceName, metrics[metric], metric, arg);
                                    }
                                }
                            }
                        }
                        for (metric in metrics) {
                            if (metrics.hasOwnProperty(metric) && MonitorDashboard.prototype.metricOrder.indexOf(metric) < 0) {
                                for (arg in metrics[metric]) {
                                    if (metrics[metric].hasOwnProperty(arg)) {
                                        this.updateChart(instanceDiv, instanceName, metrics[metric], metric, arg);
                                    }
                                }
                            }
                        }
                    }
                }
            }
//            break;
        }

    }

    if (hosts) {
        for (var hostIp in hosts) {
            if (hosts.hasOwnProperty(hostIp)) {
                var host = hosts[hostIp];
                var hostId = hostIp.replace(/\./g, "_");
                var hostDiv = this.container.find('div.group-row#' + hostId);
                var hostName = "host-" + hostId;
                if (!hostDiv.length) {
                    // if not create div.service
                    hostDiv = $('<div/>', {
                        id: hostId,
                        "class": "group-row"
                    });
                    hostDiv.append($('<div/>', {
                        "class": "group-row-title"
                    }).text(hostIp));
                    this.container.append(hostDiv);

                    // if not create div.instance
                    var hostMetricDiv = $('<div/>', {
                        id: hostName,
                        "class": "instance"
                    });
                    hostDiv.append(hostMetricDiv);

                    this.charts[hostName] = {};

                }

                // check individual charts
                metrics = ((((data || {}).results || {}).hosts || {})[hostIp] || {});

//                        for (var metric in metrics) {
                metric = null;
                for (i = 0; i < MonitorDashboard.prototype.metricOrder.length; i++) {
                    metric = MonitorDashboard.prototype.metricOrder[i];
                    if (metrics.hasOwnProperty(metric)) {
                        for (arg in metrics[metric]) {
                            if (metrics[metric].hasOwnProperty(arg)) {
                                this.updateChart(hostMetricDiv, hostName, metrics[metric], metric, arg);
                            }
                        }
                    }
                }
                for (metric in metrics) {
                    if (metrics.hasOwnProperty(metric) && MonitorDashboard.prototype.metricOrder.indexOf(metric) < 0) {
                        for (arg in metrics[metric]) {
                            if (metrics[metric].hasOwnProperty(arg)) {
                                this.updateChart(hostMetricDiv, hostName, metrics[metric], metric, arg);
                            }
                        }
                    }
                }
            }
        }
    }

    console.log("Done refreshing page");

    // for each host

//                rt_ts_chart = new RealTimeSeriesChart("#placeholder", getMonitorData);
//                rt_ts_chart.start();
};

MonitorDashboard.prototype.updateChart = function(instanceDiv, instanceName, series, metric, arg) {
//    console.log(instanceName, metric, series);
    // check if chart exists.
//    for (var arg in series) {
//        if (series.hasOwnProperty(arg)){
            var chartId = instanceName + "-" + metric + "-" + arg;
            var chart = this.charts[chartId];
            if (!chart) {
                // create placeholder
                var chartTitleDiv = $('<div/>', {
                    "class": "chart-title"
                }).text(metric + (arg ? " - " + arg : ""));
                chartTitleDiv.append($('<div/>', {
                    id: chartId,
                    "class": "chart"
                }));
                instanceDiv.append(chartTitleDiv);
                chart = new RealTimeSeriesChart("#" + chartId);
                this.charts[chartId] = chart;
            }
            var ts = null;
//            if (metric == "virt_cpu_total" && instanceName == "mysql-0001") {
//                console.log("Values received", series);
////                var values = ((series || {})["null"] || {})["null"];
////                for (var i = 0; i < values.length; i++) {
////                    var pt = values[i];
////                    console.log(pt[0], pt[1]);
////                }
//                ts = chart.addData(series[arg], true);
//            } else {
            ts = chart.addData(series[arg]);
//            }
            this.updateMinMaxTs(ts);
//        }
//    }
};

MonitorDashboard.prototype.update = function() {
    this.getMonitorData();
    if (!this.paused)
        this.timeout = setTimeout(this.update.bind(this), this.updateInterval);
};

MonitorDashboard.prototype.start = function() {
    if (this.paused) {
        this.paused = false;
        this.update();
    }
};

MonitorDashboard.prototype.pause = function() {
    // race conditions?
    if (this.timeout) {
        clearTimeout(this.timeout);
    }
    this.paused = true;
};

//    // Add the Flot version string to the footer
//    $("#footer").prepend("Flot " + $.plot.version + " &ndash; ");




