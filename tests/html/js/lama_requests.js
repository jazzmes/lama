// author: Tiago Carvalho

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function LamaRequests(dispatchers) {
    console.log("Given dispatchers:", dispatchers);
    this.dispatchers = dispatchers;
    console.log("Current dispatchers:", this.dispatchers);
}

LamaRequests.prototype.add_dispatcher = function(dispatcher) {
    if (!dispatcher.ip_address) {
        console.error("Bad dispatcher - no IP", dispatcher);
        return false;
    } else if (!dispatcher.port) {
        console.error("Bad dispatcher - no Port", dispatcher);
        return false;
    }

    for(var i=0; i < this.dispatchers.length; i++) {
        if(dispatcher.ip_address == this.dispatchers[i].ip_address) return false;
    }
    this.dispatchers.push(dispatcher);
    return true;
};

LamaRequests.prototype.ajax = function(url, options) {
    var req_options = {
        url: url,
        headers: { "X-CSRFToken": getCookie("csrftoken") },
        type: 'POST',
        cache: false,
        dataType: "json",
        contentType: "application/json"
    };
//    console.debug(req_options);
    var o = $.extend({}, req_options, options, {
        success: function (data, textStatus, jqXHR) {
            if (data && data.hasOwnProperty("result")){
                if (data.result){
                    return options.success(data, textStatus, jqXHR);
                } else {
                    console.error("LAMA Error: ", data.hasOwnProperty("msg") ? data.msg : data);
                }
            } else {
                console.error("BAD data: ", data, textStatus, jqXHR);
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
        }
    });

//    console.log(o)
    if (o && o.hasOwnProperty("data")) {
//        console.log("stringify");
        o.data = JSON.stringify(o.data);
    }
//    console.log("Request: ", o);
    $.ajax(o);
};

LamaRequests.prototype.dispatcher = function(action, options){
    console.debug("request from dispatcher: ", this.dispatchers);
    if (this.dispatchers.length == 0) {
        console.debug("no dispatchers available");
        this.web("dispatchers", {
           success: function(data){
               console.debug("success!")
               if (data.hasOwnProperty("dispatchers")){
                   if (data.dispatchers.length > 0){
                       this.dispatchers = data.dispatchers;
                   }
               }
               this.dispatcher(action, options);
           }.bind(this)
        });
    } else {
        console.debug("loading data");
        this.agent(this.dispatchers[0].ip_address, this.dispatchers[0].port, action, options);
    }
};

LamaRequests.prototype.provider = function(host, port, action, options){
    this.agent(host, port, action, options);
};

LamaRequests.prototype.web = function(action, options){
    var url = "/monitor/" + action + "/";
    this.ajax(url, options);
};

LamaRequests.prototype.agent = function(host, port, action, options){
    var url = "http://" + host + ":" + port + "/";
    if (options && options.hasOwnProperty("data")){
        options.data["action"] = action;
    } else {
        options.data = {
            action: action
        };
    }

    console.log("Requesting from", url, " with ", options);
    this.ajax(url, options);
};

LamaRequests.prototype.file_upload = function(host, port, action, options){


    var url = "http://" + host + ":" + port + "/";
//    console.log("I am here!");
    if (options && options.hasOwnProperty("data")){
        options.data.append("action", action);
        console.log("appended!");
    } else {
        console.log("no data...");
    }

    var final_options = $.extend({}, {
        url: url,
        type: 'POST',
//        xhr: function () {  // Custom XMLHttpRequest
//            var myXhr = $.ajaxSettings.xhr();
//            if (myXhr.upload) { // Check if upload property exists
//                myXhr.upload.addEventListener('progress', on_progress, false); // For handling the progress of the upload
//            }
//            return myXhr;
//        },
//        success: function(data){
//            console.log(data);
//        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log("error", xhr, ajaxOptions, thrownError);
        },
        cache: false,
        contentType: false,
        processData: false
    }, options);

    console.log(final_options);
    $.ajax(final_options);

};

// make it available
var lamaRequests = new LamaRequests([]);
