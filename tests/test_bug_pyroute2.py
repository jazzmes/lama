import traceback
from pyroute2 import netns
from pyroute2.ipdb import IPDB
from pyroute2.netns import NetNS
import logging

__author__ = 'tiago'

if __name__ == "__main__":
    logging.getLogger().addHandler(logging.StreamHandler())
    logging.getLogger().setLevel(logging.NOTSET)

    # sudo ovs-vsctl add-br br-test
    # sudo ovs-vsctl add-port br-test tap-test -- set Interface tap-test type=internal

    bridge_name = "br-test"
    ns_name = "ns-test"
    tap_name = "tap-test"

    print("connect and release: %s" % ns_name)
    ipdb_ns = IPDB(nl=NetNS(ns_name))
    ipdb_ns.release()

    ip = IPDB()
    try:
        print("loading interface: '%s'" % tap_name)
        with getattr(ip.interfaces, tap_name) as i:
            print("adding interface to netns '%s'" % ns_name)
            i.net_ns_fd = ns_name
    except Exception as e:
        print(traceback.format_exc())
    finally:
        ip.release()

    print("connect and release again: %s" % ns_name)
    ipdb_ns = IPDB(nl=NetNS(ns_name))
    ipdb_ns.release()
    # print("remove netns: %s" % ns_name)
    # netns.remove(ns_name)