import traceback
from pyroute2 import IPDB
from pyroute2 import NetNS


def create_veth_pair(ns_name, if_name, if_ip, peer_name, peer_ip):
    ipdb = IPDB()
    ipdb_ns = None
    try:
        ipdb_ns = IPDB(nl=NetNS(ns_name))
    except Exception as e:
        print("Failed to create, retrieve or connect to namespace!")
        ipdb.release()
        if ipdb_ns:
            ipdb_ns.release()
        raise e

    try:
        if if_name not in ipdb.interfaces:
            print("Create veth interfaces")
            ipdb.create(ifname=if_name, kind="veth", peer=peer_name).commit()

            with getattr(ipdb.interfaces, peer_name) as veth_peer:
                veth_peer.net_ns_fd = ns_name

        else:
            print(" SKIP Creation - Interface exists")

        with getattr(ipdb.interfaces, if_name) as veth_main:
            print("Configure main interface: %s" % if_name)
            if if_ip:
                veth_main.add_ip(if_ip)
            veth_main.up()

        with getattr(ipdb_ns.interfaces, peer_name) as veth_peer:
            print("Configure peer interface: %s" % peer_name)
            if peer_ip:
                veth_peer.add_ip(peer_ip)
            veth_peer.up()

    except Exception as e:
        raise e

    finally:
        ipdb.release()
        ipdb_ns.release()


if __name__ == "__main__":
    ns_name = "test"
    iname1 = "test-0"
    iname2 = "test-1"
    ip1 = "172.168.1.1/24"
    ip2 = "172.168.1.2/24"
    gw = "172.168.1.1"


    create_veth_pair(ns_name, iname1, ip1, iname2, ip2)

    # add default route
    ipdb_ns = IPDB(nl=NetNS(ns_name))
    try:
        print("Add default route")
        ret = ipdb_ns.routes.add({'dst': "default", "gateway": gw})
    except Exception as e:
        print(traceback.format_exc())
    finally:
        ipdb_ns.release()
