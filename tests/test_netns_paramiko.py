import subprocess
import paramiko
import select
from pyroute2.ipdb import IPDB
from pyroute2.netns import setns, NetNS
from paramiko import SSHClient
import multiprocessing as mp


def test_connect():
    client = SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect("192.168.1.1", username="root", password="Cm4rt!")

    stdin, stdout, stderr = client.exec_command("ls -la")

    while not stdout.channel.exit_status_ready():
        # Only print data if there is data to read in the channel
        if stdout.channel.recv_ready():
            print("STDOUT: %s" % stdout.channel.recv(1024).decode("utf-8"))
    # print("STDIN:  %s" % stdin.read())
    # print("STDERR: %s" % stderr)
    client.close()


def test_current_namespace():
    cmd = subprocess.Popen(["ip", "link"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = cmd.communicate()

    print("Out: %s" % out.decode())
    print("Err: %s" % err.decode())


if __name__ == "__main__":
    test_current_namespace()
    setns("gw-cmart-000001")
    test_current_namespace()
