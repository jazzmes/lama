from twisted.internet import reactor, protocol
from datetime import datetime

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'


class Echo(protocol.Protocol):


    def dataReceived(self, data):
        s = str(data, encoding='utf-8')
        print("[%s] data received: %s" % (datetime.now(), s))
        self.transport.write(bytes(s + " back", encoding='utf-8'))

    def connectionLost(self, reason=protocol.connectionDone):
        print("[%s] connection lost" % datetime.now())


def main():
    factory = protocol.ServerFactory()
    factory.protocol = Echo
    reactor.listenTCP(8000, factory)
    reactor.run()

if __name__ == '__main__':
    main()