import traceback
from lama.agent.events import Event
from lama.utils.logging_lama import logger
from lama.utils.snippets import MethodNameConversion

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class StateMachineInactiveException(Exception):
    """
    Exception raised when an action is taken while the state machine is inactive.
    """
    pass


class StateMachineEventException(Exception):
    """
    Base class for event related exceptions.
    """
    pass


class EventNotSupportedByStateException(StateMachineEventException):
    """
    Exception raised when an event is not supported by the current state.
    """
    def __init__(self, event, state):
        self.event = event
        self.state = state

    def __str__(self):
        return "Event '%s' not supported at state '%s" % (self.event, self.state)


class BadEventHandlerSignature(StateMachineEventException):
    """
    Exception raised when the handler for an event is wrongly defined.
    """
    def __init__(self, event, state, method, arg_count, filename, line_number):
        self.event = event
        self.state = state
        self.method = method
        self.arg_count = arg_count
        self.filename = filename
        self.line_number = line_number

    def __str__(self):
        return "Handler for event '%s' is wrongly defined at state '%s' - %s [%s:%s] " \
               "(should take %s arg(s) + **context[optional], takes %s arg(s)). Args: %s" \
               % (self.event.type.name, self.state, self.method.__name__, self.filename,
                  self.line_number, len(self.event.args), self.arg_count, Event.get_args(self.event.type))


class State(object):
    """
    Base class to define a state.
    The user should:
     - add an enter(self, event, prev_state) function for processing when the process enters the state
     - add a function to process each event it wants to support: (states that want to support an event should define
     a method named process_[event name using underscore naming convention]).
    """
    def enter(self, event, prev_state):
        pass

    def process_event(self, event, handler=None, **kwargs):
        """
        Calls the method correspondent to the event.
        Raises EventNotSupportedByStateException if the event handler is not defined.
        Raises BadEventHandlerSignature if the event handler is incorrectly defined.

        When compared to traditional (non State Pattern) FSM implementations,
        exit actions are executed in this function.
        :rtype: type - State class of the state where the machine should go to
        :param event: the event that was just received
        @type event: Event
        """

        # add event id to context
        kwargs['origin_event_id'] = event.event_id

        if not handler:
            handler = self
        try:
            method_name = "process_" + MethodNameConversion.CAMELCASING_RE.sub(r'_\1', event.type.name).lower()
            # logger.detail("Looking for processing method '%s' at state: %s/%r", method_name, self, self)
            method = getattr(handler, method_name)
        except AttributeError:
            raise EventNotSupportedByStateException(event, handler.__class__)

        # argument count minus 'self'
        # noinspection PyBroadException
        if method.__code__.co_argcount - 1 != len(event.args):
            raise BadEventHandlerSignature(event, handler, method, method.__code__.co_argcount - 1,
                                           method.__code__.co_filename, method.__code__.co_firstlineno)

        # noinspection PyBroadException
        try:
            if method.__code__.co_flags & 8:  # 8 : **kwargs
                return method(*event.args, **kwargs)
            else:
                return method(*event.args)
        except:
            logger.error("Error running event handler. Trace: %s", traceback.format_exc())

    def __str__(self):
        return self.__class__.__name__

    def process_event_send_failure(self, event_type, event_args, msg, **context):
        logger.warn("At state '%s', failed to send event: %s - %s - %s", self, event_type, event_args, msg)


class StateMachine(object):
    """
    Class that defines a state machine.
    """

    def __init__(self, initial_state, identifier=None, **kwargs):
        """
        :param initial_state: the state where the state machine should start on
        :param kwargs: the arguments that will be passed to each state when they are initialized
        """
        self.current_state = None
        """
        @type :State
        """
        self.initial_state = initial_state
        self.kwargs = kwargs
        self._identifier = "[FSM: %s] " % identifier if identifier else ""

    def start(self):
        """
        Starts the state machine
        """
        self.change_state(self.initial_state)

    def event(self, event, **kwargs) -> bool:
        """
        Sends a new event to the state machine and updates the state if necessary.
        """
        result = False
        if not self.current_state:
            raise StateMachineInactiveException()

        # logger.debug("%sReceived event: %s (%s)", self._identifier, event, kwargs)
        # noinspection PyBroadException
        try:
            next_state = self.current_state.process_event(event, **kwargs)
            logger.detail("%sProcessed event: %s (%s)", self._identifier, event, kwargs)
            result = True
            if next_state:
                self.change_state(next_state)
        except EventNotSupportedByStateException as e:
            logger.debug("%sEvent '%s' is not supported by state '%s'",
                          self._identifier, event.type, self.current_state)
            result = False
        except BadEventHandlerSignature as e:
            result = True
            # logger.info("%sReceived event processed - FAIL: %s (%s)", self._identifier, event, kwargs)
            logger.error("%sBad signature: %s", self._identifier, e)
        except Exception as e:
            result = True
            # logger.info("%sReceived event processed - FAIL: %s (%s)", self._identifier, event, kwargs)
            logger.error("%sEvent handler for '%s' returned an error. Trace: %s",
                         self._identifier, event.type, traceback.format_exc())

        return result

    def change_state(self, next_state, **kwargs):
        logger.detail("Change state: %s", next_state)
        if isinstance(self.current_state, next_state):
            logger.debug("%sSame state transition (%s). Nothing to do.", self._identifier, self.current_state)
            return

        if self.current_state:
            logger.debug("%sState transition: '%s' -> '%s'", self._identifier, self.current_state, next_state.__name__)
        else:
            logger.debug("%sEntering initial state: '%s'", self._identifier, next_state.__name__)

        args = self.kwargs.copy()
        args.update(kwargs)
        prev_state = self.current_state

        self.current_state = next_state(**args)
        next_state = self.current_state.enter(None, prev_state)
        if next_state:
            self.change_state(next_state)
