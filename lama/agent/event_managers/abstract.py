import traceback

from lama.agent.state_machine import EventNotSupportedByStateException
from lama.utils.logging_lama import logger


__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class AbstractEventManager(object):

    def __init__(self, event_handler=None):
        self.event_handler = event_handler

    def send(self, event, dip, dport):
        """
        Function that sends an event using UDP.

        :rtype : None
        :param event: event to be sent
        :param dip: destination ip
        :param dport: destination port
        """
        raise NotImplementedError

    def send_reliable(self, event, dip, dport, timeout):
        """
        Function that realiably sends an event using TCP.

        :rtype : boolean (True if able to send within the timeout, False otherwise)
        :param event: event to be sent
        :param dip: destination ip
        :param dport: destination port
        :param timeout: time limit to send the event and get confirmation (message could be received but acknowledge could be delayed)
        """
        raise NotImplementedError

    def send_with_response(self, event, dip, dport, timeout, default_response_event):
        """
        Function that realiably sends an event using TCP and provider a response.

        :param event: event to be sent
        :param dip: destination ip
        :param dport: destination port
        :param timeout: time limit to send the event and get confirmation (message could be received but acknowledge could be delayed)
        :param default_response_event: the event to be returned if an event is not received from peer
        """
        raise NotImplementedError

    def schedule(self, time, func, *args):
        raise NotImplementedError

    def start(self):
        raise NotImplementedError

    def _event_handler(self, event, **context):
        # noinspection PyBroadExceptsion
        try:
            self.event_handler(event, **context)
        except EventNotSupportedByStateException:
            logger.warning("Event '%s' is not supported!")
        except Exception as e:
            if not self.event_handler:
                logger.warning("No event handler defined!")
            else:
                logger.error("Bad event handler. Function at %s:%s. Trace: %s",
                             self.event_handler.__code__.co_filename,
                             self.event_handler.__code__.co_firstlineno,
                             traceback.format_exc())

    def set_event_handler(self, handler):
        """
        Set the function to handle received events.
        The handler is a function f(event, respond) where the input arguments are:
        - :param event: received event
        - :param origin: source of the event
        - :param response_handler: the function f(event) to respond to the source with an event
        """
        self.event_handler = handler

    def call_in_thread(self, command):
        raise NotImplementedError()

    def launch_process(self, name, exe, args):
        raise NotImplementedError()

    def run_async(self, func_to_run, cb_when_done):
        raise NotImplementedError()