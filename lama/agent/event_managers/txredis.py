import traceback
from functools import partial

from twisted.application import internet
from twisted.application import service
from twisted.internet import reactor
from twisted.internet.protocol import ClientCreator

import lama.agent.event_managers.txredisapi as redis
from lama.agent.specs.metrics import Metric
from lama.utils.logging_lama import logger

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class RedisPubSubProtocol(redis.SubscriberProtocol):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._channels = {}
        self._patterns = {}

    def connectionMade(self):
        logger.debug("TXREDIS - Connection made")
        if self.factory.connection_callback:
            self.factory.connection_callback(self)

    def messageReceived(self, pattern, channel, message):
        logger.detail("TXREDIS - Message received: %s %s %s", pattern, channel, message)
        full_message = {
            'channel': channel,
            'pattern': pattern,
            'data': message
        }
        for callback in self._channels.get(channel), self._patterns.get(pattern):
            if callback:
                callback(full_message)

    def subscribe_with_callback(self, channel, callback):
        self._channels[channel] = callback
        self.subscribe(channel)

    def psubscribe_with_callbacks(self, **patterns):
        self._patterns.update(patterns)
        self.psubscribe(*patterns.keys())


class RedisFactory(redis.SubscriberFactory):
    maxDelay = 120
    continueTrying = True
    protocol = RedisPubSubProtocol

    def __init__(self, connection_callback=None):
        super().__init__()
        self.connection_callback = connection_callback


class LamaPubSub(object):

    def __init__(self, name, host='localhost', port=6379):
        self.name = name
        self.host = str(host)
        self.port = port
        self.sub_channels = set()
        self._factory = RedisFactory(self._connection_callback)

        logger.debug("TXREDIS - Start connection: %s:%s", self.host, self.port)
        reactor.connectTCP(self.host, self.port, self._factory)

        # ClientCreator(
        #     reactor,
        #     RedisPubSubProtocol
        # ).connectTCP(
        #     self.host,
        #     self.port
        # ).callback = self._connection_callback
        self._pubsub = None
        self._pending_subscriptions = []

    def start(self):
        pass

    def stop(self):
        logger.debug("Not really stopping... for now...")

    def subscribe_by_resource(self, resource_type, callback):
        if self._pubsub:
            # subscribe for respective collectd channel
            resource_metric = Metric.create(resource_type)
            if resource_metric:
                for sub_ch in resource_metric.get_metrics():
                    if sub_ch not in self.sub_channels:
                        logger.debug("MYDEBUG SUBSCRIBE '%s' => CALLBACK %s @ %s", sub_ch, self.base_handler, self.host)
                        # self._pubsub.subscribe(sub_ch, callback)
                        self._pubsub.psubscribe_with_callbacks(
                            **{sub_ch: lambda m: callback(self.host, m)}
                            # **{sub_ch: self.base_handler}
                        )
                        self.sub_channels.add(sub_ch)

            return self.sub_channels
        else:
            logger.warn("NOT ACTIVE By Resource: %s", resource_type)
            self._pending_subscriptions.append(partial(self.subscribe_by_resource, resource_type, callback))
            return

    def subscribe_by_instance(self, app_name, instance_name, callback):
        if self._pubsub:
            key = "libvirt-lama_%s_%s" % (app_name, instance_name)
            logger.debug("MYDEBUG SUBSCRIBE %s:'%s' => CB:%s", self.host, "%s/*" % key, callback)
            self._pubsub.psubscribe_with_callbacks(
                **{"%s/*" % key: lambda m: callback(self.host, m)}
                # **{sub_ch: self.base_handler}
            )
        else:
            logger.warn("NOT ACTIVE By Instance: %s %s", app_name, instance_name)
            self._pending_subscriptions.append(partial(self.subscribe_by_instance, app_name, instance_name, callback))
            return

    def subscribe(self, channel):
        logger.warn("Request to subscribe that we did not follow: %s", channel)

    def base_handler(self, message):
        logger.detail("MESSAGE :) : %s", message)

    def _connection_callback(self, protocol):
        logger.debug("TXREDIS - Redis defer callback: protocol=%s", protocol)
        logger.debug("TXREDIS - Redis pubsub: %s", self._pubsub)
        logger.debug("TXREDIS - Processing pending subscriptions")
        self._pubsub = protocol
        for sub in self._pending_subscriptions.copy():
            try:
                logger.debug("TXREDIS - Sub: %s", sub)
                sub()
            except:
                logger.error("Failed subscription: %s. Tb: %s", sub, traceback.format_exc())


if __name__ == '__main__':
    print("Connect")
    lps = LamaPubSub('test', host='10.1.1.42')
