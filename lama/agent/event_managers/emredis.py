import traceback
from redis.exceptions import ConnectionError

from lama.agent.specs.metrics import Metric
from redis import Redis, StrictRedis
from lama.utils.logging_lama import logger

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class RedisPubSub(object):

    def __init__(self, name, host='localhost', sleep_time=False):
        logger.temp("Connect to redis @ %s", host)
        # logger.temp("Redis version: %s", redis.__version__)
        self.redis = StrictRedis(decode_responses=True, host=str(host))
        self.pubsub = self.redis.pubsub()
        self.name = name
        self.sleep_time = sleep_time
        self.channel = "channel:%s" % self.name

        self.ip = host
        # self.hostname = socket.gethostbyaddr(str(host))
        # logger.detail("IP: %s, Hostname: %s" % (self.ip, self.hostname))
        self.sub_channels = set()
        self.running = False

    def start_blocking(self):
        logger.detail("Starting LamaPubSub (control channel: %s)", self.channel)
        for message in self.pubsub.listen():
            if message["channel"] == self.channel and message["data"] == "stop":
                self.pubsub.close()
                break

    def start(self):
        self.running = True
        self.start_blocking()

        # if self.sleep_time:
        #     self.thread = self.pubsub.run_in_thread(self.sleep_time)
        # else:
        #     self.thread = self.start_push_thread()

    # def raw_message(self, message, handler):
    #     handler(self.host, message)
    #
    # def subscribe(self, metric, handler):
    #     self.pubsub.psubscribe(
    #         **{
    #             metric: lambda m: self.raw_message(m, handler)
    #         }
    #     )

    def stop(self):
        # logger.debug("Publishin 'stop' to channel %s", self.channel)
        # Redis().publish(self.channel, "stop")

        logger.debug("Unsubscribe")
        self.pubsub.unsubscribe()
        self.pubsub.punsubscribe()
        # if self.thread:
        #     print("Stopping listening thread...")
        #     self.thread.stop()
        # self.pubsub.close()


class LamaPubSub(RedisPubSub):

    def subscribe_by_resource(self, resource_type, callback):
        logger.detail("MYDEBUG Resource: %s", resource_type)
        # subscribe for respective collectd channel
        resource_metric = Metric.create(resource_type)
        if resource_metric:
            for sub_ch in resource_metric.get_metrics():
                # sub_ch = "collectd/%s/%s" % (self.hostname, metric)
                # callback = lambda v: resource_metric.update(resource_metric, v)
                # callback = self.convert_to_local

                if sub_ch not in self.sub_channels:
                    logger.detail("MYDEBUG SUBSCRIBE '%s' => CALLBACK %s @ %s", sub_ch, self.base_handler, self.ip)
                    # self.pubsub.subscribe(sub_ch, callback)
                    self.pubsub.psubscribe(
                        **{sub_ch: lambda m: callback(self.ip, m)}
                        # **{sub_ch: self.base_handler}
                    )
                    self.sub_channels.add(sub_ch)

        return self.sub_channels

    def subscribe_by_instance(self, app_name, instance_name, callback):
        key = "libvirt-lama_%s_%s" % (app_name, instance_name)
        logger.detail("MYDEBUG SUBSCRIBE %s:'%s' => CB:%s", self.ip, "%s/*" % key, callback)
        self.pubsub.psubscribe(
            **{"%s/*" % key: lambda m: callback(self.ip, m)}
            # **{sub_ch: self.base_handler}
        )

    # def subscribe_aggregation(self, channel, time=None, ):
    # # TODO: special metrics like aggregation and such
    # #

    def base_handler(self, message):
        logger.detail("MESSAGE :) : %s", message)

    @property
    def subscribed(self):
        return self.pubsub.subscribed

    def publish(self, channel, message):
        try:
            self.redis.publish(channel, message)
        except:
            logger.error("Failed to publish REDIS message @ %s. Traceback: %s", channel, traceback.format_exc())


class LamaKeyStore(object):

    def __init__(self, host='localhost', port=6379):
        self.redis = None
        self._host = host
        try:
            self.redis = StrictRedis(decode_responses=True, host=str(host), port=port)
        except:
            logger.error("Error connecting to redis!")

    def put_dict(self, key, d):
        key = key.lower()
        try:
            self.redis.hmset(key, d)
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)

    def get_dict(self, key):
        key = key.lower()
        try:
            return {str(k): str(v) if v != 'None' else None for k, v in self.redis.hgetall(key).items()}
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)

    def put(self, key, value):
        key = key.lower()
        try:
            self.redis.set(key, value)
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)

    def get(self, key):
        key = key.lower()
        try:
            val = self.redis.get(key)
            return str(val) if val is not None else None
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)

    def delete(self, key):
        key = key.lower()
        try:
            val = self.redis.delete(key)
            return str(val) if val is not None else None
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)

    def keys(self, pattern):
        pattern = pattern.lower()
        logger.debug("Get key with pattern '%s'", pattern)
        try:
            return set(self.redis.keys(pattern=pattern))
        except ConnectionError as e:
            logger.error("Connection Error: %s", e)

    def publish(self, channel, message):
        self.redis.publish(channel, message)

# if __name__ == "__main__":
#     def handle_message(message):
#         print("Received message: %s" % message)
#
#     # lps = RedisPubSub("test")
#     # lps.subscribe("load", handle_message)
#     # lps.start()
#     #
#     # sleep(15)
#     #
#     # lps.stop()
#
#     lps = LamaPubSub(name="test")
#     if lps.subscribe_with_resource(DiskSpec, handle_message):
#         lps.start()
#         sleep(15)
#     lps.stop()
