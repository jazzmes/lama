import getpass
import json
from functools import partial

import os
import pickle
import traceback
import ntpath
from tempfile import NamedTemporaryFile
import errno

from netaddr import IPAddress

from config.settings import PATH_TMP, PATH_LOG
from twisted.protocols.basic import FileSender
from twisted.python.log import addObserver
from twisted.internet import reactor, threads
from twisted.internet.interfaces import IDelayedCall
from twisted.internet.protocol import Protocol, ServerFactory, ClientFactory, DatagramProtocol, ProcessProtocol, \
    connectionDone, ReconnectingClientFactory
from twisted.internet.error import ConnectionDone, ProcessDone, ProcessTerminated, ProcessExitedAlready
from twisted.web.http import Request
from twisted.web.resource import Resource
from twisted.web.server import Site, NOT_DONE_YET

from lama.agent.app.app_arch import AppVirtualSpec
from lama.utils.codecs import LamaJSONEncoder
from lama.agent.events import EventType, Event, EventAttributeNumberError
from lama.agent.event_managers.abstract import AbstractEventManager
from lama.utils.logging_lama import logger
from lama.utils.snippets import get_ip_address, MethodNameConversion


__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


def run_async_static(method_to_run, callback=None, err_callback=None, timeout=0, *args, **kwargs):

    def middle_func():
        try:
            result = method_to_run(*args, **kwargs)
            if callback:
                callback(result)
        except Exception as e:
            # logger.warn("Error on async method: %s", traceback.format_exc())
            if err_callback:
                try:
                    err_callback(e)
                except Exception as e1:
                    logger.error("Error on callback: %s, %s", e, traceback.format_exc())

    d_timeout = reactor.callLater(timeout, middle_func)
    return d_timeout


# def run_in_thread(method, *args, **kwargs):
#     return reactor.callInThread(method, *args, **kwargs)
#
def run_in_thread(method, *args, callback=None, err_callback=None, **kwargs):
    d = threads.deferToThread(method, *args, **kwargs)
    if callback:
        d.addCallback(callback)
    if err_callback:
        d.addErrback(err_callback)
    return d


class FileSpec(object):
    def __init__(self, action, app_name, service_name, instance_name, filename, filesize, **kwargs):
        self.action = action
        self.app_name = app_name
        self.service_name = service_name
        self.instance_name = instance_name
        self.filename = filename
        self.filesize = filesize
        self.extra = kwargs

    def __str__(self):
        return "FileSpec[action=%s, app=%s, service=%s, instance=%s, filename=%s, size=%s, extra=%s]" % (
            self.action, self.app_name, self.service_name, self.instance_name, self.filename, self.filesize, self.extra
        )


class FileSpecConfirmation(object):
    def __init__(self, app_name, service_name, instance_name, filename, filesize):
        self.app_name = app_name
        self.service_name = service_name
        self.instance_name = instance_name
        self.filename = filename
        self.filesize = filesize


class FileStatus(object):

    def __init__(self, file_spec):
        self.file_spec = file_spec
        logger.temp("File spec: %s", file_spec)
        self.data_recv = 0

        # TODO: Does this avoid path injection?
        # self.local_filename = PATH_TMP + ntpath.basename(self.file_spec.filename)
        # logger.debug("Local filename: %s", self.local_filename)
        # self.file = open(self.local_filename, "wb")

        try:
            os.makedirs(PATH_TMP)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise

        self.file = NamedTemporaryFile(mode='wb', prefix='lamatmp_', dir=PATH_TMP, delete=False)
        self.local_filename = self.file.name
        logger.debug("[%s] Local filename: %s", id(self), self.local_filename)

        self.last_perc = 5

    def recv_bytes(self, data):
        try:
            self.file.write(data)
            # write to disk
            self.data_recv += len(data)

            perc = self.data_recv * 100 // self.file_spec.filesize
            # if perc > 98:
                # logger.temp("Perc=%s, Recv=%s, Total:%s", perc, self.data_recv, self.file_spec.filesize)
            if perc % 5 == 0 and perc > self.last_perc:
                logger.temp("[%s] %d%% done: Received %d of %d bytes", id(self), perc, self.data_recv, self.file_spec.filesize)
                self.last_perc = perc
        except:
            logger.error("Some error occurred on data transfer: %s", traceback.format_exc())
        if self.file_complete():
            self.close()
            return True
        return False

    def file_complete(self):
        # TODO: add crc
        # logger.debug("File Complete: %s/%s", self.data_recv, self.file_spec.filesize)
        return self.data_recv == self.file_spec.filesize

    def remove_file(self):
        logger.debug("Removing file: %s", self.local_filename)
        os.remove(self.local_filename)

    def close(self):
        logger.debug("Closing file ...")
        if not self.file.closed:
            logger.debug("Really closing file...")
            self.file.close()


class ObjectProtocol(Protocol, object):
    """
    Class that transmits and receives (picklable) Python objects using twisted framework.
    """
    _buffer = b''
    _file_status = None

    def dataReceived(self, data):
        """
        Implement the Protocol.dataReceived function.
        Once an object is recognized in the stream, call object_received() callback.
        """

        if self._file_status:
            # logger.temp("@File mode, received %d bytes", len(data))
            # write to disk!
            if self._file_status.recv_bytes(data):
                logger.temp("Done receiving file")
                # return

                # generate event
                self._buffer = b''
                try:
                    args = {
                        "app_name": self._file_status.file_spec.app_name,
                        "service_name": self._file_status.file_spec.service_name,
                        "instance_name": self._file_status.file_spec.instance_name,
                        "filename": self._file_status.local_filename,
                        "file": None
                    }

                    args.update(self._file_status.file_spec.extra)
                    # print(self._file_status.file_spec.action, self._file_status.file_spec.args())
                    event, msg = Event.get_event_from_action(self._file_status.file_spec.action,
                                                             **args)
                    if event:
                        logger.temp("Event: %s", event)
                        self.object_received(event,
                                             address=IPAddress(self.transport.getPeer().host),
                                             response_handler=self.send_object)
                    else:
                        logger.warn("No Event: %s", msg)
                except:
                    if not self.object_received:
                        logger.warning("No event handler!")
                    else:
                        logger.error("Bad event handler @ %s:%s", self.object_received.__code__.co_filename,
                                     self.object_received.__code__.co_firstlineno)
                        logger.error("Trace: %s", traceback.format_exc())
                    self.transport.abortConnection()
                    return
                finally:
                    # self._file_status = None
                    # self.close()
                    return
            else:
                return

        logger.temp("@dataReceived: %d (type=%s) %d (type_of_buffer=%s)", len(data), type(data), len(self._buffer), type(self._buffer))

        self._buffer += data
        # noinspection PyBroadException
        try:
            obj = pickle.loads(self._buffer)
            self._buffer = b''

        # according to docs (https://docs.python.org/3.4/library/pickle.html) the loads() function can generate
        # many different kind of exceptions
        # in our case we want to catch every exception that may occur when performing loads has we are looking to
        # get a complete unpicklable object (thus, we add the noinspection tag for the entire statement)
        except (pickle.PicklingError, pickle.UnpicklingError, EOFError):
            logger.warn("Pickling error - waiting for more data.")
            # Trace: %s", traceback.format_exc())
            # self.transport.abortConnection()
            return
        except RuntimeError as e:
            logger.error("Runtime error (recursion? %s): %s", "maximum recursion" in str(e), e)
            self.transport.abortConnection()
            return

        # check if it is a file
        if isinstance(obj, FileSpec):
            logger.debug("Receiving file, changing to file mode.")
            # if it is, change mode to file received
            self._file_status = FileStatus(obj)
            self.send_object(FileSpecConfirmation(obj.app_name, obj.service_name, obj.instance_name, obj.filename, obj.filesize))
            # logger.temp("%s -> %s", type(obj.file))
            return


        # noinspection PyBroadException
        try:
            # logger.temp("@ ObjectProtocol::dataReceived from %s", self.transport.getPeer())
            self.object_received(obj,
                                 address=IPAddress(self.transport.getPeer().host),
                                 response_handler=self.send_object)
        except:  # external function
            if not self.object_received:
                logger.warning("No event handler!")
            else:
                logger.error("Bad event handler @ %s:%s", self.object_received.__code__.co_filename,
                             self.object_received.__code__.co_firstlineno)
                logger.error("Trace: %s", traceback.format_exc())
            self.transport.abortConnection()
            return

    def send_object(self, obj):
        """
        Sends a (picklable) Python object.
        :param obj: the Python object to be sent (must be picklable)
        """
        try:
            if isinstance(obj, AppVirtualSpec):
                logger.temp("Sending AppVirtualSpec: %s (vars: %s)", obj, obj.__dict__)
            data = pickle.dumps(obj)

            try:
                pickle.loads(data)
            except Exception as e:
                logger.error("Error testing depickle: %s", e)
                logger.error("Tb: %s", traceback.format_exc())

            logger.temp("@ Send data to %s [size: %s]", self.transport.getPeer(), len(data))
            self.transport.write(data)
        except pickle.PicklingError:
            logger.error("Non-picklable object: %s. Trace: %s", obj, traceback.format_exc())
        except:
            logger.error("Unknown error. Trace: %s", traceback.format_exc())

    def object_received(self, obj, **context):
        logger.temp("@ ObjectProtocol::object_received")
        logger.warn("NotImplemented!")
        self.close()

    def connectionLost(self, reason=connectionDone):
        # logger.temp("connectionLost: %s", reason)
        if self._file_status:
            self._file_status.close()
            if self._file_status.file_complete():
                logger.temp("Received %s bytes", self._file_status.data_recv)
            else:
                self._file_status.remove_file()
                pass
            self._file_status = None
        if reason.check(ConnectionDone):
            # noinspection PyBroadException
            logger.temp("Connection Done: %r", self)
            try:
                self.on_close()
                logger.temp("Closed!")
            except:
                try:
                    getattr(self, "on_close")
                except AttributeError:
                    # function does not exist
                    # logger.warn("Function on_close does not exist")
                    pass
                else:
                    logger.error("Closing: Bad 'on_close' function defined! Trace: %s", traceback.format_exc())
        else:
            logger.warning("Connection lost. Reason: %s", reason)

    def connectionMade(self):
        logger.temp("connectionMade: %r", self)

        # noinspection PyBroadException
        try:
            self.connection_made()
        except:
            pass

    def close(self):
        logger.debug("Close: %r", self)
        self.transport.loseConnection()


class FileObjectProtocol(ObjectProtocol):

    def __init__(self, filename, filespec):
        self.filespec = filespec
        self.filename = filename
        # self.wait_response = wait_response
        self.file_sent = False
        self.file = open(self.filename, 'rb')
        self.total_sent = 0
        self.next_level = 1

    def connection_made(self):
        logger.debug("Connection made")
        try:
            self.send_object(self.filespec)
            # reactor.callLater(0.001, self.send_file)
        except:
            pass

    def object_received(self, obj, address=None, response_handler=None):
        logger.debug("@FileObjectProtocol::object_received: %s", obj)
        if isinstance(obj, FileSpecConfirmation):
            self.send_file()
        else:
            # logger.debug("Unsupported response received!")
            self.close()

    def cb_transfer_completed(self, last):
        """ """
        logger.debug("Transfer completed (last: %d)" % len(last))
        logger.debug("Not closing on client... waiting for server...")
        # self.close()

    def monitor(self, data):
        self.total_sent += len(data)
        l = 100 * 1024 * 1024
        # logger.temp(self.total_sent, l, self.total_sent // l, self.next_level)
        if self.total_sent // l >= self.next_level:
            logger.temp("[%s] Sent %d of %d", id(self), self.total_sent, self.filespec.filesize)
            self.next_level += 1

        return data

    def send_file(self):
        try:
            logger.debug("File spec sent")
            sender = FileSender()
            sender.CHUNK_SIZE = 2 ** 16
            logger.debug("Start sending file")
            d = sender.beginFileTransfer(self.file, self.transport, self.monitor)
            d.addCallback(self.cb_transfer_completed)
        except:
            logger.error(traceback.format_exc())

    def on_close(self):
        logger.temp("on_close")
        self.file.close()


class FileProtocolFactory(ClientFactory):
    """
    class that sends a tcp packet and receives a response
    """
    def __init__(self, filename, file_spec, cb_finished=None, cb_failed=None):
        self.filename = filename
        self.file_spec = file_spec
        self.proto = None
        self.cb_finished = cb_finished
        self.cb_failed = cb_failed

    def buildProtocol(self, addr):
        logger.temp("[%s] Building protocol", id(self))
        self.proto = FileObjectProtocol(self.filename, self.file_spec)
        return self.proto

    def close(self):
        logger.temp("[%s] Closing connection", id(self))
        if self.proto:
            self.proto.close()
        self.proto = None

    def clientConnectionLost(self, connector, reason):
        if reason.check(ConnectionDone):
            if self.cb_finished:
                self.cb_finished()
        else:
            logger.warn("[%s] Connection lost - Proto: %s, Reason: %s", id(self), self.proto, reason)
            try:
                if self.cb_failed:
                    self.cb_failed()
            except:
                logger.error("Error running callback: %s", traceback.format_exc())
            # else:
        #     self.abort()

    def clientConnectionFailed(self, connector, reason):
        logger.warn("[%s] Connection lost - Proto: %s, Reason: %s", id(self), self.proto, reason)
        # logger.temp("abort")
        # self.abort()

        if self.cb_failed:
            self.cb_failed()

    @classmethod
    def start(cls, filename, action, app, service, instance, dip, dport, init=False, cb_finished=None, cb_failed=None, timeout=5):
        logger.temp("Start TCP client protocol to: %s:%s (o=%s)" % (dip, dport, filename))
        filesize = os.stat(filename).st_size
        base_filename = ntpath.basename(filename)
        fs = FileSpec(action, app, service, instance, base_filename, filesize, init=init)

        factory = cls(filename, fs, cb_finished, cb_failed)
        logger.temp("[%s] Connecting...", id(factory))
        reactor.connectTCP(str(dip), dport, factory, timeout=timeout)
        # return factory


class EventObjectProtocol(ObjectProtocol):

    def __init__(self, event, wait_response=False):
        self.event = event
        self.wait_response = wait_response

    def connection_made(self):
        logger.debug("Connection made: %s", self)
        logger.debug("Send event: %s", self.event)
        try:
            self.send_object(self.event)
        except:
            logger.error("Trace: %s", traceback.format_exc())

        if not self.wait_response:
            self.close()

    def object_received(self, data, **kwargs):
        logger.temp("@ EventObjectProtocol::object_received")
    #     self.close()


class TCPClient(ReconnectingClientFactory):
    """
    class that sends a tcp packet and receives a response
    """

    def __init__(self, event, event_handler=None, default_response_event=None):
        self.initialDelay = self.delay = 1
        self.maxDelay = 10

        self.event = event
        self.event_handler = event_handler
        self.proto = None
        self.default_response_event = default_response_event
        self.response_received = False
        self.remote = None

    def buildProtocol(self, addr, *args, **kwargs):
        logger.debug("args=%s", (addr, args, kwargs))
        self.proto = EventObjectProtocol(self.event, self.event_handler is not None)
        self.proto.object_received = self.object_received
        logger.debug("Build protocol: %s", self.proto)
        return self.proto

    def object_received(self, obj, **context):
        logger.temp("@ TCPClient::object_received")
        try:
            self.event_handler(obj, **context)
        except:
            logger.error("Trace: %s", traceback.format_exc())
        self.response_received = True
        # logger.temp("closing")
        self.close()

    def abort(self):
        logger.warn("Abort: remote=%s!", self.remote)

        if self.proto:
            self.close()
            if self.event_handler and self.default_response_event and not self.response_received:
                self.event_handler(self.default_response_event)

    def close(self):
        logger.debug("Closing connection: %s", self.proto)
        if self.proto:
            self.proto.close()
        self.proto = None

    def clientConnectionLost(self, connector, reason):
        logger.debug("Connection lost - Proto: %s, Reason: %s", self.proto, reason)
        if reason.check(ConnectionDone):
            pass
        else:
            logger.warn("Connection lost - Proto: %s, Reason: %s", self.proto, reason)
            ReconnectingClientFactory.clientConnectionLost(self, connector, reason)
            # self.abort()

    #     logger.debug("proto: %s", self.proto)
    #     logger.debug("Connection lost: %s", reason)

    def clientConnectionFailed(self, connector, reason):
        logger.warn("Connection failed: reason=%s, proto=%s", reason, self.proto)
        ReconnectingClientFactory.clientConnectionFailed(self, connector, reason)
        # self.abort()

    @classmethod
    def start(cls, event, dip, dport, timeout, event_handler=None, default_response_event=None):
        logger.debug("Start TCP client protocol to: %s:%s (T=%s)", dip, dport, timeout)
        factory = TCPClient(event, event_handler, default_response_event)
        factory.remote = "%s:%s" % (dip, dport)
        logger.debug("reactor.running=%s, factory=%s", reactor.running, factory)
        conn = reactor.connectTCP(str(dip), dport, factory, timeout)
        logger.debug("Connection: %s", conn)
        return factory


class TCPServer(ServerFactory):

    def __init__(self, event_handler):
        self.proto = None
        self.event_handler = event_handler

    def buildProtocol(self, addr):
        self.proto = ObjectProtocol()
        self.proto.object_received = self.object_received
        return self.proto

    def object_received(self, event, **context):
        logger.temp("@ TCPServer::object_received")
        return self.event_handler(event, **context)

    @classmethod
    def start(cls, event_handler, port):
        logger.info("Listening for TCP EVENTS @ port %s", port)
        factory = cls(event_handler)
        res = reactor.listenTCP(port, factory)
        logger.temp("Res: %s", res)
        return factory


class WebServer(Resource):
    isLeaf = True
    allowedMethods = ('GET', 'POST', )

    def __init__(self, event_handler):
        super().__init__()
        # self.putChild("event", EventResource(event_handler))
        self.event_handler = event_handler

        # set tmp dir for request
        Request.tmpdir = PATH_TMP

    @staticmethod
    def start(event_handler, port):
        logger.info("Listening for WEB EVENTS @ port %s", port)
        web = Site(WebServer(event_handler))
        reactor.listenTCP(port, web)
        return web

    def render_OPTIONS(self, request):
        # logger.debug("OPTIONS serving request")
        request.setHeader(b'Access-Control-Allow-Origin', '*')
        # request.setHeader(b'Access-Control-Allow-Headers', 'x-prototype-version,x-requested-with,x-csrftoken,x-content-type')
        request.setHeader(b'Access-Control-Allow-Headers', 'Content-Type, X-CSRFToken')
        request.finish()
        return NOT_DONE_YET

    def render_POST(self, request):
        contenttype = str(request.getHeader(b'content-type'))

        # logger.debug("POST serving up the json now")

        # these are the CROSS-ORIGIN RESOURCE SHARING headers required
        # learned from here: http://msoulier.wordpress.com/2010/06/05/cross-origin-requests-in-twisted/
        request.setHeader(b'Access-Control-Allow-Origin', '*')
        request.setHeader(b'Access-Control-Allow-Methods', 'POST')
        request.setHeader(b'Access-Control-Allow-Headers', 'Content-Type, X-CSRFToken')
        request.setHeader(b'Access-Control-Max-Age', 2520)  # 42 hours

        # normal JSON header
        request.setHeader(b'Content-type', 'application/json')

        result = True
        msg = None
        try:
            if contenttype.find('application/json') >= 0:
                args = json.loads(request.content.getvalue().decode("UTF-8"))
            else:
                args = {}
                for k, v in request.args.items():
                    try:
                        args[k] = v[0].decode("UTF-8")
                    except:
                        args[k] = v[0] if isinstance(v, list) else v
                if 'csrfmiddlewaretoken' in request.args:
                    del args['csrfmiddlewaretoken']
            action = args.get("action")
            logger.temp("Request: from=%s, args=%s", request.getClientIP(), args)
        except:
            result = False
            msg = "Failed parsing arguments ('%s')!" % str(list(request.args.keys()))
            logger.info("Trace: %s", traceback.format_exc())
        else:
            # call event handler
            if action:
                del args["action"]
                try:
                    event_name = "Web" + MethodNameConversion.underscore2camelcase(action)
                except:
                    result = False
                    msg = "Failed to convert action to event ('%s')!" % action
                else:
                    try:
                        event_type = EventType[event_name]
                    except:
                        result = False
                        msg = "Event type does not exist ('%s')!" % event_name
                    else:
                        try:
                            event = Event(event_type, **args)
                        except EventAttributeNumberError as e:
                            logger.error("Error creating event (%s, %s): %s", event_type, args, traceback.format_exc())
                            result = False
                            msg = str(e)
                        else:
                            self.event_handler(event, response_handler=partial(self.async_response, request))
                            # NOTE: the event handler is responsible for providing any response to the client.
                            # For instance, if the event is not supported, there will be no default response.

            else:
                result = False
                msg = "No action available!"

        if not result:
            logger.warn("Result is: %s! Responding immediately and finish!", result)
            b_json = json.dumps(
                {"result": result, "msg": msg},
                cls=LamaJSONEncoder,
                sort_keys=True
            ).encode("UTF-8")
            request.write(b_json)
            request.finish()
            return
        # else:
        #     logger.debug("Not done yet...")
        logger.warn("Return not done yet (%s)!", result)
        return NOT_DONE_YET

    def async_response(self, request, result, **data):
        logger.temp("async_response: %s %s", result, data)
        if request:
            # logger.debug("...responding!")
            out = {"result": result}
            out.update(data)
            # logger.debug("Out: %s", out)
            b_json = json.dumps(
                out,
                cls=LamaJSONEncoder,
                sort_keys=True
            )
            # logger.temp("response [%d] %s", len(b_json), b_json)
            b_json = b_json.encode("UTF-8")
            # logger.debug("Returns: %s", b_json)
            logger.temp(request)
            request.write(b_json)

            try:
                request.finish()
            except RuntimeError:
                logger.warn("Request was already finished. Async request: "
                            "too long to respond? client closed connection?")


    # def render_GET(self, request):
    #     return json.dumps({"result": True,  "stuff": "good stuff"})


class UDPObjectProtocol(DatagramProtocol):

    def __init__(self, event_handler=None):
        self.local_address, _ = get_ip_address()
        self.event_handler = event_handler

    def startProtocol(self):
        # logger.debug("start protocol")
        self.transport.setBroadcastAllowed(True)

    # def stopProtocol(self):
    #     logger.debug("stop protocol")

    def send_event(self, event, host, port):
        # logger.debug("send event")
        data = pickle.dumps(event)
        self.transport.write(data, (str(host), port))

    def datagramReceived(self, data, addr):
        if IPAddress(addr[0]) == self.local_address:
            return
        logger.temp("Received from: %s", addr)

        try:
            obj = pickle.loads(data)
        except pickle.PicklingError:
            logger.warning("Bad object received (could be too large for UDP?). From: %s", addr)
        else:
            self.event_handler(obj, address=IPAddress(addr[0]))

    @classmethod
    def start(cls, event_handler, port):
        logger.info("Listening for UDP EVENTS @ port %s", port)
        udp = cls(event_handler)
        reactor.listenUDP(port, udp)
        return udp


class LaunchAppProtocol(ProcessProtocol):
    def __init__(self, app_name, executable, args, event_handler=None):
        self.app_name = app_name
        self.executable = executable
        self.args = args
        self.event_handler = event_handler

    def outReceived(self, data):
        logger.error("@outReceived: %s", data)
        raise NotImplementedError()

    def errReceived(self, data):
        logger.error("@errReceived: %s", data)
        raise NotImplementedError()

    def start(self):
        path = os.path.abspath(os.getcwd())
        reactor.spawnProcess(self, self.executable, self.args, path=path)


class LamaProcessProtocol(ProcessProtocol):
    def __init__(self, callback=None, err_callback=None, timeout=None):
        super().__init__()
        self._callback = callback
        self._err_callback = err_callback
        self._timeout = timeout
        self._timeout_deferred = None
        logger.temp("On init process protocol")

    def killDueToTimeout(self):
        logger.temp("Timeout triggered")
        try:
            self.transport.signalProcess('KILL')
            logger.temp("Process klled")
        except ProcessExitedAlready:
            logger.temp("Process had already exited")

    def connectionMade(self):
        super().connectionMade()
        if self._timeout:
            self._timeout_deferred = reactor.callLater(self._timeout, self.killDueToTimeout)

    def outReceived(self, data):
        logger.temp("On outReceived process protocol: data=%s", data)
        super().outReceived(data)

    def errReceived(self, data):
        logger.temp("On errReceived process protocol: data=%s", data)
        super().errReceived(data)

    # def inConnectionLost(self):
    #     super().inConnectionLost()

    def outConnectionLost(self):
        super().outConnectionLost()
        if self._timeout_deferred:
            self._timeout_deferred.cancel()
    #
    # def errConnectionLost(self):
    #     super().errConnectionLost()

    def processExited(self, reason):
        super().processExited(reason)
        if isinstance(reason.value, ProcessDone):
            logger.temp("Process exited normally")
            if self._callback:
                self._callback()
        elif isinstance(reason.value, ProcessTerminated):
            logger.error("Process exited with error!")
            if self._err_callback:
                self._err_callback()
        else:
            logger.error("Unknown error: %s", reason)

    # def processEnded(self, reason):
    #     super().processEnded(reason)


class EventManager(AbstractEventManager):

    def __init__(self):
        super().__init__()
        self.tcp = None
        self.udp = None
        self.web = None

    def send(self, event, dip, dport):
        """
        Function that sends an event using UDP.

        :rtype : None
        :param event: event to be sent
        :param dip: destination ip
        :param dport: destination port
        """
        # logger.debug("send")
        protocol = UDPObjectProtocol()
        t = reactor.listenUDP(0, protocol)
        protocol.send_event(event, dip, dport)
        t.stopListening()

    def send_reliable(self, event, dip, dport, timeout):
        """
        Function that realiably sends an event using TCP.

        :rtype : boolean (True if able to send within the timeout, False otherwise)
        :param event: event to be sent
        :param dip: destination ip
        :param dport: destination port
        :param timeout: time limit to send the event and get confirmation (message could be received but
         acknowledge could be delayed)
        """
        logger.temp("send_reliable args: %s, %s, %s, %s", event, dip, dport, timeout)
        c = TCPClient.start(event, dip, dport, timeout)

    def send_with_response(self, event, dip, dport, timeout, default_response_event):
        """
        Function that realiably sends an event using TCP and provides a response.

        :param event: event to be sent
        :param dip: destination ip
        :param dport: destination port
        :param timeout: time limit to send the event and get confirmation (message could be received but acknowledge could be delayed)
        :param default_response_event: the event to be returned if an event is not received from peer
        """
        logger.temp("send_with_response args: %s, %s, %s, %s, %s", event, dip, dport, timeout, default_response_event)
        TCPClient.start(event, dip, dport, timeout,
                        event_handler=self._event_handler,
                        default_response_event=default_response_event)
        # self.schedule(timeout, proto.abort)

    def schedule(self, time, func, *args, **kwargs):
        time = time or 0.000001
        tid = reactor.callLater(time, func, *args, **kwargs)
        return tid

    def cancel(self, call: IDelayedCall):
        if call and call.active():
            call.cancel()

    def add_tcp_interface(self, port):
        self.tcp = TCPServer.start(self._event_handler, port)

    def add_udp_interface(self, port):
        self.udp = UDPObjectProtocol.start(self._event_handler, port)

    def add_web_interface(self, port=80):
        self.web = WebServer.start(self._event_handler, port)

    def log(self, d):
        if d["isError"]:
            logger.error(d["message"])
        else:
            logger.debug(d["message"])

    def call_in_thread(self, command):
        reactor.callInThread(command)

    def blocking_call_from_thread(self, f, *a, **kw):
        threads.blockingCallFromThread(reactor, f, *a, **kw)

    def start(self):
        """
        Start listening to events and be ready to send.
        """
        from twisted.python import log
        log.startLogging(open(os.path.join(PATH_LOG, "%s_provider_twisted.log" % getpass.getuser()), 'w'))

        addObserver(self.log_from_twisted)
        reactor.run()

    def log_from_twisted(self, d):
        # only isError and message are guaranteed to exist
        if d["isError"]:
            if "format":
                logger.error(d["format"], d)
            else:
                logger.error(d)
        else:
            logger.debug(d["message"])

    def run_async(self, func_to_run, cb, func_args=(), cb_args=(), err_cb=None, timeout=None):

        def timeout_callback(deferred):
            logger.temp("Timeout triggered")
            deferred.cancel()

        logger.temp("run_async: %s", [func_to_run, cb, cb_args, err_cb])
        err_cb = err_cb or self.default_err_cb
        d = threads.deferToThread(func_to_run, *func_args)
        if cb:
            logger.temp("Add callback: %s args=%s", cb, cb_args)
            d.addCallback(cb, *cb_args)
        if err_cb:
            d.addErrback(err_cb)

        if timeout:
            d_timeout = reactor.callLater(timeout, timeout_callback, d)

    def default_err_cb(self, failure):
        logger.error("Failure: %s, traceback: %s", failure, traceback.format_tb(failure.getTracebackObject()))

    def run_on_process(self, exe, uid=None, gid=None, cwd=None, callback=None, timeout=None):
        command = exe.split()
        logger.info("command: %s, uid=%s, gid=%s", command, uid, gid)
        reactor.spawnProcess(LamaProcessProtocol(callback=callback, timeout=timeout), command[0], args=command, uid=uid, gid=gid, path=cwd)
