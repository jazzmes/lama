import sys
import copy
import traceback

from lama.utils.snippets import UnitRep
from lama.utils.logging_lama import logger

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class ResourceSet(object):
    """
    Represents a set of resources.
    """
    
    def __init__(self, *args):
        self.resources = {}
        for sub in args:
            for k in sub:
                try:
                    name = k if k.endswith("Spec") else k.title() + "Spec"
                    sargs = args[0][k].split(",")
                    for a in sargs:
                        self.add_resource_spec(globals()[name].from_config(a))
#                     self.resources[k] = 
                except:
                    print("Resource not supported: {}".format(k))
                    print("Exception: {}".format(sys.exc_info()[0]))
                    raise

    def to_config(self):
        cfg = {}
        for name, ress in self.resources.items():
            name = name.strip("Spec").lower()
            out = []
            for res in ress:
                out.append(res.to_config())
            cfg[name] = ", ".join(out)
        return cfg

    def add_resource_spec(self, spec):
        # maybe guarantee that spec has the right parent class? 
        # not very python like
        stype = spec.__class__.__name__
        if stype not in self.resources:
            self.resources[stype] = list()
        self.resources[stype].append(spec)

    def replace_resource_spec(self, spec):
        stype = spec.__class__.__name__
        self.resources[stype] = [spec]

    def to_dict(self):
        resdict = dict()
        for k, v in self.resources.items():
            resdict[k] = [r.to_dict() for r in v] #v.to_dict()
        return resdict

    def to_pretty_dict(self):
        resdict = dict()
        for k, v in self.resources.items():
            resdict[globals()[k].pretty_name()] = [r.to_pretty_dict() for r in v] #v.to_dict()
        return resdict

    @property
    def types(self):
        return set(self.resources.keys())

    @classmethod
    def from_dict(cls, d):
        new = cls()
        if d:
            for k, v in d.items():
                for vitem in v:
                    new.add_resource_spec(globals()[k](**vitem))
        return new

#     def res_dict(self):
#         return self.resources

    def includes(self, rset):
        # order dependent...
        logger.detail("looking if local: %s includes %s", self.resources, rset)
        for t, rlist in rset.resources.items():
            # logger.detail("t: %s, rlist: %s", t, rlist)
            if t not in self.resources:
                return False

            tmp = copy.deepcopy(self.resources[t])
            # logger.detail("tmp: %s", tmp)
            for res in rlist:
                # logger.detail("\t * res: %s", res)
                for rtmp in tmp:
                    # logger.detail("\t    + rtmp: %s --> %s", rtmp, res <= rtmp)
                    if res <= rtmp:
                        rtmp -= res
                    else:
                        logger.detail("\t    + FAIL returning FALSE")
                        return False

        logger.detail("\t    + returning TRUE")
        return True

    def subtract(self, rset):
        backup = copy.deepcopy(self.resources)

        try:
            for t, rlist in rset.resources.items():
                if t not in self.resources:
                    raise OverflowError("Impossible to subtract Resource Set :: No resource '%s'" % t)
    
                for res in rlist:
                    for rtmp in self.resources[t]:
                        if res <= rtmp:
                            rtmp -= res
                        else:
                            raise OverflowError("Impossible to subtract Resource Set :: Not enough resource '%s'" % t)
        except Exception as e:
            self.resources = backup
            raise e
        #
        # return backup

    def __add__(self, other):
        backup = copy.deepcopy(self)
        if other:
            backup.join(other)
        return backup

    def add(self, rset):
        backup = copy.deepcopy(self.resources)
        try:
            for t, rlist in rset.resources.items():
                if t not in self.resources:
                    raise OverflowError("Impossible to add Resource Set :: No resource '%s'" % t)

                for res in rlist:
                    for rtmp in self.resources[t]:
                        rtmp += res
        except Exception as e:
            self.resources = backup
            raise e

    def join(self, rset):
        backup = copy.deepcopy(self.resources)
        try:
            for t, rlist in rset.resources.items():
                for res in rlist:
                    if t in self.resources:
                        for rtmp in self.resources[t]:
                            rtmp += res
                    else:
                        self.resources[t] = copy.deepcopy(rlist)

        except Exception as e:
            self.resources = backup
            raise e

    def split(self, *res_types):
        assert all(isinstance(r, str) for r in res_types), "All resource types should be strings"
        in_res = ResourceSet()
        out_res = ResourceSet()
        for rt in self.resources.keys():
            (in_res if rt in res_types else out_res).resources[rt] = self.resources.get(rt)

        return in_res, out_res

    def extend(self, rset):
        backup = copy.deepcopy(self.resources)
        try:
            for t, rlist in rset.resources.items():
                if t not in self.resources:
                    self.resources[t] = rlist

                else:
                    for res in rlist:
                        for rtmp in self.resources[t]:
                            rtmp += res
        except Exception as e:
            self.resources = backup
            raise e

    def get(self, res_type):
        assert issubclass(res_type, ResSpec), "ResourceSet::g   et only accepts subclasses of ResSpec (err = %s)" % res_type
        return self.resources.get(res_type.__name__)

    def __str__(self):
        return "ResourceSet: %s" % self.to_dict()


class ResSpec(object):

    @classmethod
    def from_config(cls, arg, separator=':'):
        args = arg.split(separator)
        args = [UnitRep.str2val(a) for a in args]
        return cls(*args)

    @staticmethod
    def from_key_args(key, args, separator=':'):
        try:
            name = key if key.endswith("Spec") else key.title() + "Spec"
            return globals()[name].from_config(args, separator)
        except:
            print("Resource not supported: {}".format(key))
            print("Exception: %s" % traceback.format_exc())
            raise

    def to_config(self, separator=':'):
        raise NotImplementedError("Implement on child.")

    def to_dict(self):
        return self.__dict__

    def to_pretty_dict(self):
        return self.__dict__
    
    def __le__(self, res):
        raise NotImplementedError()
    
    def __ge__(self, res):
        raise NotImplementedError()

    def __isub__(self, res):
        raise NotImplementedError()

    def __iadd__(self, other):
        raise NotImplementedError()

    def __str__(self):
        return "%r :: %s" % (self.__class__.__name__,self.to_dict())

    @classmethod
    def pretty_name(cls):
        return cls.__name__.strip("Spec")

    def get_channels(self):
        return None


class CpuSpec(ResSpec):

    def __init__(self, n_processors, processor_speed):
        # characteristics of the machine
        # here we are assuming that we have identical processors
        self.n_processors = UnitRep.str2val(n_processors)
        self.processor_speed = UnitRep.str2val(processor_speed)

    def to_config(self, separator=':'):
        return separator.join([str(k) for k in [self.n_processors, self.processor_speed]])

    def __le__(self, res):
        
        if self.n_processors <= res.n_processors and self.processor_speed <= res.processor_speed:
            return True
        return False 

    def __ge__(self, res):
        logger.detail("GE: Comparing self (%s) with res (%s)", (self.n_processors, self.processor_speed), (res.n_processors, res.processor_speed))
        if self.n_processors >= res.n_processors and self.processor_speed >= res.processor_speed:
            logger.detail("GE: return True")
            return True
        logger.detail("GE: return False")
        return False
    
    def __isub__(self, res):
        if self.processor_speed < res.processor_speed:
            raise OverflowError("CPU not fast enough!")
        
        self.n_processors -= res.n_processors

    def __iadd__(self, res):
        # if self.processor_speed != res.processor_speed:
        #     raise OverflowError("Different CPUs!")

        self.n_processors += res.n_processors

    def to_pretty_dict(self):
        return {
            "Number of Processors": self.n_processors,
            "Processor Speed": UnitRep.val2str(self.processor_speed, "Hz")
        }

    @classmethod
    def pretty_name(cls):
        return "CPU"


class DiskSpec(ResSpec):
    """ DiskSpec: <# of disk>,<size of disks>,<io speed>"""

    def __init__(self, disk_size, rd_speed, wr_speed, name=None, device=None, collectd=False):
        self.disk_size = UnitRep.str2val(disk_size)
        self.rd_speed = UnitRep.str2val(rd_speed)
        self.wr_speed = UnitRep.str2val(wr_speed)
        self.name = name
        self.device = device

    def to_config(self, separator=':'):
        return separator.join([str(k) for k in [self.disk_size, self.rd_speed, self.wr_speed]])

    def __le__(self, res):
        if self.disk_size <= res.disk_size and self.rd_speed <= res.rd_speed and self.wr_speed <= res.wr_speed:
            return True
        return False 

    def __ge__(self, res):
        if self.disk_size >= res.disk_size and self.rd_speed >= res.rd_speed and self.wr_speed >= res.wr_speed:
            return True
        return False 
    
    def __isub__(self, res):
        if self.disk_size < res.disk_size:
            raise OverflowError("Insufficient space!")
        if self.rd_speed < res.rd_speed:
            raise OverflowError("Insufficient Read speed!")
        if self.wr_speed < res.wr_speed:
            raise OverflowError("Insufficient Write speed!")
        
        self.disk_size -= res.disk_size
        self.rd_speed -= res.rd_speed
        self.wr_speed -= res.wr_speed

    def __iadd__(self, res):
        # if self.disk_size < res.disk_size:
        #     raise OverflowError("Insufficient space!")
        # if self.rd_speed < res.rd_speed:
        #     raise OverflowError("Insufficient Read speed!")
        # if self.wr_speed < res.wr_speed:
        #     raise OverflowError("Insufficient Write speed!")

        self.disk_size += res.disk_size
        self.rd_speed += res.rd_speed
        self.wr_speed += res.wr_speed

    def to_pretty_dict(self):
        return {
            "Size": UnitRep.val2str(self.disk_size),
            "Read Speed": UnitRep.val2str(self.rd_speed) + "/s",
            "Write Speed": UnitRep.val2str(self.wr_speed) + "/s",
            "Device": self.device
        }


class NetSpec(ResSpec):
    
    def __init__(self, tx_bw, rx_bw):
        self.tx_bw = UnitRep.str2val(tx_bw)
        self.rx_bw = UnitRep.str2val(rx_bw)

    def to_config(self, separator=':'):
        return separator.join([str(k) for k in [self.tx_bw, self.rx_bw]])

    def __le__(self, res):
        if self.tx_bw <= res.tx_bw and self.rx_bw <= res.rx_bw:
            return True
        return False 

    def __ge__(self, res):
        if self.tx_bw >= res.tx_bw and self.rx_bw >= res.rx_bw:
            return True
        return False 
    
    def __isub__(self, res):
        if self.tx_bw < res.tx_bw:
            raise OverflowError("Insufficient TX bandwidth!")
        if self.rx_bw < res.rx_bw:
            raise OverflowError("Insufficient RX bandwidth!")
        
        self.tx_bw -= res.tx_bw
        self.rx_bw -= res.rx_bw

    def __iadd__(self, res):
        # if self.tx_bw < res.tx_bw:
        #     raise OverflowError("Insufficient TX bandwidth!")
        # if self.rx_bw < res.rx_bw:
        #     raise OverflowError("Insufficient RX bandwidth!")

        self.tx_bw += res.tx_bw
        self.rx_bw += res.rx_bw

    def to_pretty_dict(self):
        return {
            "Bw/TX": UnitRep.val2str(self.tx_bw, "b/s"),
            "Bw/RX": UnitRep.val2str(self.rx_bw, "b/s")
        }

    @classmethod
    def pretty_name(cls):
        return "Network"


class RamSpec(ResSpec):

    def __init__(self, size): # speed?
        self.size = UnitRep.str2val(size)

    def to_config(self, separator=':'):
        return str(self.size)

    def __le__(self, res):
        if self.size <= res.size:
            return True
        return False 

    def __ge__(self, res):
        if self.size >= res.size:
            return True
        return False
    
    def __isub__(self, res):
        if self.size < res.size:
            raise OverflowError("Insufficient RAM!")
        
        self.size -= res.size

    def __iadd__(self, res):
        # if self.size < res.size:
        #     raise OverflowError("Insufficient RAM!")

        self.size += res.size

    def to_pretty_dict(self):
        return {
            "Size": UnitRep.val2str(self.size)
        }

    @classmethod
    def pretty_name(cls):
        return "RAM"
