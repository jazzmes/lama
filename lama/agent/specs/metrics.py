import sys
from lama.utils.logging_lama import logger

__author__ = 'tiago'


class Metric(object):

    @staticmethod
    def create(resource_type):
        """
        Creates a metric object that includes a reference to the resource
        :param resource: representation of a resource (typically subclass of specs.Resource)
        :return: the corresponding subclass of Metric
        """
        try:
            return getattr(sys.modules[__name__], "%sMetric" % resource_type)()
        except AttributeError:
            logger.warning("'%sMetric' not defined!", resource_type)
            return None



class DiskSpecMetric(Metric):

    def __init__(self):
        # metric variables
        # df
        self.free = 0
        self.used = 0
        self.reserved = 0

        # disk
        self.time_read = 0
        self.time_write = 0
        self.ops_read = 0
        self.ops_write = 0
        self.octets_read = 0
        self.octets_write = 0
        self.merged_read = 0
        self.merged_write = 0

        # setup channels
        self.channels = {}

        # if self.resource.name:
        #     self.channels.update({
        #         "df-%s/complex_free" % self.resource.name: "free",
        #         "df-%s/complex_used" % self.resource.name: "used",
        #         "df-%s/complex_reserved" % self.resource.name: "reserved"
        #     })
        #
        # if self.resource.device:
        #     self.channels.update({
        #         "disk-%s/time_read" % self.resource.device: "time_read",
        #         "disk-%s/time_write" % self.resource.device: "time_write",
        #         "disk-%s/ops_read" % self.resource.device: "ops_read",
        #         "disk-%s/ops_write" % self.resource.device: "ops_write",
        #         "disk-%s/octets_read" % self.resource.device: "octets_read",
        #         "disk-%s/octets_write" % self.resource.device: "octets_write",
        #         "disk-%s/merged_read" % self.resource.device: "merged_read",
        #         "disk-%s/merged_write" % self.resource.device: "merged_write"
        #     })

    def update(self, channel, value):
        if channel in self.channels:
            setattr(self, self.channels[channel], value)

    def get_metrics(self):
        return [
            "df-*",
            "disk-*"
        ]


class NetSpecMetric(Metric):

    def get_metrics(self):
        return [
            "interface-*",
        ]


class CpuSpecMetric(Metric):

    def get_metrics(self):
        return [
            "cpu-*",
            "load-*"
        ]


class RamSpecMetric(Metric):

    def get_metrics(self):
        return [
            "memory",
            "vmem",
            "numa-"
        ]