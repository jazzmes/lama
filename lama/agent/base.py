from collections import namedtuple
import traceback
from timeit import default_timer as timer

from netaddr import IPAddress

from lama.agent.events import Event, EventType
from lama.agent.event_managers.emredis import LamaKeyStore
from lama.utils.logging_lama import logger, LamaLogger

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"

Repeat = namedtuple("Repeat", "number wait")


class AgentAddress(object):
    def __init__(self, ip="127.0.0.1", port=None):
        self.ip_address = IPAddress(ip)
        self.port = port


class LamaAgent(object):
    def __init__(self):
        self.event_manager = None
        """
        :type event_manager: AbstractEventManager
        """
        self.handlers = []

    def set_event_manager_platform(self, event_manager, initial_function=None, event_handler=None,
                                   tcp_port=None, udp_port=None, web_port=None):
        """
        Define the event manager for the agent.
        The event manager should listen for events, provide methods to exchange events among agents
         and call the event handler when events are received.
        """
        self.event_manager = event_manager
        if initial_function:
            self.event_manager.schedule(0, initial_function)
        if web_port:
            self.event_manager.add_web_interface(port=web_port)
        if tcp_port:
            self.event_manager.add_tcp_interface(port=tcp_port)
        if udp_port:
            self.event_manager.add_udp_interface(port=udp_port)
        if event_handler:
            self.event_manager.set_event_handler(event_handler)
        else:
            self.event_manager.set_event_handler(self.event_handler_multiple)

    def start(self):
        """
        Start agent. Blocking function.
        """
        self.event_manager.start()

    def event_handler_multiple(self, event, propagate=False, **kwargs):
        handled = 0
        start = timer()
        logger.temp("Event received [event=%s, id=%s]", event.type.name, event.event_id)
        for h in self.handlers:
            # logger.detail("handler: %s", h)
            result = h(event, **kwargs)
            if result:
                elapsed = timer() - start
                if elapsed > 1:
                    logger.error("Event [time=%s, event=%s, id=%s]", timer() - start, event.type.name, event.event_id)
                elif elapsed > 0.1:
                    logger.warn("Event [time=%s, event=%s, id=%s]", timer() - start, event.type.name, event.event_id)
                else:
                    logger.temp("Event [time=%s, event=%s, id=%s]", timer() - start, event.type.name, event.event_id)
                handled += 1
                if not propagate:
                    return handled

        return handled

    def add_handler(self, event_handler):
        """
        Add an event handler to the list of event handlers.
        :param event_handler: function that handler events (handlers to be used in multiple processing should return True if they processed the event)
        """
        if event_handler not in self.handlers:
            self.handlers.append(event_handler)
            return True

        return False

    @staticmethod
    def initialize_log_file(name):
        """
        Sets the filename of the global logger for the current run. Filename is derived from the application name.
        """
        if logger:
            log_filename = name
            LamaLogger.set_filename(logger, log_filename)

    def send_event_with_response(self, address: AgentAddress, event: Event, timeout: int = 5):
        logger.debug("Send event (WR) to '%s': %s", address.ip_address, event)
        if self.event_manager:
            self.event_manager.send_with_response(
                event,
                address.ip_address,
                address.port,
                timeout=timeout,
                default_response_event=Event(EventType.EventSendFailure, event.type, event.args, "User Timeout"))
        else:
            logger.warning("Event manager was not initialized!")

    def send_event_reliably(self, address: AgentAddress, event: Event, timeout=10):
        logger.info("Send event (R) to '%s': %s", address.ip_address, event)
        if self.event_manager:
            self.event_manager.send_reliable(
                event,
                address.ip_address,
                address.port,
                timeout=timeout)
        else:
            logger.warning("Event manager was not initialized!")

    def send_datagram(self, address: AgentAddress, event: Event):
        logger.info("Send event (D) to '%s':%s - %s", address.ip_address, address.port, event)
        if self.event_manager:
            self.event_manager.send(
                event,
                address.ip_address,
                address.port)
        else:
            logger.warning("Event manager was not initialized!")

    def recurring(self,
                  method,
                  args,
                  short_repeat, long_repeat,
                  on_true=None,
                  on_true_args=(),
                  on_false=None,
                  on_false_args=()):
        """
        Function that runs a given method (M) repeatedly. The repetition is defined using a short and
        a long repeat intervals. First, the function repeats the execution the number of times and with the interval
        defined in short_repeat (SR). The function will repeat this procedure according with the definitions of the
        long_repeat (LR), i.e. M - SR - M - SR - ... - M - LR - M - SR - M - SR - ... - M - LR - ...

        :param method: the method to be executed repeatedly (should return to terminate the recurring function)
        :param args: tuple containing the given method's required argument
        :param short_repeat: a Repeat object defining the interval between runs and number of repeats
        :param long_repeat: a Repeat object defining the interval between short repeat cycles and number of repeats
        :param on_true: the function to run once the given method returns true
        :param on_true_args:
        """
        f = RecurringFunction(self.event_manager.schedule, method, args, short_repeat, long_repeat, on_true,
                              on_true_args=on_true_args, on_false=on_false, on_false_args=on_false_args)
        f.run()
        return f

    def run_async(self, *args, **kwargs):
        self.event_manager.run_async(*args, **kwargs)


class KeyStore(object):
    def __init__(self, host='localhost', port=6379):
        self.key_store = None
        self.host = host
        self.port = port

    def save(self, key, value):
        # logger.debug("K/S Saving: %s => %s", key, value)
        logger.debug("K/S Saving: %s", key)
        try:
            if not self.key_store:
                self.key_store = LamaKeyStore(host=self.host, port=self.port)
        except AttributeError:
            self.key_store = LamaKeyStore(host=self.host, port=self.port)
        self.key_store.put(key, value)

    def save_dict(self, key, d):
        logger.debug("K/S Saving: %s", key)
        # logger.debug("K/S Saving: %s => %s", key, d)
        try:
            if not self.key_store:
                self.key_store = LamaKeyStore(host=self.host)
        except AttributeError:
            self.key_store = LamaKeyStore(host=self.host)
        self.key_store.put_dict(key, d)

    def get(self, key):
        return self.key_store.get(key) if self.key_store else None

    def get_dict(self, key):
        return self.key_store.get_dict(key) if self.key_store else None

    def delete(self, key):
        return self.key_store.delete(key)


class RecurringFunction(object):
    def __init__(self, schedule_function, method, args, short_repeat, long_repeat, on_true=None, on_true_args=(),
                 on_false=None, on_false_args=()):
        """
        Class that runs a given method (M) repeatedly. The repetition is defined using a short and
        a long repeat intervals. First, the function repeats the execution the number of times and with the interval
        defined in short_repeat (SR). The function will repeat this procedure according with the definitions of the
        long_repeat (LR), i.e. M - SR - M - SR - ... - M - LR - M - SR - M - SR - ... - M - LR - ...

        :param method: the method to be executed repeatedly (should return to terminate the recurring function)
        :param args: tuple containing the given method's required argument
        :param short_repeat: a Repeat object defining the interval between runs and number of repeats
        :param long_repeat: a Repeat object defining the interval between short repeat cycles and number of repeats
        :param on_true: the function to run once the given method returns true
        """
        self._tid = None
        self._count_short_repeats = 0
        self._count_long_repeats = 0
        self._schedule_function = schedule_function
        self._method = method
        self._args = args
        self._short_repeat = short_repeat
        self._long_repeat = long_repeat
        self._on_true = on_true
        self._on_true_args = on_true_args
        self._on_false = on_false
        self._on_false_args = on_false_args

    def run(self):
        if self._method(*self._args):
            # noinspection PyBroadException
            try:
                self._on_true(*self._on_true_args)
            except Exception as e:
                if self._on_true:
                    logger.error(traceback.format_exc())
            logger.debug("Returned True: terminating recurring function (%s).", self._method)
            return

        if self._count_long_repeats < self._long_repeat.number:
            self._count_short_repeats += 1
            if self._count_short_repeats == self._short_repeat.number:
                self._count_short_repeats = 0
                self._count_long_repeats += 1
                # logger.debug("Long wait: %s sec.", self._long_repeat.wait)
                self._tid = self._schedule_function(self._long_repeat.wait, self.run)

            else:
                # logger.debug("Short wait: %s sec.",self._short_repeat.wait)
                self._tid = self._schedule_function(self._short_repeat.wait, self.run)

        else:
            logger.debug("Exhausted repeats: terminating recurring function (%s).", self._method)
            try:
                self._on_false(*self._on_false_args)
            except Exception as e:
                if self._on_false:
                    logger.error(traceback.format_exc())

    def stop(self):
        if self._tid and self._tid.active():
            self._tid.cancel()
