import random
import string
import traceback
from datetime import datetime, timedelta

from dateutil.parser import parse
from netaddr import IPNetwork, IPSet, IPAddress

import config.exp_constraints as constraints
from config.settings import DISPATCHER_AG_WEB_PORT, DISPATCHER_AG_PORT, INITIAL_CANDIDATES, PROVIDER_AG_PORT, \
    PROVIDER_AG_WEB_PORT, TIMEOUT_APP_DEPLOY_DROP, TIMEOUT_APP_DEPLOY_REPLY
from lama.agent.app.app_arch import AppLogicalSpec
from lama.agent.base import LamaAgent, AgentAddress, EventType
from lama.agent.events import Event
from lama.ops.experiments import launch_experiment_server
from lama.utils.lama_collections import PersistentSyncDict
from lama.agent.event_managers import twisted
from lama.generators.app_generators import Simple3TierAppGenerator
from lama.agent.state_machine import State, StateMachine
from lama.utils.enum import Enum
from lama.utils.logging_lama import logger
from lama.utils.snippets import escape_name


__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMA Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class ActiveState(State):

    def __init__(self, agent):
        """
        @type agent: DispatcherAgent
        """
        self.agent = agent

    def process_provider_auth_request_event(self, agent_id, provider_address, prefix, resources, **context):
        # logger.debug([agent_id, provider_address, prefix, resources, context])
        address = context.get("address")
        response_handler = context.get("response_handler")

        if not address:
            logger.warning("No provider address specified in the context!")
            return

        if not address == provider_address:
            logger.warning("Context address (%s) does not match event address (%s)", address, provider_address)
            return

        provider = self.agent.catalog.register_provider(agent_id, address, prefix, resources)
        if provider:
            event = Event(EventType.ProviderAuthResponseEvent, True, provider.id)
        else:
            event = Event(EventType.ProviderAuthResponseEvent, False, None)

        if response_handler:
            response_handler(event)
        else:
            self.agent.send_event_reliably(address, event)

    def process_peer_candidate_probe_event(self, agent_id, conditions, **context):
        agent_ip = context["address"]
        logger.debug("agent_ip: %s", agent_ip)
        if self.agent.catalog.is_registered(agent_id, agent_ip):
            ips = self.agent.catalog.get_random_providers(INITIAL_CANDIDATES, IPSet([agent_ip]))

            if len(ips):
                event = Event(EventType.PeerCandidateResponseEvent, agent_id, ips)
                # logger.info("Respond with %d ips." % len(ips))
                self.agent.send_datagram(AgentAddress(agent_ip, PROVIDER_AG_PORT), event)
            else:
                logger.warning("No candidates available to send to provider!")

        else:
            logger.warning("Received message from non-registered agent: %s @ %s",
                           agent_id, agent_ip)

    # def process_app_deploy_response_event(self, ):

    # noinspection PyCallingNonCallable
    def process_web_provider_agent_clusters(self, **context):
        handler = context.get("response_handler")
        try:
            providers = [
                {
                     "name": p.address,
                     "subnet": p.subnet.cidr,
                     "port": PROVIDER_AG_WEB_PORT
                } for p in self.agent.catalog.get_providers()
            ]
        except:
            # logger.error("Trace: %s", traceback.format_exc())
            if handler:
                handler(False, msg=traceback.format_exc())
        else:
            # logger.warning("Returning info: %s", info)
            if handler:
                handler(True, providers=providers)

    # noinspection PyCallingNonCallable
    def process_web_generate_app_spec(self, **context):
        handler = context.get("response_handler")
        try:
            g = Simple3TierAppGenerator()
            app = g.generate()
        except:
            # logger.error("Trace: %s", traceback.format_exc())
            if handler:
                handler(False, msg=traceback.format_exc())
        else:
            # logger.warning("Returning info: %s", info)
            if handler:
                handler(True, app=app)

    # noinspection PyCallingNonCallable
    def process_web_app_create(self, app_spec, **context):
        handler = context.get("response_handler")

        logger.debug("Processing web app create")
        try:
            app_spec = AppLogicalSpec.from_dict(app_spec)
            logger.temp("App requested - Spec: %s", app_spec.detail_str())
            # convert app_name to internal name...
            # get the app parsed name and replace on apps name
            app_spec.name = escape_name(app_spec.name)
            status, app_entry = self.agent.catalog.register_app(app_spec)

            # deploy application agent at providers
            if status is AppStatus.Accepted:
                logger.info("New application request '%s' (status: %s)",
                            app_entry.name, status)
                self.agent.catalog.clients[app_entry.name] = handler
                self.agent.deploy_app(app_entry)

            elif status is AppStatus.Duplicated:
                logger.info(
                    "Duplicate application request '%s' (status: %s)",
                    app_entry.name, status)
                logger.debug("App: %s", app_entry)
                if handler:
                    handler(True, status=AppStatus.Duplicated,
                            provider=app_entry.provider,
                            port=app_entry.port)
        except:
            if handler:
                handler(False, msg=traceback.format_exc())

    def process_web_app_list(self, **context):
        handler = context.get("response_handler")
        try:
            apps = [
                {
                    "name": a.name,
                    "provider": a.provider,
                    "provider_port": PROVIDER_AG_WEB_PORT,
                    "port": a.port,
                    "status": a.status,
                    "last_updated": a.updated
                } for a in self.agent.catalog.get_apps()
            ]
        except:
            # logger.error("Trace: %s", traceback.format_exc())
            if handler:
                handler(False, msg=traceback.format_exc())
        else:
            # logger.warning("Returning info: %s", info)
            if handler:
                handler(True, apps=apps)

    def process_web_app_info(self, app_name, **context):
        handler = context.get("response_handler")
        try:
            app = self.agent.catalog.get_app(app_name)
            appinfo = None
            if app:
                appinfo = {
                    "name": app.name,
                    "provider": app.provider,
                    "provider_port": PROVIDER_AG_WEB_PORT,
                    "port": app.port,
                    "status": app.status,
                    "last_updated": app.updated
                }
        except:
            # logger.error("Trace: %s", traceback.format_exc())
            if handler:
                handler(False, msg=traceback.format_exc())
        else:
            # logger.warning("Returning info: %s", info)
            if handler:
                handler(True, app=appinfo)

    def process_app_deploy_accept_event(self, app_name, port, **context):
        logger.detail([app_name, context])

        app = self.agent.catalog.get_app(app_name)
        if app.status == AppStatus.Deployed:
            raise NotImplementedError()

        elif app.status == AppStatus.Accepted:
            self.agent.catalog.cancel_redeploy_timer(app_name)
            app = self.agent.catalog.get_app(app_name)
            self.agent.catalog.apps.update(app_name, status=AppStatus.Deployed, port=port)
            self.agent.catalog.response_to_client(app_name, True, {"app_name": app_name, "provider": app.provider, "port": port})

        else:
            logger.detail("App: %s", app)
            raise NotImplementedError()

    def process_app_deploy_refuse_event(self, app_name, **context):
        self.agent.redeploy_app(app_name)

    def process_event_send_failure(self, event_type, args, msg, **context):
        logger.detail([event_type, args, msg, context])
        if event_type == EventType.AppDeployEvent:
            app_name = args[0]
            self.agent.redeploy_app(app_name)

    def process_app_deploy_timeout_event(self, app_name, **context):
        self.agent.redeploy_app(app_name)


class DispatcherAgent(LamaAgent):

    def __init__(self):
        super().__init__()

        self.initialize_log_file("dispatcher")
        logger.info("Dispatcher agent started!")

        self.address = AgentAddress(port=DISPATCHER_AG_PORT)

        # Provider catalog (providers that registered with the dispatcher)
        self.catalog = Catalog()

        self.fsm = StateMachine(ActiveState, identifier=self.__class__.__name__, agent=self)

        # configure communication platform and schedule first event (start of the FSM)
        # dispatcher will include web_server events
        self.set_event_manager_platform(
            twisted.EventManager(),
            initial_function=self.fsm.start,
            event_handler=self.event_handler,
            tcp_port=self.address.port,
            web_port=DISPATCHER_AG_WEB_PORT,
            udp_port=self.address.port
        )

        self.response_handler = None

        # launch experiments server
        launch_experiment_server(dispatcher_ip=self.address.ip_address)
        # self.event_manager.run_async(launch_experiment_server, None, func_args=(self.address.ip_address, DISPATCHER_EXPERIMENT_SERVER_PORT))
        # launch_experiment_server(self.address.ip_address, port=DISPATCHER_EXPERIMENT_SERVER_PORT)

    def event_handler(self, event, **context):
        logger.info("Received event: %s (context: %s)", event, context)
        self.fsm.event(event, **context)

    def redeploy_app(self, app_name):
        if app_name in self.catalog.apps:
            self.catalog.cancel_redeploy_timer(app_name)
            app_entry = self.catalog.get_app(app_name)
            self.deploy_app(app_entry)

    def deploy_app(self, app_entry):
        if app_entry.name in self.catalog.redeploy:
            del self.catalog.redeploy[app_entry.name]

        if app_entry.status is not AppStatus.Accepted:
            logger.warning("SHOULD NOT BE HERE:: Sending ACCEPTED response to client!")
            raise NotImplementedError()

        if datetime.now() - app_entry.updated > timedelta(seconds=TIMEOUT_APP_DEPLOY_DROP):
            logger.info("Unable to deploy the application... Sending REFUSED response to client!")
            self.catalog.delete_app(app_entry.name)
            self.catalog.response_to_client(app_entry.name, False,
                                            {"status": AppStatus.Refused.name, "reason": "Deploy timeout"})
            return

        # ls = self.catalog.get_random_providers(1)
        ls = self.catalog.get_provider_for_app()

        if not ls:
            logger.info("Refused application. No providers available...")
            self.catalog.delete_app(app_entry.name)
            self.catalog.response_to_client(app_entry.name, False,
                                            {"status": AppStatus.Refused.name, "reason": "No provider available"})
            return

        # server = ls[0]
        server = None
        forced_allocations = getattr(constraints, 'FORCE_ALLOCATIONS', {})
        try:
            server = IPAddress(forced_allocations.get(app_entry.name, {}).get('provider'))
        except:
            pass

        try:
            server = server or IPAddress(forced_allocations.get('DEFAULT', {}).get('provider'))
        except:
            pass

        logger.temp("output of forced server: %s", server)
        server = server or random.choice(list(ls))

        self.catalog.apps.update(app_entry.name, provider=server)
        logger.info("Sending application request '%s' to %s:%s", app_entry.name, server, PROVIDER_AG_PORT)

        e = Event(EventType.AppDeployEvent, app_entry.name, app_entry.nid, app_entry.spec)
        self.send_event_with_response(
            AgentAddress(ip=server, port=PROVIDER_AG_PORT),
            e
        )

        event_timeout = Event(EventType.AppDeployTimeoutEvent, app_entry.name)
        self.catalog.redeploy[app_entry.name] = self.event_manager.schedule(TIMEOUT_APP_DEPLOY_REPLY,
                                                                            self.fsm.event,
                                                                            event_timeout)


class ProviderStatus(Enum):
    accepted = 1
    deployed = 2


class ProviderEntry(object):

    def __init__(self, pid, address, subnet, resources, last_seen=None):
        self.id = pid
        self.address = address
        self.subnet = subnet
        self.resources = resources
        self.last_seen = parse(last_seen) if last_seen and isinstance(last_seen, str) else datetime.now()

    def __str__(self):
        return "PA[%s]" % self.address


class AppStatus(Enum):
    Idle = 0
    Refused = 1
    Duplicated = 2
    Accepted = 3
    Deployed = 4


class AppEntry(object):

    __app_id_counter = 10

    def __init__(self, name, spec, status=AppStatus.Idle):
        self.name = name
        self.spec = spec
        self.status = status
        self.updated = datetime.now()
        self.provider = None
        self.port = None
        self.nid = self.get_nid()

    def get_nid(self):
        nid = AppEntry.__app_id_counter
        AppEntry.__app_id_counter += 1
        return nid

    def __str__(self):
        return "App '%s' (#: %s) is %s @ %s:%s (%s)" % (self.name,
                                                        self.nid,
                                                        self.status.name,
                                                        self.provider,
                                                        self.port,
                                                        self.updated)


class ClusterEntry(object):

    def __init__(self, net):
        self.net = net

    def add(self, ip):
        self.net.add(ip)


class Catalog(object):
    """
    The Dispatcher keeps a list of:
     - provider agents
     - applications
     - clusters
    The Catalog keeps the list of providers, but does not keep track of their resource utilization.
    The main goal of the Catalog is NOT to drive the whole LAMA operation, but serve as a point of access to some
    information.
    The catalog is also used for maintaining information about clusters and returning cluster info to providers.
    Eventually we can consider some MAS approach to network discovery based on, for instance, SDNs
    """

    def __init__(self):
        self.providers = PersistentSyncDict("providers", ProviderEntry)
        self.providers_ip_set = IPSet([p.address for p in self.providers.values()])
        self.apps = PersistentSyncDict("apps", AppEntry)
        self.clusters = PersistentSyncDict("clusters", ClusterEntry)

        self.redeploy = {}
        self.clients = {}
        self._provider_used_for_apps = IPSet()

    @staticmethod
    def generate_id(size=10, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
        return datetime.now().strftime("%Y%m%d%H%M%S") + ''.join(random.choice(chars) for _ in range(size))

    def register_provider(self, provider_id, provider_address, prefix, resources):
        logger.info("Registering provider: id = %s, address = %s", provider_id, provider_address)

        net = IPNetwork("%s/%s" % (provider_address, prefix))
        if net not in self.clusters:
            self.clusters.add(str(net), ClusterEntry(set()))

        if provider_id is None:
            provider_id = self.generate_id()

        provider = self.providers.get(provider_id)
        if provider:
            if provider.address != provider_address:
                self.providers_ip_set.remove(provider.address)
                self.providers_ip_set.add(provider_address)
            self.providers.update(provider_id, address=provider_address, subnet=net, resources=resources)
        else:
            provider = ProviderEntry(provider_id, provider_address, net, resources)
            self.providers.add(provider_id, provider)
            self.providers_ip_set.add(provider.address)

        self.clusters.get(str(net)).add(provider_id)

        logger.info("List of providers: %s", ", ".join([str(v) for v in self.providers.values()]))
        return provider

    def get_providers(self):
        return self.providers.values()

    def get_apps(self):
        return self.apps.values()

    def register_app(self, app_spec):
        if app_spec is None:
            return AppStatus.Refused, None

        if app_spec.name in self.apps:
            app_entry = self.apps.get(app_spec.name)
            """:type: AppEntry"""
            if app_entry and app_entry.spec and app_entry.provider:
                logger.detail("Duplicate app :: State - %s", app_entry)
                logger.detail("Before: %s", app_entry)
                return AppStatus.Duplicated, app_entry
            else:
                logger.detail("App will be replaced")
                self.apps.delete(app_entry.name)

        app_entry = AppEntry(app_spec.name, app_spec, status=AppStatus.Accepted)
        self.apps.add(app_entry.name, app_entry)
        return AppStatus.Accepted, app_entry

    # def add_or_update_app(self, name, spec, address, port):
    #     app_state = self.get_app(name)
    #     ":type: AppState"
    #     if not app_state:
    #         app_state = AppState(app=None, status=Catalog.Status.DEPLOYED)
    #         self.applications[name] = app_state
    #     app_state.status = Catalog.Status.DEPLOYED
    #     app_state.update_server(address, port)
    #
    def get_app(self, name):
        return self.apps.get(name)

    def delete_app(self, app_name):
        if app_name is None:
            return False

        # logger.debug("@unregister_app :: application name = {}".format(app_name))

        if app_name not in self.apps:
            return False

        self.cancel_redeploy_timer(app_name)
        self.apps.delete(app_name)

        return True

    def is_registered(self, provider_id, addr):
        provider = self.providers.get(provider_id)
        if not provider or provider.address != addr:
            return False
        return True
    #
    # def delete(self, provider):
    #     net = IPNetwork("%s/%s" % (provider.address, Catalog.IP_PREFIX))
    #     self.clusters[net].remove(provider.id)
    #     self.providers.delete(provider.id)
    #
    # def get_ipset(self):
    #     return self.providers.ipset
    #
    # def get_clusters(self):
    #     return self.clusters
    #
    # def close(self):
    #     self.applications.close()

    def get_provider_for_app(self):
        result = self.get_random_providers(1, self._provider_used_for_apps)
        if not result:
            self._provider_used_for_apps = IPSet()
            result = self.get_random_providers(1, self._provider_used_for_apps)

        self._provider_used_for_apps.update(result)
        return result

    def get_random_providers(self, number_of_nodes, ipset_to_exclude=None):
        ip_set = self.providers_ip_set - ipset_to_exclude if ipset_to_exclude else self.providers_ip_set
        if number_of_nodes < len(set(ip_set)):
            return IPSet(random.sample(set(ip_set), number_of_nodes))
        else:
            return ip_set

    def cancel_redeploy_timer(self, app_name):
        if app_name in self.redeploy:
            call = self.redeploy[app_name]
            if call.active():
                call.cancel()
            del self.redeploy[app_name]

    def response_to_client(self, app_name, result, response):
        if app_name in self.clients:
            # noinspection PyCallingNonCallable
            self.clients[app_name](result, **response)
            del self.clients[app_name]
        else:
            logger.warning("No function to respond to client!")

