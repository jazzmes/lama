import json
import datetime
from collections import OrderedDict
from json import JSONEncoder, JSONDecoder
import traceback
import sys

from netaddr import IPAddress
from networkx.classes.digraph import DiGraph

from lama.agent.specs.resource import ResourceSet
from lama.utils.enum import Enum
from lama.utils.snippets import escape_name
from lama.utils.logging_lama import logger


__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class AppArchEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, IPAddress):
            return {'__type__': 'IPAddress',
                    'args': [str(obj)]}
        elif isinstance(obj, datetime.datetime):
            args = ('year', 'month', 'day', 'hour', 'minute',
                    'second', 'microsecond')
            return {'__type__': 'datetime.datetime',
                    'args': [getattr(obj, a) for a in args]}
        elif isinstance(obj, datetime.date):
            args = ('year', 'month', 'day')
            return {'__type__': 'datetime.date',
                    'args': [getattr(obj, a) for a in args]}
        elif isinstance(obj, datetime.time):
            args = ('hour', 'minute', 'second', 'microsecond')
            return {'__type__': 'datetime.time',
                    'args': [getattr(obj, a) for a in args]}
        elif isinstance(obj, datetime.timedelta):
            args = ('days', 'seconds', 'microseconds')
            return {'__type__': 'datetime.timedelta',
                    'args': [getattr(obj, a) for a in args]}
        # elif isinstance(obj, decimal.Decimal):
        #     return {'__type__': 'decimal.Decimal',
        #             'args': [str(obj), ]}
        else:
            return super().default(obj)


class AppArchDecoder(JSONDecoder):
    def __init__(self, *args,
                 **kwargs):
        # TODO] Maybe shift to a virtual environment?
        # TODO] If not virtualenv, then clean kwargs better
        # logger.debug("args=%s, kwargs=%s", args, kwargs)
        super().__init__(*args, object_hook=self.object_hook,
                         **kwargs)

    def object_hook(self, d):
        if '__type__' not in d:
            return d
        o = sys.modules[__name__]
        for e in d['__type__'].split('.'):
            o = getattr(o, e)
        args, kwargs = d.get('args', ()), d.get('kwargs', {})
        return o(*args, **kwargs)


class Node(object):
    """
    Base node class.
    """
    type = None

    def __init__(self, arch, node_id):
        """
        :param arch: application architecture (so that nodes can execute related action on the app arch)
        :param node_id: the id of the node (varies based on node subclass)
        @type arch: AppArch
        """
        self.id = node_id
        self.arch = arch

    def to_dict(self):
        """
        Should return a JSON-serializable dictionary version of the node
        """
        raise NotImplementedError()

    def remove(self):
        """
        Should erase all the dependent nodes.
        """
        raise NotImplementedError()


class LogicalNodeSubtype(Enum):
    service = 1
    image = 2
    dhcp = 3
    client = 4
    lb = 5


class InstanceState(Enum):
    Active = 1
    Hot = 2
    Warm = 3
    Cold = 4


class LogicalNode(Node):
    """
    Node class representing a logical node in the application architecture.
    The Logical architecture level represents the desired logical services and QoS
    """
    type = "logical"

    def __init__(self, arch, name, spec, subtype=LogicalNodeSubtype.service, **extra_properties):
        super(LogicalNode, self).__init__(arch, name)
        logger.detail("Adding logical node: name=%s, subtype=%s", name, subtype)
        self.name = name
        self.resources = spec
        if isinstance(subtype, LogicalNodeSubtype):
            self.subtype = subtype
        elif isinstance(subtype, int):
            self.subtype = LogicalNodeSubtype(subtype)
        elif isinstance(subtype, str):
            self.subtype = LogicalNodeSubtype[subtype]
        else:
            raise AttributeError("subtype should be either (LogicalNodeSubtype, int, str)")
        logger.detail("Added logical node: name=%s, subtype=%s", self.name, self.subtype)
        self.vm_id = 1
        self.extra = extra_properties

    def __getitem__(self, index):
        return self.extra.get(index)

    def to_dict(self):
        d = self.__dict__.copy()
        d["resources"] = self.resources.to_dict()
        d["subtype"] = self.subtype.value
        del d["arch"]
        return d

    def add_instance(self, **kwargs):
        """
        Add a new virtual instance of this services.
        """
        vm_name = "%s-%04d" % (self.name, self.vm_id)
        self.vm_id += 1
        logger.temp("Adding instance: %s", vm_name)
        return self.arch.add_instance(self.name, vm_name, self.resources, **kwargs)

    def get_all_instances(self):
        return self.get_instances(states=())

    def get_instances(self, states=(InstanceState.Active,)):
        """
        Returns a list of the ACTIVE instances (Virtual Nodes) of this service.
        :param states: Filter by state. If None return all instances.
        :return: list of instances
        """
        return [self.arch.graph.node[succ_id]["node"]
                for succ_id in self.arch.graph.successors(self.id)
                if isinstance(self.arch.graph.node[succ_id]["node"], VirtualNode) and
                (not states or self.arch.graph.node[succ_id]["node"].state in states)]

    def get_image_service(self, init=False):
        """
        :rtype: LogicalNode
        """
        nodes = [self.arch.graph.node[pred_id]["node"]
                 for pred_id in self.arch.graph.predecessors(self.id)
                 if (isinstance(self.arch.graph.node[pred_id]["node"], LogicalNode)
                     and self.arch.graph.node[pred_id]["node"].subtype == LogicalNodeSubtype.image)]
        # if not nodes:
        #     logger.detail(nodes)
        #     logger.detail([self.arch.graph.node[pred_id]["node"] for pred_id in self.arch.graph.predecessors(self.id)])
        #     logger.detail(self.arch.graph.predecessors(self.id))
        #     logger.detail(self.arch.graph.nodes())
        #     logger.detail(self.arch.graph.edges())

        # if more than one image, choose the one that is an init service
        if len(nodes) > 1:
            nodes = [n for n in nodes if n.extra.get("init")]

        if not len(nodes) or len(nodes) > 1:
            return None
        return nodes[0]

    def set_resources(self, spec):
        self.resources = spec

    def is_allocated(self):
        """
        Verify if the service is allocated, i.e. its requirements and performance goals are met.
        Without any performance or resilience requirements, this function returns if the service is deployed in at
        least one server.
        """
        # logger.debug("ALLOCATED::name = %s", self.name)
        vms = self.get_instances()
        # logger.debug("ALLOCATED::vms = %s", vms)
        # logger.debug("ALLOCATED::len = %s", len(vms))
        if not len(vms):
            return False
        for vm in vms:
            # logger.debug("ALLOCATED::vm, host = %s, %s", vm, vm.get_host())
            if not vm.get_host():
                return False
        return True

    def get_successors(self):
        nodes = [self.arch.graph.node[succ_id]["node"]
                 for succ_id in self.arch.graph.successors(self.id)
                 if (isinstance(self.arch.graph.node[succ_id]["node"], LogicalNode)
                     and self.arch.graph.node[succ_id]["node"].subtype == LogicalNodeSubtype.service)]
        return nodes

    def get_all_successors(self):
        nodes = [self.arch.graph.node[succ_id]["node"]
                 for succ_id in self.arch.graph.successors(self.id)
                 if isinstance(self.arch.graph.node[succ_id]["node"], LogicalNode)]
        return nodes

    def get_predecessors(self):
        nodes = [self.arch.graph.node[pred_id]["node"]
                 for pred_id in self.arch.graph.predecessors(self.id)
                 if (isinstance(self.arch.graph.node[pred_id]["node"], LogicalNode)
                     and self.arch.graph.node[pred_id]["node"].subtype == LogicalNodeSubtype.service)]
        return nodes

    def get_all_predecessors(self):
        nodes = [self.arch.graph.node[pred_id]["node"]
                 for pred_id in self.arch.graph.predecessors(self.id)
                 if isinstance(self.arch.graph.node[pred_id]["node"], LogicalNode)]
        return nodes

    def get_predecessor(self, subtype):
        try:
            return [n for n in self.get_all_predecessors() if n.subtype == subtype][0]
        except:
            return None

    def set_extra(self, key, value):
        self.extra[key] = value

    def is_scalable(self):
        return "scaling" in self.extra

    def remove(self):
        for instance in self.get_instances():
            instance.remove()

        self.arch.logical.nodes.remove(self.id)

    def __str__(self):
        return "Service[ %s ]: %s, %s" % (self.name, self.subtype, self.extra)


class VirtualNode(Node):
    """
    Node class representing a virtual node in the application architecture.
    The Virtual architecture level represents the virtual instances that compose each service.
    """
    type = "virtual"

    def __init__(self, arch, service_name, vm_name, spec, state=None, mac=None, ip=None,
                 virtual_mac=None, virtual_ip=None, accept_ip=None, **kwargs):
        super(VirtualNode, self).__init__(arch, vm_name)
        self.service_name = service_name
        self.name = vm_name
        self.resources = spec
        self.allocation = None
        self.mac = mac
        self.ip = ip
        self.virtual_mac = virtual_mac
        self.virtual_ip = virtual_ip
        self.accept_ip = accept_ip
        self.state = state or InstanceState.Active
        self.active_ts = None
        self.launched_ts = None
        self.ready_ts = None

    def to_dict(self):
        d = self.__dict__.copy()
        if d["resources"]:
            d["resources"] = self.resources.to_dict()
        if d["allocation"]:
            d["allocation"] = self.allocation.to_dict()
        del d["arch"]
        d["state"] = self.state.value
        logger.detail("return: %s", d)
        return d

    def set_addresses(self, mac=None, ip=None):
        if mac:
            self.mac = mac
        if ip:
            self.ip = ip

    def update_allocation(self, allocation):
        assert not allocation or isinstance(allocation, ResourceSet), "TypeError: %s" % type(allocation)
        self.allocation = allocation

    # def get_service(self) -> LogicalNode:
    #     edges = [(s, t) for s, t, d in self.arch.graph.in_edges(self.name, data=True) if "type" in d and d["type"] == "instance"]
    #     if len(edges) > 1:
    #         raise AppArch.DataError("Too many services (%s) for VM '%s'" % (edges, self.name))
    #     try:
    #         # confirm if it is a service
    #         if not edges[0][0] in self.arch.logical.nodes:
    #             raise AppArch.DataError("'%s' is not a service", edges[0][0])
    #         service = self.arch.get_service(edges[0][0])
    #         return service
    #     except Exception as e:
    #         logger.error("Error getting service: %s", edges)
    #         logger.error("Traceback: %s", traceback.format_exc())
    #     return None

    def get_service(self):
        services = [
            self.arch.graph.node[pred_id]["node"]
             for pred_id in self.arch.graph.predecessors(self.id)
             if isinstance(self.arch.graph.node[pred_id]["node"], LogicalNode)
        ]

        if len(services) > 1:
            raise AppArch.DataError("Too many services (%s) for VM '%s'".format(services, self.name))
        if not services:
            raise AppArch.DataError("Service not found for VM '{}'".format(self.name))

        return services[0]

    def get_host(self):
        hosts = [self.arch.graph.node[succ_id]["node"]
                 for succ_id in self.arch.graph.successors(self.id)
                 if isinstance(self.arch.graph.node[succ_id]["node"], PhysicalNode)]

        # if not hosts:
        #     logger.detail(hosts)
        #     logger.detail([self.arch.graph.node[succ_id]["node"] for succ_id in self.arch.graph.successors(self.id)])
        #     logger.detail(self.arch.graph.successors(self.id))
        #     logger.detail(self.arch.graph.nodes())
        #     logger.detail(self.arch.graph.edges())

        if len(hosts) == 0:
            return None

        if len(hosts) > 1:
            raise AppArch.DataError("Too many hosts (%s) for VM '%s'", hosts, self.name)

        return hosts[0]

    def add_host(self, ip_address):
        self.arch.physical.add_node(ip_address)
        self.arch.add_allocation(self.id, ip_address)

    def delete_host(self):
        host = self.get_host()
        if host:
            self.arch.graph.remove_edge(self.id, host.id)
            if len(host.get_vms()) == 0:
                self.arch.remove_node(host.id)

    def get_parent_lb_service(self):
        return self.get_service().get_predecessor(LogicalNodeSubtype.lb)

    def remove(self):
        logger.temp("Removing instance: %s", self.name)
        self.delete_host()
        self.arch.virtual.nodes.remove(self.id)

    def __str__(self):
        return "VM[ %s:%s%s - %s]" % (self.service_name, self.name, ":%s" % self.ip if self.ip else "", self.state.name)

    def status_str(self):
        return "launched=%s, ready=%s, active=%s" % (self.launched_ts, self.ready_ts, self.active_ts)


class PhysicalNode(Node):
    """
    Node class representing a physical node in the application architecture.
    The Physical architecture level represents the physical components (servers) in the DC.
    """
    type = "physical"

    def __init__(self, arch, ip_address, **kwargs):
        assert isinstance(ip_address, str) or isinstance(ip_address, IPAddress)
        super(PhysicalNode, self).__init__(arch, ip_address)
        self.ip_address = ip_address

    def to_dict(self):
        d = self.__dict__.copy()
        d["ip_address"] = str(self.ip_address)
        del d["arch"]
        return d

    def get_vms(self):
        return [self.arch.graph.node[succ_id]["node"]
                for succ_id in self.arch.graph.predecessors(self.id)
                if isinstance(self.arch.graph.node[succ_id]["node"], VirtualNode)]

    def remove(self):
        self.arch.physical.nodes.remove(self.id)

    def __str__(self):
        return "Host[ %s ]" % self.ip_address


class Edge(object):
    def __init__(self, edge_type, spec=ResourceSet(), **kwargs):
        if not isinstance(spec, ResourceSet):
            raise Exception(":)")
        self.resources = spec
        self.type = edge_type

    def to_dict(self):
        d = self.__dict__.copy()
        d["resources"] = self.resources.to_dict()
        return d


# class LogicalEdge(Edge):
#
#     def __init__(self, spec):
#         self.resources = spec
#
#
# class VirtualEdge(Edge):
#
#     def __init__(self, spec):
#         self.resources = spec
#         self.allocation = None


# class PhysicalEdge(Edge):
#     pass
#
#
# class InstanceEdge(Edge):
#     pass
#
#
# class AllocationEdge(Edge):
#     pass


class SubArch(object):
    def __init__(self, main, node_type):
        self.main = main
        self.node_type = node_type

        self.nodes = set()
        self.edges = set()

    def add_node(self, *args, **kwargs):
        logger.detail("initializing node '%s' with: %s", self.node_type, [self.main, args, kwargs])
        n = self.node_type(self.main, *args, **kwargs)
        self.nodes.add(n.id)
        self.main.add_node(n)
        return n

    def add_edge(self, id1, id2, *args):
        assert id1 != id2, "Same node!"
        if id1 not in self.nodes:
            raise KeyError("node '%s' not found" % id1)
        if id2 not in self.nodes:
            raise KeyError("node '%s' not found" % id2)

        if (id1, id2) not in self.edges:
            e = Edge(self.node_type.type, *args)
            self.edges.add((id1, id2))
            self.main.add_edge(id1, id2, e)


def observable(notify_before=False):
    def wrap(func):
        def notify(self, *args, **kwargs):
            res = None
            if not notify_before:
                logger.debug("Calling func before: %s", func.__name__)
                res = func(self, *args, **kwargs)
            for observer in self.observers.keys():
                logger.debug("Calling views")
                try:
                    observer.update(func.__name__, *args, **kwargs)
                except:
                    logger.error("Error on observer: %s", traceback.format_exc())
            if notify_before:
                logger.debug("Calling func after: %s", func.__name__)
                res = func(self, *args, **kwargs)

            logger.debug("Returning res:%s", res)
            return res
        return notify
    return wrap


def observable_old(notify_before=False):

    def warp(func):
        def notify(self, *args, **kwargs):
            res = None
            if not notify_before:
                logger.debug("Calling func: %s", func.__name__)
                res = func(self, *args, **kwargs)
                logger.debug("Calling views")
            for observer in self.observers.keys():
                try:
                    observer.update(func.__name__, *args, **kwargs)
                except:
                    logger.error("Error on observer: %s", traceback.format_exc())
            if notify_before:
                logger.debug("Calling func: %s", func.__name__)
                res = func(self, *args, **kwargs)
                logger.debug("Calling views")

            logger.debug("Returning res")
            return res

        return notify


class AppArch(object):
    """
    Data structure that olds information about the application architecure. It is composed of three levels: logical
    (the high level application services, and QoS requirements), virtual (the virtual machine level which determines
    the number of instances for each service and the need hardware characteristics on the physical level) and physical
    (the actual physical machines and links in use by the application).

        Sample use:
        arch = AppArch(app_name)

        arch.logical.add_node(service_name, spec)
        arch.virtual.add_node(service_name, vm_name, alloc_spec)
        arch.physical.add_node(vm_name, ip_address)

        arch.logical.add_edge(service_name1, service_name2) > flow
        arch.virtual.add_edge(vm_name1, vm_name2) > connection
        arch.physical.add_edge(ip_address1, ip_address2) > link

        arch.add_instance(service_name, vm_name)
        arch.add_placement(vm_name, ip_address)
    """

    class DataError(Exception):
        pass

    class BadNodeFormat(Exception):
        pass

    def __init__(self, name):
        """
        Create empty App architecture.
        """
        # graph properties
        self.name = name
        self.pname = escape_name(name)
        self.extra = {}

        # base graph
        self.graph = DiGraph()

        self.logical = SubArch(self, LogicalNode)
        self.virtual = SubArch(self, VirtualNode)
        self.physical = SubArch(self, PhysicalNode)

        self.observers = OrderedDict()

    @classmethod
    def from_filename(cls, filename):
        with open(filename, "r") as fp:
            json_str = fp.read()
            logger.temp(json_str)
        return cls.from_dict(json.loads(json_str))

    # @classmethod
    # def from_logic_spec(cls, logic_spec):
    #     logger.debug("Logic spec [%s]: %s", logic_spec, logic_spec.name)
    #     a = cls(logic_spec.name)
    #
    #     # copy data from given logic spec
    #     for service in logic_spec.get_services():
    #         a.logical.add_node(service.name, service.resources)
    #
    #     for flow in logic_spec.get_flows():
    #         a.logical.add_edge(flow[0], flow[1], flow[2]['flow'])
    #
    #     return a

    @classmethod
    def from_dict(cls, d):
        """@type d: dict"""

        aa = cls(str(d["graph"]["name"]))

        aa.extra = d.get("extra")

        for node in d["nodes"]:
            # logger.debug(node)
            if aa.logical and node["type"] == "logical":
                if "subtype" in node:
                    if isinstance(node["subtype"], str):
                        n = aa.logical.add_node(node["name"], ResourceSet.from_dict(node["resources"]),
                                                subtype=LogicalNodeSubtype[node["subtype"]], **node.get("extra", {}))
                    else:
                        n = aa.logical.add_node(node["name"], ResourceSet.from_dict(node["resources"]),
                                                subtype=LogicalNodeSubtype(node["subtype"]), **node.get("extra", {}))
                else:
                    n = aa.logical.add_node(node["name"], ResourceSet.from_dict(node["resources"]), **node.get("extra", {}))
                n.vm_id = node["vm_id"]
            elif aa.virtual and node["type"] == "virtual":
                vnode = aa.virtual.add_node(node["service_name"], node["name"], ResourceSet.from_dict(node["resources"]),
                                            state=InstanceState(node["state"]))
                vnode.set_addresses(mac=node.get("mac"), ip=node.get("ip"))
            elif aa.physical and node["type"] == "physical":
                aa.physical.add_node(node["ip_address"])

        for edge in d["edges"]:
            if aa.logical and aa.virtual and "type" in edge and edge["type"] == "instance":
                aa.add_instance_edges(edge["source"], edge["target"])
            elif aa.physical and aa.virtual and "type" in edge and edge["type"] == "allocation":
                aa.add_allocation(edge["source"], edge["target"])  #, ResourceSet.from_dict("resources"))
            else:
                source = aa.get_node(edge["source"])
                target = aa.get_node(edge["target"])
                if aa.logical and isinstance(source, LogicalNode) \
                        and isinstance(target, LogicalNode):
                    aa.logical.add_edge(edge["source"], edge["target"], ResourceSet.from_dict(edge["resources"]))
                elif aa.virtual and isinstance(source, VirtualNode) \
                        and isinstance(target, VirtualNode):
                    aa.virtual.add_edge(edge["source"], edge["target"], ResourceSet.from_dict(edge["resources"]))
                elif aa.physical and isinstance(source, PhysicalNode) \
                        and isinstance(target, PhysicalNode):
                    aa.physical.add_edge(edge["source"], edge["target"], ResourceSet.from_dict(edge["resources"]))

        return aa

    def add_observer(self, observer):
        self.observers[observer] = 1

    def add_node(self, node):
        self.graph.add_node(node.id, node=node)

    def add_edge(self, id1, id2, edge):
        self.graph.add_edge(id1, id2, edge=edge)

    @observable()
    def add_instance(self, id_logical, id_virtual, spec, **kwargs):
        if id_logical not in self.logical.nodes:
            raise KeyError("logical node '%s' not found", id_logical)
        if id_virtual in self.virtual.nodes:
            raise KeyError("virtual node '%s' already exists", id_virtual)

        vnode = self.virtual.add_node(id_logical, id_virtual, spec, **kwargs)

        self.add_instance_edges(id_logical, id_virtual)
        logger.temp("returning : %s", vnode)
        return vnode

    def add_instance_edges(self, id_logical, id_virtual):
        if id_logical not in self.logical.nodes:
            raise KeyError("logical node '%s' not found", id_logical)
        if id_virtual not in self.virtual.nodes:
            raise KeyError("virtual node '%s' not found", id_virtual)

        self.graph.add_edge(id_logical, id_virtual, type="instance")

        # add edges to all virtual nodes (from the services that depend on it
        # - get downstream services
        service = self.get_service(id_logical)
        for lnode in service.get_successors():
            for vnode in lnode.get_instances():
                self.virtual.add_edge(id_virtual, vnode.id)

        # - get upstream services
        for lnode in service.get_predecessors():
            for vnode in lnode.get_instances():
                self.virtual.add_edge(vnode.id, id_virtual)

        return id_virtual

    @observable()
    def add_allocation(self, id_virtual, id_physical):
        if id_virtual not in self.virtual.nodes:
            raise KeyError("virtual node '%s' not found", id_virtual)
        if id_physical not in self.physical.nodes:
            # raise KeyError("physical node '%s' not found", id_physical)
            self.physical.add_node(id_physical)

        self.graph.add_edge(id_virtual, id_physical, type="allocation")

    @observable()
    def add_image(self, id_logical, spec, init):
        if id_logical not in self.logical.nodes:
            raise KeyError("logical node '%s' not found", id_logical)

        name = "img%s_%s" % ('_init' if init else '', id_logical)
        if name not in self.logical.nodes:
            self.logical.add_node(name, spec, LogicalNodeSubtype.image, init=init)
            self.logical.add_edge(name, id_logical)
        else:
            img_node = self.get_service(name)
            img_node.set_resources(spec)

        return name

    def add_dhcp(self):
        id_dhcp = "dhcp_" % self.pname
        if id_dhcp in self.logical.nodes:
            raise KeyError("logical node for DHCP already exists. Only 1 logical node allowed!")

        self.logical.add_node(id_dhcp, {}, LogicalNodeSubtype.dhcp)
        for node_id in self.logical.nodes:
            node = self.get_node(node_id)
            if node.subtype == LogicalNodeSubtype.service:
                self.logical.add_edge(node.id, id_dhcp)

    def to_dict(self):
        # logger.detail("graph: %r", self.graph)
        # logger.detail("nodes: %s", self.graph.nodes(data=True))
        # logger.detail("edges: %s", self.graph.edges(data=True))

        return {
            "graph": {"name": self.name},
            "extra": self.extra,
            "nodes": [dict(list(attrs["node"].to_dict().items()) + list({"type": attrs["node"].type}.items()))
                      for id, attrs in self.graph.nodes(data=True)],
            "edges": [dict(list({"source": src, "target": tgt}.items())
                           + (list(attrs["edge"].to_dict().items()) if "edge" in attrs else list(attrs.items())))
                      for src, tgt, attrs in self.graph.edges(data=True)]
        }

    def get_logical_spec(self):
        return AppLogicalSpec(self.name, self.graph.subgraph(self.logical.nodes))

    def get_virtual_spec(self):
        return AppVirtualSpec(self.name, self.graph.subgraph(self.virtual.nodes))

    def get_physical_spec(self):
        return AppPhysicalSpec(self.name, self.graph.subgraph(self.physical.nodes))

    def get_virtual_unallocated_spec(self):
        """
        Returns mismatches between virtual and physical spec.
        :rtype : AppVirtualSpec
        """

        # all VIRTUAL nodes that do not have hosts
        selected = []
        logger.detail("nodes: %s", self.virtual.nodes)
        for vm_name in self.virtual.nodes:
            logger.detail("vm_name: %s", vm_name)
            vm = self.get_vm(vm_name)
            logger.detail("vm: %s", vm)

            if not vm:
                raise AppArch.DataError("VM does not exist! App arch data is corrupted!")
            if not vm.get_host():
                logger.detail("appended")
                selected.append(vm_name)

        g = self.graph.subgraph(selected)
        logger.detail(g.nodes(data=True))

        return AppVirtualSpec(self.name, self.graph.subgraph(selected))

    # get entities
    def get_node(self, node_id):
        try:
            return self.graph.node[node_id]["node"]
        except KeyError:
            return None

    def get_vm(self, vm_name) -> VirtualNode:
        if vm_name in self.virtual.nodes:
            return self.graph.node[vm_name]["node"]
        return None

    def get_service(self, service_name):
        if service_name in self.logical.nodes:
            return self.graph.node[service_name]["node"]
        return None

    def get_host(self, ip_address):
        if ip_address in self.physical.nodes:
            return self.graph.node[ip_address]["node"]
        return None

    def get_services(self, subtype=None, not_subtype=None):
        if subtype:
            if isinstance(subtype, LogicalNodeSubtype):
                subtypes = {subtype}
            else:
                subtypes = set(subtype)
            return [n for n in self.get_services() if n.subtype in subtypes]
        elif not_subtype:
            if isinstance(not_subtype, LogicalNodeSubtype):
                not_subtypes = {not_subtype}
            else:
                not_subtypes = set(not_subtype)
            return [n for n in self.get_services() if n.subtype not in not_subtypes]
        else:
            return [self.get_service(n) for n in self.logical.nodes]

    def get_client_service(self):
        clients = [n for n in self.get_services() if n.subtype == LogicalNodeSubtype.client]
        assert 0 <= len(clients) <= 1, "Should not have more than one client service: %s" % clients
        return clients[0] if clients else None

    def get_vms(self):
        return [self.get_vm(n) for n in self.virtual.nodes]

    def get_hosts(self):
        return [self.get_host(n) for n in self.physical.nodes]

    def number_of_services(self):
        return len(self.logical.nodes)

    def number_of_vms(self):
        return len(self.virtual.nodes)

    def number_of_hosts(self):
        return len(self.physical.nodes)

    @observable(notify_before=True)
    def remove_node(self, node):
        if isinstance(node, str):
            n = self.get_node(node)
        else:
            n = node

        if not n or not isinstance(n, Node):
            logger.warn("Non-existent node: %s!", node)
            return

        # erase node
        n.remove()
        self.graph.remove_node(n.id)

    # pickling
    def __getstate__(self):
        d = self.__dict__.copy()
        # logger.temp("D: %s", d)
        d.pop('observers', None)
        return d

    def __setstate__(self, d):
        # logger.detail("D: %s", d)
        self.__dict__ = d

    def __str__(self):
        out = "\n%s :: App name: '%s'\n" % (self.__class__.__name__, self.name)
        out += "\tExtra: %s\n" % str(self.extra)
        # logical
        if self.logical:
            out += "Logical Arch.:\n"
            for service_id in self.logical.nodes:
                out += "\t%s\n" % self.get_service(service_id)
                for node_id in self.graph[service_id].keys():
                    if isinstance(self.graph.node[node_id]["node"], LogicalNode):
                        out += "\t\t-> %s\n" % node_id

        # virtual
        if self.virtual:
            out += "Virtual Arch.:\n"
            for vm_id in self.virtual.nodes:
                out += "\t%s\n" % self.get_vm(vm_id)
                for node_id in self.graph[vm_id].keys():
                    node = self.graph.node[node_id]["node"]
                    if isinstance(node, VirtualNode):
                        out += "\t\t-> %s\n" % (node_id)

        # logical
        if self.physical:
            out += "Physical Arch.:\n"
            for host_id in self.physical.nodes:
                out += "\t%s\n" % self.get_host(host_id)
                for node_id in self.graph[host_id].keys():
                    if isinstance(self.graph.node[node_id]["node"], PhysicalNode):
                        out += "\t\t-> %s\n" % id

        # instances
        if self.logical and self.virtual:
            out += "Instances:\n"
            for service_id in self.logical.nodes:
                service = self.get_service(service_id)
                out += "\t%s: %s\n" % (service.name, ",".join([v.name for v in service.get_all_instances()]))

        # allocations
        if self.virtual and self.physical:
            out += "Allocations:\n"
            for vm_id in self.virtual.nodes:
                vm = self.get_vm(vm_id)
                h = vm.get_host()
                if h:
                    out += "\t%s: %s\n" % (vm.name, vm.get_host().ip_address)

        return out

    def detail_str(self):
        out = "\n%s :: App name: '%s'\n" % (self.__class__.__name__, self.name)
        # logical
        if self.logical:
            out += "Logical Arch.:\n"
            for service_id in self.logical.nodes:
                out += "\t%s\n" % self.get_service(service_id)
                out += "\tSpec: %s\n" % self.get_service(service_id).resources
                for node_id in self.graph[service_id].keys():
                    if isinstance(self.graph.node[node_id]["node"], LogicalNode):
                        out += "\t\t-> %s\n" % node_id

        # virtual
        if self.virtual:
            out += "Virtual Arch.:\n"
            for vm_id in self.virtual.nodes:
                out += "\t%s\n" % self.get_vm(vm_id)
                for node_id in self.graph[vm_id].keys():
                    node = self.graph.node[node_id]["node"]
                    if isinstance(node, VirtualNode):
                        out += "\t\t-> %s\n" % (node_id)

        # logical
        if self.physical:
            out += "Physical Arch.:\n"
            for host_id in self.physical.nodes:
                out += "\t%s\n" % self.get_host(host_id)
                for node_id in self.graph[host_id].keys():
                    if isinstance(self.graph.node[node_id]["node"], PhysicalNode):
                        out += "\t\t-> %s\n" % id

        # instances
        if self.logical and self.virtual:
            out += "Instances:\n"
            for service_id in self.logical.nodes:
                service = self.get_service(service_id)
                out += "\t%s: %s\n" % (service.name, ",".join([v.name for v in service.get_instances()]))

        # allocations
        if self.virtual and self.physical:
            out += "Allocations:\n"
            for vm_id in self.virtual.nodes:
                vm = self.get_vm(vm_id)
                h = vm.get_host()
                if h:
                    out += "\t%s: %s\n" % (vm.name, vm.get_host().ip_address)

        return out

    def analyze_virtual(self, custom_analysis=None):
        """
        Generates a virtual architecture from the higher logical architecture.
        Currently, we just make sure that we have at least one instance providing each service.
        In the future: we are going to allow custom analysis that can be provided by the user. An analysis can
            use reliability, security, performance or other factors to make decisions on how many VMs and what specs
            are needed.

        :param custom_analysis:
        :return: None
        """
        # compute VIRTUAL based on LOGIC
        # for each logic service determine:
        for service_id in self.logical.nodes:
            logger.debug("Service: %s", service_id)
            service = self.graph.node[service_id]["node"]
        #   1. are there enough virtual machines (start with simple 1 on 1 rule)
            if service.subtype in (LogicalNodeSubtype.image, LogicalNodeSubtype.lb):
                vms = service.get_instances()
                min_n_instances = service.extra.get("scaling_min_instances", 1)
                for _ in range(len(vms), min_n_instances):
                    service.add_instance()
            elif service.subtype in (LogicalNodeSubtype.service, LogicalNodeSubtype.client) and not self.extra.get('manual'):
                vms = service.get_instances()
                min_n_instances = service.extra.get("scaling_min_instances", 1)
                for _ in range(len(vms), min_n_instances):
                    service.add_instance()


class AppSpec(AppArch):

    def __init__(self, app_name, graph=None, **kwargs):
        super(AppSpec, self).__init__(app_name)
        self.extra = kwargs
        if graph:
            self.graph = graph
            self.logical.nodes.update([k for k, v in self.graph.nodes(data=True) if isinstance(v["node"], LogicalNode)])
            self.virtual.nodes.update([k for k, v in self.graph.nodes(data=True) if isinstance(v["node"], VirtualNode)])
            self.physical.nodes.update(
                [k for k, v in self.graph.nodes(data=True) if isinstance(v["node"], PhysicalNode)]
            )
            # self.extra.update(self.graph.graph)
            # FIXME: redundant data?
            self.graph.graph.update(self.extra)
        self.base = None

    @classmethod
    def from_json(cls, data):
        d = json.loads(data, cls=AppArchDecoder)
        return cls.from_dict(d)

    def to_json(self, cls=AppArchEncoder):
        d = self.to_dict()
        return json.dumps(d, cls=cls, sort_keys=True).encode("UTF-8")


class AppLogicalSpec(AppSpec):
    def __init__(self, *args, **kwargs):
        super(AppLogicalSpec, self).__init__(*args, **kwargs)
        self.base = self.logical
        self.virtual = None
        self.physical = None

    def add_service(self, *args, **kwargs):
        logger.detail("%s --- %s", args, kwargs)
        self.logical.add_node(*args, **kwargs)

    def add_flow(self, *args):
        logger.debug(args)
        self.logical.add_edge(*args)


class AppVirtualSpec(AppSpec):
    def __init__(self, *args):
        super(AppVirtualSpec, self).__init__(*args)
        self.base = self.virtual
        self.logical = None
        self.physical = None
        self.constraints = {}
        self.dependencies = {}

    def get_subgraph(self, nodes=None):
        return AppVirtualSpec(self.name, self.graph.subgraph(nodes))

    def add_constraints(self, constraints):
        self.constraints = constraints

    def add_dependencies(self, dependencies):
        self.dependencies = dependencies


class AppPhysicalSpec(AppSpec):
    def __init__(self, *args):
        super(AppPhysicalSpec, self).__init__(*args)
        self.base = self.physical
        self.logical = None
        self.virtual = None


# class AppArchEncoder(JSONEncoder):
#     def default(self, obj):
#         if isinstance(obj, IPAddress):
#             return str(obj)
#         return JSONEncoder.default(self, obj)


