import math
import traceback
import json

import numpy as np
from collections import namedtuple
from datetime import timedelta, datetime

from networkx.algorithms.traversal import dfs_postorder_nodes
from networkx.classes.digraph import DiGraph
from networkx.readwrite.json_graph.node_link import node_link_data

from lama.agent.app.app_arch import LogicalNode, PhysicalNode, VirtualNode, LogicalNodeSubtype, InstanceState
from lama.agent.app.monitor.strategies.commands import MonitoringStrategyCommands
from lama.agent.app.views.base import AppArchObserver, AppArchStrategy
from lama.agent.define import FailureDomain
from lama.agent.event_managers.twisted import run_async_static
from lama.utils.enum import Enum
from lama.utils.logging_lama import logger


__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


Failure = namedtuple('Failure', ['name', 'default_prob'])


class FailureType(Enum):
    Crash = 'crash'
    # Crash = Failure('crash', 0.01)


class FailureNode(object):

    def __init__(self, app_node, failure_domain, failure_prob=None, failure_type=FailureType.Crash):
        self.app_node = app_node
        self._failure_type = failure_type
        self.domain = failure_domain
        # self._dependent_failures = set()
        self.eid = "{}:{}".format(self.app_node.id, self._failure_type.name)
        self.penalty = 1.0
        self.active = False
        self.instances_on_trigger = set()

    # def add_dependent_failure(self, failure_node):
    #     self._dependent_failures.add(failure_node)

    def activate(self):
        self.active = True

    def to_dict(self):
        return {
            'id': self.eid,
            'app_node': self.app_node.id,
            'type': self._failure_type.name,
            'penalty': self.penalty
        }

    def __str__(self):
        return " ".join(["{}={}".format(k, v) for k, v in self.to_dict().items()])


class AggregationFunction(object):

    @staticmethod
    def mean(*values):
        return np.mean(values)

    @staticmethod
    def min(*values):
        return min(values)

    @staticmethod
    def sum(*values):
        return sum(values)


class ResilienceNode(object):

    def __init__(self, failure=None, agg=AggregationFunction.min):
        self._failure = failure

    @classmethod
    def from_arch_node(cls, node):
        if isinstance(node, PhysicalNode):
            return NotImplementedError()
        elif isinstance(node, VirtualNode):
            raise NotImplementedError()
        elif isinstance(node, LogicalNode):
            raise NotImplementedError()
        else:
            raise NotImplementedError()


class PerformanceNode(object):
    monitor = None
    recovery_estimator = None
    configuration = None

    def __init__(self, app_node):
        self.performance_index = 0
        self.resilience_index = None
        self.recovery_time = None
        self.resilience_per_failure = {}
        self.recovery_per_failure = {}
        self.recovery_time_per_failure = {}
        self.dependencies_per_recovery = {}

        self.ready = False

        self.app_node = app_node

    def get_type(self):
        if self.app_node:
            return self.app_node.type
        return None

    def get_state(self):
        if isinstance(self.app_node, VirtualNode):
            return self.app_node.state
        elif isinstance(self.app_node, PhysicalNode):
            return InstanceState(min(inst.state.value for inst in self.app_node.get_vms()))

        return None

    # def get_state(self):
    #     try:
    #         return self.app_node.state
    #     except:
    #         return None

    def to_dict(self):
        state = self.get_state()
        return {
            'performance_index': self.performance_index,
            'resilience_index': self.resilience_index,
            'ready': self.ready,
            'recovery_time': self.recovery_time,
            'state': state.name if state else None

        }

    def eval(self, *children):
        self.eval_performance(*children)
        logger.temp("\tPerformance Index = %s", self.performance_index)
        logger.temp("")
        self.eval_resilience(*children)
        logger.temp("\tResilience Index = %s", self.resilience_index)

    # @abstractmethod
    def eval_performance(self, *children):
        pass

    # @abstractmethod
    def eval_resilience(self, *children):
        pass

    def resilience_str(self):
        return "ri = {0} {2}{1}{2}".format(
            self.resilience_index,
            "\n".join(["\t{} : {}".format(f, r) for f, r in self.resilience_per_failure.items()]),
            "\n" if len(self.resilience_per_failure) > 1 else ""
        )


class HostPerformanceNode(PerformanceNode):

    def __init__(self, app_node):
        super().__init__(app_node)
        self.performance_index = 1
        self.ready = True

    def eval_performance(self, *children):
        pass

    def eval_resilience(self, *children):
        pass


class InstancePerformanceNode(PerformanceNode):
    # used only for virtual instances

    def __init__(self, app_node):
        super().__init__(app_node)
        self.base_value = None
        self.timestamp = None
        self.a = 10
        self.b = 100
        self.active_recovery = False
        # configure target resource usage value
        self.target_usage = self.invert_performance(self.configuration.sla_performance_index)

    def is_recovery_node(self):
        # return isinstance(self.app_node, VirtualNode) and self.app_node.state != InstanceState.Active
        return self.app_node.state != InstanceState.Active or self.active_recovery

    def performance(self, val):
        perf_val = 1 - math.exp(self.a * (val / self.b - 1))
        if perf_val > 1:
            perf_val = 1
        elif perf_val < 0:
            perf_val = 0
        # if args:
        #     logger.temp("Computing performance with extra values: %s", args)

        return perf_val

    def invert_performance(self, perf_val):
        return self.b * (math.log(1 - perf_val) / self.a + 1)

    def refresh_base_value(self):
        d = self.monitor.get_data_value(self.app_node, metrics={'virt_cpu_total'})
        # TODO: should change how metrics are saved
        # first None: sub_metrics, second None: no arguments
        self.base_value = None
        val = d.get('virt_cpu_total', {}).get(None, {}).get(None)
        if val:
            self.timestamp, self.base_value = val[-1]
            logger.temp("\tts=%s, val=%s", self.timestamp, self.base_value)

        return self.base_value

    def eval_performance(self, *children):
        val = self.refresh_base_value()
        logger.temp("\tts=%s, val=%s", self.timestamp, self.base_value)

        if val is not None:
            self.performance_index = self.performance(val)
        else:
            self.performance_index = 0

        self.ready = self.app_node.ready_ts is not None

    def eval_resilience(self, *children):
        logger.temp("Children: %s", children)
        self.resilience_index = None

        # NOT GONNA HAPPEN
        failures = set(k for _, child in children for k in child.resilience_per_failure.keys())
        self.resilience_per_failure = {}
        if failures:
            logger.temp("\tFailures: %s", failures)
            for failure in failures:
                self.resilience_per_failure[failure] = self.performance_index * min(
                    child.resilience_per_failure.get(failure, child.performance_index) for _, child in children
                )

                logger.temp("\t\tfailure=%s resilience=%s", failure, self.resilience_per_failure[failure])

        for edge, child in children:
            for failure_id, failure in edge.get('events', {}).items():
                logger.temp("Failure id: %s - penalty: %s", failure_id, failure.penalty)
                self.resilience_per_failure[failure_id] = self.performance_index * (1 - failure.penalty)

        logger.temp("resilience per failure: %s", self.resilience_per_failure)
        self.resilience_index = min(self.resilience_per_failure.values()) if self.resilience_per_failure else None


class AppPerformanceNode(PerformanceNode):

    def __init__(self):
        super().__init__(None)
        self._agg_function = AggregationFunction.min

    def eval_performance(self, *children):
        children_indexes = [child.performance_index for edge, child in children]
        logger.temp("\t\tchildren indexes = %s", children_indexes)
        self.performance_index = self._agg_function(*children_indexes) if children_indexes else 0

        self.ready = all(c.performance_index for _, c in children)
        logger.temp("\t\tready = %s", self.ready)

    def eval_resilience(self, *children):

        logger.temp("\t\tApp node")

        failures = set(k for _, child in children for k in child.resilience_per_failure.keys())
        logger.temp("\t\tFailures: %s", failures)
        self.resilience_per_failure = {}
        for failure in failures:
            self.resilience_per_failure[failure] = self._agg_function(
                *[child.resilience_per_failure.get(failure, child.performance_index) for _, child in children]
            )

            logger.temp("\t\t\tfailure=%s resilience=%s", failure, self.resilience_per_failure[failure])

            args = [child.recovery_time_per_failure.get(failure)
                    for _, child in children if failure in child.recovery_time_per_failure]
            logger.temp("args=%s", args)
            self.recovery_time_per_failure[failure] = max(args) if args else timedelta(0)
            logger.temp("\t\tAgg recovery: failure=%s time=%s",
                        failure, self.recovery_time_per_failure[failure])

        self.resilience_index = min(self.resilience_per_failure.values()) if self.resilience_per_failure else 0
        self.recovery_time = max(
            r for r in self.recovery_time_per_failure.values()
        ).total_seconds() if self.recovery_time_per_failure else None
        logger.temp("\t\tResilience: %s", self.resilience_str())
        logger.temp("\t\tRecovery: %s\t%s", self.recovery_time, self.recovery_time_per_failure)


class ServicePerformanceNode(PerformanceNode):

    def __init__(self, app_node):
        super().__init__(app_node)
        self._agg_function = AggregationFunction.mean
        self.allow_increase_ratio = 1

    def eval_performance(self, *children):
        # logger.debug("Computing performance for node: agg=%s children=%s", self._agg_function, children)
        # logger.debug("values: %s", [c.performance_index for c in children])

        # for edge, child in children:
        #     logger.temp("edge=%s, child=%s", edge, child)

        logger.temp("\t\tchildren = %s", children)
        # children_indexes = [child.performance_index for edge, child in children if not child.is_recovery_node()]
        children_indexes = [
            child.performance_index
            for edge, child in children
            if child.app_node.state == InstanceState.Active and child.app_node.ready_ts
            ]

        logger.temp("\t\tchildren indexes = %s", children_indexes)
        self.performance_index = self._agg_function(*children_indexes) if children_indexes else 0

        # get max increase factor
        max_increased_factor = []
        for edge, child in children:
            # if not child.is_recovery_node():
            if child.app_node.state == InstanceState.Active:
                # if not child.app_node.ready_ts:
                if not child.app_node.ready_ts or datetime.now() - child.app_node.ready_ts < timedelta(seconds=30):
                    maxinc = 10
                    logger.temp("\t\t...child=%s usage=%s tgt_usage=%s maxinc=%s INSTANCE NOT READY LONG ENOUGH (%s)",
                                child.app_node.name, child.base_value, child.target_usage, maxinc,
                                datetime.now() - child.app_node.active_ts if child.app_node.ready_ts else None)
                else:
                    if child.base_value is None:
                        maxinc = 10
                    else:
                        maxinc = child.target_usage / child.base_value if child.base_value else 10
                    logger.temp("\t\t...child=%s usage=%s tgt_usage=%s maxinc=%s",
                                child.app_node.name, child.base_value, child.target_usage, maxinc)
                max_increased_factor.append(maxinc)

        m_factor = min(max_increased_factor) if max_increased_factor else 0

        if m_factor > 1:
            logger.temp("\t\tmin max increased factor: %s", m_factor)
            n_remove = len(max_increased_factor) * (1 - 1/m_factor)
            logger.temp("\t\tmax instances to remove: %s (%s)", int(n_remove), n_remove)

            n_remove = int(n_remove)
            needed = len(children) - n_remove
        else:
            needed = len(children)

        for _, child in sorted(children, key=lambda c: c[1].app_node.name):
            if needed:
                logger.temp("\t\t...instance=%s ACTIVE", child.app_node.name)
                child.active_recovery = False
                needed -= 1
            else:
                logger.temp("\t\t...instance=%s RECOVERY", child.app_node.name)
                child.active_recovery = True

        self.allow_increase_ratio = m_factor

        self.ready = all(c.performance_index for _, c in children if c.app_node.state == InstanceState.Active)
        logger.temp("\t\tready = %s", self.ready)

    def eval_resilience(self, *children):

        children = sorted(children, key=lambda c: c[1].app_node.name)

        logger.temp("\tService node")
        # service
        failures = {}
        # columns: usage, penalty, failure
        # rows: children
        usage_arr = np.zeros(len(children))
        # FIXME
        ready_resilience = [False] * len(children)

        n_ready_children = 0
        for i, (edge, child) in enumerate(children):
            logger.temp("\t\tChild i=%s child=%s", i, child.app_node.name)
            logger.temp("\t\tEdges:")
            for ev, n in edge.get('events', {}).items():
                logger.temp("\t\t\tFailure node: %s", n)

            if child.ready:
                n_ready_children += 1

            child_resource_usage = child.base_value or 0
            usage_arr[i] = child_resource_usage
            ready_resilience[i] = True if child.base_value else False

            for event, _ in edge.get("events", {}).items():
                failure_child_mask = failures.get(event, None)
                if failure_child_mask is None:
                    failure_child_mask = np.ones(len(children), dtype=bool)
                    failures[event] = failure_child_mask
                # TODO: Add the penalty!
                failure_child_mask[i] = False
                logger.temp("\t\t\tfailure=%s masked=%s", event, failure_child_mask)

            # / EDIT_INSTANCE_FAILURES
            for event, _ in child.resilience_per_failure.items():
                failure_child_mask = failures.get(event, None)
                if failure_child_mask is None:
                    failure_child_mask = np.ones(len(children), dtype=bool)
                    failures[event] = failure_child_mask
                # TODO: Add the penalty!
                failure_child_mask[i] = False
                logger.temp("\t\t\tfailure=%s masked=%s", event, failure_child_mask)
            # \ EDIT_INSTANCE_FAILURES

        # TODO: when necessary, consider zero elements in usage_arr
        # norm_usage_arr = 1 / usage_arr
        # norm_usage_arr /= sum(norm_usage_arr)

        logger.temp("\t\tusage array=%s", usage_arr)
        # logger.temp("\tnorm_usage=%s", norm_usage_arr)

        self.resilience_per_failure = {}
        self.recovery_per_failure = {}
        self.dependencies_per_recovery = {}
        active_capacity = sum((not child.is_recovery_node()) for _, child in children)
        # active_capacity = sum(child.app_node.state == InstanceState.Active for _, child in children)

        # logger.temp("\t\tno failure capacity=%s", no_failure_capacity)
        logger.temp("\t\tactive capacity=%s", active_capacity)

        for failure, child_mask in failures.items():
            logger.temp("\t\tfailure: %s", failure)
            masked_usage = np.ma.array(usage_arr, mask=~child_mask)
            logger.temp("\t\t\tmasked_usage: %s", masked_usage)
            logger.temp("\t\t\tchild_mask: %s", child_mask)
            # active_failure_capacity = sum(mask and not child[1].is_recovery_node()
            #                               for child, mask in zip(children, child_mask))
            active_failure_capacity = sum(mask and child[1].app_node.state == InstanceState.Active
                                          for child, mask in zip(children, child_mask))
            failure_capacity = sum(mask for child, mask in zip(children, child_mask))

            logger.temp("\t\t\tfailure capacity: active=%s latent=%s", active_failure_capacity, failure_capacity)
            if active_failure_capacity:
                # increase_factor = 2 - failure_capacity/len(children)
                increase_factor = active_capacity / active_failure_capacity
                logger.temp("\t\t\tincrease factor: %s", increase_factor)
                logger.temp("\t\t\tarray for resilience: %s",
                            [children[i][1].performance(usage_arr[i] * increase_factor)
                             for i, mask in enumerate(child_mask) if mask])
                self.resilience_per_failure[failure] = self._agg_function(
                    *[children[i][1].performance(usage_arr[i] * increase_factor)
                      for i, mask in enumerate(child_mask) if mask]
                )
                logger.temp("\t\t\tresilience: %s", self.resilience_per_failure[failure])
            else:
                # all servers lost
                self.resilience_per_failure[failure] = 0

            logger.temp("\t\t\tfailure=%s resilience=%s", failure, self.resilience_per_failure[failure])

            if isinstance(self.app_node, LogicalNode):
                # need to search for the lost capacity due to failure...
                if self.resilience_per_failure[failure] >= self.configuration.sla_performance_index:
                    needed_capacity = 0
                else:
                    needed_capacity = active_capacity - active_failure_capacity

                logger.temp("\t\t\tLogical node: computing recovery time")
                # get all children that are instance nodes that are not depend on the failure

                recovery_instances = [
                    (child[1].get_state(), self.recovery_estimator.get_deployment_time(child[1].get_state(), self.app_node.name))
                    for child, mask in zip(children, child_mask)
                    if mask and child[1].is_recovery_node()
                ]

                recovery_instances.sort(reverse=True, key=lambda x: x[1])

                logger.temp("\t\t\trecovery_instances: %s", recovery_instances)
                logger.temp("\t\t\tneeded_capacity: %s", needed_capacity)
                if len(recovery_instances) >= needed_capacity:
                    self.recovery_per_failure[failure] = recovery_instances
                    if needed_capacity:
                        logger.temp("\t\t\tWorst case recovery: failure=%s time=%s",
                                    failure, self.recovery_per_failure[failure][0][1].total_seconds())
                    else:
                        logger.temp("\t\t\tWorst case recovery: failure=%s time=0", failure)
                else:
                    # worst case
                    image_number = 1
                    concurrency_factor = (needed_capacity - len(recovery_instances)) / image_number
                    recovery_instances = [(InstanceState.Cold, self.recovery_estimator.get_deployment_time(
                        InstanceState.Cold, self.app_node.name,
                        concurrency_factor=concurrency_factor
                    ))] * (needed_capacity - len(recovery_instances)) + recovery_instances

                    self.recovery_per_failure[failure] = recovery_instances
                    logger.temp("\t\t\tCold recovery (#instances=%s, cf=%s): failure=%s time=%s",
                                needed_capacity - len(recovery_instances), concurrency_factor,
                                failure, self.recovery_per_failure[failure][0][1].total_seconds())

            logger.temp("\t\t\trecovery_per_failure: %s", self.recovery_per_failure[failure])
            self.recovery_time_per_failure[failure] = timedelta(0) if not self.recovery_per_failure[failure] else max(
                self.recovery_per_failure[failure], key=lambda x: x[1]
            )[1]
            # logger.temp("recover_per_failure: %s", self.recovery_per_failure)
            logger.temp("\t\t\trecovery_time_per_failure: %s", self.recovery_time_per_failure[failure])

        if not n_ready_children:
            logger.temp("\t\tNo ready children")
            self.resilience_index = 0

        self.resilience_index = min(self.resilience_per_failure.values()) if self.resilience_per_failure else 0
        if not all(ready_resilience):
            self.resilience_index = 0

        self.recovery_time = max(
            r for r in self.recovery_time_per_failure.values()
        ).total_seconds() if self.recovery_time_per_failure else None
        logger.temp("\t\tResilience: %s", self.resilience_str())
        logger.temp("\t\tRecovery: %s\t%s", self.recovery_time, self.recovery_time_per_failure)


class Downtime(object):

    def __init__(self):
        self.downtime = []
        self.last_down_start = None
        self.ready_once = False

    def start_downtime(self, offset=0):
        if not isinstance(offset, timedelta):
            offset = timedelta(seconds=offset)
        if self.ready_once and not self.last_down_start:
            self.last_down_start = datetime.now() - offset
            logger.temp("DT DEBUG: start downtime = %s", self.last_down_start)

    def stop_downtime(self):
        self.ready_once = True
        if self.last_down_start:
            self.downtime.append((self.last_down_start, datetime.now()))
            self.last_down_start = None
            logger.temp("DT DEBUG: downtime = [%s, %s]", *self.downtime[-1])

    def get_downtime(self, window):
        if not isinstance(window, timedelta):
            window = timedelta(seconds=window)
        wend = datetime.now()
        wstart = wend - window
        logger.temp("DT DEBUG: window = [%s, %s]", wstart, wend)
        total = timedelta(seconds=0)
        for t1, t2 in self.downtime:
            if t2 > wstart:
                delta = t2 - max(wstart, t1)
                logger.temp("DT DEBUG: Add = [%s, %s] = %s", t1, t2, delta)
                total += delta

        if self.last_down_start:
            total += wend - self.last_down_start

        logger.temp("DT DEBUG: downtime = %s", total)
        return total


class ResilienceGraph(AppArchObserver, AppArchStrategy):

    def __init__(self, controller, recovery_estimator, app_configuration,
                 recompute_delay=timedelta(seconds=10)):
        self.controller = controller
        self._app_graph = controller.app
        self._estimator = recovery_estimator
        self._configuration = app_configuration

        # self._monitor = monitor
        self.controller.monitor.set_op_controller(MonitoringStrategyCommands(self))
        PerformanceNode.monitor = self.controller.monitor
        PerformanceNode.recovery_estimator = self._estimator
        PerformanceNode.configuration = self._configuration

        self._app_graph.add_observer(self)
        # self._resilience_nodes = {}
        self._resilience_graph = DiGraph()

        self.failure_nodes = {}

        self.create_graph()

        self.update_methods = {
            'add_instance': self.update_add_instance,
            'add_allocation': self.update_add_allocation,
            'remove_node': self.update_remove_node
        }

        # set up timer for periodic recomputations
        self.recompute_delay = recompute_delay
        self.recompute_timer = None
        self.initial_deployment_ready = False
        self.dependencies = {}

        self.downtime_controller = Downtime()
        self.dynamic = True

        self._csv_filename = '/var/log/lama/resilience_values_{}.csv'.format(self._app_graph.name)
        with open(self._csv_filename, 'w') as f:
            f.write("Datetime,Node,Resilience,Performance,Downtime\n")

    def create_graph(self):
        self._resilience_graph.add_node('app', node=AppPerformanceNode())

        for service in self._app_graph.get_services(not_subtype=LogicalNodeSubtype.client):
            self._resilience_graph.add_node(service.name, node=ServicePerformanceNode(service))

            # add failures per service: none for now

            self._resilience_graph.add_edge('app', service.name)

            for instance in service.get_instances():
                self._process_add_instance(service, instance)

        logger.debug("Created graph: %s", self)

    def check(self):
        self._evaluate()

    def _evaluate(self):

        try:
            self.recompute_timer.cancel()
        except:
            pass

        self.eval()

        if self.initial_deployment_ready or self._resilience_graph.node['app']['node'].ready:
            old_dependencies = self.dependencies
            self.initial_deployment_ready = True
            # FIXME: this is a temporary adjustment to avoid overloading the network and image services during start up
            # Should be replaced.
            self.adjust_scaling()
            target_instances = self.adjust()
            self._adapt_instances(target_instances)

            logger.temp("Failure Recovery instances")
            for failure_key, failure in self.failure_nodes.items():
                logger.temp("\t%s: %s", failure_key, ", ".join(failure.instances_on_trigger))

            logger.temp("Dependencies:\n%s", self.dependencies)

            # compare dependencies
            self.compare_dependencies(old_dependencies)
        else:
            logger.info("Not adjusting - not ready init=%s, ready=%s",
                        self.initial_deployment_ready,
                        self._resilience_graph.node['app']['node'].ready)

        self.recompute_timer = run_async_static(self._evaluate, timeout=self.recompute_delay.total_seconds())

    def notify_instance_ready(self):
        self._evaluate()

    def compare_dependencies(self, old_deps):
        changed = False
        for instance_name, old_instance_deps in old_deps.items():
            if instance_name in self.dependencies:
                instance_deps = self.dependencies.get(instance_name, {})
                for domain, dependents in old_instance_deps.items():
                    removed = dependents - instance_deps.get(domain, set())
                    added = instance_deps.get(domain, set()) - dependents
                    if removed:
                        changed = True
                        logger.temp("\tRemoved=%s instance=%s domain=%s", removed, instance_name, domain)
                    if added:
                        changed = True
                        logger.temp("\tAdded=%s instance=%s domain=%s", added, instance_name, domain)
                    if added or removed:
                        # notify dependency changes
                        logger.temp("Will send notification: instance=%s host=%s",
                                    instance_name, self._app_graph.get_vm(instance_name).get_host().ip_address)
        if changed:
            logger.temp("Current deps: %s", old_deps)
            logger.temp("Old deps    : %s", self.dependencies)

    def mark_instance_down(self, service_name, instance_name):

        key = (FailureType.Crash, instance_name)
        failure_node = self.failure_nodes.get(key)
        if failure_node:
            failure_node.activate()

        self._handle_failure(key)

        logger.temp("Removing failed instance: %s:%s", service_name, instance_name)
        self.controller.remove_instance(service=service_name, instance=instance_name)
        logger.temp("Re-evaluate after removing instance: %s:%s", service_name, instance_name)
        self._evaluate()

    def mark_host_down(self, ip_address):
        # # FIXME: hack
        # self.controller.notify_failed_host(ip_address)

        key = (FailureType.Crash, ip_address)
        failure_node = self.failure_nodes.get(key)
        if failure_node:
            failure_node.activate()

        self._handle_failure(key)

        logger.temp("Removing failed host: %s", ip_address)
        self.controller.remove_host(ip_address=ip_address)

        logger.temp("Re-evaluate after removing host: %s", ip_address)
        self._evaluate()

    def eval(self):
        # app_node = self._resilience_graph.node['app']
        # logger.debug('sample node: %s %s', app_node, type(app_node))
        logger.temp("\n\n================= EVAL START =================\n")
        logger.temp("")
        for node_id in dfs_postorder_nodes(self._resilience_graph, 'app'):
            logger.debug("============ Node: %s", node_id)
            children = [
                (self._resilience_graph.edge[edge[0]][edge[1]], self._resilience_graph.node[edge[1]]['node'])
                for edge in self._resilience_graph.edges(node_id)
            ]
            node = self._resilience_graph.node[node_id]['node']
            if isinstance(node.app_node, LogicalNode):
                children.sort(key=lambda x: getattr(x, x[1].app_node.name, ''))
            node.eval(
                *children
                # *[self._resilience_graph.node[nid]['node'] for nid in self._resilience_graph.successors(node_id)]
            )

            with open(self._csv_filename, 'a') as f:
                f.write(
                    "{},{},{},{},{}\n".format(
                        datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                        node_id,
                        node.resilience_index,
                        node.performance_index,
                        self.downtime_controller.get_downtime(self._configuration.sla_period).total_seconds()
                    )
                )
            logger.debug("================================")
        logger.temp("\n\n================= EVAL END =================\n")

        if self._resilience_graph.node['app']['node'].performance_index > 0:
            self.downtime_controller.stop_downtime()
        else:
            self.downtime_controller.start_downtime(timedelta(seconds=30))

    def _handle_failure(self, failure):
        failure_node = self.failure_nodes.get(failure)
        if not failure_node:
            logger.error("Failure not in model: %s", failure)
            return

        logger.info("Recover from failure: %s", failure_node)
        for instance_name in failure_node.instances_on_trigger:
            logger.info("\tActivate instance: %s", instance_name)
            instance = self._app_graph.get_vm(instance_name)
            self.controller.change_instance_state(
                instance.get_service().name,
                instance_name,
                instance.state,
                InstanceState.Active
            )

    def adjust_scaling(self):
        service_edges = self._resilience_graph.edges('app')
        new_active_nodes_per_service = {}
        for e in service_edges:
            service_node = self._resilience_graph.node[e[1]]['node']
            logger.temp("Adjust service: %s if=%s", e[1], service_node.allow_increase_ratio)

            # check for scaling
            # TODO: may include a C factor
            c = 0
            if service_node.allow_increase_ratio < 1 - c:
                result = self.controller.add_instance(
                    service_node.app_node.name,
                    state=InstanceState.Active
                )

    def adjust(self):
        recovery_nodes_per_service = {}

        service_edges = self._resilience_graph.edges('app')
        original_max_recovery_time = self._configuration.sla_period * self._configuration.sla_miss_fraction

        if self.dynamic:
            downtime = self.downtime_controller.get_downtime(self._configuration.sla_period)
            max_recovery_time = max(
                original_max_recovery_time - downtime,
                timedelta(seconds=0)
            )
            logger.temp("Max recovery: %s / %s Downtime: %s", max_recovery_time, original_max_recovery_time, downtime)
        else:
            max_recovery_time = original_max_recovery_time
            logger.temp("Max recovery: %s / %s", max_recovery_time, original_max_recovery_time)

        # reset instances on trigger...
        for failure in self.failure_nodes.values():
            failure.instances_on_trigger = set()

        self.dependencies = {}

        # for each service
        for e in service_edges:
            service_node = self._resilience_graph.node[e[1]]['node']
            logger.temp("Adjust service: %s", e[1])

            # 1. select the recovery nodes: active non-required instances + latent instances
            recovery_nodes = recovery_nodes_per_service.setdefault(e[1], {})
            for service_id, instance_id in self._resilience_graph.edges(e[1]):
                instance_edge = self._resilience_graph.edge[service_id][instance_id]
                instance_node = self._resilience_graph.node[instance_id]['node']
                if instance_node.is_recovery_node():
                    state_rn = recovery_nodes.setdefault(instance_node.get_state(), [])
                    state_rn.append({
                        'id': instance_id,
                        'events': list(instance_edge.get('events').keys()),
                        'old_state': instance_node.get_state(),
                        'needed_state': InstanceState.Cold,
                        'failures': set(),
                        'new': False
                    })
            logger.temp("\tRecovery nodes:")
            for k, v in recovery_nodes.items():
                logger.temp("\t\t%s: %s", k.name, ', '.join(v1['id'] for v1 in v))
            if not recovery_nodes:
                logger.temp("\t\t---")

            target_instance_state = self._estimator.get_coldest_instance_state(
                service_node.app_node.name, max_recovery_time
            )
            logger.temp("\tTarget state: %s for recovery in %s (< %s)",
                        target_instance_state,
                        self._estimator.get_deployment_time(target_instance_state, service_node.app_node),
                        max_recovery_time)

            if target_instance_state != InstanceState.Cold:
                # get all recoveries needed ordered by instance state
                list_of_recoveries = []
                # 2. for each failure, select recoveries...
                for failure, recoveries in service_node.recovery_per_failure.items():
                    logger.temp("\tConsidering failure: %s target_state=%s #recover=%s %s",
                                failure, target_instance_state, len(recoveries), recoveries)
                    for recovery in recoveries:
                        # check if there is a node in the desired state that respects the constraints
                        logger.debug("\t\tConsidering recovery: %s", recovery)
                        recovery_node = None
                        new_state = InstanceState.Active
                        while not recovery_node and new_state != InstanceState.Cold:
                            logger.temp("\t\tTrying state: %s", new_state)
                            for n in recovery_nodes.get(new_state, []):
                                if failure not in n['events']:
                                    # found it
                                    logger.temp("\t\tFailure will be covered by instance: %s", n['id'])
                                    recovery_node = n['id']
                                    n['needed_state'] = target_instance_state
                                    n.setdefault('events', []).append(failure)
                                    if not n['new']:
                                        f = self.failure_nodes.get(failure)
                                        f.instances_on_trigger.add(n['id'])
                                        self.dependencies.setdefault(n['id'], {}).setdefault(
                                            f.domain, set()
                                        ).add(f.app_node.id)
                                    else:
                                        n['failures'].add(failure)

                            new_state = InstanceState(new_state.value + 1)
                            logger.temp("\t\tIncremented state to: %s", new_state)

                        if not recovery_node:
                            # need to
                            n = {
                                    'id': 'new_instance',
                                    'events': [failure],
                                    'old_state': None,
                                    'needed_state': target_instance_state,
                                    'failures': set(),
                                    'new': True
                                }

                            recovery_nodes.setdefault(target_instance_state, []).append(n)
                            logger.temp("\t\tFailure will be covered by NEW instance: %s state=%s",
                                        n['id'], n['needed_state'])

            logger.temp("\tFinal recovery nodes: %s", recovery_nodes)
            for k, v in recovery_nodes.items():
                logger.temp("\t\t%s: %s", k.name, ', '.join(v1['id'] for v1 in v))

        return recovery_nodes_per_service
        # target_instances = {}
        # for e in service_edges:
        #     service_node = self._resilience_graph.node[e[1]]['node']
        #     service_instances = target_instances.setdefault(service_node.app_node.name, {
        #         InstanceState.Cold: 0,
        #         InstanceState.Hot: len(service_node.app_node.get_instances(states={InstanceState.Hot})),
        #         InstanceState.Warm: len(service_node.app_node.get_instances(states={InstanceState.Warm})),
        #         InstanceState.Active: len(service_node.app_node.get_instances())
        #     })
        # return target_instances

    def _adapt_instances(self, instances):

        for service, service_instances in instances.items():
            logger.temp("Changes for service: %s", service)
            for state, nodes in service_instances.items():
                for node in nodes:
                    if node['new']:
                        logger.temp("\t\tAdd node new %s instance to service %s", node['needed_state'].name, service)
                        result = self.controller.add_instance(
                            service,
                            state=node['needed_state']
                        )
                        if result[0]:
                            instance_name = result[1]
                            for failure in node['events']:
                                f = self.failure_nodes.get(failure)
                                f.instances_on_trigger.add(instance_name)
                                self.dependencies.setdefault(instance_name, {}).setdefault(
                                    f.domain, set()
                                ).add(f.app_node.id)

            for state, nodes in service_instances.items():
                for node in nodes:
                    if not node['new'] and node['needed_state'] != node['old_state']:
                        # special case:
                        if node['needed_state'] != InstanceState.Cold:
                            logger.temp("\tChange state %s -> %s for node %s",
                                        node['old_state'].name, node['needed_state'].name, node['id'])
                            self.controller.change_instance_state(
                                service,
                                node['id'],
                                node['old_state'],
                                node['needed_state'],
                            )

            for state, nodes in service_instances.items():
                for node in nodes:
                    if node['needed_state'] == InstanceState.Cold:
                        logger.temp("\tRemove node: %s", node['id'])
                        self.controller.remove_instance(service, node['id'])

            logger.temp("")
        self.controller.check_allocation_controller()

    def add_allocation_constraints(self, spec):
        # self.eval()

        logger.debug("Setting constraints: %s", spec)
        logger.debug("Resilience graph: %s", self)
        logger.debug("Dependencies: %s", self.dependencies)

        constraints = {}
        dependencies = {}
        for instance in spec.get_vms():
            service = instance.get_service()

            # get edge between service and instance
            try:
                service_node = self._resilience_graph.node[service.name]['node']
                instance_node = self._resilience_graph.node[instance.name]['node']
                edge = self._resilience_graph.edge[service.name][instance.name]

                logger.temp("\tservice node: %s", service_node.app_node.name)
                logger.temp("\tinstance node: %s", instance_node.app_node.name)
                logger.temp("\tedge: %s", edge)
                logger.temp("\tresilience_per_failure: %s", service_node.resilience_per_failure)

                if len(service_node.resilience_per_failure):
                    for failure, resilience in service_node.resilience_per_failure.items():
                        logger.temp("\t\tfailure=%s", failure)
                        logger.temp("\t\tresilience=%s", resilience)
                        logger.temp("\t\tnode_type=%s", self._resilience_graph.node.get(failure[1], {})['node'])
                        logger.temp("\t\tnode=%s", self._resilience_graph.node.get(failure[1], {})['node'].app_node)
                        if resilience < self._configuration.sla_performance_index\
                                and failure[1] != instance.name:
                            constraints.setdefault(instance.name, set()).add((
                                self._resilience_graph.node.get(failure[1], {})['node'].get_type(), str(failure[1])
                            ))
                            dependencies.setdefault(instance.name, self.dependencies.get(instance.name))
                else:
                    logger.temp("\t\tNo event currently affecting this service")

            except KeyError as e:
                logger.temp("KeyError - Service does not exist: %s", e.args[0])

        logger.temp("Constraints: %s", constraints)
        logger.temp("Dependencies: %s", dependencies)
        spec.add_constraints(constraints)
        spec.add_dependencies(dependencies)

        return spec

    def update(self, method_name, *args, **kwargs):
        try:
            self.update_methods[method_name](*args, **kwargs)
        except KeyError:
            logger.warn (
                "Failure graph is not updating: %s(%s %s)",
                method_name,
                args,
                kwargs
            )
        except:
            logger.error(
                "Failure graph is not updating ERROR: %s(%s %s). Trace: %s",
                method_name,
                args,
                kwargs,
                traceback.format_exc()
            )

    def _process_add_instance(self, service, instance):
        if self._resilience_graph.has_node(service.name) and not self._resilience_graph.has_node(instance.name):
            self._resilience_graph.add_node(instance.name, node=InstancePerformanceNode(app_node=instance))

            events = {}
            # add failures per instance to edge
            key = (FailureType.Crash, instance.name)
            failure_node = self.failure_nodes.get(key)
            if not failure_node:
                failure_node = FailureNode(instance, FailureDomain.Instance, failure_type=FailureType.Crash)
                self.failure_nodes[key] = failure_node
            events[key] = failure_node

            # # add failure per host to edge
            # host = instance.get_host()
            # if host:
            #     key = (FailureType.Crash, host.name)
            #     failure_node = self.failure_nodes.get(key)
            #     if not failure_node:
            #         failure_node = FailureNode(instance, failure_type=FailureType.Crash)
            #         self.failure_nodes[key] = failure_node
            #     events[key] = failure_node

            self._resilience_graph.add_edge(service.name, instance.name, events=events)

            logger.debug("Added attributes. edge: %s", self._resilience_graph.edge[service.name][instance.name])

    def _process_add_host(self, instance, host):
        # self._process_add_host_failure_service_level(instance, host)
        self._process_add_host_failure_instance_level(instance, host)

    def _process_add_host_failure_service_level(self, instance, host):
        if not self._resilience_graph.has_node(instance.name):
            logger.info("Instance not in graph: %s. Not adding allocation", instance)
            return

        if not self._resilience_graph.has_node(host.ip_address):
            self._resilience_graph.add_node(host.ip_address,
                                            node=HostPerformanceNode(app_node=host))

        self._resilience_graph.add_edge(instance.name, host.ip_address, events={})

        service = instance.get_service()
        edge = self._resilience_graph.edge[service.name][instance.name]
        if host:
            key = (FailureType.Crash, host.ip_address)
            failure_node = self.failure_nodes.get(key)
            if not failure_node:
                failure_node = FailureNode(host, FailureDomain.Host, failure_type=FailureType.Crash)
                self.failure_nodes[key] = failure_node
            edge['events'][key] = failure_node

    def _process_add_host_failure_instance_level(self, instance, host):
        logger.debug("Adding host %s to instance %s.", host, instance)
        if not self._resilience_graph.has_node(instance.name):
            logger.info("Instance not in graph: %s. Not adding allocation", instance)
            return

        if not self._resilience_graph.has_node(host.ip_address):
            self._resilience_graph.add_node(host.ip_address,
                                            node=HostPerformanceNode(app_node=host))

        if host:
            key = (FailureType.Crash, host.ip_address)
            failure_node = self.failure_nodes.get(key)
            if not failure_node:
                failure_node = FailureNode(host, FailureDomain.Host, failure_type=FailureType.Crash)
                self.failure_nodes[key] = failure_node
            # edge['events'][key] = failure_node
            edge = self._resilience_graph.add_edge(instance.name, host.ip_address, events={
                key: failure_node
            })

            logger.debug("Added attributes. edge: %s", edge)

    def _process_remove_instance(self, instance):
        logger.temp("Process remove instance: %s type=%s", instance, type(instance))
        service = instance.get_service()
        if self._resilience_graph.has_node(instance.name):
            node = self._resilience_graph.node[instance.name]
            successors = self._resilience_graph.successors(instance.name)
            predecessors = self._resilience_graph.predecessors(instance.name)
            logger.temp("Deleting node: %s", node)
            logger.temp("Successors: %s", successors)

            # delete node
            self._resilience_graph.remove_node(instance.name)
            # remove from failure dict
            del self.failure_nodes[(FailureType.Crash, instance.name)]

            # for edge in self._resilience_graph.in_edges(instance.name, data=True):
            #     del edge['events'][(FailureType.Crash, instance.name)]

            # for successors (hosts)
            for host in successors:
                logger.temp("Check host: %s", host)
                logger.temp("With edges: %s", self._resilience_graph.in_edges(host))
                logger.temp("Failures: %s", self.failure_nodes.get((FailureType.Crash, host)))
                # check if number of edges greater than 0
                if not self._resilience_graph.in_edges(host):
                    # if so, delete node and delete form failure
                    self._resilience_graph.remove_node(host)
                    del self.failure_nodes[(FailureType.Crash, host)]

                # for edge in self._resilience_graph.in_edges(instance.name, data=True):
                #     del edge['events'][(FailureType.Crash, host)]

    def update_add_instance(self, id_logical, id_virtual, spec, **kwargs):
        service = self._app_graph.get_service(id_logical)
        instance = self._app_graph.get_vm(id_virtual)
        self._process_add_instance(service, instance)
        logger.debug("Updated graph with instance=%s graph=%s", id_virtual, self)

    def update_add_allocation(self, id_virtual, host_ip):
        instance = self._app_graph.get_vm(id_virtual)
        service = instance.get_service()
        host = self._app_graph.get_host(host_ip)

        if service.subtype in {LogicalNodeSubtype.service, LogicalNodeSubtype.lb}:
            self._process_add_host(instance, host)

    def update_remove_node(self, node_id):
        node = self._app_graph.get_node(node_id)
        logger.temp("Update remove node: %s", node)
        if isinstance(node, VirtualNode):
            self._process_remove_instance(node)
        elif isinstance(node, PhysicalNode):
            # we will ignore the removal of physical nodes for now.
            pass

    def to_dict(self):

        d = node_link_data(self._resilience_graph)
        for node in d['nodes']:
            node.update(node['node'].to_dict())
            del node['node']
        logger.detail(self.failure_nodes)
        d['failures'] = [f.to_dict() for f in self.failure_nodes.values()]
        for link in d['links']:
            events = link.get('events')
            if events:
                link['events'] = [f.eid for f in link['events'].values()]
        # logger.temp('d: %s', d)

        # TODO: should not compute it every time ?
        res = {}
        for service in self._app_graph.get_services([LogicalNodeSubtype.service, LogicalNodeSubtype.lb]):
            res[service.name] = {
                'cold': self._estimator.get_deployment_time(InstanceState.Cold, service).total_seconds(),
                'warm': self._estimator.get_deployment_time(InstanceState.Warm, service).total_seconds(),
                'hot': self._estimator.get_deployment_time(InstanceState.Hot, service).total_seconds(),
            }
        d['deployment_estimator'] = res

        d['configuration'] = self._configuration.to_dict()
        logger.temp("resilience graph: %s", self)
        logger.temp("dict: %s", d)
        return d

    def __str__(self):
        return "ResilienceGraph : {}".format(self._resilience_graph.nodes(data=True))


# TODO:
# 1. create the graph with resilience nodes (top node is no failures): if we did not consider performance, it would be 1
# 2. Each node is resilience node associated with one failure. Compute resilience. if we did not consider performance, it would be either 1 or 0
# 3. Introduce manual function of performance
# 4. Make sure you build the graph and adapt on new and old nodes
# 5. Work on the interface to show the graphs: app graph, failure tree, if possible some connection between the two
#   (highlighting for instance),
# 6. Work on scale up with failure constraints (required + desired) -> gives a scoring for allocation
# 7. Scale down?
# 8. Use ARIMA models to introduce workload estimation for the periods that take to get a new instance up and running
# 9. Incorporate workload predictions in the resilience computation




# Issues:
# - How to handle changes on app graph? Inheritance? Callbacks/notification?
#       * I will settle for notifications:
#           does not change behavior of app_graph,
#           does not imply overriding a bunch of methods for building the app graph,
#           does not interfere with actions on app graph,
#           notification might be useful for other modules (Graph views?)
#           this means we are implementing the observer pattern with all its limitations (need to be careful writing
#               the views, and should change to asynchronous as soon as and if posssible)
#           despite the limitations it works on our scenario as long as we keep synchronous action sort
#
# - Risk: impact on application: prob on not meeting the SLA, mttr: is a time during which the applicaiton is underperforming