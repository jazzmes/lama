__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class AppArchObserver(object):

    def update(self, method_name, *args, **kwargs):
        pass


class AppArchStrategy(object):

    def add_allocation_constraints(self, spec):
        """
        Add if the view should impose any allocation constraints.
        :param spec:
        :return:
        """
        return spec
