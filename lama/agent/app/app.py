import port_for
import os
import traceback
from datetime import timedelta
from collections import namedtuple
from netaddr import IPAddress
from config.settings import PROVIDER_AG_PORT, PATH_PERSISTENCE, PROVIDER_AG_WEB_PORT
from lama.agent.app.app_arch import AppArch
from lama.agent.app.management import AppController
from lama.agent.base import LamaAgent, AgentAddress, Repeat
from lama.agent.event_managers import twisted
from lama.agent.events import Event, EventType
from lama.agent.notification import Notification
from lama.agent.state_machine import StateMachine, State
from lama.api.event_log import EventLog, Category
from lama.utils.lama_collections import PersistentWrapper
from lama.utils.logging_lama import logger
from lama.utils.snippets import get_ip_address

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'


AuthConfigParams = namedtuple('AuthConfigParams', ['max_retries', 'short_retry_timeout', 'long_retry_timeout'])


class AppAgentConfiguration(object):
    sla_period = timedelta(minutes=60)
    sla_performance_index = 0.9
    sla_miss_fraction = 0.01

    def __init__(self, **kwargs):
        if kwargs:
            self.update(**kwargs)

    def update(self, **kwargs):
        for k, v in kwargs.items():
            if hasattr(self, k):
                if k == 'sla_period':
                    self.sla_period = timedelta(seconds=v)
                else:
                    setattr(self, k, v)

    def to_dict(self):
        return {
            'sla_period': self.sla_period.total_seconds(),
            'sla_performance_index': self.sla_performance_index,
            'sla_miss_fraction': self.sla_miss_fraction
        }

    def __str__(self):
        return ', '.join('{}={}'.format(k, v) for k, v in self.to_dict().items())


class AppAgent(LamaAgent):

    AuthConfig = AuthConfigParams(3, 5, 60)

    def __init__(self, provider_ip, nid, filename, port, controller_ip, controller_port):
        super().__init__()
        self.initialize_log_file("app_general")

        self.configuration = AppAgentConfiguration()

        # determine agent's port
        try:
            port = int(port)
            logger.info("App agent configured with port %s", port)
        except ValueError:
            raise AttributeError("Invalid port: %s", port)
        if port == 0:
            port = port_for.select_random()
            logger.info("App agent using random port %s", port)

        self.controller_ip = IPAddress(controller_ip)
        try:
            self.controller_port = int(controller_port)
        except ValueError:
            raise AttributeError("Invalid controller port: %s" % controller_port)

        logger.info("App controller: %s:%s", controller_ip, controller_port)

        # set up a state machine to manage agent's workflow
        self.fsm = StateMachine(InitializationState, identifier=self.__class__.__name__, agent=self)

        # agent's properties
        self.nid = nid
        self.filename = filename
        self.app = None
        self.ip_address, self.prefix = get_ip_address()
        self.address = AgentAddress(ip=self.ip_address, port=port)

        self.event_log = EventLog("app-@%s:%s" % (self.ip_address, port))

        # set up local provider: main hub
        self.local_provider = AgentAddress(ip=provider_ip, port=PROVIDER_AG_PORT)

        # configure communication platform and schedule first event (start of the FSM)
        self.notifications = Notification(self.fsm.event)
        self.event_handler = self.notifications.event_handler
        self.set_event_manager_platform(
            twisted.EventManager(),
            initial_function=self.fsm.start,
            event_handler=self.notifications.event_handler,
            tcp_port=port)

        self.app_controller = None

        self._current_resource_requests = set()

    def respond(self, handler, event):
        logger.detail("Respond with event '%s'", event)
        if handler:
            handler(event)
        else:
            self.send_event_reliably(
                self.local_provider,
                event
            )


class BaseState(State):

    def __init__(self, agent):
        """
        @type agent:AppAgent
        """
        self.agent = agent


class InitializationState(BaseState):

    def enter(self, event, prev_state):
        # load the app from config file (to get at least the app_name)
        logger.info("Agent starting from file: %s", self.agent.filename)
        app = AppArch.from_filename(self.agent.filename)

        self.agent.initialize_log_file("app_%s" % app.pname)
        # load app from state
        self.agent.app = PersistentWrapper(
            app,
            filename=os.path.join(PATH_PERSISTENCE, "app_arch_" + app.pname + ".db")
        )

        logger.info("Started application - %s @ %s:%s\nApp architecture: %s",
                    self.agent.app.name,
                    self.agent.ip_address,
                    self.agent.address.port,
                    self.agent.app)

        self.agent.configuration.update(**self.agent.app.extra.get('sla', {}))
        logger.info("App SLA configuration: %s", self.agent.configuration)

        if not self.agent.app_controller:
            self.agent.app_controller = AppController(self.agent)

        self.agent.event_log = EventLog("app-%s-%s" % (self.agent.app.name, self.agent.ip_address))
        self.agent.event_log.add_event(
            "app-%s" % self.agent.app.name,
            Category.Management,
            "AppAgentCreated" if self.agent.app.new else "AppAgentRestart")
        return AuthState


class AuthState(BaseState):
    """
    Authentication state: authenticates the app agent at the local provider.
    """
    def __init__(self, agent):
        """@type agent: AppAgent"""
        super().__init__(agent)
        self.recurrent_auth = None

    def enter(self, event, prev_state):
        event = Event(
            EventType.AppAuthRequestEvent,
            self.agent.app.name,
            os.getpid(),
            self.agent.address.port)

        self.recurrent_auth = self.agent.recurring(
            self.agent.send_event_with_response,
            (self.agent.local_provider, event),
            Repeat(number=3, wait=3), Repeat(number=float("inf"), wait=60))

    # noinspection PyUnusedLocal
    def process_app_auth_response_event(self, result, agent_id, **context):
        if result:
            logger.info("App authenticated. Agent ID = %s" % agent_id)
            self.recurrent_auth.stop()
            return ActiveState


class ActiveState(BaseState):
    """
    Adaptive state: analyzes app and requests resource allocation changes to local provider.
    """

    def __init__(self, agent):
        super().__init__(agent)
        logger.debug("App agent in Active state")

    def enter(self, event, prev_state):
        self.agent.app_controller.start()

    def process_app_info_query_event(self, **context):
        handler = context.get("response_handler")
        app_info = self.agent.app_controller.get_app_info()
        self.agent.respond(handler, Event(EventType.AppInfoResponseEvent, self.agent.app.name, app_info))

    def process_app_create_image_event(self, service_name, size, init, **context):
        handler = context.get("response_handler")

        img_name, allocated = self.agent.app_controller.add_image(service_name, size, init)
        if allocated:
            self.app_create_image_response(handler, img_name)
        else:
            self.agent.notifications.subscribe(EventType.AppServiceAllocatedEvent, conditions={"service_name": img_name},
                                               cb=lambda event: self.app_create_image_response(handler, img_name))

    def app_create_image_response(self, handler, img_name):
        vms = self.agent.app.get_service(img_name).get_instances()
        addresses = {vm.name: (vm.get_host().ip_address, PROVIDER_AG_WEB_PORT) for vm in vms}
        self.agent.respond(handler, Event(EventType.AppCreateImageResponseEvent, self.agent.app.name, img_name, addresses))

    def process_vm_allocated_event(self, vm_name, provider_address, resources, **context):
        self.agent.app_controller.process_allocated_instance(vm_name, provider_address, resources)

    def process_app_image_uploaded_event(self, app_name, service_name, instance_name, init, **context):
        self.agent.app_controller.register_image_uploaded(service_name, instance_name)

    def process_app_start_event(self, app_name, **context):
        result, msg = self.agent.app_controller.start_app()
        client_address = context.get("address")
        self.agent.send_event_reliably(
            AgentAddress(client_address, PROVIDER_AG_PORT),
            Event(EventType.AppStartResponseEvent, app_name, result, msg)
        )

    def process_provider_deleted_latent_notification(self, app_name, vm_name, **context):
        logger.temp("Process Deleted Latent: %s:%s", app_name, vm_name)
        self.agent.app_controller.remove_latent_instance(vm_name)

    def process_app_change_workload_event(self, app_name, kwargs, **context):
        handler = context.get("response_handler")

        config, workload_spec = map(kwargs.get, ('config', 'workload_spec'))
        try:
            if config:
                result, msg = self.agent.app_controller.change_workload_by_clients(config)

            elif workload_spec:
                result, msg = self.agent.app_controller.change_workload_by_spec(workload_spec)
            else:
                result = False
                msg = "Need to specify 'clients' or 'workload_spec' options."

        except Exception as e:
            logger.warning("Exception: %s", traceback.format_exc())
            result = False
            msg = "%s" % e
        except NotImplementedError:
            result = False
            msg = "Not implemented: change workload by 'n_clients'"

        self.agent.respond(handler, Event(EventType.AppChangeWorkloadResponseEvent, app_name, result, msg))

    def process_app_status_data_query_event(self, options, **context):
        handler = context.get("response_handler")

        try:
            results = {
                "success": True,
                "app_status": self.agent.app_controller.monitor.get_arch_json(**{k: options[k] for k in options}),
                "resilience_graph": self.agent.app_controller.management_strategy.to_dict()
            }
        except:
            logger.error("Traceback: %s", traceback.format_exc())
            results = {
                'success': False
            }

        self.agent.respond(
            handler,
            Event(EventType.AppStatusDataResponseEvent, self.agent.app.name, results)
        )

