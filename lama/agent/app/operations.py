import socket
import traceback
from functools import partial

import demjson
from collections import namedtuple
from datetime import datetime
from config.settings import PROVIDER_AG_PORT
from lama.agent.app.app_arch import LogicalNodeSubtype, InstanceState
from lama.agent.app.connectors.app_connector import VirtualIPConfigurator
from lama.agent.app.recovery.estimator import DeploymentPhase
from lama.agent.base import AgentAddress, Repeat
from lama.agent.event_managers.twisted import run_in_thread
from lama.agent.events import Event, EventType
from lama.api.event_log import Category
from lama.utils.logging_lama import logger
from lama.agent.app.connectors import app_connector as conns

__author__ = 'Tiago'

LaunchResult = namedtuple("LaunchResult", ["success", "message"])


def get_connector_class(service):
    try:
        connector_name = service.extra.get("connector")
        if not connector_name:
            connector_name = "%sConnector" % service.subtype.name.capitalize()
        connector = getattr(conns, connector_name)
        return connector
    except AttributeError:
        logger.debug("Connector was not defined for service: %s", service.name)
        return


class ConnectorWrapper(object):
    def __init__(self, app, connector):
        self._app = app
        self._connector = connector

    def get_instance_connector(self, instance):
        try:
            service = instance.get_service()
            return get_connector_class(service)(self._app.name, instance, **service.extra)
        except:
            pass

    def stop_instance(self, instance):
        (self.get_instance_connector(instance) or self._connector).stop_instance(instance)

    def start_instance(self, instance):
        (self.get_instance_connector(instance) or self._connector).start_instance(instance)

    def update_instance(self, instance):
        (self.get_instance_connector(instance) or self._connector).update_instance(instance)


class Operation(object):
    controller = None
    publisher = None
    estimator = None

    def __init__(self, instance, connector):
        assert Operation.controller is not None, "Controller for operations not configured"
        self.notifications = self.controller.agent.notifications

        self.instance = instance
        self.connector = ConnectorWrapper(self.controller.app, connector) if connector else None

    @staticmethod
    def check_ssh_by_ip_async(ip, callback, err_callback=None):

        def connect_ssh(ip):
            logger.debug("Check ssh in instance: %s", ip)
            if not ip:
                return None
            sock = None
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.connect((str(ip), 22))
                logger.debug("[%s] Connected to instance", ip)
                return True
            except Exception as e:
                raise ConnectionError("[{}] Not connected to instance: {}".format(ip, e))
            finally:
                if sock:
                    sock.close()
        logger.temp("SSH in thread")
        run_in_thread(partial(connect_ssh, ip), callback=callback, err_callback=err_callback)
        logger.temp("SSH after call thread")

    def preprocess_remove_instance(self):
        logger.info("Remove[%s:%s] VM is active: update configuration", self.instance.name, self.instance.ip)

        logger.temp("Connector: %s", self.connector)
        if self.connector:
            try:
                self.connector.stop_instance(self.instance)
            except:
                logger.error("Error using connector: %s", traceback.format_exc())

        service = self.instance.get_service()
        scaling = service.extra.get("scaling")
        if scaling:
            #   2.1. if auto, call post_processing_scaling_auto:
            #       dependencies are computed automatically will update the load balancer
            if scaling == "auto":
                logger.temp("Remove[%s:%s] Scaling out post processing", self.instance.name, self.instance.ip)
                # get the lb service
                pre_service = [n for n in service.get_all_predecessors() if n.subtype == LogicalNodeSubtype.lb]
                if len(pre_service) == 1:
                    pre_service = pre_service[0]
                else:
                    logger.error("Remove[%s:%s] Wrong number of LBs: %d (%s)", self.instance.name, self.instance.ip,
                                 len(pre_service), pre_service)
                    return

                logger.temp("Remove[%s:%s] Predecessor service: %s", self.instance.name, self.instance.ip, pre_service)
                for pre_instance in pre_service.get_instances():
                    logger.temp("Remove[%s:%s] Predecessor instance: %s", self.instance.name, self.instance.ip,
                                pre_instance)
                    Operation.check_ssh_by_ip_async(
                        pre_instance.ip,
                        callback=lambda x: self.use_connector_async("update_by_vm", pre_instance, method_args=(pre_instance.ip,)),
                        err_callback=lambda x: logger.warn(
                            "Remove[%s:%s] Instance %s not accessible! Skip.",
                            self.instance.name,
                            self.instance.ip,
                            pre_instance
                        )
                    )
            else:
                logger.error("Non-'auto' scaling was not implemented yet!")

    def use_connector_async(self, method, instance, attempts=1, max_attempts=100, method_args=(), conn_class=None):

        def use_connector_success(*args, **kwargs):
            logger.temp("Use connector terminated successfully: %s %s", args, kwargs)

        def use_connector_error(*args, **kwargs):
            logger.temp("Use connector failed: %s %s", args, kwargs)

        run_in_thread(
            self.use_connector_sync, method, instance, attempts=attempts, max_attempts=max_attempts,
            method_args=method_args, conn_class=conn_class,
            callback=use_connector_success,
            err_callback=use_connector_error
        )

    def use_connector_sync(self, method, instance, attempts=1, max_attempts=100, method_args=(), conn_class=None):
        logger.temp("use_connector args: inst=%s, f=%s(%s) conn=%s %s/%s",
                    instance, method, method_args, conn_class, attempts, max_attempts)
        service = instance.get_service()
        logger.detail("Service extra info: %s", service.extra)

        if not conn_class:
            conn_class = get_connector_class(service)

        if not conn_class:
            logger.temp("Connector class for service '%s:%s' does not exist!",
                        self.controller.app.name, service.name)
            return

        # noinspection PyBroadException
        try:
            logger.temp("app_name=%s, instance=%s, conn_class=%s, method=%s", self.controller.app.name, instance,
                        conn_class, method)
            connector = conn_class(self.controller.app.name, instance, **service.extra)

            # add config if exists
            if "config" in service.extra:
                json_config = service.extra.get("config")
                # noinspection PyBroadException
                try:
                    config = demjson.decode(json_config)
                    connector.add_config(config)
                except:
                    logger.warn("Exception adding config: '%s' : %s", json_config, traceback.format_exc())

            getattr(connector, method)(*method_args)
            return
        except (ConnectionRefusedError, OSError) as e:
            logger.warn("SSH not available yet - %s '%s'", e, instance.name)
        except AttributeError as e:
            logger.temp("Method not available in connector: %s", e)
            return
        except Exception:
            logger.error("Unrecognized error configuring '%s': %s", instance.name, traceback.format_exc())

        if attempts < max_attempts:
            logger.debug("Trying again in 5...")
            self.controller.agent.event_manager.schedule(5,
                                                         self.use_connector_async,
                                                         method, instance, attempts + 1, max_attempts=max_attempts,
                                                         method_args=method_args)
        else:
            logger.error("Giving up! Last traceback: %s", traceback.format_exc())


class LaunchInstance(Operation):
    def __init__(self, instance, init=True, post_processing_callback=None, connector=None,
                 run_default_post_processing=True):
        super().__init__(instance, connector)

        # controller service should have:
        # - app_arch (application architecture)
        # - notifications (notification system)
        # - setup network
        # - dns object
        # - set_deployed_vm
        # - publish_vms_info
        # - publish_app
        # - agent (within agent: send_event_reliably, local_provider)
        # - address manager
        # TODO: reduce this dependency

        self.image_location = None
        self.host_ip = None
        self.init = init
        self.run_default_post_processing = run_default_post_processing
        self.post_processing_callback = post_processing_callback

    def start(self):
        try:
            self.estimator.add_start_timestamp(DeploymentPhase.Configuration, datetime.now(), self.instance)

            logger.info("Launch[%s:%s] Launching instance", self.instance.name, self.instance.ip)
            logger.info("\t- post proc cb: %s", self.post_processing_callback)
            logger.info("\t- connector: %s", self.connector)
            # initial setup
            result = self.setup_network()
            logger.temp("Launch[%s:%s] result after setup_network", self.instance.name, self.instance.ip)
            result = result or self.check_image()
            result = result or self.check_host()

            # launch request
            if not result:
                self.notifications.subscribe(
                    EventType.ProviderStartInstanceResponseEvent,
                    conditions={
                        "app_name": self.controller.app.name,
                        "service_name": self.instance.get_service().name,
                        "instance_name": self.instance.name
                    },
                    cb=self.process_instance_deployed
                )

                result = self.launch()

            return result
        except Exception as e:
            logger.error("Launch[%s:%s] Unexpected exception: %s", self.instance.name, self.instance.ip,
                         traceback.format_exc())
            return LaunchResult(success=False, message="Unexpected exception: %r" % e)

    def setup_network(self):
        service = self.instance.get_service()
        self.controller.setup_network()
        changes = False
        if not self.instance.ip or not self.instance.mac:
            addresses = self.controller.address_manager.get_instance_address(service.name, self.instance.name)
            if not addresses:
                return LaunchResult(success=False, message="Unable to get addresses")
            else:
                ip, mac = addresses
                self.instance.set_addresses(ip=ip, mac=mac)
                self.controller.dns.add_host(self.instance.name, self.instance.mac, self.instance.ip)

                self.controller.app.save()
                changes = True

        port = service.extra.get("scaling_port")
        if port:
            # if port and port is not 80:
            parent_lb_service = self.instance.get_parent_lb_service()
            logger.temp("Parent service: %s", parent_lb_service)
            if parent_lb_service:
                logger.temp("Add accept ip")
                logger.temp("Service ip: %s", self.controller.address_manager.get_virtual_ip(parent_lb_service.name))
                self.instance.accept_ip = self.controller.address_manager.get_virtual_ip(parent_lb_service.name)
                logger.temp("Added accept ip: %s", self.instance.accept_ip)
            changes = True
        elif service.subtype == LogicalNodeSubtype.lb and not self.instance.virtual_ip:
            child_service = service.get_successors()
            if child_service:
                child_service = child_service[0]
            port = child_service.extra.get("scaling_port")
            # if port and port is not 80:
            if port:
                # if not service.extra.get("virtual_ip"):
                logger.temp("Add virtual ip")
                self.instance.virtual_ip = self.controller.address_manager.get_virtual_ip(service.name)
                # self.instance.virtual_mac = self.controller.address_manager.get_mac_address()
                # self.controller.dns.add_host(self.instance.name, self.instance.virtual_mac,
                #                              self.controller.address_manager.get_virtual_ip(service.name))
                # service.set_extra("virtual_ip", self.controller.address_manager.get_virtual_ip(service.name))
                changes = True

        if changes:
            self.controller.publish_vms_info(vm_name=self.instance.name)
            self.controller.publish_app()

        logger.info("Launch[%s:%s] Instance addresses: ip=%s, mac=%s %s", self.instance.name, self.instance.ip,
                    self.instance.ip, self.instance.mac,
                    " (vip=%s)" % service.extra.get("virtual_ip") if service.extra.get("virtual_ip") else "")

    def check_image(self):
        service = self.instance.get_service()
        image = service.get_image_service(init=self.init)
        logger.detail("Launch[%s:%s] Image: %s", self.instance.name, self.instance.ip, image)
        if not image:
            return LaunchResult(success=False, message="Image service not found!")
        image_instances = image.get_instances()
        if len(image_instances):
            # TODO: Image services are not currently allowed to scale
            self.image_location = image_instances[0].get_host().ip_address
            self.init = self.init and image.extra.get("init", False)
        else:
            return LaunchResult(success=False, message="Image service has no instances!")
        logger.info("Launch[%s:%s] Instance image location: %s : %s", self.instance.name, self.instance.ip,
                    self.image_location, self.init)

    def check_host(self):
        host = self.instance.get_host()
        if not host:
            return LaunchResult(success=False, message="No host found - Instance not allocated")

        self.host_ip = host.ip_address
        logger.info("Launch[%s:%s] Instance target host: %s", self.instance.name, self.instance.ip, self.host_ip)

    def launch(self):
        logger.info("Launch[%s:%s] Launch VM", self.instance.name, self.instance.ip)
        logger.info("Launch[%s:%s] \tMAC: %s", self.instance.name, self.instance.ip, self.instance.mac)
        if self.instance.virtual_ip:
            logger.info("Launch[%s:%s] \tVirtual IP: %s", self.instance.name, self.instance.ip,
                        self.instance.virtual_ip)
        if self.instance.accept_ip:
            logger.info("Launch[%s:%s] \tAccept IP: %s", self.instance.name, self.instance.ip, self.instance.accept_ip)

        self.controller.agent.event_log.add_event(
            "app-%s" % self.controller.app.name,
            Category.Management,
            "VMStarted",
            description="VM: %s" % self.instance.name
        )

        if not self.host_ip or not self.image_location:
            return LaunchResult(
                success=False,
                message="Did not get all details: host=%s, image=%s" % (
                    self.host_ip, self.image_location
                )
            )

        logger.temp("Launch[%s:%s] Send ProviderStartInstanceEvent to %s", self.instance.name, self.instance.ip,
                    self.host_ip)
        self.estimator.add_end_timestamp(DeploymentPhase.Configuration, datetime.now(), self.instance)
        self.estimator.add_start_timestamp(DeploymentPhase.DeployWithImageCopy, datetime.now(), self.instance)

        self.controller.agent.send_event_reliably(
            AgentAddress(self.host_ip, PROVIDER_AG_PORT),
            Event(
                EventType.ProviderStartInstanceEvent,
                self.controller.app.name,
                self.instance.get_service().name,
                self.instance.name,
                self.controller.agent.controller_ip,
                self.controller.agent.controller_port,
                self.instance.mac,
                self.image_location,
                self.init,
                self.instance.state,
                self.instance.virtual_mac
            )
        )
        return LaunchResult(success=True, message="Instance start request processed.")

    def process_instance_deployed(self, event):
        self.estimator.add_end_timestamp(DeploymentPhase.DeployWithImageCopy, datetime.now(), self.instance)
        self.estimator.add_start_timestamp(DeploymentPhase.StartUp, datetime.now(), self.instance)

        logger.info("Launch[%s:%s] Start Instance request successfully received",
                    self.instance.name, self.instance.ip)
        if self.instance.state == InstanceState.Active and not self.instance.active_ts:
            self.instance.active_ts = datetime.now()

            logger.debug("Launch[%s:%s] Publish instance active", self.instance.name, self.instance.ip)
            self.publisher.publish(
                "lama_events:active_instances",
                "|".join((
                    self.controller.app.name, self.instance.id, self.instance.name,
                    str(self.instance.get_host().ip_address), str(self.controller.agent.local_provider.ip_address),
                    str(self.instance.active_ts)
                ))
            )

            self.wait_instance_active(event)
        else:
            logger.info("Launch[%s:%s] Instance state is %s - No post processing",
                        self.instance.name, self.instance.ip, self.instance.state.name)

    # noinspection PyUnusedLocal
    def wait_instance_active(self, event):
        # check if instance is alive
        logger.temp("Launch[%s:%s] Callback: %s", self.instance.name, self.instance.ip, self.post_processing_callback)

        # if self.post_processing_callback:
        # self.controller.agent.run_async(
        #     partial(self.check_ssh, self.instance.ip),
        #     partial(self.post_processing_callback, event)
        # )
        # self.controller.agent.run_async(
        #     self.check_ssh,
        #     self.post_check_ssh,
        #     cb_args=(event,)
        # )
        run_in_thread(
            partial(
                self.controller.agent.recurring,
                self.check_ssh,
                (),
                Repeat(1000, 5), Repeat(1, 5),
                on_true=partial(self.post_check_ssh, True),
                on_true_args=(event,),
                on_false=partial(self.post_check_ssh, False),
                on_false_args=(event,)
            )
        )
        # self.controller.agent.recurring(
        #     self.check_ssh,
        #     (),
        #     Repeat(1000, 5), Repeat(1, 5),
        #     on_true=partial(self.post_check_ssh, True),
        #     on_true_args=(event,),
        #     on_false=partial(self.post_check_ssh, False),
        #     on_false_args=(event,)
        # )

    # noinspection PyUnusedLocal
    def post_processing(self, event):
        logger.info("Launch[%s:%s] VM is active: update configuration", self.instance.name, self.instance.ip)
        self.controller.set_deployed_vm(self.instance.name)

        logger.info("Launch[%s:%s] virtual_ip=%s, accept_ip=%s", self.instance.name, self.instance.ip,
                    self.instance.virtual_ip, self.instance.accept_ip)
        # check if we need to configure a virtual ip
        if self.instance.virtual_ip or self.instance.accept_ip:
            self.use_connector_async("start_by_vm", self.instance, conn_class=VirtualIPConfigurator,
                               method_args=(self.instance.ip,))

        service = None
        try:
            service = self.instance.get_service()
        except:
            pass

        if not service:
            logger.info("Launch[%s:%s] Service not found, instance must have been deleted. Stop processing...",
                        self.instance.name, self.instance.ip)
            return

        conn_class = get_connector_class(service)
        if self.connector:
            try:
                self.connector.start_instance(self.instance)
            except:
                logger.error("Error using connector: %s", traceback.format_exc())

        service = self.instance.get_service()
        scaling = service.extra.get("scaling")
        if scaling:
            #   2.1. if auto, call post_processing_scaling_auto:
            #       dependencies are computed automatically will update the load balancer
            if scaling == "auto":
                logger.temp("Launch[%s:%s] Scaling out post processing", self.instance.name, self.instance.ip)
                # get the lb service
                pre_service = [n for n in service.get_all_predecessors() if n.subtype == LogicalNodeSubtype.lb]
                if len(pre_service) == 1:
                    pre_service = pre_service[0]
                else:
                    logger.error("Launch[%s:%s] Wrong number of LBs: %d (%s)", self.instance.name, self.instance.ip,
                                 len(pre_service), pre_service)
                    return

                logger.temp("Launch[%s:%s] Predecessor service: %s", self.instance.name, self.instance.ip, pre_service)
                for pre_instance in pre_service.get_instances():
                    logger.temp("Launch[%s:%s] Predecessor instance: %s", self.instance.name, self.instance.ip,
                                pre_instance)
                    LaunchInstance.check_ssh_by_ip_async(
                        pre_instance.ip,
                        callback=lambda x: self.use_connector_async("update_by_vm", pre_instance, method_args=(pre_instance.ip,)),
                        err_callback=lambda x: logger.warn(
                            "Launch[%s:%s] Instance %s not accessible yet! Skip.",
                            self.instance.name,
                            self.instance.ip,
                            pre_instance
                        )
                    )
            else:
                logger.error("Non-'auto' scaling was not implemented yet!")
        else:
            logger.temp("Launch[%s:%s] Not scaling: update predecessors", self.instance.name, self.instance.ip)
            for pre_service in service.get_all_predecessors():
                logger.temp("Launch[%s:%s] Predecessor service: %s", self.instance.name, self.instance.ip, pre_service)
                for pre_instance in pre_service.get_instances():
                    logger.temp("Launch[%s:%s] Predecessor instance: %s", self.instance.name, self.instance.ip,
                                pre_instance)
                    LaunchInstance.check_ssh_by_ip_async(
                        pre_instance.ip,
                        callback=lambda x: self.connector.update_instance(pre_instance) if self.connector else None,
                        err_callback=lambda x: logger.warn(
                            "Launch[%s:%s] Instance %s not accessible yet! Skip.",
                            self.instance.name,
                            self.instance.ip,
                            pre_instance
                        )
                    )
                    # if LaunchInstance.check_ssh_by_ip(pre_instance.ip):
                    #     if self.connector:
                    #         self.connector.update_instance(pre_instance)
                    # else:
                    #     logger.info("Launch[%s:%s] Instance %s not accessible yet! Skip.", self.instance.name,
                    #                 self.instance.ip,
                    #                 pre_instance)

    # def check_ssh(self, block=True, max_attempts=1000):
    #     n = 0
    #     while n < max_attempts:
    #         connected = self.check_ssh_by_ip(self.instance.ip)
    #         logger.temp("Launch[%s:%s] block=%s, connected=%s", self.instance.name, self.instance.ip, block, connected)
    #         if connected:
    #             return True
    #         elif block:
    #             logger.temp("Launch[%s:%s] Instance not accessible. Will repeat in 5s", self.instance.name,
    #                         self.instance.ip)
    #             sleep(5)
    #         else:
    #             return connected
    #         n += 1

    def check_ssh(self, block=True):

        def check_ssh_by_ip_sync(ip):

            logger.debug("Check ssh in instance: %s", ip)
            if not ip:
                return None
            sock = None
            try:
                # sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock = socket.create_connection((str(ip), 22), 0.2)
                # sock.connect((str(ip), 22))
                logger.debug("[%s] Connected to instance", ip)
                return True
            except Exception as e:
                logger.warn("[%s] Not connected to instance: %s", ip, e)
                return False
            finally:
                if sock:
                    sock.close()

        connected = check_ssh_by_ip_sync(self.instance.ip)
        logger.temp("Launch[%s:%s] block=%s, connected=%s", self.instance.name, self.instance.ip, block, connected)
        if not connected:
            logger.temp("Launch[%s:%s] Instance not accessible. Will repeat in 5s", self.instance.name,
                        self.instance.ip)
        return not block or connected

    def post_check_ssh(self, connected, event):
        logger.temp("Launch[%s:%s] SSH %s", self.instance.name, self.instance.ip,
                    "ENABLED" if connected else "DISABLED")

        if self.run_default_post_processing:
            self.post_processing(event)

        self.instance.ready_ts = datetime.now()

        if self.post_processing_callback:
            try:
                self.post_processing_callback(event)
            except:
                logger.error("Error running post_processing_callback: %s", traceback.format_exc())

        # publish ready instance
        if self.publisher:
            logger.debug("Launch[%s:%s] Publish instance ready", self.instance.name, self.instance.ip)
            self.publisher.publish(
                "lama_events:ready_instances",
                "|".join((
                    self.controller.app.name, self.instance.id, self.instance.name,
                    str(self.instance.get_host().ip_address),
                    str(self.controller.agent.local_provider.ip_address),
                    str(self.instance.ready_ts)
                ))
            )
        self.estimator.add_end_timestamp(DeploymentPhase.StartUp, datetime.now(), self.instance)
        self.controller.management_strategy.notify_instance_ready()


class ChangeInstanceState(Operation):
    def __init__(self, instance, post_processing_callback=None, connector=None):
        super().__init__(instance, connector)
        self.post_processing_callback = post_processing_callback

    def start(self, next_state):
        # raise NotImplementedError()

        # 3 downward cases:
        # * Active -> Hot: suspend machine + pre/post_processing
        # * Active -> Warm: shutdown machine... or destroy + pre/post_processing
        # * Hot -> Warm: shutdown machine
        old_state = self.instance.state
        if old_state == next_state:
            logger.error("Requested change of state to SAME state: %s", old_state.name)
            return
        logger.temp("Change state operation: %s:%s -> %s:%s", old_state.value, old_state, next_state.value, next_state)
        # going more active
        if next_state.value < old_state.value:
            self.instance.state = next_state
            self.controller.publish_app()

            if next_state == InstanceState.Active:
                li = LaunchInstance(self.instance, connector=self.connector,
                                    post_processing_callback=self.post_processing_callback)
                li.start()
            else:
                self.notifications.subscribe(
                    EventType.ProviderChangeInstanceStateResponseEvent,
                    conditions={
                        "app_name": self.controller.app.name,
                        # "service_name": service.name,
                        "instance_name": self.instance.name
                    },
                    cb=self.process_instance_changed_state
                )

                e = Event(
                    EventType.ProviderChangeInstanceStateEvent,
                    self.controller.app.name,
                    self.instance.get_service().name,
                    self.instance.name,
                    old_state,
                    next_state
                )
                logger.temp("Sending event to change state: %s", e)
                self.controller.agent.send_event_reliably(
                    AgentAddress(self.controller.agent.local_provider.ip_address, PROVIDER_AG_PORT), e
                )

                # raise NotImplementedError("Upward cases not implemented yet")
        else:
            if self.instance.state == InstanceState.Active:
                self.preprocess_remove_instance()

            self.instance.state = next_state
            self.controller.publish_app()

            self.notifications.subscribe(
                EventType.ProviderChangeInstanceStateResponseEvent,
                conditions={
                    "app_name": self.controller.app.name,
                    # "service_name": service.name,
                    "instance_name": self.instance.name
                },
                cb=self.process_instance_changed_state
            )

            self.controller.agent.send_event_reliably(
                AgentAddress(self.controller.agent.local_provider.ip_address, PROVIDER_AG_PORT),
                Event(
                    EventType.ProviderChangeInstanceStateEvent,
                    self.controller.app.name,
                    self.instance.get_service().name,
                    self.instance.name,
                    old_state,
                    self.instance.state
                )
            )

    # noinspection PyUnusedLocal
    def process_instance_changed_state(self, event):
        logger.info("Change State[%s:%s] Instance state is %s - No post processing",
                    self.instance.name, self.instance.ip, self.instance.state.name)


class RemoveInstance(Operation):
    def __init__(self, instance, connector=None):
        super().__init__(instance, connector)
        self.service = instance.get_service()

    def start(self):
        # get dependent services
        # service = self.instance.get_service()
        # lb_service = self.instance.get_parent_lb_service()
        # host = self.instance.get_host()

        logger.info("About to remove: %s", self.instance.name)

        self.publisher.publish(
            "lama_events:removed_instances",
            "|".join((
                self.controller.app.name, self.instance.id, self.instance.name,
                str(self.instance.get_host().ip_address), str(self.controller.agent.local_provider.ip_address),
                str(datetime.now())
            ))
        )

        # warn connector that instance is being stopped
        if self.connector:
            try:
                self.connector.stop_instance(self.instance)
            except:
                logger.error("Error using connector: %s", traceback.format_exc())

        # delete from DNS
        self.controller.dns.remove_host(self.instance.name, self.instance.mac, self.instance.ip)
        # delete from Redis
        self.controller.unpublish_vms_info(self.instance)
        # release IP address
        # TODO: release IP once we make RYU adapt... use redis as communication
        # self.controller.address_manager.release_ip(self.instance.ip)

        # remove rules at the agent?
        logger.warn("Remove[%s:%s] Check rules at the agent", self.instance.name, self.instance.ip)

        self.notifications.subscribe(
            EventType.ProviderTerminateInstanceResponseEvent,
            conditions={
                "app_name": self.controller.app.name,
                # "service_name": service.name,
                "instance_name": self.instance.name
            },
            cb=self.process_instance_terminated
        )

        self.preprocess_remove_instance()

        # delete from app arch
        logger.temp("Remove[%s:%s] Remove node: %s", self.instance.name, self.instance.ip, self.instance.id)
        logger.temp("Type of instance.id: %s %r", type(self.instance.id), self.instance.id)
        self.controller.app.remove_node(self.instance.id)
        logger.temp("Remove[%s:%s] App arch after removal: %s",
                    self.instance.name, self.instance.ip, self.controller.app)

        self.controller.publish_app()

        # terminate instance: remove allocation, odestroy and undefine and remove local image
        self.controller.agent.send_event_reliably(
            # AgentAddress(host.ip_address, PROVIDER_AG_PORT),
            AgentAddress(self.controller.agent.local_provider.ip_address, PROVIDER_AG_PORT),
            Event(
                EventType.ProviderTerminateInstanceEvent,
                self.controller.app.name,
                # service.name,
                self.instance.name,
                self.instance.ip,
                self.instance.mac
            )
        )
        logger.debug("Leaving start...")

    # noinspection PyUnusedLocal
    def process_instance_terminated(self, event):
        logger.info("Remove[%s:%s] VM was removed update configuration", self.instance.name, self.instance.ip)
        for pre_service in self.service.get_all_predecessors():
            logger.temp("Remove[%s:%s] Predecessor service: %s", self.instance.name, self.instance.ip, pre_service)
            for pre_instance in pre_service.get_instances():
                logger.temp("Remove[%s:%s] Predecessor instance: %s", self.instance.name, self.instance.ip,
                            pre_instance)
                Operation.check_ssh_by_ip_async(
                    pre_instance.ip,
                    callback=lambda x: self.connector.update_instance(pre_instance) if self.connector else None,
                    err_callback=lambda x: logger.warn(
                        "Remove[%s:%s] Instance %s not accessible yet! Skip.",
                        self.instance.name,
                        self.instance.ip,
                        pre_instance
                    )
                )
                # if LaunchInstance.check_ssh_by_ip(pre_instance.ip):
                #     if self.connector:
                #         self.connector.update_instance(pre_instance)
                # else:
                #     logger.info(
                #         "Remove[%s:%s] Instance %s not accessible yet! Skip.", self.instance.name,
                #         self.instance.ip,
                #         pre_instance
                #     )
