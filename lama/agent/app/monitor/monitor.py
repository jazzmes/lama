from argparse import ArgumentTypeError
from importlib import import_module
from csv import writer
import traceback
import re
from datetime import datetime
from redis.client import StrictRedis
from lama.agent.app.app_arch import LogicalNodeSubtype, PhysicalNode, VirtualNode
from lama.agent.app.monitor.channels import ChannelFactory
from lama.agent.event_managers.emredis import LamaKeyStore
from lama.agent.event_managers.txredis import LamaPubSub
from lama.agent.event_managers.twisted import run_in_thread
from lama.agent.specs.resource import CpuSpec, RamSpec, DiskSpec, NetSpec
from lama.utils.flask.decorators import crossdomain
from lama.utils.logging_lama import logger
from flask import Flask, request, jsonify, abort
# noinspection PyUnresolvedReferences
import lama.agent.app.monitor.strategies

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2015, Carnegie Melon University'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

# constants
MonitorPort = 8400


class CsvHandler(object):
    def __init__(self, filename, header):
        self.csv_filename = filename
        self.header = header
        self.handler = open(self.csv_filename, mode='w')
        self.writer = writer(self.handler)

        self.prepare_file()

    def prepare_file(self):
        logger.debug("Add header to csv: %s", self.header)
        self.writer.writerow(self.header)

    def push_row(self, values):
        row = [values.get(h) for h in self.header]
        logger.debug("Add to csv: %s", row)
        self.writer.writerow(row)

    def push_rows(self, values):
        for v in values:
            self.push_row(v)

    def close(self):
        self.handler.close()


class EntityMonitor(object):
    entity_type = "Generic"

    def __init__(self, name, strategy=None, csv_handler=None, redis_handler=None):
        self.name = name
        self.strategy = strategy
        self._channels = {}
        self.csv_handler = csv_handler
        self.redis_handler = redis_handler

    def handle_metric(self, src, message):
        logger.debug("Received value @ EM[%s]: src=%s, message=%s", self.name, src, message)
        logger.warn("Override this method to process the metric")

    def add_metric_value(self, metric, arg, timestamp, values, sub_metric=None):

        # for each metric install
        # if new install metric channel
        # else add metric
        ch = self._channels.get((metric, arg))
        if not ch:
            ch = ChannelFactory.get_channel(self.name, metric, arg)
            self._channels[(metric, arg)] = ch
            # logger.debug("Create channel - name=%s, '%s' for m=%s a=%s sm=%s", self.name, ch.__class__.__name__, metric, arg, sub_metric)
            self.register_channel(ch)

            # logger.detail("Keys: %s", list(self._channels.keys()))

        # logger.temp("Will add value to channel: %s %s", self.name, ch.__class__.__name__)
        ch.add_value(timestamp, values, sub_metric)
        if self.csv_handler:
            csv_rows = []
            # for i, v in enumerate(values):
            #     csv_rows.append({
            #         "Timestamp": timestamp,
            #         "EntityType": self.entity_type,
            #         "Entity": self.name,
            #         "Metric": "%s%s%s" % (ch.metric, ":%s" % sub_metric if sub_metric else "", ":%d" % i if len(values) > 1 else ""),
            #         "Value": v
            #     })
            vs = ch.get_last_values(sub_metric)
            # logger.temp("New data: [T]%s : [e]%s : [E]%s : [M]%s : [V]%s",
            #             timestamp, self.entity_type, self.name, ch.metric, vs)
            for sm, v in vs:
                m = "%s%s%s" % (ch.metric, ":%s" % arg if arg else "", ":%s" % sm if sm else "")

                csv_rows.append({
                    "Timestamp": timestamp,
                    "EntityType": self.entity_type,
                    "Entity": self.name,
                    "Metric": m,
                    "Value": v
                })
            self.csv_handler.push_rows(csv_rows)

        if self.redis_handler:
            vs = ch.get_last_values(sub_metric)
            # if ch.metric == "response_time":
            # logger.temp("New data: T[%s] : e[%s] : E[%s] : M[%s] : SM[%s] : V[%s]",
            #             timestamp, self.entity_type, self.name, ch.metric, sub_metric, vs)
            for sm, v in vs:
                m = "%s%s%s" % (ch.metric, ":%s" % arg if arg else "", ":%s" % sm if sm else "")
                msg = "%s|%s|%s|%s|%s" % (timestamp, self.entity_type, self.name, m, v)
                # logger.temp("Publish %s : %s", "lama:metrics:%s" % m, msg)
                self.redis_handler.publish(
                    "lama:metrics:%s" % m,
                    msg
                )

    def get_dict(self, min_ts=None, metric_filters=None):
        res = {}
        # logger.debug("Entity to dict: name=%s, keys=%s", self.name, list(self._channels.keys()))

        for k, ch in self._channels.items():
            # logger.temp("processing key: %s, filters=%s", k, metric_filters)
            if not metric_filters or k[0] in metric_filters:
                if k[0] not in res:
                    res[k[0]] = {}
                # logger.temp('add values: %s', ch.get_dict(min_ts))
                res[k[0]][k[1]] = ch.get_dict(min_ts)
                # else:
                #     logger.temp("Metric filtered out: (%s, %s) set=%s", self.name, k, metric_filters)

        # logger.temp("Return res: %s", res)
        return res

    def register_channel(self, ch):
        logger.debug("Base register channel: %s", self.strategy.__class__.__name__)
        if self.strategy:
            # noinspection PyBroadException
            try:
                self.strategy.register_channel(ch)
            except:
                logger.warn("No register_channel callback in strategy")


class InstanceMonitor(EntityMonitor):
    # register metric channels
    entity_type = "Instance"

    def __init__(self, instance, **kwargs):
        super().__init__(instance["name"], **kwargs)
        logger.debug(instance)
        self.instance = instance
        self.ip = instance.get('ip', None)

    def handle_metric(self, src, message):
        # logger.debug("Received value @ IM[%s]: src=%s, message=%s", self.name, src, message)

        # get metric
        _, metric = message["channel"].split("/")
        arg = None
        try:
            metric, arg = metric.split("-")
        except ValueError:
            pass

        # get value
        f = message["data"].split(":")
        ts = datetime.fromtimestamp(float(f[0]))
        vals = [float(v) for v in f[1:]]

        logger.detail("IM[%s] Received :: metric=%s, arg=%s, ts=%s, values=%s", self.name, metric, arg, ts, vals)
        self.add_metric_value(metric, arg, ts, vals)

    def register_channel(self, ch):
        # logger.debug("Calling register channel: %s", self.strategy.__class__.__name__)
        if self.strategy:
            # noinspection PyBroadException
            try:
                self.strategy.register_instance_channel(ch)
            except:
                logger.warn("No valid register_instance_channel callback in strategy. Tb: %s", traceback.format_exc())


class HostMonitor(EntityMonitor):
    entity_type = "Host"

    def __init__(self, ip, **kwargs):
        super().__init__(ip, **kwargs)
        self.ip = ip

    def handle_metric(self, src, message):
        # logger.debug("Received value @ IM[%s]: src=%s, message=%s", self.name, src, message)

        # get metric
        metric, sub_metric = message["channel"].split("/")
        i = metric.index("-")
        metric, arg = metric[0:i], metric[i + 1:]

        # get value
        f = message["data"].split(":")
        ts = datetime.fromtimestamp(float(f[0]))
        vals = [float(v) for v in f[1:]]
        if metric == 'cpu' and sub_metric == 'cpu-idle' and arg == '0':
            logger.temp("HM[%s] Received :: metric=%s (sub_metric=%s), arg=%s, ts=%s, values=%s",
                        self.name, metric, sub_metric, arg, ts, vals)

        self.add_metric_value(metric, arg, ts, vals, sub_metric)

    def register_channel(self, ch):
        if self.strategy:
            # noinspection PyBroadException
            try:
                self.strategy.register_host_channel(ch)
            except:
                logger.warn("No valid register_host_channel callback in strategy. Tb: %s", traceback.format_exc())


class ServiceMonitor(object):
    """
    Monitors events associated with a service.
    Currently this class does not inherit from EntityMonitor, as it is not a time series but rather
    an event-based monitor. Also because it is suppose to include a composite of EntityMonitors.
    It should be able to handle events in the lower Entity Monitors.
    In the future, it should manage:
        - Creation of entity monitors for instances of the service
        - Set rules and callbacks for all instance monitor channels
    """

    def __init__(self, name):
        self.name = name
        self.instances = {}

    def get_instance(self, name):
        return self.instances.get(name)

    # def add_instance(self, name, service, instance):
    #     instance.update({"name": name, "service": service})
    #     self.instances[name] = instance

    def add_instance_monitor(self, im):
        self.instances[im.name] = im

    def add_event(self):
        raise NotImplementedError()

    def get_dict(self):
        return {k: {} for k, i in self.instances.items()}


class Subscriber(object):
    def __init__(self, app_name, host):
        super().__init__()
        self.app_name = app_name
        self.host = host
        logger.detail("Connect to LamaPubSub: %s", (app_name, host))
        self.pubsub = LamaPubSub("mon:%s:%s" % (app_name, host), host=host)
        logger.detail("connected...")
        self.pubsub.subscribe("dummy_channel")

    def subscribe_by_instance(self, name, cb):
        self.pubsub.subscribe_by_instance(self.app_name, name, cb)

    def subscribe_by_resource(self, res_name, cb):
        self.pubsub.subscribe_by_resource(res_name, cb)

    def stop(self):
        logger.warn("Stopping subscriber for host %s", self.host)
        self.pubsub.stop()

    def run(self):
        logger.warning("Run thread")
        self.pubsub.start()
        logger.warning("Leave thread")

    def start(self):
        run_in_thread(self.run)


class SubscriptionManager(object):
    def __init__(self, app_name, strategy=None, csv_handler=None, redis_handler=None):
        self.sub_hosts = {}
        self.sub_instances = {}
        self.services = {}

        self.app_name = app_name

        self.pubsubs = {}

        self.test_redis = StrictRedis()
        self.strategy = strategy
        self.csv_handler = csv_handler
        self.redis_handler = redis_handler

    def get_pubsub(self, host):
        pubsub = self.pubsubs.get(host)
        if not pubsub:
            # pubsub = LamaPubSub(self.app_name, host=host)
            logger.detail("Initialize pubsub for host %s", host)
            try:
                pubsub = Subscriber(self.app_name, host)
                if pubsub:
                    self.pubsubs[host] = pubsub
                    pubsub.start()
            except Exception as e:
                logger.error("Exception: %s", e)
                logger.error("Traceback: %s", traceback.format_exc())
        if not pubsub:
            logger.warn("Unable to connect to Redis/pubsub (host: %s)", host)
        return pubsub

    def stop(self):
        for p in self.pubsubs.values():
            p.stop()

    def subscribe_instance(self, im):
        # host is local or remote?
        try:
            logger.detail("im = %r/%s", im, im)
            name = im.name
            logger.detail("name=%s, subi=%s", name, self.sub_instances)
            if name not in self.sub_instances:
                logger.temp("Subscribing instance: %s", im.name)
                if "host" in im.instance and im.instance["host"]:
                    self.sub_instances[name] = im
                    # save using IP and NAME as key
                    pubsub = self.get_pubsub(im.instance['host'])
                    self.subscribe_host(im.instance["host"])
                    pubsub.subscribe_by_instance(name, im.handle_metric)
                else:
                    return None
            return self.sub_instances.get(name)
        except Exception as e:
            logger.error("Traceback: %s", traceback.format_exc())
            return None

    def subscribe_host(self, host):
        logger.detail("Subscribing host: %s", host)
        if host in self.sub_hosts:
            return

        hm = HostMonitor(host, strategy=self.strategy, csv_handler=self.csv_handler, redis_handler=self.redis_handler)
        self.sub_hosts[host] = hm
        for res in [CpuSpec, ]:
            # for res in [CpuSpec, RamSpec, DiskSpec, NetSpec]:
            pubsub = self.get_pubsub(host)

            logger.detail("Subscribing: %s", hm)
            pubsub.subscribe_by_resource(res.__name__, hm.handle_metric)
        return hm

    def get_instance(self, key):
        return self.sub_instances.get(key, None)

    def get_data_value(self, node, metrics):
        value = {}
        try:
            if isinstance(node, PhysicalNode):
                value = self.sub_hosts[node.ip_address].get_dict(metric_filters=metrics)
            elif isinstance(node, VirtualNode):
                value = self.sub_instances[node.name].get_dict(metric_filters=metrics)
        except:
            pass

        if not value:
            logger.warn("Unable to get data for node of type: %s", type(node))
        return value

    def get_instances_dict(self, min_ts=None, filters=None):
        filter_set = set()
        if filters:
            for iname in filters.get('instances', []):
                filter_set.add(iname)
        metric_filter_set = filters.get('metrics')

        if not filter_set:
            return {iname: im.get_dict(min_ts, metric_filter_set) for iname, im in self.sub_instances.items()}
        else:
            return {iname: im.get_dict(min_ts, metric_filter_set) for iname, im in self.sub_instances.items() if
                    iname in filter_set}

    def get_hosts_dict(self, min_ts=None, filters=None):
        # for hname, hm in self.sub_hosts.items():
        #     print(hname, hm.name, hm, hm._channels)
        filter_set = filters.get('hosts')
        metric_filter_set = filters.get('metrics')

        if not filter_set:
            return {hname: hm.get_dict(min_ts, metric_filter_set) for hname, hm in self.sub_hosts.items()}
        else:
            return {hname: hm.get_dict(min_ts, metric_filter_set) for hname, hm in self.sub_hosts.items() if
                    hname in filter_set}

    def get_dict(self, name):
        try:
            e = self.pubsubs[name].get_dict()
            return e
        except KeyError:
            return {}


class AppMonitor(object):
    def __init__(self, app, strategy=None, pool_interval=30, redis_port=6379,
                 csv_filename=None, app_arch_filename=None, redis_host='localhost', pub_redis=True,
                 monitor_port=MonitorPort, op_controller=None):
        self.app = app
        self.pool_interval = pool_interval
        self.timer = None
        self.stop_requested = False
        self.monitor_port = monitor_port

        self.strategy = strategy
        self.strategy.set_app(app)
        if op_controller:
            self.strategy.set_op_controller(op_controller)

        self.csv_handler = None
        if csv_filename:
            self.csv_handler = CsvHandler(
                csv_filename,
                (
                    "Timestamp",
                    "EntityType",
                    "Entity",
                    "Metric",
                    "Value"
                )
            )
        self.app_arch_filename = app_arch_filename

        # this script only contacts redis at the localhost
        logger.debug("Connecting to Redis on %s:%s", "127.0.0.1", 6379)
        self.redis = LamaKeyStore(host=redis_host, port=redis_port)
        self.pub_redis = pub_redis

        # Initialize metric manager
        self.subscriptions = SubscriptionManager(self.app.name, strategy=self.strategy, csv_handler=self.csv_handler,
                                                 redis_handler=self.redis if self.pub_redis else None)

        self.services = {}

        if redis_host:
            redis_host = str(redis_host)
        logger.info("Created a queue: %s", "%s:monitor:main" % self.app.name)
        # Add access to the operations queue to the strategy

        # self.operations_queue = LamaRedisQueue("%s:monitor:main" % self.app_name, host=redis_host)
        # self.strategy.set_op_queue(self.operations_queue)

        # self.http_thread = Thread(target=start_http_interface, args=(self.monitor_port, self))

    def set_op_controller(self, op_controller):
        self.strategy.set_op_controller(op_controller)

    def start(self):
        res = run_in_thread(start_http_interface, self.monitor_port, self)
        logger.debug("Start the http interface... Result: %s", res)
        # self.http_thread.start()
        # logger.warn("No http interface")

    def stop(self):
        logger.warn("STOPPING")
        self.stop_requested = True
        self.subscriptions.stop()

    def check_instance(self, inst):
        """
        :param inst: An instance (VirtualNode)
        :return: A dictionary of the new instance, if key is new, None otherwise
        """

        # check if service exists
        service_name = inst.get_service().name
        service = self.services.get(service_name)
        if not service:
            service = ServiceMonitor(name=service_name)
            self.services[service_name] = service
            # instances = {}
            # self.app[fields[2]] = instances
            logger.info("Monitoring new service: %s", service_name)

        # check if instance exists
        im = service.get_instance(inst.name)
        if not im:
            host = inst.get_host()
            instance = {
                "name": inst.name,
                "service": service_name,
                "host": host.ip_address if host else None,
                "ip": inst.ip
            }
            im = InstanceMonitor(instance, strategy=self.strategy, csv_handler=self.csv_handler,
                                 redis_handler=self.redis if self.pub_redis else None)
            service.add_instance_monitor(im)

            logger.info("Monitoring new instance: %s ", instance)
            return im

        if not im.instance["host"]:
            host = inst.get_host()
            im.instance["host"] = host.ip_address if host else None

        if not im.instance["ip"]:
            im.instance["ip"] = inst.ip

        return im

    def notify_app_changed(self):
        for inst in self.app.get_vms():
            instance = self.check_instance(inst)

            if instance:
                self.subscriptions.subscribe_instance(instance)

    def get_dict(self, min_ts=None):
        arch = {}
        for service in self.app.get_services(
                subtype={LogicalNodeSubtype.service, LogicalNodeSubtype.client, LogicalNodeSubtype.lb}
        ):
            s = {}
            for instance in service.get_instances():
                h1 = instance.get_host()
                s[instance.name] = h1.ip_address if h1 else None
            arch[service.name] = s

        res = {
            # "arch": {k: v.get_dict() for k, v in self.app.items()},
            "arch": arch,
            "instances": self.subscriptions.get_instances_dict(min_ts),
            "hosts": self.subscriptions.get_hosts_dict(min_ts),
        }
        return res

    def get_arch_json(self, **params):
        d = {
            "services": {},
            "hosts": {},
            "agent": "127.0.0.1"
        }

        sts = {st for st in LogicalNodeSubtype}
        if bool(params.get("filter-support")):
            sts.remove(LogicalNodeSubtype.image)
            sts.remove(LogicalNodeSubtype.dhcp)

        for service in self.app.get_services(subtype=sts):
            s = {
                "name": service.name,
                "links": [],
                "instances": {}
            }
            for instance in service.get_instances():
                state = self.strategy.get_instance_state(instance.name)
                host = instance.get_host()
                s["instances"][instance.name] = {
                    "state": state.name if state else None,
                    "values": self.strategy.get_instance_value(instance.name),
                    "ip": instance.ip,
                    "host": host.ip_address if host else None
                }

            for peer in service.get_all_successors():
                s["links"].append(peer.name)

            d["services"][service.name] = s

        for host in self.app.get_hosts():
            h1 = {
                "value": 50
            }
            d["hosts"][str(host.ip_address)] = h1

        return d

    def get_data_value(self, node, metrics):
        res = self.subscriptions.get_data_value(node, metrics)
        if not res:
            logger.warn("Unable to get value for node: %s", node)
        return res

    def get_monitor_data(self, filters=None, min_ts=None, **kwargs):
        logger.temp("Monitor data - params: filters=%s, min_ts=%s", filters, min_ts)
        try:
            if min_ts:
                try:
                    min_ts = float(min_ts) / 1000
                except:
                    logger.temp("Ignoring min_ts parameter - bad format: %s", min_ts)
                    min_ts = None

            if filters:
                instances_set = set(filters.get('instances', []))
                for sname in filters.get('services', []):
                    service = self.app.get_service(sname)
                    if service:
                        for instance in service.get_instances():
                            instances_set.add(instance.name)
                filters['instances'] = instances_set

            res = {
                "instances": self.subscriptions.get_instances_dict(min_ts, filters),
                "hosts": self.subscriptions.get_hosts_dict(min_ts, filters),
            }
            logger.temp("Monitor data - res=%s", res)

        except:
            logger.error("Monitor data - error: %s", traceback.format_exc())
            res = {
                "msg": "Error loading data!"
            }

        return res


def start_http_interface(port, monitor):
    app = Flask(__name__)

    @app.route('/arch')
    @crossdomain(origin='*')
    def get_arch():
        # noinspection PyBroadException
        try:
            if monitor.app:
                results = {
                    "success": True,
                    "results": monitor.get_arch_json(**{k: request.args[k] for k in request.args}),
                    "actions": []
                }
            else:
                results = {
                    "success": False,
                    "results": "App not yet loaded",
                    "actions": []
                }
        except:
            results = {
                "success": False,
                "results": "%s" % traceback.format_exc(),
                "actions": []
            }
            logger.error("Error: %s", traceback.format_exc())
        # noinspection PyBroadException
        try:
            return jsonify(**results)
        except:
            logger.error(traceback.format_exc())

    # default only GET requests
    @app.route('/metrics')
    @crossdomain(origin='*')
    def get_metrics():
        # noinspection PyBroadException
        try:
            min_ts = request.args.get("min_ts")
            if min_ts:
                min_ts = float(min_ts) / 1000
            logger.debug("min_ts: %s", datetime.fromtimestamp(min_ts) if min_ts else min_ts)

            results = {
                "success": True,
                "results": monitor.get_dict(min_ts),
                "actions": []
            }

        except:
            results = {
                "success": False,
                "results": "%s" % traceback.format_exc(),
                "actions": []
            }
            logger.error("Error: %s", traceback.format_exc())

        logger.debug("results: %s" % results)
        return jsonify(**results)

    @app.route('/', methods=['POST'])
    @crossdomain(origin='*')
    def process_metrics():
        # noinspection PyBroadException
        try:
            # process data
            data = request.get_json(force=True)

            # get instance monitor
            # call handle metric OR add_metric_value
            logger.debug("Received RT data for instance: %s", data["name"])
            logger.debug("data: %s", data)
            instance = monitor.subscriptions.get_instance(data["name"])
            ts = datetime.fromtimestamp(float(data["timestamp"]))
            if instance:
                logger.debug("Instance found: %s", instance)
                for metric, metrics in data.get("metrics").items():
                    for arg, series in metrics.items():
                        logger.debug("arg=%s    series=%s", arg, series)
                        if isinstance(series, dict):
                            for subm, val in series.items():
                                instance.add_metric_value(metric="response_time", arg=arg, timestamp=ts,
                                                          values=[val], sub_metric=subm)
                        else:
                            instance.add_metric_value(metric="response_time", arg=arg, timestamp=ts, values=[series])
                return jsonify({"result": True})
            else:
                logger.info("No instance")
                return jsonify({"result": False, "error": "No instance"})

        except:
            logger.info("Traceback: %s", traceback.format_exc())
            abort(400)

    logger.info("Start webserver on port: %s", port)
    app.run(host='0.0.0.0', port=port)


def type_strategy(s):
    chs = "[a-zA-Z_][a-zA-Z0-9_]*"
    vre = re.compile("^%s\.%s$" % (chs, chs))

    try:
        parts = s.split(":")

        if len(parts) <= 0 or len(parts) > 2:
            raise Exception("bad format")

        sname = parts[0]
        sargs = parts[1] if len(parts) > 1 and parts[1] else {}

        if not vre.match(sname):
            raise Exception("bad strategy name: %s" % sname)

        parts = sname.split(".")
        if len(parts) != 2:
            raise Exception("bad strategy name: %s" % sname)

        mod_name = parts[0]
        cls_name = parts[1]

        if len(sargs) > 1:
            try:
                sargs = {k: v for k, v in [p.split("=") for p in sargs.split(",")]}
                for k in sargs.keys():
                    v = sargs[k]
                    # noinspection PyBroadException
                    try:
                        sargs[k] = float(v)
                        sargs[k] = int(v)
                    except:
                        pass
            except Exception:
                raise Exception("bad list of arguments: %s" % sargs)

        try:
            mod = import_module('.' + mod_name, package='lama.agent.app.monitor.strategies')
            logger.info("Module '%s' loaded", mod)
            cls = getattr(mod, cls_name)
        except:
            print("Traceback: %s" % traceback.format_exc())
            raise Exception("strategy '%s' does not exist" % sname)

        try:
            return cls(**sargs)
        except:
            traceback.print_exc()
            raise Exception("wrong arguments ['%s'] for class '%s'" % ("', '".join(sargs.keys()), sname))
    except Exception as e:
        raise ArgumentTypeError("Bad strategy format - %s: <package_name>.<strategy_name>:"
                                "<arg_name_1>=<arg_value_1>,<arg_name_2>=<arg_value_2>,..." % str(e))

# if __name__ == "__main__":
#     argparse = ArgumentParser()
#     argparse.add_argument("-a", dest="app_name", help="Name of the App to monitor", required=True)
#     argparse.add_argument("-l", dest="log_level", help="Log level", default="DEBUG",
#                           choices=("NOTSET", "DEBUG", "INFO", "WARNING", "ERROR"))
#     argparse.add_argument("-d", dest="duration", help="Time to run (seconds)", type=int, default=None)
#     argparse.add_argument("-s", dest="strategy",
#                           help="Management Strategy: <package_name>.<strategy_name>:"
#                                "<arg_name_1>=<arg_value_1>,<arg_name_2>=<arg_value_2>,...\n"
#                                "Accepts string, int and float values.",
#                           type=type_strategy, default="fsm.HierarchicalFSMStrategy")
#     argparse.add_argument("--httpd", dest="http",
#                           help="Port to start http interface (0 for default port: %d)" % MonitorPort,
#                           type=int, default=None)
#     argparse.add_argument("--csv", dest="csv_file", nargs='?',
#                           help="Save to CSV file as specified (default filename is results.csv).",
#                           type=str, const="results.csv", default=None)
#     argparse.add_argument("--redis", dest="pub_redis",
#                           help="Publish results to redis on local host.", action='store_true')
#
#     pargs = argparse.parse_args()
#     # set logger
#     LamaLogger.set_filename(logger, "app_monitor_%s.log" % pargs.app_name)
#
#     sh = StreamHandler()
#     sh.setFormatter(ColorFormatter())
#     sh.setLevel(pargs.log_level)
#     logger.addHandler(sh)
#
#     # change level for all handlers....
#     for handler in logger.handlers:
#         handler.setLevel(pargs.log_level)
#
#     logger.setLevel(pargs.log_level)
#     mon = AppMonitor(pargs.app_name, strategy=pargs.strategy, csv_filename=pargs.csv_file)
#
#     from threading import Thread, Timer
#     # start the server
#     if pargs.http is not None:
#         pargs.http = 0 or MonitorPort
#         http = Thread(target=start_http_interface, args=(pargs.http, mon))
#         http.start()
#
#     mon.start()
#
#     if pargs.duration:
#         Timer(pargs.duration, mon.stop).start()
