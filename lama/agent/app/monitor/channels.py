from lama.agent.app.monitor.data import TimeAwareDataBuffer
# from lama.agent.app.monitor.strategy.rules import MinMaxFSM
from lama.utils.logging_lama import logger

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2015, Carnegie Melon University'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class Channel(object):

    def __init__(self, entity_name, metric, arg, period=None, n=10, strategy=None):
        """

        :param metric:
        :param period: Duration of period between metrics.
        (Not implemented yet: If none, will try to detect it automatically as the minimum between timestamps)
        :return:
        """
        self.entity_name = entity_name
        self.metric = metric
        self.arg = arg
        self.auto_period = period
        self.last_entry = None
        self.n = n
        self.data = {}

        self.processors = {}

    def add_value(self, ts, values, sub_metric=None):
        self._add_value(ts, values, sub_metric)
        # logger.temp("[%s:%s:%s:%s] val=%s, processor: %s", self.entity_name, self.metric, sub_metric, self.arg, values, self.processor)
        processors = self.processors.get(sub_metric)
        if processors:
            # logger.temp("[%s:%s:%s:%s] processor: %s", self.entity_name, self.metric, sub_metric, self.arg, processor)
            # processor.process_value(ts, values[0])
            last_value = self.get_last_value(sub_metric)
            # logger.temp("e=%s, ch='%s:%s:%s': #processor=%s vals=%s", self.entity_name,
            #             self.__class__.__name__, self.metric, sub_metric, processor.__class__.__name__, last_value)
            # if last_value is not None:
            for processor in processors:
                processor.process_value(ts, last_value)

    def _add_value(self, ts, values, sub_metric=None):
        """
        Adds a value to the data array. By default adds only the first element received.
        To add more values sub-class this class add create your add value function,
        because each series needs to be distinguished using domain knowledge.
        :param ts: timestamp
        :param values: array of values
        :param sub_metric: submetric
        """
        arr = self.data.setdefault(sub_metric, TimeAwareDataBuffer())
        arr.add_with_time(ts, values[0])
        # logger.debug("Added value to channel '%s:%s:%s': #events=%d : %s",
        #               self.__class__.__name__, self.metric, sub_metric, len(arr), values[0])

    def get_dict(self, ts=None):
        r = {}
        for sub_metric, buf in self.data.items():
            r[sub_metric] = buf.get_data(ts)
        # print("channel returns: %s" % r)
        return r

    def add_processor(self, rule, sub_metric=None):
        self.processors.setdefault(sub_metric, []).append(rule)

    def low_cb(self, sub_metric=None):
        logger.info("Default low callback: %s, %s:%s", self.arg, self.metric, sub_metric)

    def high_cb(self, sub_metric=None):
        logger.info("Default high callback: %s, %s:%s", self.arg, self.metric, sub_metric)

    def get_last_values(self, sub_metric=None):
        buf = self.data.get(sub_metric)
        if buf:
            return [(sub_metric, buf.get_last())]
        return []

    def get_last_value(self, sub_metric):
        buf = self.data.get(sub_metric)
        return buf.get_last() if buf else None


class OffsetChannel(Channel):
    """
    Channel that publish the offset to the last value.
    """
    def __init__(self, entity_name, metric, arg, factor=1.0, *args, **kwargs):
        super().__init__(entity_name, metric, arg, *args, **kwargs)
        self.last_val = {}
        self.factor = factor

    def _add_value(self, ts, value, sub_metric=None):
        # logger.temp("DiffChannel: sub_metric=%s, last_vals=%s", sub_metric, self.last_val)
        # logger.temp("DiffChannel: new_ts=%s, new_val=%s", ts, value)
        if sub_metric in self.last_val:
            old_ts, old_val = self.last_val[sub_metric]
            # logger.detail("DiffChannel: old_ts=%s, old_val=%s", old_ts, old_val)
            if (ts - old_ts).total_seconds() and self.factor:
                val = (value[0] - old_val) / ((ts - old_ts).total_seconds() * self.factor)
                # logger.detail("DiffChannel: final_val==%s", val)
                super()._add_value(ts, [val], sub_metric)
        self.last_val[sub_metric] = (ts, value[0])


class ListChannel(Channel):

    def __init__(self, series, entity_name, metric, arg, **kwargs):
        super().__init__(entity_name, metric, arg)
        self.series = series

    def _add_value(self, ts, value, sub_metric=None):
        """
        Split interface statics into RX and TX channels
        :param ts:
        :param value:
        :param sub_metric:
        :return:
        """
        # process
        for d, v in zip(self.series, value):
            super()._add_value(ts, [v], "%s-%s" % (d, sub_metric))

    def get_last_values(self, sub_metric=None):
        sm = "-%s" % sub_metric
        return [(k, v.get_last()) for k, v in self.data.items() if k and k.endswith(sm)]


class OffsetListChannel(ListChannel, OffsetChannel):
    """
    Helper to create an offset list channel: ListChannel should always be called first to separate the channels before computing the offset.
    """

    def __init__(self, series, entity_name, metric, arg):
        super().__init__(series, entity_name, metric, arg)


class CPUChannel(OffsetChannel):

    def __init__(self, entity_name, metric, arg, **kwargs):
        super().__init__(entity_name, metric, arg)


class VirtCPUChannel(OffsetChannel):

    def __init__(self, entity_name, metric, arg, **kwargs):
        super().__init__(entity_name, metric, arg, factor=kwargs.get("factor") or 1E7)

        logger.info("New VirtCPUChannel (%s, %s, %s)!!!", self.entity_name, metric, arg)
        # add channel default rules
        # r = MinMaxFSM(
        #     low_thresh=kwargs.get("low_threshold") or 30,
        #     high_thresh=kwargs.get("high_threshold") or 70,
        #     low_count_limit=kwargs.get("low_count") or 3,
        #     high_count_limit=kwargs.get("high_count") or 3
        # )
        # r.low_cb = kwargs.get("low_cb") or self.low_cb
        # r.high_cb = kwargs.get("high_cb") or self.high_cb
        # self.add_rule(r)

    def _add_value(self, ts, value, sub_metric=None):
        # logger.temp("DiffChannel: sub_metric=%s, last_vals=%s", sub_metric, self.last_val)
        # logger.temp("DiffChannel: new_ts=%s, new_val=%s", ts, value)
        super()._add_value(ts, value, sub_metric)


class InterfaceChannel(OffsetListChannel):

    def __init__(self, entity_name, metric, arg, **kwargs):
        super().__init__(("tx", "rx"), entity_name, metric, arg)


class DiskChannel(OffsetListChannel):

    def __init__(self, entity_name, metric, arg, **kwargs):
        super().__init__(("rd", "wr"), entity_name, metric, arg)


class ChannelFactory(object):

    spec_channels = {
        "cpu": CPUChannel,
        "disk": DiskChannel,
        # "df": Channel,
        "interface": InterfaceChannel,
        "virt_cpu_total": VirtCPUChannel,
        "virt_vcpu": VirtCPUChannel,
        "disk_octets": DiskChannel,
        "disk_ops": DiskChannel,
        "if_errors": InterfaceChannel,
        "if_octets": InterfaceChannel,
        "if_packets": InterfaceChannel,
        "if_dropped": InterfaceChannel
    }

    @staticmethod
    def get_channel(entity_name, metric, arg, **kwargs):
        return ChannelFactory.spec_channels.get(metric, Channel)(entity_name=entity_name, metric=metric, arg=arg, **kwargs)

