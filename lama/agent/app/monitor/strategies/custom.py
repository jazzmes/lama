from lama.agent.app.monitor.strategies.base import Strategy
from lama.utils.logging_lama import logger

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class SlidingAverageProcessor(object):

    def __init__(self, window_size=5):
        self._values = []
        self._ts = []
        self._window_size = window_size
        self._sum = 0

        self._conditions = []

    def process_value(self, ts, value):
        self._values.append(value)
        self._ts.append(ts)
        # show start for average
        self._sum += value
        if len(self._ts) > self._window_size:
            self._values.remove(0)
            self._ts.remove(0)
            self._sum -= value

        for condition, callback in self._conditions:
            if condition(self):
                callback(self)

    def add_condition(self, condition, callback):
        self._conditions.append(
            (condition, callback)
        )

    def get_value(self):
        return self._sum / self._window_size

    def get_last_value(self):
        return self._values[-1]

    def get_last_ts(self):
        return self._ts[-1]


# class Condition(object):
#
#     def __init__(self, condition, trigger_count):
#         self._condition = condition
#         self._trigger_count = trigger_count
#         self._current_count = 0
#
#     def check(self, value):
#         logger.temp("Process value: ts=%s val=%s", ts, value)
#         if self._condition(value):
#             self._current_count += 1
#             if self._current_count >= self._trigger_count:
#                 return True
#
#         else:
#             self._trigger_count = 0
#         return False
#
#
# class ConditionProcessor(object):
#
#     def __init__(self, channel, condition, callback):
#         self._channel = channel
#         self._condition = condition
#         self._callback = callback
#
#     def process_value(self, ts, value):
#         if self._condition.check(value):
#             self._callback(self._channel)


# noinspection PyChainedComparisons
class ApacheMigrationChecker(object):

    def __init__(self, controller, channel):
        self._controller = controller
        self._trigger_channel = channel

        self._service = self._controller.app_arch.get_service('apache')

    def process_value(self, ts, value):
        # logger.temp("TIER_AWARE :: Process value of response time: ts=%s value=%s", ts, value)

        for instance in self._service.get_instances():
            # get values
            # host CPUs
            host = instance.get_host()
            host_processor_id = (host.ip_address, 'cpu')
            channels = self._controller.channels.get(host_processor_id, {})
            host_cpu_sum = 0
            host_cpu_count = 0
            for arg, channel in channels.items():
                # logger.temp("TIER_AWARE :: host=%s m=cpu arg=%s user=%s system=%s",
                #             host.ip_address, arg, channel.data.get('cpu-user').get_last(),
                #             channel.data.get('cpu-system').get_last())
                host_cpu_sum += channel.data.get('cpu-user').get_last() + channel.data.get('cpu-system').get_last()
                host_cpu_count += 1

            # instance VCPU
            instance_processor_id = (instance.name, 'virt_cpu_total')
            channels = self._controller.channels.get(instance_processor_id, {})
            instance_cpu = 9
            for arg, channel in channels.items():
                # logger.temp("TIER_AWARE :: instance=%s m=cpu arg=%s value=%s", instance.name, arg,
                #             channel.data.get(None).get_last())
                instance_cpu += channel.data.get(None).get_last()

            conditions = [
                value > 25,
                host_cpu_sum / host_cpu_count > 70,
                instance_cpu < 70
            ]
            result = all(conditions)
            logger.temp("TIER_AWARE :: instance=%s, migrate=%s, rt=%s (%s), host=%s (%s), instance=%s (%s)",
                        instance.name,
                        result,
                        conditions[0], value,
                        conditions[1], host_cpu_sum / host_cpu_count,
                        conditions[2], instance_cpu)
            if result:
                self._controller.migrate(instance)


class MySQLMigrationChecker(object):

    def __init__(self, controller, channel):
        self._controller = controller
        self._trigger_channel = channel

        self._service = self._controller.app_arch.get_service('mysql')

    def process_value(self, ts, value):
        # logger.temp("TIER_AWARE :: Process value of response time: ts=%s value=%s", ts, value)

        for instance in self._service.get_instances():
            # get values
            # host CPUs
            host = instance.get_host()
            host_processor_id = (host.ip_address, 'cpu')
            channels = self._controller.channels.get(host_processor_id, {})
            host_cpu_sum = 0
            host_cpu_count = 0
            for arg, channel in channels.items():
                # logger.temp("TIER_AWARE :: host=%s m=cpu arg=%s wait=%s",
                #             host.ip_address, arg, channel.data.get('cpu-wait').get_last())
                host_cpu_sum += channel.data.get('cpu-wait').get_last()
                host_cpu_count += 1

            # instance VCPU
            instance_processor_id = (instance.name, 'virt_cpu_total')
            channels = self._controller.channels.get(instance_processor_id, {})
            instance_cpu = 9
            for arg, channel in channels.items():
                # logger.temp("TIER_AWARE :: instance=%s m=cpu arg=%s value=%s", instance.name, arg,
                #             channel.data.get(None).get_last())
                instance_cpu += channel.data.get(None).get_last()

            conditions = [
                value > 300,
                host_cpu_sum / host_cpu_count > 20,
                instance_cpu < 70
            ]
            result = all(conditions)
            logger.temp("TIER_AWARE :: instance=%s, migrate=%s, rt=%s (%s), cpu-wait=%s (%s), instance=%s (%s)",
                        instance.name,
                        result,
                        conditions[0], value,
                        conditions[1], host_cpu_sum / host_cpu_count,
                        conditions[2], instance_cpu)
            if result:
                self._controller.migrate(instance)


class TierAwareController(object):

    ExpertControllers = {
        'apache', 'mysql'
    }

    Triggers = {
        'ref_0': {
            'checker': ApacheMigrationChecker,
            'sub_metric': '95avg'
        },
        'ref_9': {
            'checker': MySQLMigrationChecker,
            'sub_metric': '95avg'
        }
    }

    def __init__(self, op_controller, app_arch):
        self.app_arch = app_arch
        self.channels = {}
        self.op_controller = op_controller

    def register_instance_channel(self, instance, channel):
        service = instance.get_service()
        if service.name == 'client':
            if channel.metric == 'response_time':
                if channel.arg in self.Triggers:
                    trigger = self.Triggers.get(channel.arg)
                    # noinspection PyCallingNonCallable
                    channel.add_processor(trigger.get('checker')(self, channel), sub_metric=trigger['sub_metric'])

        elif service.name in self.ExpertControllers:
            processor_id = (instance.name, channel.metric)
            channels = self.channels.setdefault(processor_id, {})
            # logger.temp("TIER_AWARE :: save channel: %s -> %s -> %s", processor_id, channel.arg, channel)
            channels.setdefault(channel.arg, channel)

    def register_host_channel(self, host, channel):
        processor_id = (host.ip_address, channel.metric)
        channels = self.channels.setdefault(processor_id, {})
        # logger.temp("TIER_AWARE :: save channel: %s -> %s -> %s", processor_id, channel.arg, channel)
        channels.setdefault(channel.arg, channel)

    def migrate(self, instance, conditions=None):
        self.op_controller.migrate_instance(instance.name, conditions)


class TierAwareHostFlawDetection(Strategy):

    InstanceMetrics = ('virt_cpu_total', 'response_time')
    HostMetrics = ('cpu', )

    def __init__(self):
        super().__init__()
        self.controller = TierAwareController(self.op_controller, self.app_arch)

    def set_app(self, app_arch):
        super().set_app(app_arch)
        self.controller.app_arch = app_arch

    def set_op_controller(self, controller):
        super().set_op_controller(controller)
        self.controller.op_controller = controller

    def register_instance_channel(self, ch):
        if ch.metric in self.InstanceMetrics:
            instance = self.app_arch.get_vm(ch.entity_name)
            if instance:
                logger.info("Register channel of type '%s' - Instance: %s", type(ch), instance)
                self.controller.register_instance_channel(instance, ch)

    def register_host_channel(self, ch):
        if ch.metric in self.HostMetrics:
            logger.temp("Checking channel: %s - %s", ch, ch.entity_name)
            host = self.app_arch.get_host(ch.entity_name)
            if host:
                logger.info("Register channel of type '%s' - Host: %s", type(ch), host)
                self.controller.register_host_channel(host, ch)
