import traceback

from lama.utils.logging_lama import logger
from lama.utils.enum import Enum

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class MonitoringStrategyCommands(object):

    def __init__(self, controller):
        self.controller = controller

    def mark_instance_down(self, service_name, instance_name):
        try:
            self.controller.mark_instance_down(service_name, instance_name)
        except Exception as e:
            if e.args[0].endswith("'mark_instance_down'"):
                logger.info("Instance %s:%s marked DOWN - IGNORED", service_name, instance_name)
            else:
                logger.error("Error on command handler: %s", traceback.format_exc())
        # self.controller.mark_instance_down(service_name, instance_name)
        # self.controller.remove_instance(service=service_name, instance=instance_name)

    def mark_instance_up(self, service_name, instance_name):
        try:
            self.controller.mark_instance_up(service_name, instance_name)
        except:
            logger.info("Instance %s:%s marked UP - IGNORED", service_name, instance_name)

        # self.controller.mark_instance_up(service_name, instance_name)

    def mark_host_down(self, ip_address):
        try:
            self.controller.mark_host_down(ip_address)
        except Exception as e:
            if e.args[0].endswith("'mark_instance_down'"):
                logger.info("Host %s marked DOWN - IGNORED", ip_address)
            else:
                logger.error("Error on command handler: %s", traceback.format_exc())
                # self.controller.mark_instance_down(service_name, instance_name)
                # self.controller.remove_instance(service=service_name, instance=instance_name)

    def mark_host_up(self, ip_address):
        try:
            self.controller.mark_instance_up(ip_address)
        except:
            logger.info("Instance %s marked UP - IGNORED", ip_address)

        # self.controller.mark_instance_up(service_name, instance_name)    def remove_instance(self, service_name, instance_name=None):
        logger.error("DEPRECATED : Do not use this directly")

    def change_instances(self, service_name, active_instances, hot_instances=0, warm_instances=0):
        logger.error("DEPRECATED : Do not use this directly")
        # raise NotImplementedError("Should not use this directly")
        # self.controller.change_instances(
        #         service=service_name,
        #         active_instances=active_instances,
        #         hot_instances=hot_instances,
        #         warm_instances=warm_instances
        # )

    def migrate_instance(self, instance_name, conditions):
        logger.error("DEPRECATED : Do not use this directly")
        # self.controller.migrate_instance(
        #     instance_name=instance_name,
        #     conditions=conditions
        # )
