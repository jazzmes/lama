from lama.utils.logging_lama import logger

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2015, Carnegie Melon University'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class Strategy(object):
    """
    Base class to implement management (scaling, failure handling) strategies for applications.
    """

    def __init__(self):
        self.app_arch = None
        self.propagate = True
        self.op_controller = None

    def set_app(self, app_arch):
        logger.temp("app_arch object: %r", app_arch)
        self.app_arch = app_arch

    def set_op_controller(self, controller):
        self.op_controller = controller

    # function to process a new channel value
    def process_channel_value(self, ts, value):
        raise NotImplementedError("Strategy interface: implement this method to process channel values")

    # def process_instance_state(self, service, instance, metric):
    #     raise NotImplementedError()

    def register_channel(self, ch):
        raise NotImplementedError()

    def register_instance_channel(self, ch):
        raise NotImplementedError()

    def register_host_channel(self, ch):
        raise NotImplementedError()

    def get_instance_state(self, instance):
        return None

    def get_instance_value(self, instance):
        return None

    def get_host_value(self, host_name):
        return None


class CompositeStrategy(Strategy):

    def __init__(self, *strategies):
        super().__init__()
        self.__strategies = strategies
        logger.temp("Composite strategy: %s", self.__strategies)

    def get_instance_value(self, instance):
        for strategy in self.__strategies:
            try:
                return strategy.get_instance_value(instance)
            except:
                pass

    def get_instance_state(self, instance):
        for strategy in self.__strategies:
            try:
                return strategy.get_instance_state(instance)
            except:
                pass

    def set_app(self, app_arch):
        for strategy in self.__strategies:
            strategy.set_app(app_arch)

    def register_instance_channel(self, ch):
        for strategy in self.__strategies:
            strategy.register_instance_channel(ch)

    def register_channel(self, ch):
        for strategy in self.__strategies:
            strategy.register_channel(ch)

    def register_host_channel(self, ch):
        for strategy in self.__strategies:
            strategy.register_host_channel(ch)

    def process_channel_value(self, ts, value):
        logger.temp("process_channel_value")
        for strategy in self.__strategies:
            strategy.process_channel_value(ts, value)

    def set_op_controller(self, controller):
        for strategy in self.__strategies:
            strategy.op_controller = controller
