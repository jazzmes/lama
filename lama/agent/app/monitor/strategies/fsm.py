import traceback
from functools import partial
from datetime import datetime, timedelta

from math import ceil

from lama.agent.app.app_arch import LogicalNodeSubtype
from lama.agent.app.monitor.channels import VirtCPUChannel, CPUChannel
from lama.agent.app.monitor.strategies.base import Strategy
from lama.agent.app.utils import EstimateAvgValue
from lama.agent.event_managers.twisted import run_async_static
from lama.utils.enum import Enum
from lama.utils.logging_lama import logger

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2015, Carnegie Melon University'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class Processor(object):
    def process_value(self, ts, value):
        raise NotImplementedError()


class State(object):
    def __init__(self, fsm):
        self.fsm = fsm

    def process(self, ts, value):
        raise NotImplementedError("Please implement the process(ts, value) function in the child class (%s).",
                                  self.__class__.__name__)

    def enter(self):
        # Might not be implemented
        logger.info("[%s] Enter state: %s", self.fsm.get_id(), self.__class__.__name__)


class FSM(object):
    FSM_ID = 1

    def __init__(self, initial_state):
        self._id = "FSM %03d" % MinMaxFSM.FSM_ID
        MinMaxFSM.FSM_ID += 1
        self._state = initial_state

    def change_state(self, new_state):
        self._state = new_state(self)
        self._state.enter()

    def get_state(self):
        return self._state

    def get_id(self):
        return self._id


class MinMaxFSM(FSM):
    class InstanceLevel(Enum):
        LOW = 1
        MAYBE_LOW = 2
        NORMAL = 3
        MAYBE_HIGH = 4
        HIGH = 5

    class LowState(State):

        def enter(self):
            super().enter()
            if self.fsm.low_cb:
                self.fsm.low_cb()

        def process(self, ts, value):
            # logger.temp("Processing: %s:%s", ts, value)
            if value > self.fsm.low_thresh:
                # logger.info("[%s] New normal/high point: %s", self.fsm.get_id(), value)
                self.fsm.change_state(MinMaxFSM.NormalState)

        def get_instance_state(self):
            return MinMaxFSM.InstanceLevel.LOW

    class MaybeLowState(State):

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self._counter = 1

        def process(self, ts, value):
            if value < self.fsm.low_thresh:
                self._counter += 1
                logger.info("[%s] New low point - Counter: %d", self.fsm.get_id(), self._counter)
                if self._counter >= self.fsm.low_count_limit:
                    self.fsm.change_state(MinMaxFSM.LowState)
            else:
                logger.info("[%s] New normal/high point: %s", self.fsm.get_id(), value)
                self.fsm.change_state(MinMaxFSM.NormalState)

        def get_instance_state(self):
            return MinMaxFSM.InstanceLevel.MAYBE_LOW

    class NormalState(State):

        def process(self, ts, value):
            logger.temp("Processing: %s:%s", ts, value)
            if value < self.fsm.low_thresh:
                logger.info("[%s] New low point - Counter: 1", self.fsm.get_id())
                self.fsm.change_state(MinMaxFSM.MaybeLowState)
            elif value > self.fsm.high_thresh:
                logger.info("[%s] New high point - Counter: 1", self.fsm.get_id())
                self.fsm.change_state(MinMaxFSM.MaybeHighState)

        def get_instance_state(self):
            return MinMaxFSM.InstanceLevel.NORMAL

    class MaybeHighState(State):

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self._counter = 1

        def process(self, ts, value):
            if value > self.fsm.high_thresh:
                self._counter += 1
                logger.info("[%s] New high point - Counter: %d", self.fsm.get_id(), self._counter)
                if self._counter >= self.fsm.high_count_limit:
                    self.fsm.change_state(MinMaxFSM.HighState)
            else:
                logger.info("[%s] New normal/high point: %s", self.fsm.get_id(), value)
                self.fsm.change_state(MinMaxFSM.NormalState)

        def get_instance_state(self):
            return MinMaxFSM.InstanceLevel.MAYBE_HIGH

    class HighState(State):

        def enter(self):
            super().enter()
            if self.fsm.high_cb:
                self.fsm.high_cb()

        def process(self, ts, value):
            if value < self.fsm.high_thresh:
                self.fsm.change_state(MinMaxFSM.NormalState)

        def get_instance_state(self):
            return MinMaxFSM.InstanceLevel.HIGH

    def __init__(self, low_thresh=30, high_thresh=70, low_count_limit=3, high_count_limit=3, identifier=None):
        super().__init__(MinMaxFSM.NormalState(self))
        self.low_count_limit = low_count_limit
        self.high_count_limit = high_count_limit
        self.low_thresh = low_thresh
        self.high_thresh = high_thresh

        self.low_cb = lambda: print("Low usage")
        self.high_cb = lambda: print("High usage")

        self.last_value = None

        if identifier:
            self._id = "%s %s" % (self._id, identifier)

        logger.info("New FSM: %s", self._id)

    def process_value(self, ts, value):
        self.last_value = value
        # logger.temp("Start: %s", self._state)
        self._state.process(ts, value)

    def get_state(self):
        return self._state.get_instance_state()

    def get_last_value(self):
        return self.last_value


class SimpleMajorityScalingFSM(FSM):
    class ScaleDown(State):

        def update(self, instance, processor):
            pass

    class ScaleUp(State):

        def update(self, instance, processor):
            pass

    class Steady(State):

        def update(self, instance, processor):
            """
            :return:
            """
            if datetime.now() - self.fsm.grace_period_start < self.fsm.grace_period:
                logger.temp("In grace period for %s ",
                            self.fsm.grace_period - (datetime.now() - self.fsm.grace_period_start))
                return

            n = l = h = u = 0
            for instance in self.fsm.service.get_instances():
                n += 1
                s = self.fsm.get_instance_state(instance.name)
                if not s:
                    u += 1
                elif s == MinMaxFSM.InstanceLevel.LOW:
                    l += 1
                elif s == MinMaxFSM.InstanceLevel.HIGH:
                    h += 1

            logger.info("Update scaling: service=%s, n=%s (low/norm/high/unknown = %s/%s/%s/%s)", self.fsm.service.name,
                        n, l, n - (l + h), h, u)

            if l > h:
                if n > 1 and float(l) / n > self.fsm.prop_threshold:
                    logger.info("SCALE DOWN : %s of %s with LOW usage", l, n)
                    self.fsm.op_controller.change_instances(self.fsm.service.name, n - 1)
                else:
                    if n == 1:
                        logger.info("Not scaling: Minimum number of instances active")
                    else:
                        logger.info("Not scaling: Not enough LOW instances")
                if h > 0:
                    logger.info("WEIRD: there are instances with high usage in a low usage environment!")
            elif h > l:
                if float(h) / n > self.fsm.prop_threshold:
                    logger.info("SCALE UP: %s of %s with HIGH usage", h, n)
                    self.fsm.op_controller.change_instances(self.fsm.service.name, n + 1)
                else:
                    logger.info("Not scaling: Not enough HIGH instances")
                if l > 0:
                    logger.info("WEIRD: there are instances with low usage in a high usage environment!")

            else:
                # do nothing
                # we might want to consider weird cases where l == h != 0
                if l > 0:
                    logger.info("WEIRD: there are instances with low and high usage!")

    def __init__(self, strategy, service_name, prop_threshold=0.5, grace_period=60, op_controller=None):
        super().__init__(SimpleMajorityScalingFSM.Steady(self))

        self.strategy = strategy
        self.service_name = service_name
        self.instances_processor = {}
        self.op_controller = op_controller

        self.prop_threshold = prop_threshold
        self.grace_period = timedelta(seconds=grace_period)
        self.grace_period_start = datetime.now()

    @property
    def service(self):
        # logger.temp("accessing service - app_arch object: %r", self.strategy.app_arch)
        return self.strategy.app_arch.get_service(self.service_name)

    def update(self, instance_name, processor):
        logger.info("Updating instance '%s' = %s/%s", instance_name, processor.get_state(), processor.get_last_value())
        # self.instances_state[instance] = processor.get_state()
        # self.instances_value[instance] = processor.get_last_value()
        self._state.update(self.strategy.app_arch.get_vm(instance_name), processor)

    def add_processor(self, instance_name, processor):
        self.instances_processor[instance_name] = processor

    def get_instance_state(self, instance_name):
        instance = self.instances_processor.get(instance_name)
        return None if not instance else instance.get_state()

    def get_instance_value(self, instance_name):
        instance = self.instances_processor.get(instance_name)
        return None if not instance else instance.get_last_value()


class HierarchicalFSMStrategy(Strategy):
    InstanceMetrics = (VirtCPUChannel,)

    def __init__(self):
        super().__init__()
        self.service_fsm = {}

    def register_instance_channel(self, ch):
        # logger.info("Channel: %s %s %s", type(ch), ch.__class__.__name__, ch.metric)
        # if isinstance(ch, self.InstanceMetrics):
        if ch.metric == 'virt_cpu_total':
            instance = self.app_arch.get_vm(ch.entity_name)
            if instance:
                logger.info("Register channel of type '%s' - Instance: %s", type(ch), instance.name)
                scaling_fsm = self.get_scaling_fsm(instance)
                r = MinMaxFSM(identifier=instance.name)
                r.low_cb = partial(scaling_fsm.update, instance_name=instance.name, processor=r)
                r.high_cb = partial(scaling_fsm.update, instance_name=instance.name, processor=r)
                r.normal = partial(scaling_fsm.update, instance_name=instance.name, processor=r)
                ch.add_processor(r)
                scaling_fsm.add_processor(instance.name, r)
                # if iinstance(ch, )

    def register_host_channel(self, ch):
        if isinstance(ch, ()):
            pass

    def get_scaling_fsm(self, instance):
        # logger.info("Instance '%s' is at state: %s", ch.entity_name, processor.get_state())

        # get service for instance
        # instance = self.app_arch.get_vm(instance_name)
        # logger.info("app: %s", self.app_arch)
        # logger.info("instance: %s", instance)
        if instance:
            service = instance.get_service()

            # get or create, if necessary, an fsm for the service
            return self.service_fsm.setdefault(
                service.name, SimpleMajorityScalingFSM(self, service.name, op_controller=self.op_controller)
            )
        return None
        # # add value to the fsm to process
        # fsm.update(instance, processor)

    def get_instance_state(self, instance_name):
        instance = self.app_arch.get_vm(instance_name)
        service = instance.get_service()
        fsm = self.service_fsm.get(service.name)
        return fsm.get_instance_state(instance_name) if fsm else None

    def get_instance_value(self, instance_name):
        instance = self.app_arch.get_vm(instance_name)
        service = instance.get_service()
        fsm = self.service_fsm.get(service.name)
        return {"CPU": fsm.get_instance_value(instance_name) if fsm else None}

    def get_host_value(self, host_name):
        return None


class ChannelState(Processor):
    def __init__(self, entity_name, last=None, period=None, threshold=3,
                 up_callback=None,
                 down_callback=None):
        self.entity_name = entity_name
        self.last = last
        self._last_value = None
        self.period = EstimateAvgValue()
        if period:
            self.period.estimate(period if isinstance(period, timedelta) else timedelta(seconds=period))

        self.threshold = threshold
        self.down = False
        logger.debug("Registered ChannelState for %s", self.entity_name)

        self.check_timer = None
        self.up_callback = up_callback
        self.down_callback = down_callback

    def update(self, ts):
        if self.last and ts > self.last:
            self.period.estimate(ts - self.last)

        self.last = ts
        logger.debug("[%s] ts=%s estimate=%s", self.entity_name, ts, self.period.get_estimate())

    def check(self):
        if self.last and self.period.val:
            margin = self.period.val * self.threshold - (datetime.now() - self.last)
            logger.temp("Check margin: %s", margin)
            return self.period.val * self.threshold > datetime.now() - self.last
        else:
            return True

    def schedule_check(self, delay):
        if self.period.val:
            if self.check_timer:
                logger.debug("Canceled timer %s", self.check_timer)
                try:
                    self.check_timer.cancel()
                except:
                    pass
            # self.check_timer = Timer(ceil(delay.seconds), self.check_state)
            round_delay = ceil(delay.total_seconds())
            logger.debug("Timer %s in %s", self.check_timer, round_delay)
            self.check_timer = run_async_static(self.check_state, timeout=round_delay)

    def process_value(self, ts, value):
        self._last_value = value
        logger.temp("[%s] Process value: %s %s", self.entity_name, ts, value)
        self.update(ts)
        offset = datetime.now() - self.last
        logger.temp("[%s] Offset between now and ts: %s", self.entity_name, offset)
        if self.period.val:
            self.schedule_check(self.period.val * self.threshold)

    def check_state(self):
        check = self.check()
        logger.temp("[%s] down=%s check=%s", self.entity_name, self.down, check)
        if self.down and check:
            # instance is now up...
            if self.up_callback:
                self.up_callback()
            # self.op_controller.mark_instance_up(self.entity_name)
            logger.debug("[%s] Channel back up", self.entity_name)
        elif not self.down and not check:
            # if is out of date
            logger.debug("[%s] Channel seems to be down", self.entity_name)
            self.down = True
            if self.down_callback:
                self.down_callback()
                # self.op_controller.mark_instance_down(service_name=self.service_name, instance_name=self.instance_name)
                # self.op_controller.remove_instance(service_name=self.service_name, instance_name=self.instance_name)

        if self.period.val:
            if self.down:
                d = self.period.val
            else:
                margin = self.period.val * self.threshold - (datetime.now() - self.last)
                d = max(margin, timedelta())
                logger.temp("Margin: %s", margin)
            self.schedule_check(d)

    def get_last_value(self):
        return None if self.down else self._last_value

    def get_state(self):
        if self._last_value < 30:
            return MinMaxFSM.InstanceLevel.LOW
        elif self._last_value < 75:
            return MinMaxFSM.InstanceLevel.NORMAL
        else:
            return MinMaxFSM.InstanceLevel.HIGH


class InstanceChannelState(ChannelState):
    def __init__(self, service_name, instance_name, op_controller, last=None, period=None, threshold=3):
        self.service_name = service_name
        self.instance_name = instance_name
        self.op_controller = op_controller

        super().__init__(
            "{}:{}".format(service_name, instance_name),
            last=None, period=None, threshold=3,
            up_callback=partial(
                self.op_controller.mark_instance_up, service_name=service_name, instance_name=instance_name
            ),
            down_callback=partial(
                self.op_controller.mark_instance_down, service_name=service_name, instance_name=instance_name
            )
        )


class HostChannelState(ChannelState):
    def __init__(self, ip_address, op_controller, last=None, period=None, threshold=3):
        super().__init__(
            ip_address,
            last=None, period=None, threshold=3,
            up_callback=partial(
                op_controller.mark_host_up, ip_address=ip_address
            ),
            down_callback=partial(
                op_controller.mark_host_down, ip_address=ip_address
            )
        )


class OnOffStrategy(Strategy):
    def process_channel_value(self, ts, value):
        logger.error("process_channel_value called!!")

    def register_channel(self, ch):
        logger.error("register_channel called!!")

    def __init__(self):
        super().__init__()
        self.instance_channels = {}
        self.host_channels = {}

    def register_instance_channel(self, ch):
        # logger.info("Channel: %s %s %s", type(ch), ch.__class__.__name__, ch.metric)
        if isinstance(ch, VirtCPUChannel) and ch.metric == 'virt_cpu_total':
            instance = self.app_arch.get_vm(ch.entity_name)
            if instance:
                service = instance.get_service()
                if service.subtype == LogicalNodeSubtype.client:
                    # TODO: Review - skip client in the current workflow
                    return
                logger.info("Register channel of type '%s' - Instance: %s", ch.__class__.__name__, instance.name)
                p = InstanceChannelState(service.name, instance.name, op_controller=self.op_controller)
                self.instance_channels.setdefault(
                    instance.name,
                    p
                )
                ch.add_processor(p)

    def register_host_channel(self, ch):
        if isinstance(ch, CPUChannel) and ch.metric == 'cpu' and str(ch.arg) == "0":
            host = self.app_arch.get_host(ch.entity_name)
            logger.temp("Registering host channel: entity=%s host=%s", ch.entity_name, host)
            if host:
                logger.info("Register channel of type '%s' - Host: %s", ch.__class__.__name__, host.ip_address)
                p = HostChannelState(host.ip_address, op_controller=self.op_controller)
                self.host_channels.setdefault(
                    host.ip_address,
                    p
                )
                ch.add_processor(p, 'cpu-idle')

    def get_instance_value(self, instance_name):
        instance = self.instance_channels.get(instance_name)
        return None if not instance else {"CPU": instance.get_last_value()}

    def get_instance_state(self, instance_name):
        instance = self.instance_channels.get(instance_name)
        return None if not instance else instance.get_state()


class RuleChannelState(Processor):
    def __init__(self, entity_name, rule, true_callback=None, false_callback=None, max_count=3):
        self.entity_name = entity_name
        self.rule = rule
        self.down = False
        logger.debug("Registered RuleChannelState for %s", self.entity_name)

        self.true_callback = true_callback
        self.false_callback = false_callback

        self._max_count = max_count
        self._count = 0
        self._state = False

    def process_value(self, ts, value):
        logger.temp("[%s] Process value: %s %s", self.entity_name, ts, value)
        if value:
            try:
                if self.rule(value):
                    if not self._state:
                        self._count += 1
                        logger.temp("[%s] Increase count: %s %s", self.entity_name, ts, value)
                        if self._count >= self._max_count:
                            self._state = True
                            if self.true_callback:
                                self.true_callback()
                else:
                    if self._state and self.false_callback:
                        self.false_callback()
                    self._state = False
                    self._count = 0

            except:
                logger.error("Error evaluating value: %s. Traceback: %s", value, traceback.format_exc())


class Hang100PerCentStrategy(Strategy):
    def __init__(self):
        super().__init__()
        self.instance_channels = {}
        self.host_channels = {}

    def register_instance_channel(self, ch):
        # logger.info("Channel: %s %s %s", type(ch), ch.__class__.__name__, ch.metric)
        # ch.arg is None for the total cpu: TODO: explicitly set total as arg
        if isinstance(ch, VirtCPUChannel) and ch.metric == 'virt_cpu_total':
            instance = self.app_arch.get_vm(ch.entity_name)
            if instance:
                service = instance.get_service()

                logger.info("Register channel of type '%s' - Instance: %s", ch.__class__.__name__, instance.name)
                p = RuleChannelState(
                    instance.name, lambda x: x > 98, max_count=5,
                    true_callback=partial(
                        self.op_controller.mark_instance_down, service_name=service.name,
                        instance_name=instance.name
                    ),
                )
                ch.add_processor(p)

    def register_host_channel(self, ch):
        pass

    def process_channel_value(self, ts, value):
        pass

    def register_channel(self, ch):
        pass

