import numpy as np
from datetime import datetime

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2015, Carnegie Melon University'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class DataBuffer(object):
    """
    DataBuffer supported by numpy array. Stores the last <size> values.
    Currently does not support removal of values (so, it always uses the last values, for as old as they might be)
    """

    def __init__(self, size=10):
        self.data = np.zeros(size, dtype='f')
        self.maxsize = size
        self.index = 0
        self.size = 0

    def add(self, val):
        self.data[self.index] = val
        self.index = (self.index + 1) % self.data.size
        self.size = self.size + 1 if self.maxsize > self.size else self.maxsize

    def get_average(self):
        if self.size < self.maxsize:
            return np.average(self.data[0:self.index])
        else:
            return np.average(self.data)

    def get_last(self):
        if self.size:
            return self.data[(self.index - 1) % self.maxsize]
        return None

    def __len__(self):
        return self.size


class TimeAwareDataBuffer(DataBuffer):

    def __init__(self, size=10):
        super().__init__(size)
        # self.ts = np.zeros(size, dtype='datetime64')
        # was unable to use numpy for this and get seconds...
        self.ts = [0] * size

    def add_with_time(self, ts, val):
        self.ts[self.index] = self.ts_to_milliseconds(ts)
        self.add(val)

    def get_data(self, min_ts=None):
        # print("get ts: %s" % self.ts)
        # print("get data: %s" % self.data)
        # print("size: %s" % self.size)
        r = []
        if min_ts:
            idx = (self.index - self.size) % self.maxsize
            for i in range(self.size):
                # print("ts = %s , min_ts = %s" % (self.ts[idx], min_ts))
                if not min_ts or self.ts[idx] > min_ts:
                    r.append((self.ts[idx], float(self.data[idx])))
                idx = (idx + 1) % self.maxsize
            # print("buffer return: %s" % r)
        else:
            r = []
            if self.size > 0:
                idx = (self.index - 1) % self.maxsize
                r.append((self.ts[idx], float(self.data[idx])))
        return r

    def ts_to_milliseconds(self, dt):
        return dt.timestamp() * 1000

