__author__ = 'tiago'

# WORKLOAD
# Create simple predictor of the variation of workload based on previous t seconds (currently
# we have one data point per 10 seconds)
# We can use either number of requests at the user level, the volume of incoming network traffic
# or number of TCP connections
# Returns a likelihood matrix of the volume of workload for the next window


# RESOURCE USAGE
# Map from workload to resource usage
# Eventually, we can try different probability distributions based on the workload value.
# Independently We can only due to it to CPU, and IO operations.
# We can maybe have different maps for different RAM memory values.

# WORKLOAD / RESOURCES MAP
# Simple calculation of the resources to deploy based on the likelihood of the workload

# WHA INSTRUMENTATION
# Given the evolution of resources needed


class AppResponseTimeEstimator(object):

    def __init__(self, app_arch):
        self.app_arch = app_arch


class BayesNetworkEstimator(AppResponseTimeEstimator):
    pass

    # should create a naive bayes(?) model to estimate response time
    # should be able to answer quesiton like -+ n % in workload... what is the probability
    # of response time greater than some limit?
