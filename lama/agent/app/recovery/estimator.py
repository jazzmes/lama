from datetime import timedelta

from lama.agent.app.app_arch import InstanceState
from lama.agent.app.utils import EstimateAvgValue
from lama.utils.enum import Enum
from lama.utils.logging_lama import logger

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class DeploymentPhase(Enum):
    Allocation = 1
    Configuration = 2
    DeployWithImageCopy = 3
    DeployWithoutImageCopy = 4
    StartUp = 5
    Resume = 6


class RecoveryEstimator(object):

    def __init__(self, detection_time=30):
        self._detection_time = timedelta(seconds=detection_time)
        self.defaults = {
            # Time taken by LAMA to allocate an instance
            DeploymentPhase.Allocation: 5,
            # Time taken by LAMA to configure a new instance
            DeploymentPhase.Configuration: 10,
            # Time taken to copy the image:
            # TODO: should depend on the link characteristics
            DeploymentPhase.DeployWithImageCopy: 60,
            DeploymentPhase.DeployWithoutImageCopy: 5,
            # Time taken to start the image
            DeploymentPhase.StartUp: 20,
            # Time to unpause
            DeploymentPhase.Resume: 5,
        }

        self.state_phases = {
            InstanceState.Cold: [
                DeploymentPhase.Allocation,
                DeploymentPhase.Configuration,
                DeploymentPhase.DeployWithImageCopy,
                DeploymentPhase.StartUp,
            ],
            InstanceState.Warm: [
                DeploymentPhase.StartUp,
            ],
            InstanceState.Hot: [
                DeploymentPhase.Resume,
            ],
            InstanceState.Active: [
            ]
        }

        self.concurrency_sensitive_phases = {
            DeploymentPhase.DeployWithImageCopy: lambda v, f: v * f,
        }

        self._intermediary_times = {}
        # internal timestamps
        self._deployment_times = {}

    def add_start_timestamp(self, phase, timestamp, instance):
        logger.temp("Added new START timestamp to %s: %s -> %s", phase.name, instance.name, timestamp)
        self._intermediary_times.setdefault(phase, {}).setdefault(instance.name, timestamp)
        logger.temp("%s %s", self, self._intermediary_times)

    def add_end_timestamp(self, phase, timestamp, instance):
        logger.temp("%s %s", self, self._intermediary_times)
        req_ts = self._intermediary_times.get(phase, {}).get(instance.name)
        logger.temp("Added new END timestamp to %s: %s -> %s (start: %s)", phase.name, instance.name, timestamp, req_ts)
        if req_ts:
            delta = timestamp - req_ts
            self.add_estimation(phase, delta, instance)
            del self._intermediary_times.get(phase, {})[instance.name]

    def add_estimation(self, phase, delta, instance):
        service = instance.get_service()

        logger.temp("Added new time to %s: %s -> %s", phase.name, service.name, delta)
        self._deployment_times.setdefault(
            phase, {}
        ).setdefault(
            service.name, EstimateAvgValue(
                default=self.defaults.get(phase)
            )
        ).estimate(delta)

    def get_deployment_time(self, state, service, concurrency_factor=None):
        service_name = service if isinstance(service, str) else service.name
        logger.temp("Get deployment time: state=%s, service=%s", state.name, service_name)
        # logger.temp("\t%s", self._deployment_times)
        # total = timedelta(seconds=0)
        total = self._detection_time
        for phase in self.state_phases.get(state):
            service_est = self._deployment_times.get(phase, {}).get(service_name)
            if not service_est:
                value = timedelta(seconds=self.defaults[phase])
            else:
                value = service_est.get_estimate()
            if concurrency_factor and phase in self.concurrency_sensitive_phases:
                value = self.concurrency_sensitive_phases.get(phase)(value, concurrency_factor)
            total += value

        return total

    def get_coldest_instance_state(self, service, max_time):
        for i in range(InstanceState.Cold.value, 0, -1):
            state = InstanceState(i)
            if self.get_deployment_time(state, service) <= max_time:
                return state

        return InstanceState.Active