import traceback
import demjson
from lama.agent.app.connectors.app_connector import LamaAppConnector, Connection
from lama.utils.logging_lama import logger

__author__ = 'Tiago'


class Connector(LamaAppConnector):

    def __init__(self, app, user='lama', password='llama', *args, **kwargs):
        super().__init__(app, user, password)

        self.handlers = {
            "apache": self.handle_apache_instance,
            "mysql": self.handle_mysql_instance,
            "mysqlc-man": self.handle_mysql_man_instance,
            "mysqlc-sql": self.handle_mysql_sql_instance,
            "mysqlc-data": self.handle_mysql_data_instance,
            "client": self.handle_client_instance,
        }

        self.client = RubbosClientHandler(app.name)
        self.manager = RubbosMysqlClusterManagerHandler(app.name)
        self.apache = RubbosApacheHandler(app.name)

    def start_instance(self, instance):
        # identify type of node
        # call correspond instance
        logger.temp("Instance '%s' is starting", instance)
        service = instance.get_service()
        handler = self.handlers.get(service.name)
        if handler:
            handler(instance)

        logger.temp("Done handling start_instance for %s", instance)

    def run_instance(self, instance, config=None):
        logger.temp("Running instance: %s", instance)
        service = instance.get_service()
        if service.name == "client":
            with Connection(instance.ip, self._user, self._password) as conn:
                self.client.run(instance, conn, config)

    def update_instance(self, instance):
        self.start_instance(instance)

    def stop_instance(self, instance):
        logger.temp("Stopping instance %s - Nothing to do", instance)

    def get_manager_instance(self):
        service = self._app.get_service("mysqlc-man")
        try:
            return service.get_instances()[0]
        except:
            return None

    def handle_mysql_data_instance(self, instance):
        man = self.get_manager_instance()
        if man:
            # pass
            self.handle_mysql_man_instance(man, start=False)
        else:
            logger.warn("Unable to get MySQL Cluster Manager instance")

    def handle_mysql_sql_instance(self, instance):
        man = self.get_manager_instance()
        if man:
            # pass
            self.handle_mysql_man_instance(man, start=False)
        else:
            logger.warn("Unable to get MySQL Cluster Manager instance")

    def handle_mysql_instance(self, instance):
        pass

    def handle_mysql_man_instance(self, instance, start=True):
        logger.temp("Handle manager instance")
        service = instance.get_service()
        logger.temp("Service: %s", service)
        if "config" in service.extra:
            json_config = service.extra.get("config")
            try:
                config = demjson.decode(json_config)
                logger.temp("Adding custom config: %s", config)
                self.manager.add_config(config)
            except:
                logger.warn("Exception adding config: '%s' : %s", json_config, traceback.format_exc())

        logger.temp("Connecting to instance: %s@%s", self._user, instance.ip)
        try:
            with Connection(instance.ip, self._user, self._password) as conn:
                if start:
                    self.manager.start_node(instance, conn)
                else:
                    self.manager.update_node(instance, conn)
        except OSError:
            logger.detail("Unable to connect to manager!")

        if self.manager.changed:
            self.manager.changed = False
            # restart DATA instances...
            # cmd1 = "sudo /bin/systemctl stop ndbd"
            # cmd2 = "sudo /bin/systemctl start ndbd"
            # make sure the working directory exists... TODO: move to the startup script inside the VM
            cmds = (
                "sudo mkdir -p /usr/local/mysql/data",
                "sudo /etc/init.d/ndbd.sh stop",
                "sudo /etc/init.d/ndbd.sh start"
            )

            prefix = service.extra.get("prefix_data_nodes")
            self.run_commands_for_prefix(service, prefix, *cmds)

            # logger.temp("Prefix: %s", prefix)
            # if not prefix:
            #     logger.warn("Prefix not defined (prefix_data_nodes)")
            # else:
            #     for s in service.get_successors():
            #         if s.name.startswith(prefix):
            #             instances = s.get_instances()
            #             instances.sort(key=lambda x: str(x.ip))
            #             logger.temp("Restarting instances: %s", instances)
            #             for i in instances:
            #                 if i.ip:
            #                     try:
            #                         with Connection(i.ip, self._user, self._password) as conn:
            #                             conn.exec(cmd1)
            #                             conn.exec(cmd2)
            #                     except OSError:
            #                         pass
            #                     except:
            #                         logger.error("Unhandled ERROR when conecting to instance %s: %s", i.ip, traceback.format_exc())

            cmd1 = "sudo /etc/init.d/mysql.server stop"
            cmd2 = "sudo /etc/init.d/mysql.server start"
            prefix = service.extra.get("prefix_api_nodes")
            self.run_commands_for_prefix(service, prefix, *(cmd1, cmd2))

    def run_commands_for_prefix(self, service, prefix, *cmds):
        logger.temp("Prefix: %s", prefix)
        if not prefix:
            logger.warn("Prefix not defined (prefix_api_nodes)")
        else:
            for s in service.get_successors():
                if s.name.startswith(prefix):
                    instances = s.get_instances()
                    instances.sort(key=lambda x: str(x.ip))
                    logger.temp("Restarting instances: %s", instances)
                    for i in instances:
                        if i.ip:
                            try:
                                with Connection(i.ip, self._user, self._password) as conn:
                                    for cmd in cmds:
                                        conn.exec(cmd)
                            except OSError:
                                pass
                            except:
                                logger.error("Unhandled ERROR when conecting to instance %s: %s", i.ip, traceback.format_exc())

    def handle_client_instance(self, instance):
        logger.temp("Handle client instance")
        with Connection(instance.ip, self._user, self._password) as conn:
            self.client.start_node(instance, conn)

    def handle_apache_instance(self, instance):
        logger.temp("Handle apache instance")
        with Connection(instance.ip, self._user, self._password) as conn:
            self.apache.start_node(instance, conn)


class RubbosClientHandler(object):

    DefaultConfigFile = "/home/lama/rubbos/rubbos.properties"

    def __init__(self, app_name, overwrite_config=False, **kwargs):
        """

        :param overwrite_config: if config is to be rewritten or only add options are to be overwritten
        :return:
        """
        self.default_config = {}
        self.configs = {}
        self.overwrite_config = overwrite_config

    def add_config(self, config_dict, vm_ip=None):
        """

        :param config_dict: dictionary with all the config options to be set (configuration is replaced)
        :param vm_ip: optionally indicate to which VM the configuration apply; if not set,
        configuration will be default for all VMs that do not have a specific configuration
        :return:
        """
        if vm_ip:
            self.configs[vm_ip] = config_dict
        else:
            self.default_config = config_dict

    def update_node(self, instance, conn):
        # setup
        self.edit_config_file(instance, conn)

    def start_node(self, instance, conn):
        # setup
        self.edit_config_file(instance, conn)

    def run(self, instance, conn, config):
        self.add_config(config)
        self.edit_config_file(instance, conn)

        # testing run command
        cmd = "cd rubbos && nohup make emulator > rubbos.out 2>&1"
        logger.info("[%s] Execute command: %s", conn.ip, cmd)
        res = conn.exec(cmd, get_pty=False, background=True)
        logger.debug("[%s] Result of command: %s", conn.ip, res)

    def edit_config_file(self, instance, conn):
        logger.temp("Writing configuration")
        # get the configuration for this VM
        cfg = self.configs[conn.ip] if conn.ip in self.configs else self.default_config
        content = []

        try:
            service = instance.get_service()
            logger.temp("service = %s, successors = %s", service, service.get_all_successors())
            child_service = service.get_all_successors()[0]
            logger.temp("child_service = %s, instances = %s", service, child_service.get_instances())
            first_instance = child_service.get_instances()[0]
            cfg["httpd_hostname"] = first_instance.virtual_ip or first_instance.ip
        except:
            logger.warn("Error editing the configuration (apache app): %s", traceback.format_exc())

        logger.temp("Adding configuration options: %s", cfg)

        if not cfg:
            return

        used_keys = set(cfg.keys())
        # logger.temp("Config: %s", cfg)

        if self.overwrite_config:
            content = ["%s=%s" % (k, v) for k, v in cfg.items()]
        else:
            original_content = conn.get_file(self.DefaultConfigFile)
            # logger.debug(original_content)
            if original_content:
                original_content = original_content.splitlines()
                # logger.temp("Content received: %s", original_content)
                # read all the lines
                for line in original_content:
                    # logger.temp("Line: %s", line)
                    # for each line, check if is not a comment, split, and verify if we have a new configuration
                    fields = line.strip().split("=")
                    # edit if necessary
                    if len(fields) == 2:
                        k, v = fields
                        k = k.strip()
                        if k.strip() in cfg:
                            l = "%s = %s" % (k, cfg[k])
                            used_keys.remove(k.strip())
                            line = l
                    # else:
                    #     logger.temp("Line: %s", line)

                    content.append(line)

                if used_keys:
                    logger.warn("The following config keys were not used: %s", ", ".join(used_keys))

        logger.debug("Content: \n%s" % "\n".join(content))
        if content:
            conn.put_file(self.DefaultConfigFile, "\n".join(content))


class RubbosApacheHandler(object):

    DefaultConfigFile = "/home/lama/rubbos/PHP/config.php"

    def __init__(self, app_name, overwrite_config=False, **kwargs):
        """

        :param overwrite_config: if config is to be rewritten or only add options are to be overwritten
        :return:
        """
        self.default_config = {}
        self.configs = {}
        self.overwrite_config = overwrite_config

    def add_config(self, config_dict, vm_ip=None):
        """

        :param config_dict: dictionary with all the config options to be set (configuration is replaced)
        :param vm_ip: optionally indicate to which VM the configuration apply; if not set,
        configuration will be default for all VMs that do not have a specific configuration
        :return:
        """
        if vm_ip:
            self.configs[vm_ip] = config_dict
        else:
            self.default_config = config_dict

    def update_node(self, instance, conn):
        # setup
        self.edit_config_file(instance, conn)

    def start_node(self, instance, conn):
        # setup
        self.edit_config_file(instance, conn)

    def edit_config_file(self, instance, conn):
        logger.temp("Writing configuration")
        # get the configuration for this VM
        cfg = self.configs[conn.ip] if conn.ip in self.configs else self.default_config
        content = []

        try:
            service = instance.get_service()
            logger.temp("service = %s, successors = %s", service, service.get_all_successors())
            child_service = service.get_all_successors()[0]
            logger.temp("child_service = %s, instances = %s", service, child_service.get_instances())
            first_instance = child_service.get_instances()[0]
            cfg["host"] = first_instance.virtual_ip or first_instance.ip
        except:
            logger.warn("Error editing the configuration (apache app): %s", traceback.format_exc())

        logger.temp("Adding configuration options: %s", cfg)

        if not cfg:
            return

        used_keys = set(cfg.keys())
        # logger.temp("Config: %s", cfg)

        if self.overwrite_config:
            logger.error("Overwrite config not supported at %s", self.__class__.__name__)
            # content = ["%s=%s" % (k, v) for k, v in cfg.items()]
        else:
            original_content = conn.get_file(self.DefaultConfigFile)
            logger.debug("Original content: \n%s" % "\n".join(content))

            # logger.debug(original_content)
            if original_content:
                original_content = original_content.splitlines()
                # read all the lines
                for line in original_content:
                    # for each line, check if is not a comment, split, and verify if we have a new configuration
                    fields = line.strip().split("=>")
                    # edit if necessary
                    if len(fields) == 2:
                        k, v = fields
                        k = k.strip("'\" ,")
                        # v = v.strip("'\" ,")
                        if k.strip() in cfg:
                            l = "\t'%s' => '%s'," % (k, cfg[k])
                            used_keys.remove(k.strip())
                            line = l

                    content.append(line)

                if used_keys:
                    logger.warn("The following config keys were not used: %s", ", ".join(used_keys))

        logger.debug("Final content: \n%s" % "\n".join(content))
        if content:
            conn.put_file(self.DefaultConfigFile, "\n".join(content))


class RubbosMysqlClusterManagerHandler(object):

    DefaultConfigFile = "/var/lib/mysql-cluster/config.ini"
    NodeIdMax = 63

    def __init__(self, app_name, **kwargs):
        """

        :param overwrite_config: if config is to be rewritten or only add options are to be overwritten
        :return:
        """
        self.default_config = {
            "NoOfReplicas": 1,
            "DataMemory": "1G",
            "IndexMemory": "150M",
        }
        self.configs = {}
        self.node_ids = {}
        self.used_ids = set()
        self.changed = True

    def add_config(self, config_dict, vm_ip=None):
        """
        :param config_dict: dictionary with all the config options to be set (configuration is replaced)
        :param vm_ip: optionally indicate to which VM the configuration apply; if not set,
        configuration will be default for all VMs that do not have a specific configuration
        :return:
        """
        logger.temp("@add config")

        if vm_ip:
            self.configs[vm_ip] = config_dict
        else:
            self.default_config.update(config_dict)

    def start_node(self, instance, conn):
        logger.temp("@start node: %s", instance)
        # setup
        self.edit_config_file(instance, conn, force_restart=True)

    def update_node(self, instance, conn):
        logger.temp("@update node: %s", instance)
        # setup
        self.edit_config_file(instance, conn)

    def get_id(self, ip):
        node_id = self.node_ids.get(ip)
        if not node_id:
            for temp_id in range(1, RubbosMysqlClusterManagerHandler.NodeIdMax + 1):
                if temp_id not in self.used_ids:
                    node_id = temp_id
                    self.node_ids[ip] = node_id
                    self.used_ids.add(node_id)
                    break

        return node_id

    def edit_config_file(self, instance, conn, force_restart=False):
        logger.warn("RubbosMysqlClusterManagerConnector: Currently only supports scaling sql nodes!")
        logger.temp("Writing configuration")

        cfg = self.default_config.update(self.configs[conn.ip]) if conn.ip in self.configs else self.default_config
        logger.temp("Adding configuration options: %s", cfg)

        original_file = conn.get_file(self.DefaultConfigFile)

        # head of file
        content = [
            "# Auto generated by LAMA",
            "[ndbd default]",
        ]

        content += ["%s=%s" % (k, v) for k, v in cfg.items()]
        # "NoOfReplicas=1",
        # "DataMemory=320M",
        # "IndexMemory=80M"

        content += [
            "",
            "[tcp default]",
            ""
        ]

        # manager section
        node_id = self.get_id(instance.ip)
        content += [
            "[ndb_mgmd]",
            "nodeid=%s" % node_id,
            "hostname=%s" % instance.ip,
            "datadir=/var/lib/mysql-cluster",
            ""
        ]

        logger.temp("Add data nodes")
        service = instance.get_service()
        # logger.temp("service extra: %s", service.extra)
        # data nodes
        prefix = service.extra.get("prefix_data_nodes")
        logger.temp("Prefix: %s", prefix)
        if not prefix:
            logger.warn("Prefix not defined (prefix_data_nodes)")
        else:
            for s in service.get_successors():
                logger.temp("Service name: %s", s.name)
                if s.name.startswith(prefix):
                    instances = s.get_instances()
                    instances.sort(key=lambda x: str(x.ip))
                    logger.temp("Instances: %s", instances)
                    for i in instances:
                        logger.temp("Instance IP: %s", i.ip)
                        if i.ip:
                            node_id = self.get_id(i.ip)
                            logger.temp("Node Id: %s", node_id)
                            content += [
                                "[ndbd]",
                                "nodeid=%s" % node_id,
                                "hostname=%s" % i.ip,
                                "datadir=/usr/local/mysql/data",
                                ""
                            ]
                        else:
                            logger.warn("Instance '%s' not added to the configuration: no IP assigned yet!", i.name)

        logger.temp("Add API nodes")
        # api nodes
        prefix = service.extra.get("prefix_api_nodes")
        if not prefix:
            logger.warn("Prefix not defined (prefix_api_nodes)")
        else:
            for s in service.get_successors():
                if s.name.startswith(prefix):
                    instances = s.get_instances()
                    instances.sort(key=lambda x: str(x.ip))
                    for i in instances:
                        if i.ip:
                            node_id = self.get_id(i.ip)
                            content += [
                                "[mysqld]",
                                "nodeid=%s" % node_id,
                                "hostname=%s" % i.ip,
                                ""
                            ]
                        else:
                            logger.warn("Instance '%s' not added to the configuration: no IP assigned yet!", i.name)

        new_file = "\n".join(content)
        logger.temp("New file: %s", new_file)
        logger.temp("Original file: %s", original_file)
        if new_file != original_file:
            logger.temp("Different files!")
            conn.put_file(self.DefaultConfigFile, new_file)
            self.changed = True
            self.restart_manager(conn)
        elif force_restart:
            logger.temp("No configuration change but force start/restart!")
            self.changed = True
            self.restart_manager(conn)
        else:
            logger.temp("Configuration file has not changed!")

    def restart_manager(self, conn):
        logger.info("Restart MySQL Manager @ %s", conn.ip)
        cmd = "sudo /etc/init.d/ndb_mgmd.sh restart --initial"
        logger.temp("Cmd: %s", cmd)
        conn.exec(cmd)
        logger.info("Exit exec")

    def start_manager(self, conn):
        logger.info("Start MySQL Manager @ %s", conn.ip)
        cmd = "sudo /etc/init.d/ndb_mgmd.sh start --initial"
        logger.temp("Cmd: %s", cmd)
        conn.exec(cmd)
        logger.info("Exit exec")

    def reload_manager(self, conn):
        logger.info("Reload MySQL Manager @ %s", conn.ip)
        cmd = "sudo /etc/init.d/ndb_mgmd.sh restart --reload"
        logger.temp("Cmd: %s", cmd)
        conn.exec(cmd)
        logger.info("Exit exec")

# needs to handle all requests to a rubbos application
# events: added, start

# * *     * * *   root    /etc/init.d/ndbd.sh status || /etc/init.d/ndbd.sh start