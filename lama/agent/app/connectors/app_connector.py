from importlib import import_module
import traceback
from paramiko.client import SSHClient, AutoAddPolicy
from lama.agent.app.connectors import keepalived
from lama.utils.logging_lama import logger
from multiprocessing import Process, Queue
from lama.utils.temp_netns import setns
# noinspection PyUnresolvedReferences
import lama.agent.app.connectors

__author__ = 'tiago'


class InvalidFunction(Exception):

    def __init__(self, name, exception):
        super().__init__("Function '%s' failed with exception: %s" % (name, exception))


class InvalidCommand(Exception):
    pass


class ErrorRunningCommand(Exception):
    pass


class NoConnectorException(Exception):
    pass


class LamaAppConnector(object):

    def __init__(self, app, user=None, password=None):
        """
        :param app: handle to the application
        :param user: username to access the VM
        :param password: password to access the VM
        :return:
        """
        self._app = app
        self._user = user
        self._password = password

    def start_instance(self, instance):
        logger.warn("Connector not implemented for app: %s", self._app.name)

    def update_instance(self, instance):
        logger.warn("Connector not implemented for app: %s", self._app.name)

    def stop_instance(self, instance):
        logger.warn("Connector not implemented for app: %s", self._app.name)

    @staticmethod
    def get_connector(cls_name, app, **kwargs):
        if cls_name:
            try:
                mod = import_module('.' + cls_name, package='lama.agent.app.connectors')
                logger.info("Module '%s' loaded", mod)
                return getattr(mod, "Connector")(app, **kwargs)
            except:
                logger.info("Connector '%s' does not exist" % cls_name)
                logger.temp("Traceback: %s", traceback.format_exc())
        else:
            logger.info("No app connector defined!")

        return None


class AppConnector(object):

    def __init__(self, vm_ip, user, password, ns=None):
        """
        :param ns: the network namespace name to communicate with the VMs
        :param vm_ip: the private IP of the VM (the host IP should not be necessary; we can add more VMs later)
        :param user: username to access the VM
        :param password: password to access the VM
        :return:
        """
        self.__ns = ns
        self.__user = user
        self.__password = password

        # key of connections is <vm_ip>
        # connection None means that the Connector is currently not connected to the client app
        self.__connections = {}

        self.add_node(vm_ip)

    def add_node(self, vm_ip):
        """
        Add a new client of the app. Each App can have multiple client nodes to emulate different or more complex
         workloads.
        :param vm_ip: the private IP of the VM
        :return:
        """
        if vm_ip not in self.__connections:
            if self.__ns:
                raise NotImplementedError()
                # self.__connections[vm_ip] = VMConnectionClient(self.__ns, vm_ip, self.__user, self.__password)
            else:
                self.__connections[vm_ip] = VMConnection(vm_ip, self.__user, self.__password)

    def connect(self):
        raise NotImplementedError()

    def exit(self):
        raise NotImplementedError()

    def get_connection(self, vm_ip):
        if vm_ip not in self.__connections:
            conn = VMConnection(vm_ip, self.__user, self.__password)
            self.__connections[vm_ip] = conn
        else:
            conn = self.__connections[vm_ip]

        # try:
        return conn

    def start_by_vm(self, vm_ip):
        conn = self.get_connection(vm_ip)
        conn.connect_to_vm()
        # except OSError as e:
        #     logger.warn("Unable to connect to '%s': %s", vm_ip, e)
        #     return False
        # except:
        #     logger.warn("Unable to connect to '%s'. Traceback: %s:%s", vm_ip, traceback.format_exc())
        #     return False
        try:
            self.start_node(conn)
        except Exception as e:
            logger.error("Error on start_node: %s", traceback.format_exc())
            raise e
        finally:
            conn.close()

    def update_by_vm(self, vm_ip):
        conn = self.get_connection(vm_ip)
        conn.connect_to_vm()
        try:
            self.update_node(conn)
        except Exception as e:
            logger.error("Error on start_node: %s", traceback.format_exc())
            raise e
        finally:
            conn.close()

    def stop_by_vm(self, vm_ip):
        conn = self.get_connection(vm_ip)
        conn.connect_to_vm()
        try:
            self.stop_node(conn)
        except Exception as e:
            logger.error("Error on start_node: %s", traceback.format_exc())
            raise e
        finally:
            conn.close()

    def start_instance(self, instance):
        self.start_by_vm(instance.ip)

    def update_instance(self, instance):
        self.update_by_vm(instance.ip)

    def stop_instance(self, instance):
        self.stop_by_vm(instance.ip)

    def start(self, vm_ip=None):
        """
        Start a client. If the application is already running raise a AlreadyRunningException.
        :param vm_ip: the IP of the client to be started. If a client IP is not provided all clients will be started.
        :return:
        """
        logger.detail("Start")
        vms = [vm_ip] if vm_ip else list(self.__connections.keys())
        logger.detail("Starting vms: %s", vms)
        for ip in vms:
            if ip not in self.__connections or not self.__connections[ip].connected:
                if self.__ns:
                    raise NotImplementedError("VM Connection through NS was discontinued")
                    # self.__connections[vm_ip] = VMConnectionClient(self.__ns, ip, self.__user, self.__password)
                    # self.__connections[vm_ip].connect()
                else:
                    self.__connections[vm_ip] = VMConnection(ip, self.__user, self.__password)

            try:
                self.start_node(self.__connections[vm_ip])
            except Exception as e:
                logger.error("Exception: %s", traceback.format_exc())
            finally:
                self.__connections[vm_ip].close()

    def stop_node(self, vm_ip=None):
        """
        Stop a client.
        :param vm_ip: the IP of the client to be stopped. If a client IP is not provided all clients will be stoped.
        :return:
        """
        raise NotImplementedError()

    def start_node(self, conn):
        """
        App-specific function to be overloaded in the subclass. Procedure to start a client.
        :param conn: VMConnectionClient object.
        :return:
        """
        logger.warn("start_node(connection) function not implemented: nothing to do!")

    def update_node(self, conn):
        """
        App-specific function to be overloaded in the subclass. Procedure to start a client.
        :param conn: VMConnectionClient object.
        :return:
        """
        logger.warn("update_node(connection) function not implemented: nothing to do!")


class VMConnectionClient(object):

    def __init__(self, ns, vm_ip, user, password):
        logger.detail("Adding new connection: %s", [ns, vm_ip, user, password])
        self.connected = False
        self.ns = ns
        self.ip = vm_ip
        self.user = user
        self.password = password
        self.exec_queue = Queue()
        self.resp_queue = Queue()
        self.proc = None

    def connect(self):
        """
        Connect to VM.
        :return:
        """
        logger.debug("Connection to %s:%s", self.ns, self.ip)
        # create child
        self.proc = Process(target=NSVMConnection,
                            args=(self.exec_queue, self.resp_queue, self.ns, self.ip, self.user, self.password))
        self.proc.start()

    def close(self):
        """
        Close connection.
        :return:
        """
        logger.debug("Closing connection to %s:%s", self.ns, self.ip)
        self.exec_queue.put(("close",))
        logger.debug("Joining!")
        self.proc.join()
        logger.debug("Leaving!")

    def send_command(self, *args):
        self.exec_queue.put(*args)
        resp = self.resp_queue.get(timeout=10)
        if isinstance(resp, Exception):
            raise resp
        return resp

    def exec(self, cmd):
        return self.send_command("exec", cmd)

    def get_file(self, filename):
        """
        Helper function to get a filename from the host.
        :param filename: path of the filename to get
        :return:
        """
        return self.send_command(("get_file", filename))

    def put_file(self, filename, content):
        """
        Helper function to write a filename to the host.
        :param filename: path of the filename to get.
        :param content: string with file content.
        :return:
        """
        return self.send_command(("put_file", filename, content))


class VMConnection(object):

    def __init__(self, ip, user, password):
        self.ip = str(ip)
        self.user = user
        self.password = password
        self.client = None
        # self.connect_to_vm()

    def connect_to_vm(self):
        self.client = SSHClient()
        self.client.set_missing_host_key_policy(AutoAddPolicy())
        logger.temp("Connection to %s with user %s:%s", self.ip, self.user, self.password)
        self.client.connect(self.ip, username=self.user, password=self.password, timeout=5)

    def get_file(self, filename):
        if self.client:
            sftp = self.client.open_sftp()
            with sftp.open(filename, "r") as file:
                content = file.read()
                if isinstance(content, bytes):
                    content = content.decode("utf-8")
            return content
        else:
            return None

    def put_file(self, filename, content):
        if self.client:
            sftp = self.client.open_sftp()
            # logger.warn("About to write: %s", content)
            with sftp.open(filename, 'w') as file:
                file.write(content)
            return True
        else:
            return False

    def exec(self, cmd, get_pty=True, background=False):
        logger.temp("Exec command: %s (pty: %s, bg: %s)", cmd, get_pty, background)
        logger.temp("client: %s", self.client)
        if background and not cmd.endswith(" &"):
            cmd += " &"
            get_pty = False

        stdin, stdout, stderr = self.client.exec_command(cmd, get_pty=get_pty)
        out = err = ""
        # for real time output...
        # logger.temp("Looping...")
        # while not stdout.channel.exit_status_ready():
        #     if stdout.channel.recv_ready():
        #         out = stdout.channel.recv(1024).decode("utf-8")
        #     if stderr.channel.recv_ready():
        #         err = stderr.channel.recv(1024).decode("utf-8")
        # # dont forget to read the last few lines
        # out += "\n".join(stdout.readlines())
        # err += "\n".join(stderr.readlines())
        #
        exit_status = stdout.channel.recv_exit_status()
        logger.debug("Command exit status: %s", exit_status)

        # dont forget to read the last few lines
        if not background:
            out = "\n".join(stdout.readlines())
            err = "\n".join(stderr.readlines())
            logger.temp("Out: %s, Err: %s", out, err)
        return out, err

    def close(self):
        self.client.close()


class Connection(VMConnection):

    def __enter__(self):
        self.connect_to_vm()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()


class NSVMConnection(VMConnection):

    def __init__(self, inq, outq, ns, ip, user, password):
        setns(self.ns)
        logger.detail("NSVMConnection::%s" % [inq, outq, ns, ip, user, password])
        super().__init__(ip, user, password)
        self.ns = ns
        self.in_queue = inq
        self.out_queue = outq

    def connect_to_vm(self):
        super().connect_to_vm()

        self.listen()

    def listen(self):
        # pass
        logger.detail("NSVMConnection::waiting for event")
        while True:
            e = self.in_queue.get(block=True)
            if e[0] == "close":
                logger.detail("NSVMConnection::exiting")
                self.client.close()
                break
            else:
                logger.detail("NSVMConnection::Received %s" % [e])
                try:
                    func = getattr(self, e[0])
                except Exception as e:
                    logger.detail("Operation not supported")
                    self.out_queue.put(InvalidCommand(e))
                    continue

                try:
                    resp = func(*e[1:])
                    self.out_queue.put(resp)
                except Exception as e:
                    self.out_queue.put(ErrorRunningCommand(e))

    # @classmethod
    # def start(cls, inq, outq, ns, ip, user, password):
    #     conn = NSVMConnection(inq, outq, ns, ip, user, password)
    #     logger.detail("NSVMConnection::out")


class CmartAppConnector(AppConnector):

    DefaultConfigFile = "/cmart/client/CGconfig.txt"

    def __init__(self, vm_ip, user, password, ns=None, overwrite_config=False):
        """

        :param overwrite_config: if config is to be rewritten or only add options are to be overwritten
        :return:
        """
        super().__init__(vm_ip, user, password, ns=ns)

        self.default_config = None
        self.configs = {}
        self.overwrite_config = overwrite_config

    def add_config(self, config_dict, vm_ip=None):
        """

        :param config_dict: dictionary with all the config options to be set (configuration is replaced)
        :param vm_ip: optionally indicate to which VM the configuration apply; if not set,
        configuration will be default for all VMs that do not have a specific configuration
        :return:
        """
        if vm_ip:
            self.configs[vm_ip] = config_dict
        else:
            self.default_config = config_dict

    def start_node(self, conn):
        # setup
        logger.debug("Writing configuration")
        self.edit_config_file(conn)
        logger.debug("Start CMART client")
        self.start_client(conn)
        logger.debug("Out CMART client")

    def start_client(self, conn):

        stdout, stderr = conn.exec("java -jar cmartclient.jar")
        if stdout:
            logger.info("Started client: %s", stdout)
        if stderr:
            logger.error("Error starting client: %s", stderr)

    def edit_config_file(self, conn):
        # get the configuration for this VM
        cfg = self.configs[conn.ip] if conn.ip in self.configs else self.default_config
        if not cfg:
            return

        content = []
        used_keys = set(cfg.keys())
        if self.overwrite_config:
            content = ["%s=%s" % (k, v) for k, v in cfg.items()]
        else:
            original_content = conn.get_file(CmartAppConnector.DefaultConfigFile)
            logger.debug(original_content)
            if original_content:
                original_content = original_content.splitlines()
                # logger.debug("Content received: %s", original_content)
                # read all the lines
                for line in original_content:
                    logger.debug("Line: %s", line)
                    # for each line, check if is not a comment, split, and verify if we have a new configuration
                    l = line.strip()
                    # edit if necessary
                    if not line.strip().startswith("#"):
                        fields = l.split("=")
                        if len(fields) == 2:
                            k, v = fields
                            if k.strip() in cfg:
                                l = "%s=%s" % (k, cfg[k])
                                used_keys.remove(k.strip())

                    content.append(l)

                if used_keys:
                    for k in used_keys:
                        l = "%s=%s" % (k, cfg[k])
                        content.append(l)

        logger.debug("Content: \n%s" % "\n".join(content))
        if content:
            conn.put_file(CmartAppConnector.DefaultConfigFile, "\n".join(content))


class LbHandler(object):

    def __init__(self, instance, port):
        self.instance = instance
        self.port = port

    def config(self, conn):
        raise NotImplementedError()


class LbApacheHandler(LbHandler):

    def config(self, conn):
        try:
            logger.temp("Set workers")
            # get service
            service = self.instance.get_service()
            # get child service
            children = service.get_successors()
            if len(children) is not 1:
                logger.warn("LB with more than one child service: %s", children)
                return
            child_service = children[0]

            # get child instances
            child_instances = child_service.get_instances()
            ids = []
            for cinst in child_instances:
                ids.append(str(int(cinst.name.split("-")[-1])))

            logger.temp("Set workers - IDs: %s", ids)

            filename = '/usr/share/apache2/conf.d/lb.conf'
            target_prefix = child_service.name
            cluster_name = "cluster_%s" % service.name

            conn.exec(
                "sudo /root/set-workers.py %s %s %s --ids %s" % (
                    filename,
                    cluster_name,
                    target_prefix,
                    ','.join(ids)
                )
            )
        except:
            logger.error("Error setting workers: %s", traceback.format_exc())


class LbHAProxyHandler(LbHandler):

    pass


class LbKeepalivedHandler(LbHandler):
    RouterId = 100
    DefaultConfigFile = "/etc/keepalived/keepalived.conf"

    def __init__(self, instance, port, router_id=None):
        super().__init__(instance, port)
        self._router_id = router_id
        if not self._router_id:
            self._router_id = LbKeepalivedHandler.RouterId
            LbKeepalivedHandler.RouterId += 1

    def config(self, conn):
        logger.temp("Configuring keep_alived")
        service = self.instance.get_service()
        children = service.get_successors()
        if len(children) is not 1:
            logger.warn("LB with more than one child service: %s", children)
            return
        child_service = children[0]
        instances_ips = [i.ip for i in child_service.get_instances()]

        config = keepalived.generate_config(
            service.name,
            child_service.name,
            self.instance.virtual_ip,
            child_service.extra.get("scaling_port"),
            instances_ips,
            router_id=self._router_id
        )

        conn.put_file(LbKeepalivedHandler.DefaultConfigFile, config)
        logger.temp("File: %s", LbKeepalivedHandler.DefaultConfigFile)
        logger.temp("Content: %s", config)
        conn.exec("sudo systemctl restart keepalived")


class LbConnector(AppConnector):

    # for services with low amount of traffic for high processing HAProxy might be worth it
    Handlers = {
        None: LbKeepalivedHandler,
        80: LbKeepalivedHandler,
        3306: LbKeepalivedHandler
    }

    def __init__(self, app_name, instance, user="lama", password="llama", **kwargs):
        super().__init__(instance.ip, user, password)
        self.instance = instance
        self.app_name = app_name

        service = self.instance.get_service()
        children = service.get_successors()
        if len(children) is not 1:
            logger.warn("LB with more than one child service: %s", children)
            return
        child_service = children[0]

        port = child_service.extra.get("scaling_port")
        self._handler = LbConnector.Handlers.get(port, LbKeepalivedHandler)(self.instance, port)
        logger.temp("Service: %s, Extra: %s, Handler: %s", service.name, service.extra, self._handler)

    def update_node(self, conn):
        logger.temp("Updating node")
        self._handler.config(conn)

    def start_node(self, conn):
        # run remote set workers
        self._handler.config(conn)


# class RubbosClientConnector(AppConnector):
#
#     DefaultConfigFile = "/home/lama/rubbos/rubbos.properties"
#
#     def __init__(self, app_name, instance, user, password, overwrite_config=False, **kwargs):
#         """
#
#         :param overwrite_config: if config is to be rewritten or only add options are to be overwritten
#         :return:
#         """
#         super().__init__(instance.ip, user, password)
#
#         self.default_config = {}
#         self.configs = {}
#         self.overwrite_config = overwrite_config
#         self.instance = instance
#
#     def add_config(self, config_dict, vm_ip=None):
#         """
#
#         :param config_dict: dictionary with all the config options to be set (configuration is replaced)
#         :param vm_ip: optionally indicate to which VM the configuration apply; if not set,
#         configuration will be default for all VMs that do not have a specific configuration
#         :return:
#         """
#         if vm_ip:
#             self.configs[vm_ip] = config_dict
#         else:
#             self.default_config = config_dict
#
#     def update_node(self, conn):
#         # setup
#         self.edit_config_file(conn)
#
#     def start_node(self, conn):
#         # setup
#         self.edit_config_file(conn)
#
#     def edit_config_file(self, conn):
#         logger.temp("Writing configuration")
#         # get the configuration for this VM
#         cfg = self.configs[conn.ip] if conn.ip in self.configs else self.default_config
#         content = []
#
#         try:
#             service = self.instance.get_service()
#             logger.temp("service = %s, successors = %s", service, service.get_all_successors())
#             child_service = service.get_all_successors()[0]
#             logger.temp("child_service = %s, instances = %s", service, child_service.get_instances())
#             first_instance = child_service.get_instances()[0]
#             cfg["httpd_hostname"] = first_instance.virtual_ip or first_instance.ip
#         except:
#             logger.warn("Error editing the configuration (apache app): %s", traceback.format_exc())
#
#         logger.temp("Adding configuration options: %s", cfg)
#
#         if not cfg:
#             return
#
#         used_keys = set(cfg.keys())
#         logger.temp("Config: %s", cfg)
#
#         if self.overwrite_config:
#             content = ["%s=%s" % (k, v) for k, v in cfg.items()]
#         else:
#             original_content = conn.get_file(self.DefaultConfigFile)
#             logger.debug(original_content)
#             if original_content:
#                 original_content = original_content.splitlines()
#                 # logger.temp("Content received: %s", original_content)
#                 # read all the lines
#                 for line in original_content:
#                     # logger.temp("Line: %s", line)
#                     # for each line, check if is not a comment, split, and verify if we have a new configuration
#                     fields = line.strip().split("=")
#                     # edit if necessary
#                     if len(fields) == 2:
#                         k, v = fields
#                         k = k.strip()
#                         if k.strip() in cfg:
#                             l = "%s = %s" % (k, cfg[k])
#                             used_keys.remove(k.strip())
#                             line = l
#                     # else:
#                     #     logger.temp("Line: %s", line)
#
#                     content.append(line)
#
#                 if used_keys:
#                     logger.warn("The following config keys were not used: %s", ", ".join(used_keys))
#
#         logger.debug("Content: \n%s" % "\n".join(content))
#         if content:
#             conn.put_file(self.DefaultConfigFile, "\n".join(content))
#
#
# class RubbosApacheConnector(AppConnector):
#
#     DefaultConfigFile = "/home/lama/rubbos/PHP/config.php"
#
#     def __init__(self, app_name, instance, user, password, overwrite_config=False, **kwargs):
#         """
#
#         :param overwrite_config: if config is to be rewritten or only add options are to be overwritten
#         :return:
#         """
#         super().__init__(instance.ip, user, password)
#
#         self.default_config = {}
#         self.configs = {}
#         self.overwrite_config = overwrite_config
#         self.instance = instance
#
#     def add_config(self, config_dict, vm_ip=None):
#         """
#
#         :param config_dict: dictionary with all the config options to be set (configuration is replaced)
#         :param vm_ip: optionally indicate to which VM the configuration apply; if not set,
#         configuration will be default for all VMs that do not have a specific configuration
#         :return:
#         """
#         if vm_ip:
#             self.configs[vm_ip] = config_dict
#         else:
#             self.default_config = config_dict
#
#     def update_node(self, conn):
#         # setup
#         self.edit_config_file(conn)
#
#     def start_node(self, conn):
#         # setup
#         self.edit_config_file(conn)
#
#     def edit_config_file(self, conn):
#         logger.temp("Writing configuration")
#         # get the configuration for this VM
#         cfg = self.configs[conn.ip] if conn.ip in self.configs else self.default_config
#         content = []
#
#         try:
#             service = self.instance.get_service()
#             logger.temp("service = %s, successors = %s", service, service.get_all_successors())
#             child_service = service.get_all_successors()[0]
#             logger.temp("child_service = %s, instances = %s", service, child_service.get_instances())
#             first_instance = child_service.get_instances()[0]
#             cfg["host"] = first_instance.virtual_ip or first_instance.ip
#         except:
#             logger.warn("Error editing the configuration (apache app): %s", traceback.format_exc())
#
#         logger.temp("Adding configuration options: %s", cfg)
#
#         if not cfg:
#             return
#
#         used_keys = set(cfg.keys())
#         logger.temp("Config: %s", cfg)
#
#         if self.overwrite_config:
#             logger.error("Overwrite config not supported at %s", self.__class__.__name__)
#             # content = ["%s=%s" % (k, v) for k, v in cfg.items()]
#         else:
#             original_content = conn.get_file(self.DefaultConfigFile)
#             logger.debug(original_content)
#             if original_content:
#                 original_content = original_content.splitlines()
#                 # read all the lines
#                 for line in original_content:
#                     # for each line, check if is not a comment, split, and verify if we have a new configuration
#                     fields = line.strip().split("=>")
#                     # edit if necessary
#                     if len(fields) == 2:
#                         k, v = fields
#                         k = k.strip("'\" ,")
#                         # v = v.strip("'\" ,")
#                         if k.strip() in cfg:
#                             l = "\t'%s' => '%s'," % (k, cfg[k])
#                             used_keys.remove(k.strip())
#                             line = l
#
#                     content.append(line)
#
#                 if used_keys:
#                     logger.warn("The following config keys were not used: %s", ", ".join(used_keys))
#
#         logger.debug("Content: \n%s" % "\n".join(content))
#         if content:
#             conn.put_file(self.DefaultConfigFile, "\n".join(content))
#
#
# class RubbosMysqlClusterManagerConnector(AppConnector):
#
#     DefaultConfigFile = "/var/lib/mysql-cluster/config.ini"
#
#     def __init__(self, app_name, instance, user, password, **kwargs):
#         """
#
#         :param overwrite_config: if config is to be rewritten or only add options are to be overwritten
#         :return:
#         """
#         super().__init__(instance.ip, user, password)
#
#         self.default_config = {
#             "NoOfReplicas": 1,
#             "DataMemory": "320M",
#             "IndexMemory": "80M",
#         }
#         self.configs = {}
#         self.instance = instance
#         self.username = user
#
#     def add_config(self, config_dict, vm_ip=None):
#         """
#
#         :param config_dict: dictionary with all the config options to be set (configuration is replaced)
#         :param vm_ip: optionally indicate to which VM the configuration apply; if not set,
#         configuration will be default for all VMs that do not have a specific configuration
#         :return:
#         """
#         if vm_ip:
#             self.configs[vm_ip] = config_dict
#         else:
#             self.default_config.update(config_dict)
#
#     def start_node(self, conn):
#         # setup
#         self.edit_config_file(conn)
#
#     def update_node(self, conn):
#         # setup
#         self.edit_config_file(conn)
#
#     def edit_config_file(self, conn):
#         # logger.warn("RubbosMysqlClusterManagerConnector: Currently only supports scaling sql nodes!")
#         logger.temp("Writing configuration")
#
#         cfg = self.default_config.update(self.configs[conn.ip]) if conn.ip in self.configs else self.default_config
#         logger.temp("Adding configuration options: %s", cfg)
#
#         # head of file
#         content = [
#             "# Auto generated by LAMA",
#             "[ndbd default]",
#         ]
#
#         content += ["%s=%s" % (k, v) for k, v in cfg.items()]
#         # "NoOfReplicas=1",
#         # "DataMemory=320M",
#         # "IndexMemory=80M"
#
#         content += [
#             "",
#             "[tcp default]",
#             ""
#         ]
#
#         # manager section
#         content += [
#             "[ndb_mgmd]",
#             "hostname=%s" % self.instance.ip,
#             "datadir=/var/lib/mysql-cluster",
#             ""
#         ]
#
#         service = self.instance.get_service()
#         logger.temp("service extra: %s", service.extra)
#         # data nodes
#         for s in service.get_successors():
#             if s.name.startswith(service.extra.get("prefix_data_nodes")):
#                 for i in s.get_instances():
#                     if i.ip:
#                         content += [
#                             "[ndbd]",
#                             "hostname=%s" % i.ip,
#                             "datadir=/usr/local/mysql/data",
#                             ""
#                         ]
#                     else:
#                         logger.warn("Instance '%s' not added to the configuration: no IP assigned yet!", i.name)
#
#         # api nodes
#         for s in service.get_successors():
#             if s.name.startswith(service.extra.get("prefix_api_nodes")):
#                 for i in s.get_instances():
#                     if i.ip:
#                         content += [
#                             "[mysqld]",
#                             "hostname=%s" % i.ip,
#                             ""
#                         ]
#                     else:
#                         logger.warn("Instance '%s' not added to the configuration: no IP assigned yet!", i.name)
#
#         if content:
#             conn.put_file(self.DefaultConfigFile, "\n".join(content))
#
#         logger.temp("Restart MySQL Manager @ %s", self.instance.name)
#         cmd = "sudo systemctl restart ndb_mgmd"
#         logger.temp("Cmd: %s", cmd)
#         conn.exec(cmd)


class VirtualIPConfigurator(AppConnector):

    def __init__(self, app_name, instance, user="lama", password="llama", **kwargs):
        super().__init__(instance.ip, user, password)
        self.instance = instance
        self.app_name = app_name

    def update_node(self, conn):
        self.add_virtual_ip(conn)

    def start_node(self, conn):
        # run remote set workers
        self.add_virtual_ip(conn)

    def add_virtual_ip(self, conn):
        try:
            logger.temp("VirtualIPConfigurator: %s vip=%s aip=%s", self.instance, self.instance.virtual_ip, self.instance.accept_ip)
            ip = self.instance.virtual_ip or self.instance.accept_ip
            if not ip:
                logger.temp("no ip!")
                return

            logger.temp("Add %s ip: %s", "VIRTUAL" if self.instance.virtual_ip else "ACCEPT", ip)
            cmd = "sudo add_virtual_ip.sh %s %s" % (ip, "" if self.instance.virtual_ip else "lo")
            logger.temp("Cmd: %s", cmd)

            cmd = "sudo add_virtual_ip.sh %s %s" % (ip, "" if self.instance.virtual_ip else "lo")
            logger.temp("Cmd: %s", cmd)
            conn.exec(cmd)
        except:
            logger.error("Error adding virtual IP: %s", traceback.format_exc())
