from datetime import timedelta
import numpy as np
from lama.utils.logging_lama import logger

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class EstimateAvgValue(object):

    def __init__(self, max_values=10, default=60):
        self._max_values = max_values
        self._values = []
        self._last_estimate = timedelta(seconds=default)
        self._min = None
        self._max = None

    def estimate(self, value):
        logger.temp("Estimate - newval=%s", value)
        logger.temp("Current - min=%s max=%s vals=%s", self._min, self._max, self._values)
        self._min = min(value, self._min) if self._min else value
        self._max = max(value, self._max) if self._max else value
        if len(self._values) < self._max_values:
            self._values.append(value)
        if len(self._values) > 2:
            self._last_estimate = (sum(self._values, timedelta()) - self._min - self._max) / (len(self._values) - 2)
        else:
            self._last_estimate = sum(self._values, timedelta()) / len(self._values)

    def get_estimate(self):
        return self._last_estimate

    @property
    def val(self):
        return self._last_estimate


class EstimateNormal(object):

    def __init__(self, max_values=10, default=60):
        self._max_values = max_values
        self._values = []
        self._last_estimate = timedelta(seconds=default)
        self._min = None
        self._max = None

    def estimate(self, value):
        logger.temp("Estimate - newval=%s", value)
        logger.temp("Current - min=%s max=%s vals=%s", self._min, self._max, self._values)
        self._min = min(value, self._min) if self._min else value
        self._max = max(value, self._max) if self._max else value
        if len(self._values) < self._max_values:
            self._values.append(value)
        if len(self._values) > 2:
            self._last_estimate = (sum(self._values, timedelta()) - self._min - self._max) / (len(self._values) - 2)
        else:
            self._last_estimate = sum(self._values, timedelta()) / len(self._values)

    def get_estimate(self):
        return self._last_estimate

    @property
    def val(self):
        return self._last_estimate
