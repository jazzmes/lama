import json
import traceback
from importlib import import_module
import threading
from datetime import datetime
from functools import partial
from random import randint

from config.exp_constraints import APP_DEFAULT_MONITORING_STRATEGY, PROVIDER_USE_FAILURE_GRAPH
from lama.agent.app.app_arch import AppArchEncoder, InstanceState, LogicalNodeSubtype
from lama.agent.app.connectors.app_connector import LamaAppConnector
from lama.agent.app.monitor.monitor import AppMonitor
from lama.agent.app.monitor.strategies.base import CompositeStrategy
from lama.agent.app.monitor.strategies.custom import TierAwareHostFlawDetection
from lama.agent.app.monitor.strategies.fsm import OnOffStrategy, HierarchicalFSMStrategy
from lama.agent.app.recovery import RecoveryEstimator
from lama.agent.app.recovery.estimator import DeploymentPhase
from lama.agent.app.views.resilience import ResilienceGraph
from lama.agent.app.operations import LaunchInstance, LaunchResult, Operation, RemoveInstance, ChangeInstanceState
from lama.agent.base import KeyStore
from lama.agent.event_managers.emredis import LamaPubSub
from lama.agent.events import Event, EventType
from lama.agent.helpers.address_manager import DuplicateSubnet, IPRangeExhausted, AddressManager
from lama.agent.helpers.dnsmasq_helper import Dns
from lama.agent.specs.resource import ResourceSet, DiskSpec
from lama.api.event_log import Category
from lama.utils.enum import Enum
from lama.utils.logging_lama import logger

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class OperationError(Exception):
    pass


class ClientServiceNotFound(Exception):
    pass


class InactiveInstancesNotFound(Exception):
    pass


class AppController(object):

    def __init__(self, agent):
        """
        @type agent: lama.agent.app.app.AppAgent
        """
        self.agent = agent
        self.app = agent.app
        self.app_active = False
        self.address_manager = AppAddressManager(self.app)
        Operation.controller = self

        # app connector
        self.connector = LamaAppConnector.get_connector(self.app.extra.get("connector"), self.app,
                                                        **self.app.extra)
        logger.temp("Loaded connector (str: %s): %s", self.app.extra.get("connector"), self.connector)

        # store
        self._store = KeyStore(host=self.agent.local_provider.ip_address)
        self.app_status = AppStatus()

        # external events publisher
        self.publisher = LamaPubSub("lama_event", host='10.1.1.12')
        Operation.publisher = self.publisher

        # DNS manager
        self.dns = Dns(self.app.pname)

        # strategies
        # monitor and monitoring strategy
        self.monitor = self.launch_monitor()

        # management strategy
        self._recovery_time_estimator = RecoveryEstimator()
        Operation.estimator = self._recovery_time_estimator

        self.management_strategy = ResilienceGraph(
            self, self._recovery_time_estimator, self.agent.configuration
        ) if PROVIDER_USE_FAILURE_GRAPH else None
        self.allocation_controller = AllocationController(self.app, management_strategy=self.management_strategy)

        # helper variables
        self._running_clients = set()

    def start(self):
        # Currently this is what ensures that basic image services and client
        # These are not currently used in the management strategy
        # Should be dealt with in "default" management strategy in case the custom one does not handle it
        self.app.analyze_virtual()
        self.check_app()

    def launch_monitor(self):
        logger.info("Start app monitor: host_ip=%s", self.agent.local_provider.ip_address)
        logger.warn("Thread: %s (%s)", threading.current_thread(), threading.get_ident())

        strategy_class = None
        try:
            if APP_DEFAULT_MONITORING_STRATEGY:
                strategies = APP_DEFAULT_MONITORING_STRATEGY.split(',')
                if len(strategies) == 1:
                    sub_module, class_name = APP_DEFAULT_MONITORING_STRATEGY.rsplit('.', 1)
                    strategy_class = getattr(import_module('lama.agent.app.monitor.strategies.{}'.format(sub_module)), class_name)()
                else:
                    strategy_classes = []
                    for strategy in strategies:
                        sub_module, class_name = strategy.rsplit('.', 1)
                        sub_strategy_class = getattr(import_module('lama.agent.app.monitor.strategies.{}'.format(sub_module)), class_name)()
                        strategy_classes.append(sub_strategy_class)

                    strategy_class = CompositeStrategy(*strategy_classes)
        except:
            logger.warn("Unable to load strategy class: %s. Tb: %s", APP_DEFAULT_MONITORING_STRATEGY, traceback.format_exc())

        if not strategy_class:
            logger.debug("Loading default strategy: OnOffStrategy")
            strategy_class = OnOffStrategy()

        mon = AppMonitor(self.app,
                         # strategy=TierAwareHostFlawDetection(),
                         # strategy=OnOffStrategy(),
                         # strategy=HierarchicalFSMStrategy(),
                         strategy=strategy_class,
                         redis_host=self.agent.local_provider.ip_address,
                         # op_controller=MonitoringStrategyCommands(self)
                         )
        mon.start()
        return mon

    def get_deployment_times(self):
        res = {}
        for service in self.app.get_services([LogicalNodeSubtype.service, LogicalNodeSubtype.lb]):
            res[service.name] = {
                'cold': self._recovery_time_estimator.get_deployment_time(InstanceState.Cold, service).total_seconds(),
                'warm': self._recovery_time_estimator.get_deployment_time(InstanceState.Warm, service).total_seconds(),
                'hot': self._recovery_time_estimator.get_deployment_time(InstanceState.Hot, service).total_seconds(),
            }
        return res

    def add_image(self, service_name, size, init):
        # TODO: Magic parameters '10MBps' as disk speed
        spec = ResourceSet()
        spec.add_resource_spec(DiskSpec(size, "10MBps", "10MBps"))

        img_name = self.app.add_image(service_name, spec, init=init)
        allocated = self.app.get_service(img_name).is_allocated()
        logger.info("Image created: name=%s allocated=%s", img_name, allocated)

        self.agent.event_log.add_event(
            "app-%s" % self.agent.app.name,
            Category.Management,
            "CreateImage",
            description="Service: %s" % service_name
        )

        self.app.analyze_virtual()
        self.check_allocation_controller()
        return img_name, allocated

    def get_app_info(self):
        app_info = self.app.to_dict()
        if app_info:
            app_info["agent"] = {'port': self.agent.address.port, 'ip_address': self.agent.address.ip_address}
        return app_info

    def check_app(self):
        self.management_strategy.check()
        self.check_allocation_controller()

    def check_allocation_controller(self):
        req_spec = self.allocation_controller.get_request_resource_spec()

        if req_spec and req_spec.number_of_vms():
            self.publish_app()

            logger.info("Resources required (#VMs: %s) - spec: %s",
                        req_spec.number_of_vms(), req_spec)

            for vm in req_spec.get_vms():
                self._recovery_time_estimator.add_start_timestamp(DeploymentPhase.Allocation, datetime.now(), vm)

            ev = Event(EventType.AppResourceRequest, self.app.name, req_spec, partial=True)
            self.agent.send_event_reliably(self.agent.local_provider, ev)

    def publish_app(self):
        json_str = json.dumps(self.app.to_dict(), cls=AppArchEncoder)
        self._store.save("app:%s" % self.app.pname, json_str)
        self.monitor.notify_app_changed()

        logger.debug("App: %s", self.app)

    def process_allocated_instance(self, instance_name, provider_address, resources):
        logger.info("Instance allocated: instance=%s provider=%s", instance_name, provider_address)
        vm = self.app.get_vm(instance_name)
        self._recovery_time_estimator.add_end_timestamp(DeploymentPhase.Allocation, datetime.now(), vm)
        self.publisher.publish(
            "lama_events:allocated_instances",
            "%s|%s|%s|%s|%s" % (self.app.name, instance_name, vm.state, provider_address, datetime.now())
        )

        service = vm.get_service()
        self.allocation_controller.allocated_instance(instance_name)
        if vm:
            host = vm.get_host()

            #   same provider?
            if host and host.ip_address == provider_address:
                #   yes: means that is a change to a previous allocation: update resources
                vm.update_allocation(resources)
            else:
                if host:
                    # there must be only one!
                    vm.delete_host()
                vm.add_host(provider_address)
                if service.is_allocated():
                    logger.temp("Notify service '%s' allocation", service.id)
                    self.agent.event_handler(Event(EventType.AppServiceAllocatedEvent, service.id))
                    self.agent.event_log.add_event(
                        "app-%s" % self.agent.app.name,
                        Category.Management,
                        "VMAllocated",
                        "Service: %s" % service.id)

            self.agent.app.save()

            if self.app_active and vm.state == InstanceState.Active and service.subtype is not LogicalNodeSubtype.image:
                self.start_instance(vm)

                # logger.detail("peristent app arch: %s", self.agent.shelved())
        else:
            logger.error("Erroneous VM :: Remove allocation!")
            raise OperationError("Received notification of allocation for unknown VM: '{}'".format(instance_name))

        req_spec = self.agent.app.get_virtual_unallocated_spec()
        logger.temp("Number of unallocated vms: %s", req_spec.number_of_vms())

    def start_instance(self, instance, init=True, custom_cb=None, force_callback=True):
        logger.temp("Start instance: %s", instance)
        if not self.app_active:
            logger.temp("App not active")
            return

        # check if image was uploaded
        image_service = instance.get_service().get_image_service(init)
        image_ready = False
        for image_instance in image_service.get_instances():
            if image_instance.active_ts:
                image_ready = True
                break

        if image_ready:
            if not instance.launched_ts:
                logger.info("Starting instance: %s", instance.name)
                instance.launched_ts = datetime.now()
                li = LaunchInstance(
                    instance,
                    init,
                    connector=self.connector,
                    post_processing_callback=partial(self.process_instance_ready, custom_cb=custom_cb)
                )
                return li.start()
            else:
                if force_callback:
                    self.start_instance_callback(custom_cb=custom_cb)
                return LaunchResult(success=True, message="Instance already launched")

        return LaunchResult(success=False, message="Image not ready")

    def is_app_ready(self):
        # TODO: check instances and publish -> should move to resilience index
        app_ready = True
        for service in self.app.get_services(
                subtype={
                    LogicalNodeSubtype.service,
                    LogicalNodeSubtype.client,
                    LogicalNodeSubtype.lb
                }):
            for instance in service.get_all_instances():
                if instance.state == InstanceState.Active:
                    if instance.ready_ts:
                        logger.debug("Instance '%s' ready at %s", instance.name, instance.ready_ts)
                    else:
                        logger.debug("Instance '%s' not ready!", instance.name)
                        app_ready = False
                    break

            if not app_ready:
                break
                # if any(instance.ready_ts is None for instance in service.get_all_instances()):
                #     app_ready = False
                #     logger.debug("Service not ready: %s - not marking app as ready", service.name)
                #     break

        if app_ready:
            logger.debug("App is ready!")
            self.publisher.publish(
                "lama_events:active_apps",
                "%s|%s" % (self.app.name, datetime.now())
            )

    def process_instance_ready(self, *args, custom_cb=None, **kwargs):
        self.is_app_ready()
        self.start_instance_callback(*args, custom_cb=custom_cb, **kwargs)

    def start_instance_callback(self, *args, custom_cb=None, **kwargs):
        # noinspection PyBroadException
        try:
            if custom_cb:
                custom_cb(*args, **kwargs)
        except Exception:
            logger.error("Error running start_instance_callback: %s", traceback.format_exc())

    def register_image_uploaded(self, service_name, instance_name):
        instance = self.app.get_vm(instance_name)
        instance.active_ts = str(datetime.now())
        logger.info("Image uploaded for service: %s", service_name)

        self.check_instances()

    def check_instances(self):
        """
        Ensure that instances are started.
        Current version just calls start instance for each of the instances.
        .. todo:: verify if we can simplify the operation to check instances
        """
        total = 0
        failed = 0
        if self.app_active:
            # self.app_arch.analyze_virtual()
            for service in self.app.get_services(
                    subtype={
                        LogicalNodeSubtype.client,
                        LogicalNodeSubtype.lb,
                        LogicalNodeSubtype.service}
            ):
                for instance in service.get_instances(
                    states={InstanceState.Active, InstanceState.Warm}
                ):
                    total += 1
                    result = self.start_instance(instance)
                    if not result.success:
                        failed += 1
                        logger.warn("Instance %s failed to start: %s", instance.name, result.message)

            self.publish_app()
        return total, failed

    def start_app(self):
        self.app_active = True
        total_num_instances, failed_num_instances = self.check_instances()
        logger.info("Starting app: %s/%s started", total_num_instances - failed_num_instances, total_num_instances)
        if failed_num_instances:
            return False, "App deployment: %s/%s failed" % (failed_num_instances, total_num_instances)
        else:
            return True, "App deployment started! Check back for status."

    def setup_network(self):
        self._publish_network()

    def _publish_network(self):
        self._store.save_dict("%s:%s" % (self.app.pname, "192.168.0.1"),
                              {"mac": None, "ip": "192.168.0.1", "host": self.agent.local_provider.ip_address})
        self._store.save_dict("%s:%s" % (self.app.pname, "192.168.0.2"),
                              {"mac": None, "ip": "192.168.0.2", "host": self.agent.local_provider.ip_address})

    def publish_vms_info(self, vm_name=None):
        if vm_name:
            vm = self.app.get_vm(vm_name)
            host = vm.get_host()
            data = {"mac": vm.mac, "ip": vm.ip, "host": host.ip_address if host else None}
            # data = {"mac": vm.mac, "ip": vm.ip, "host": vm.get_host().ip_address if host else None}
            if vm.mac:
                self._store.save_dict("%s:%s" % (self.app.pname, vm.mac), data)
            if vm.ip:
                self._store.save_dict("%s:%s" % (self.app.pname, vm.ip), data)
            if vm.virtual_ip:
                self._store.save_dict("%s:%s" % (self.app.pname, vm.virtual_ip), data)

            # add reference for monitoring
            service = vm.get_service()
            host = vm.get_host()
            if host:
                self._store.save_dict("app:%s:%s:%s" % (self.app.pname, service.name, vm_name), {
                    "host": host.ip_address,
                    "ip": vm.ip,
                    "mac": vm.mac,
                    "type": service.subtype.name
                })

    def unpublish_vms_info(self, vm):
        if vm:
            self._store.delete("%s:%s" % (self.app.pname, vm.mac))
            if vm.ip:
                self._store.delete("%s:%s" % (self.app.pname, vm.ip))
            if vm.virtual_ip:
                self._store.delete("%s:%s" % (self.app.pname, vm.virtual_ip))

            # del reference for monitoring
            service = vm.get_service()
            self._store.delete("app:%s:%s:%s" % (self.app.pname, service.name, vm.name))

    def set_allocated_vm(self, vm_name):
        self.app_status.set_status(vm_name, NodeStatus.ALLOCATED)

    def set_deployed_vm(self, vm_name):
        self.app_status.set_status(vm_name, NodeStatus.DEPLOYED)

    def change_workload_by_clients(self, config):
        logger.debug("Change workload requested. Config: %s", config)
        # create new client node:
        # - get client service node
        client_service = self.app.get_client_service()
        if not client_service:
            logger.debug("No client service. Services: %s",
                         ["service=[} type={}".format(s.name, s.subtype) for s in self.app.get_services()])
            raise ClientServiceNotFound()

        # search for active client
        instance = None
        client_instances = [c for c in client_service.get_instances() if c not in self._running_clients]
        if len(client_instances):
            instance = client_instances.pop(0)
            result = self.start_instance(instance,
                                         custom_cb=lambda: self.connector.run_instance(instance, config=config),
                                         force_callback=True)
            logger.debug("Using available instance: name=%s result=%s", instance.name, result)
        else:
            # create new client instance
            instance = client_service.add_instance()
            logger.temp("Created instance")
            self.app.save()
            logger.debug("Created instance: %s", instance)

            self.agent.notifications.subscribe(
                EventType.VMAllocatedEvent,
                conditions={"vm_name": instance.name},
                cb=lambda event: self.start_instance(
                    instance,
                    custom_cb=lambda: self.connector.run_instance(instance, config=config)
                )
            )

        self._running_clients.add(instance.name)

        if not len(client_instances):
            logger.debug("No instances left. Add new instance for next time...")
            self.add_instance(client_service)
            # logger.debug("Created instance: %s -> %s", new_instance.name, instance)

        # allocate
        if instance:
            return True, "New client request accepted: Instance '%s' (config=%s)" % (instance.name, config)
        else:
            return False, "Unable to process request"

    def add_instance(self, service, state=InstanceState.Active):
        try:
            if isinstance(service, str):
                service = self.app.get_service(service)
            if not service:
                logger.warn("Service was not defined!")
                return False

            logger.info("Adding instance to service '%s'", service.name)
            instance = service.add_instance(state=state)
            # logger.temp("PUBLISHING")
            # self.publish_app()

            self.agent.notifications.subscribe(
                EventType.VMAllocatedEvent,
                conditions={"vm_name": instance.name},
                cb=lambda event: self.start_instance(instance)
            )

            # logger.temp("check_allocation @ add_instance")
            return True, instance.name

        except:
            logger.error("Error adding instance: %s", traceback.format_exc())
            return False

    def remove_latent_instance(self, instance_name):
        instance = self.app.get_vm(instance_name)
        if instance:
            self.app.remove_node(instance_name)
            # instance.remove()
        else:
            logger.warn("Instance '%s' not found!", instance_name)

    def remove_instance(self, service, instance=None, state=InstanceState.Active):
        logger.info("Deleting instance of service '%s'", service)
        try:
            if isinstance(service, str):
                service = self.app.get_service(service)
            if not instance:
                logger.info("Service: %s - Instance not set", service.name)
                instances = service.get_instances(states=(state,))
                logger.temp("Instances: %s", instances)
                instances.sort(key=lambda x: x.name, reverse=True)
                logger.temp("Sorted Instances: %s", instances)
                if instances:
                    instance = instances[0]
                    logger.info("Automatically chosen: Delete %s:%s", instance, service.name)
                else:
                    logger.warn("Could not find instances of state '%s'", state.name)
            else:
                instance = self.app.get_vm(instance)
                logger.info("Manually chosen: Delete %s:%s", instance, service.name)

            if instance:
                ri = RemoveInstance(instance)
                ri.start()

                # self.check_allocation_controller()
            else:
                logger.error("No instance for deletion: should not be here!")
        except:
            logger.error("Error deleting instance: %s", traceback.format_exc())

    def remove_host(self, ip_address):
        logger.info("Deleting host '%s'", ip_address)
        host = self.app.get_host(ip_address)
        for instance in host.get_vms():
            service = instance.get_service()
            if service.subtype in {LogicalNodeSubtype.service, LogicalNodeSubtype.lb}:
                try:
                    logger.info("\tDeleting instance '%s'", instance)
                    ri = RemoveInstance(instance)
                    ri.start()
                except:
                    logger.error("Error deleting instance: %s", traceback.format_exc())

        # self.notify_failed_host(ip_address)

    def notify_failed_host(self, ip_address):
        # FIXME: temporary hack for host failure detection
        self.agent.send_event_reliably(
            self.agent.address,
            Event(
                EventType.TempNotifyHostDown,
                ip_address,
            )
        )

    def change_instances(self, service, active_instances, hot_instances, warm_instances):
        service = self.app.get_service(service)
        if not service.is_scalable() and active_instances != 1 and hot_instances and warm_instances:
            logger.info("Requesting scaling operations for non scalable service: %s", service.name)
            return

        instances = service.get_all_instances()
        current_instances = list(sum(i.state == state for i in instances) for state in
                                 (InstanceState.Active, InstanceState.Hot, InstanceState.Warm))

        logger.info("Service: %s. Instances request: %s -> %s",
                    service.name,
                    tuple(current_instances), (active_instances, hot_instances, warm_instances))

        # if we need extra active instances we search for current hot or warm instances that we might migrate.
        # then adjust the hot instances using if possible left over warm instances
        # finally add warm instances if necessary
        # logger.warn("Thread: %s (%s)", threading.current_thread(), threading.get_ident())
        # self.agent.event_manager.schedule(0, self.adjust_instances, service, current_instances,
        #                                   (active_instances, hot_instances, warm_instances))
        self.adjust_instances(service, current_instances, (active_instances, hot_instances, warm_instances))

    def migrate_instance(self, instance_name, conditions=None):
        instance = self.app.get_vm(instance_name)
        service = instance.get_service()
        host = instance.get_host()

        logger.debug("Processing migrate instance: instance=%s", instance_name)
        self.publisher.publish(
            "lama_events:experiments",
            "note|{}|{}|{}".format(
                datetime.now(),
                self.app.name,
                json.dumps(
                    {
                        'service': instance.get_service().name,
                        'instance': instance_name,
                        'action': 'migrate'
                    }
                )
            )
        )

    def adjust_instances(self, service, current_instances, target_instances, index=0):
        # logger.temp("Adjust instances: %s -> %s", current_instances, target_instances)
        # logger.temp("Index: %s", index)
        if current_instances[index] < target_instances[index]:
            # logger.temp("[Index %s] Need MORE instances", index)
            next_index = index + 1
            # search rest for an instance...
            changed = False
            while not changed and next_index < len(current_instances):
                if current_instances[next_index] > 0:
                    # logger.temp("[Index %s] Changing instance from index %d", index, next_index)
                    self.change_service_instance_state(
                        service,
                        self._get_state(next_index),
                        self._get_state(index)
                    )
                    current_instances[index] += 1
                    current_instances[next_index] -= 1
                    changed = True
                next_index += 1

            if not changed:
                self.add_instance(service, state=self._get_state(index))
                current_instances[index] += 1
        elif current_instances[index] > target_instances[index]:
            logger.temp("[Index %s] Need LESS instances", index)
            next_index = index + 1
            changed = False
            while not changed and next_index < len(current_instances):
                if current_instances[next_index] < target_instances[next_index]:
                    logger.temp("[Index %s] Changing instance to index %d", index, next_index)
                    self.change_service_instance_state(
                        service,
                        self._get_state(index),
                        self._get_state(next_index)
                    )
                    current_instances[index] -= 1
                    current_instances[next_index] += 1
                    changed = True
                next_index += 1

            if not changed:
                self.remove_instance(service, state=self._get_state(index))
                current_instances[index] -= 1

        if tuple(current_instances) != target_instances:
            self.adjust_instances(
                service, current_instances, target_instances,
                index + 1 if current_instances[index] == target_instances[index] else index
            )

    def change_instance_state(self, service, instance, current_state, next_state):
        logger.info("Change instance state: '%s' from %s to %s", instance, current_state, next_state)
        try:
            if isinstance(service, str):
                service = self.app.get_service(service)
            if isinstance(instance, str):
                instance = self.app.get_vm(instance)

            if instance.get_service() != service:
                logger.error("Bad request: instance and service do not match")
                return

            op = ChangeInstanceState(instance, connector=self.connector)
            return op.start(next_state)

        except:
            logger.error("Error changing instance state: %s", traceback.format_exc())

    def change_service_instance_state(self, service, current_state, next_state):
        if isinstance(service, str):
            service = self.app.get_service(service)
        if not service:
            logger.warn("Service was not defined!")
            return False

        instances = service.get_instances(states=(current_state,))
        # reverse True if downwards => downwards if current state < next_state
        instances.sort(key=lambda x: x.name, reverse=(current_state.value < next_state.value))
        if instances:
            instance = instances[0]
            logger.info("Change instance '%s': state %s -> state %s",
                        instance.name, current_state.name, next_state.name)
            # if next_state == InstanceState.Active \
            #         or (next_state == InstanceState.Hot and next_state == InstanceState.Warm):
            #     # lets try do it all again...
            #     self.start_instance(instance)
            # else:
            op = ChangeInstanceState(instance, connector=self.connector)
            return op.start(next_state)

        else:
            logger.warn("Could not find instances of state '%s'", current_state.name)

    @staticmethod
    def _get_state(index):
        if index == 0:
            return InstanceState.Active
        elif index == 1:
            return InstanceState.Hot
        elif index == 2:
            return InstanceState.Warm

        raise Exception("Bad index: %d" % index)


class NodeStatus(Enum):
    CREATED = 0
    ALLOCATED = 1  # should be temporary, as any created node is deployed
    DEPLOYED = 2
    ACTIVE = 3
    STANDBY = 4


class AppStatus(object):
    def __init__(self):
        # """@type app_arch: AppArch"""
        self.node_status = {}

    def get_instances(self, service, statuses):
        """
        Get instances from service filtered by status.
        :param service: service node
        :param statuses: iterable with status to filter nodes
        :return:
        """
        if NodeStatus.CREATED in statuses:
            return [i for i in service.get_instances() if
                    not self.node_status.get(i.name) or self.node_status.get(i.name) in statuses]
        else:
            return [i for i in service.get_instances() if self.node_status.get(i.name) in statuses]

    def set_status(self, node_name, status):
        self.node_status[node_name] = status


class AllocationController(object):

    def __init__(self, app, management_strategy=None):
        """
        @type app: lama.agent.app.app_arch.AppArch
        """
        self._app = app
        # the view will be used to impose constrains
        self._management_strategy = management_strategy
        self._current_resource_requests = set()

    def check_allocations(self):
        req_spec = self._app.get_virtual_unallocated_spec()

        return req_spec

    def get_request_resource_spec(self):
        req_spec = self.check_allocations()
        # req_spec = self._app.get_virtual_unallocated_spec()

        if req_spec and req_spec.number_of_vms():
            logger.temp("Unallocated req spec: %s", req_spec)

            selected_nodes = [i.name for i in req_spec.get_vms() if i.name not in self._current_resource_requests]
            filtered_req_spec = req_spec.get_subgraph(nodes=selected_nodes)

            for i in filtered_req_spec.get_vms():
                self._current_resource_requests.add(i.name)

            # impose any constraints from the view
            if self._management_strategy:
                self._management_strategy.add_allocation_constraints(spec=filtered_req_spec)
            return filtered_req_spec

        return None

    def allocated_instance(self, instance_name):
        self._current_resource_requests.remove(instance_name)


class AppAddressManager(object):
    # This class will map each virtual node to an IP address in a private address space.
    # * It will also assign MAC addresses to be used by each VM and map them to the IP
    # * It will create different subnets for the VMs associated with each service
    class NoUniqueMACAddress(Exception):
        pass

    MaxMACGenerationAttempts = 1E6

    def __init__(self, app, iprange="192.168.0.0/16"):
        self._addr_man = AddressManager(iprange)
        self._mac_to_ip = {}

        self._addr_man.create_subnet("subnet_services")

        self.__initialize_from_app(app)

    def __initialize_from_app(self, app):
        for service in app.get_services():
            subnet_name = self.get_subnet_name(service.name)
            for instance in service.get_instances():
                if instance.ip and instance.mac:
                    logger.detail("Init existing address:  %s  %s  %s", subnet_name, instance.ip, instance.mac)
                    self._addr_man.init_with_address(subnet_name, instance.ip)
                    self._mac_to_ip[instance.mac] = instance.ip

    @staticmethod
    def get_subnet_name(service_name):
        return "subnet_%s" % service_name

    def get_instance_address(self, service_name, instance_name):
        # create a subnet named "subnet_<service_name>"
        subnet_name = self.get_subnet_name(service_name)
        try:
            self._addr_man.create_subnet(subnet_name)
        except DuplicateSubnet:
            logger.detail("Subnet '%s' already exist!", subnet_name)

        # get and reserve ip
        instance_ip = None
        try:
            instance_ip = self._addr_man.get_ip(subnet_name)
            mac = self.get_mac_address()
            self._mac_to_ip[mac] = instance_ip
            return instance_ip, mac
        except IPRangeExhausted:
            logger.warn("No IPs available for %s:%s (subnet: %s)", service_name, instance_name, subnet_name)
            return None
        except AppAddressManager.NoUniqueMACAddress:
            logger.warn("Unable to obtain an unique MAC address")
            if instance_ip:
                self._addr_man.free(instance_ip)
            return None

    def get_mac_address(self):
        # 3 bytes - replicate libvirt assigned addresses: 52:54:00
        # 3 random bytes (unique)
        for i in range(int(AppAddressManager.MaxMACGenerationAttempts)):
            mac = "6A:3A:00:%02X:%02X:%02X" % (randint(0, 255), randint(0, 255), randint(0, 255))
            if mac not in self._mac_to_ip:
                return mac

        raise AppAddressManager.NoUniqueMACAddress("Tried %s times" % AppAddressManager.MaxMACGenerationAttempts)

    def get_virtual_ip(self, service_name):
        subnet_name = self.get_subnet_name(service_name)
        try:
            self._addr_man.create_subnet(subnet_name)
        except DuplicateSubnet:
            pass
        return self._addr_man.get_virtual_ip(subnet_name)

        # Actions (on app changes): --> need hooks on the app architecture
        # * on ADD logical node: add a SUBNET
        # * on DELETE logical node: delete a subnet
        # * on ADD virtual node: create a MAC (rule-based) and assign an IP
        # * on DELETE virtual node: delete and free the IP

    def release_ip(self, ip):
        self._addr_man.free(ip)
