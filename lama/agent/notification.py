import traceback
from lama.agent.events import Event
from lama.utils.logging_lama import logger

__author__ = 'tiago'


class Notification(object):
    """
    Class that will accept subscription for new events and provide an event handler that will execute callbacks
    registered for each event before handing execution to the default event manager.
    """

    def __init__(self, default_handler=None):
        self.default_handler = default_handler
        self.clients = {}

    def event_handler(self, event, **context):
        """
        Specific event handler that adds support for temporary clients
        :param event:
        :param context:
        :return:
        """
        result = None
        if self.default_handler:
            result = self.default_handler(event, **context)

        # key = self.get_key(event.type, event.args)
        # key = (event.type,) + event.args
        publish_result = self.publish(event)
        supported = bool(result or publish_result)
        (logger.debug if supported else logger.warn)(
            "event=%s supported=%s clients=%s", event, bool(result or publish_result), self.clients)

        return result

    def subscribe(self, event_type, conditions, cb, **options):
        evars_names = [t[0] for t in Event.get_args(event_type)]
        if any([k not in evars_names for k in conditions.keys()]):
            logger.error("Not subscribing - Bad subcription: %s. Available vars: %s", [event_type, conditions],
                         evars_names)
            return
        if event_type not in self.clients:
            self.clients[event_type] = []
        self.clients[event_type].append({"conditions": conditions, "callback": cb})
        # logger.temp("Subcribed: %s" % {"conditions": conditions, "callback": cb})
        # logger.debug("Notification clients: %s", self.clients)

    def publish(self, event):
        logger.detail("Publishing event: event=%s, clients=%s", event, self.clients.get(event.type))
        if event.type in self.clients:
            runs = [self.__check_and_run(h, event) for h in self.clients[event.type]]
            self.clients[event.type][:] = [c for c, run in zip(self.clients[event.type], runs) if not run]
            if not self.clients[event.type]:
                del self.clients[event.type]
            return True
        else:
            return False

    @staticmethod
    def __check_and_run(subscriber, event):
        if not subscriber["conditions"] \
                or all([cv == getattr(event, ck) for ck, cv in subscriber["conditions"].items()]):
            # noinspection PyBroadException
            # running callback
            try:
                subscriber["callback"](event)
                if "options" in subscriber and subscriber.get("options", {}).get("keep"):
                    return False
                return True
            except Exception:
                logger.error("Failed to run callback for: '%s'", subscriber)
                logger.error("Traceback: %s", traceback.format_exc())
                return True
        return False
