from netaddr import IPSet
from netaddr.ip import IPAddress

from lama.agent.app.app_arch import AppLogicalSpec, AppVirtualSpec, InstanceState
from lama.agent.specs.resource import ResourceSet
from lama.utils.enum import Enum
from lama.utils.snippets import MethodNameConversion

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class NoEventError(Exception):
    def __init__(self, etype):
        self.etype = etype

    def __str__(self):
        return "Event '%s' does not exist!" % self.etype


class EventAttributeError(Exception):
    def __init__(self, etype, arg, desired_type, given_arg):
        self.etype = etype
        self.arg = arg
        self.desired_type = desired_type
        self.given_arg = given_arg

    def __str__(self):
        return "Wrong argument '%s::%s'. Should be: '%s', given: %r (%s)" \
               % (self.etype, self.arg, self.desired_type, self.given_arg, type(self.given_arg))


class EventAttributeMissing(Exception):
    def __init__(self, etype, arg_name, arg_type):
        self.etype = etype
        self.arg_name = arg_name
        self.arg_type = arg_type

    def __str__(self):
        return "Missing argument for event '%s': %s:%s" \
               % (self.etype, self.arg_name, self.arg_type)


class EventAttributeNumberError(Exception):
    def __init__(self, etype, desired_args, given_args):
        self.etype = etype
        self.desired_args = desired_args
        self.given_args = given_args

    def __str__(self):
        return "Wrong number of arguments for '%s'. Given: '%s', should be: '%s' (%s => %s)" \
               % (self.etype, len(self.given_args), len(self.desired_args), self.given_args, self.desired_args)


# prefer module level classes (classes within classes are not pickable)
class EventType(Enum):
    EventSendFailure, \
    ProviderAuthRequestEvent, \
    ProviderAuthResponseEvent, \
    AppAuthRequestEvent, \
    AppAuthResponseEvent, \
    StartPeerSearchEvent, \
    StopPeerSearchEvent, \
    PeerCandidateProbeTimeout, \
    PeerCandidateProbeEvent, \
    PeerCandidateResponseEvent, \
    PeerRequestEvent, \
    PeerResponseEvent, \
    PeerRequestTimeout, \
    WebAgentInfo, \
    WebAgentPeersInfo, \
    WebAppAgentInfo, \
    WebAppStatusData, \
    WebAppMonitorData, \
    WebProviderAgentClusters, \
    WebGenerateAppSpec, \
    WebAppCreate, \
    WebAppList, \
    WebAppCreateImage, \
    WebImageUpload, \
    WebStartInstance, \
    WebStopInstance, \
    WebAddInstance, \
    WebStartApp, \
    WebAppInfo, \
    WebGetPluginList, \
    WebGetPluginData, \
    WebGetProviderEvents, \
    WebGetAppEvents, \
    WebChangeWorkload, \
    AppDeployEvent, \
    AppDeployAcceptEvent, \
    AppDeployRefuseEvent, \
    AppDeployTimeoutEvent, \
    AppReqsFulfilledEvent, \
    AppResourceRequest, \
    ProviderResourceReservation, \
    ProviderResourceReservationConfirmation, \
    ProviderResourceReservationFailure, \
    ProviderResourceDeletion, \
    ProviderResourceDeletionConfirmation, \
    ProviderResourceChange, \
    ProviderResourceChangeConfirmation, \
    ProviderResourceUpdate, \
    ProviderResourceSubscribeUpdates, \
    ProviderResourceCancelUpdates, \
    ProviderDeletedLatentNotification, \
    ProviderResourceDependenciesChange, \
    VMAllocatedEvent, \
    AppAllocatedEvent, \
    AppInfoQueryEvent, \
    AppInfoResponseEvent, \
    AppStatusDataQueryEvent, \
    AppStatusDataResponseEvent, \
    AppMonitorDataQueryEvent, \
    AppMonitorDataResponseEvent, \
    AppCreateImageEvent, \
    AppCreateImageResponseEvent, \
    AppImageUploadedEvent, \
    AppStartInstanceEvent, \
    AppStartInstanceResponseEvent, \
    AppAddInstanceEvent, \
    AppAddInstanceResponseEvent, \
    ProviderStartInstanceEvent, \
    ProviderStartInstanceResponseEvent, \
    ProviderChangeInstanceStateEvent, \
    ProviderChangeInstanceStateResponseEvent, \
    ProviderTerminateInstanceEvent, \
    ProviderTerminateInstanceResponseEvent, \
    AppEventsQueryEvent, \
    AppEventsResponseEvent, \
    AppChangeWorkloadEvent, \
    AppChangeWorkloadResponseEvent, \
    AppStartEvent, \
    AppStartResponseEvent, \
    AppVMAllocatedEvent, \
    AppServiceAllocatedEvent, \
    TempNotifyHostDown = range(1, 82 + 1)

    # @staticmethod
    # def add_event(event_name):
    # setattr(EventType, event_name)


class Event(object):
    # for each event define variables as array of tuples (variable_name, variable_type, allow_none[optional])
    _vars = {
        EventType.EventSendFailure: [("ref_event_type", EventType), ("ref_event_args", tuple), ("msg", str)],
        EventType.ProviderAuthRequestEvent: [("agent_id", str, True), ("address", IPAddress), ("prefix", int),
                                             ("resources", ResourceSet)],
        EventType.ProviderAuthResponseEvent: [("result", bool), ("agent_id", str, True)],
        EventType.AppAuthRequestEvent: [("app_name", str), ("pid", int), ("port", int)],
        EventType.AppAuthResponseEvent: [("result", bool), ("app_name", str, True)],
        EventType.StopPeerSearchEvent: [],
        EventType.StartPeerSearchEvent: [("conditions", tuple)],
        EventType.PeerCandidateProbeTimeout: [],
        EventType.PeerCandidateProbeEvent: [("agent_id", str), ("constraints", tuple)],
        EventType.PeerCandidateResponseEvent: [("req_agent_id", str), ("candidates", IPSet)],
        EventType.PeerRequestEvent: [("agent_id", str), ("ip_address", IPAddress), ("constraints", tuple)],
        EventType.PeerResponseEvent: [("agent_id", str), ("ip_address", IPAddress),
                                      ("req_agent_id", str), ("resources", ResourceSet)],
        EventType.PeerRequestTimeout: [("candidate_ip", IPAddress)],

        EventType.WebAgentPeersInfo: [],
        EventType.WebAgentInfo: [],
        EventType.WebAppAgentInfo: [("app_name", str)],
        EventType.WebAppStatusData: [("app_name", str), ("options", dict)],
        EventType.WebAppMonitorData: [("app_name", str), ("options", dict)],
        EventType.WebProviderAgentClusters: [],
        EventType.WebGenerateAppSpec: [],
        EventType.WebAppCreate: [("app_spec", dict)],
        EventType.WebAppList: [],
        EventType.WebAppCreateImage: [("app_name", str), ("service_name", str), ("size", int), ('init', bool)],
        EventType.WebImageUpload: [("app_name", str), ("service_name", str), ("instance_name", str), ("filename", str), ('init', bool), ('file', bytes, True)],
        EventType.WebStartInstance: [("app_name", str), ("instance_name", str)],
        EventType.WebStopInstance: [("app_name", str), ("instance_name", str)],
        EventType.WebAddInstance: [("app_name", str), ("service_name", str)],
        EventType.WebStartApp: [("app_name", str)],
        EventType.WebAppInfo: [("app_name", str)],
        EventType.WebGetPluginList: [("app_name", str), ("entity_type", str), ("name", str)],
        EventType.WebGetPluginData: [("app_name", str), ("entity_type", str), ("name", str), ("plugin", str),
                                     ("start", int), ("end", int), ("resolution", int)],
        EventType.WebGetProviderEvents: [("ip_address", str), ("start", int), ("end", int, True)],
        EventType.WebGetAppEvents: [("app_name", str)],

        EventType.WebChangeWorkload: [("app_name", str), ("kwargs", dict)],

        EventType.AppDeployEvent: [("app_name", str), ("nid", int), ("app_spec", AppLogicalSpec)],
        EventType.AppDeployAcceptEvent: [("app_name", str), ("port", int)],
        EventType.AppDeployRefuseEvent: [("app_name", str)],
        EventType.AppDeployTimeoutEvent: [("app_name", str)],

        EventType.AppReqsFulfilledEvent: [],
        EventType.AppResourceRequest: [("app_name", str), ("req_spec", AppVirtualSpec), ("partial", bool)],
        EventType.ProviderResourceReservation: [("agent_id", str), ("app_name", str), ("service_name", str),
                                                ("vm_name", str), ("resources", ResourceSet),
                                                ("state", InstanceState), ("dependencies", dict, True)],
        EventType.ProviderResourceReservationConfirmation: [("agent_id", str), ("app_name", str), ("vm_name", str)],
        EventType.ProviderResourceReservationFailure: [("agent_id", str), ("app_name", str), ("vm_name", str), ("resources", ResourceSet)],
        EventType.ProviderResourceDeletion: [("agent_id", str), ("app_name", str), ("vm_name", str)],
        EventType.ProviderResourceDeletionConfirmation: [("agent_id", str), ("app_name", str), ("vm_name", str), ("result", bool)],
        EventType.ProviderResourceChange: [("agent_id", str), ("app_name", str), ("service_name", str),
                                             ("vm_name", str), ("old_state", InstanceState),
                                             ("new_state", InstanceState)],
        EventType.ProviderResourceChangeConfirmation: [("agent_id", str), ("app_name", str), ("service_name", str),
                                                       ("vm_name", str), ("old_state", InstanceState),
                                                       ("new_state", InstanceState), ("result", bool)],
        EventType.ProviderResourceSubscribeUpdates: [("agent_id", str)],
        EventType.ProviderResourceCancelUpdates: [("agent_id", str)],
        EventType.ProviderDeletedLatentNotification: [("app_name", str), ("vm_name", str)],
        EventType.ProviderResourceDependenciesChange: [("app_name", str), ("service_name", str),
                                                       ("vm_name", str), ("added", dict), ("removed", dict)],
        EventType.ProviderResourceUpdate: [("agent_id", str), ("resources", ResourceSet)],

        EventType.VMAllocatedEvent: [("vm_name", str), ("provider_address", IPAddress), ("resources", ResourceSet)],
        EventType.AppAllocatedEvent: [],

        EventType.AppInfoQueryEvent: [],
        EventType.AppInfoResponseEvent: [("app_name", str), ("app_info", dict)],
        EventType.AppStatusDataQueryEvent: [("options", dict)],
        EventType.AppStatusDataResponseEvent: [("app_name", str), ("app_data", dict)],
        EventType.AppMonitorDataQueryEvent: [("options", dict)],
        EventType.AppMonitorDataResponseEvent: [("app_name", str), ("data", dict)],

        EventType.AppCreateImageEvent: [("service_name", str), ("size", int), ("init", bool)],
        EventType.AppCreateImageResponseEvent: [("app_name", str), ("img_name", str),
                                                ("instances", dict)],
        EventType.AppImageUploadedEvent: [("app_name", str), ("service_name", str), ("instance_name", str), ('init', bool)],
        EventType.AppStartInstanceEvent: [("app_name", str), ("instance_name", str)],
        EventType.AppStartInstanceResponseEvent: [("app_name", str), ("result", bool), ("reason", str, True)],
        EventType.AppAddInstanceEvent: [("app_name", str), ("service_name", str)],
        EventType.AppAddInstanceResponseEvent: [("app_name", str), ("event_id", int), ("result", bool), ("reason", str, True)],
        EventType.AppStartEvent: [("app_name", str)],
        EventType.AppStartResponseEvent: [("app_name", str), ("result", bool), ("reason", str, True)],
        EventType.ProviderStartInstanceEvent: [("app_name", str), ("service_name", str), ("instance_name", str),
                                               ("controller_ip", IPAddress), ("controller_port", int), ("mac", str),
                                               ("image_location", IPAddress),
                                               ("init", bool), ("instance_state", InstanceState),
                                               ("extra_nic_mac", str, True)],
        EventType.ProviderStartInstanceResponseEvent: [("app_name", str), ("service_name", str), ("instance_name", str),
                                                       ("active", bool), ("msg", str)],
        EventType.ProviderChangeInstanceStateEvent: [("app_name", str), ("service_name", str), ("instance_name", str),
                                                     ("old_state", InstanceState), ("new_state", InstanceState)],
        EventType.ProviderChangeInstanceStateResponseEvent: [("app_name", str), ("service_name", str),
                                                             ("instance_name", str), ("result", bool),
                                                             ("msg", str, True)],

        EventType.ProviderTerminateInstanceEvent: [("app_name", str), ("instance_name", str), ("ip", IPAddress, True),
                                                   ("mac", str, True)],
        EventType.ProviderTerminateInstanceResponseEvent: [("app_name", str), ("instance_name", str),
                                                       ("success", bool), ("msg", str, True)],
        EventType.AppEventsQueryEvent: [],
        EventType.AppEventsResponseEvent: [("app_name", str), ("events", list)],

        EventType.AppVMAllocatedEvent: [("vm_name", str)],
        EventType.AppServiceAllocatedEvent: [("service_name", str)],
        EventType.AppChangeWorkloadEvent: [("app_name", str), ("kwargs", dict)],
        EventType.AppChangeWorkloadResponseEvent: [("app_name", str), ("result", bool), ("msg", str)],
        EventType.TempNotifyHostDown: [("ip_address", IPAddress)]
    }

    LIMIT_EVENT_STR = 300

    def __init__(self, etype, *args, **kwargs):
        self.event_id = id(self)

        if etype not in Event._vars:
            raise NoEventError(etype)

        if len(args) + len(kwargs) != len(Event._vars[etype]):
            raise EventAttributeNumberError(etype, Event._vars[etype], list(args) + list(kwargs.keys()))

        for g, d in zip(args, Event._vars[etype]):
            if not isinstance(g, d[1]) \
                    and (len(d) < 3 or not d[2]):
                raise EventAttributeError(etype, d[0], d[1], g)

        add_args = []
        for d in Event._vars[etype][len(args):]:
            if d[0] not in kwargs and (len(d) > 2 and d[2]):
                raise EventAttributeMissing(etype, d[0], d[1])
            if not isinstance(kwargs[d[0]], d[1]) and (len(d) < 3 or not d[2]):
                raise EventAttributeError(etype, d[0], d[1], kwargs[d[0]])
            add_args.append(kwargs[d[0]])

        self.type = etype
        self.args = args + tuple(add_args)

    def __str__(self):
        sargs = [a if not isinstance(a, bytes) else "<bytes>" for a in self.args]
        s = "Event[id=%d name=%s params=%s]" % (self.event_id, self.type.name, sargs)
        return s[0:Event.LIMIT_EVENT_STR]

    def __getattr__(self, item):
        res = [i for i, t in enumerate(Event._vars[self.type]) if t[0] == item]
        if len(res) > 1:
            raise AttributeError("Two arguments with same name '%s'", item)
        if len(res) == 1:
            return self.args[res[0]]

        raise AttributeError("No attribute! Event: %s has not attribute '%s'" % (self.type, item))

    def __getstate__(self):
        return {
            'event_id': self.event_id,
            'type': self.type,
            'args': self.args
        }

    def __setstate__(self, state):
        self.__dict__.update(state)

    @staticmethod
    def get_args(event_type):
        return Event._vars[event_type]

    @staticmethod
    def get_event_from_action(action, **args):
        msg = None
        if action:
            try:
                event_name = "Web" + MethodNameConversion.underscore2camelcase(action)
            except:
                result = False
                msg = "Failed to convert action to event ('%s')!" % action
            else:
                try:
                    event_type = EventType[event_name]
                except:
                    result = False
                    msg = "Event type does not exist ('%s')!" % event_name
                else:
                    try:
                        event = Event(event_type, **args)
                    except EventAttributeNumberError as e:
                        result = False
                        msg = str(e)
                    else:
                        return event, ""
        return None, msg
