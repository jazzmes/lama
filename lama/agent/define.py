from lama.utils.enum import Enum

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


class FailureDomain(Enum):
    Instance = 1
    Host = 2
    Cluster = 3
