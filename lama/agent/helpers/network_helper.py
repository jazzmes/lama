from argparse import ArgumentParser
import logging
import sys
import traceback

from netaddr import IPSet, IPNetwork

from .controller_helper import Controller
from .lama_iptables import configure_nat
from .netns_helper import create_veth_pair
from lama.ryu.ovsdb_client import OVSDbClient
from lama.utils.logging_lama import logger
from lama.utils.snippets import get_ip_addresses
from pyroute2.ipdb import IPDB
from pyroute2.netlink import NetlinkError
from pyroute2.netns import NetNS
from lama.api.api_libvirt import LibvirtConnection
from libvirt import libvirtError
import lama.agent.helpers.network_names_helper as neth
from config.settings import PATH

__author__ = 'tiago'


class BridgeDoesNotExistException(Exception):
    pass


class IPExhaustedException(Exception):
    pass


class NetnsCreationException(Exception):
    pass


class NetworkManager(object):
    # def create_gw_netns(self, ns_name, release=True):
    #
    #     try:
    #         ns = NetNS(ns_name)
    #         ipdb_ns = IPDB(nl=ns)
    #     except Exception as e:
    #         logger.debug("Failed to create, retrieve or connect to namespace '%s'!", ns_name)
    #         raise NetnsCreationException("VETH creation failed!")
    #
    #     if release and ipdb_ns:
    #         ipdb_ns.release()
    # controller_port = 16633
    gw_network = IPNetwork('172.16.0.0/16')
    gw_network_used = IPSet()

    def __init__(self, app_name, controller_port, bridge_name=None, controller_ip=None):
        self.app_name = app_name
        self.bridge_name = bridge_name or neth.create_bridge_name(self.app_name)
        self.tap_name = neth.create_tap_name(self.app_name)
        self.gw_name = neth.create_gw_name(self.app_name)
        self.ns_name = neth.create_ns_name(self.app_name)
        self.net_name = neth.create_network_name(self.app_name)
        self.address = "127.0.0.1"
        self.controller = None
        self.controller_ip = controller_ip
        if controller_port:
            logger.debug("Controller port given - should be remote: %s", controller_port)
            self.controller_port = controller_port
        else:
            self.controller_port = NetworkManager.controller_port
            # TODO: race condition
            NetworkManager.controller_port += 1

        self.gw_interface = None

        # self._ovs_db_client = OVSDbClient()

    def create_tap(self, tap_name):
        if not OVSDbClient.bridge_exists(self.address, self.bridge_name):
            raise BridgeDoesNotExistException("%s - Should be created before adding tap." % self.bridge_name)

        logger.detail("Creating %s:%s" % (self.bridge_name, tap_name))
        return OVSDbClient.add_internal_port_to_local_bridge(self.bridge_name, tap_name)

    def create_bridge(self):
        logger.temp("Create bridge: %s", self.bridge_name)
        if not OVSDbClient.bridge_exists(self.address, self.bridge_name):
            if self.controller_ip and self.controller_port:
                logger.temp("create bridge: %s:%s", self.controller_ip, self.controller_port)
                OVSDbClient.create_bridge(self.address, self.bridge_name, controller_ip=self.controller_ip,
                                          controller_port=self.controller_port)
            else:
                raise NotImplementedError("will not create bridge without controller!")
                # logger.temp("create bridge without ")
                # OVSDbClient.create_bridge(self.address, self.bridge_name)
            # make sure the bridge has mtu 1400 (space for the vxlan header)
            # ipdb = IPDB()
            # try:
            #     with getattr(ipdb.interfaces, self.bridge_name) as i:
            #         i.mtu = 1400
            # except:
            #     logger.warn("Unable to change MTU setting. Bridge: %s", self.bridge_name)
            # ipdb.release()

    def create_controller(self, address, nid):
        logger.detail("Creating SDN Controller: %s", self.ns_name)
        if not self.controller:
            self.controller = Controller(self.app_name, address, nid, port=self.controller_port)
            self.controller.start()

    def create_gw(self):
        logger.detail("creating gateway: %s", [self.bridge_name, self.ns_name, self.tap_name, self.gw_name])
        # logger.detail("create netns named %s" % ns_name)

        # try:
        # netns.create(ns_name)
        # # self.create_gw_netns(ns_name)
        self.create_bridge()

        # "tap-gw-%s" % app_name
        # bridge_name = shorten_app_name(self.bridge_name, 15)
        # tap_name = shorten_app_name(self.tap_name, 15)
        # gw_tap_name = shorten_app_name(self.gw_tap_name, 13)
        logger.temp("*CREATE GW* Create tap port")
        res = OVSDbClient.bridge_has_port(self.bridge_name, self.tap_name)
        if not res:
            res = self.create_tap(self.tap_name)

        if res:
            logger.detail("created tap")

            logger.temp("*CREATE GW* Create namespace")
            ipdb = IPDB()
            try:
                ns = NetNS(self.ns_name)
                logger.detail("interfaces: %s", ipdb.interfaces)
                with getattr(ipdb.interfaces, self.tap_name) as iface:
                    logger.detail("adding tap to netns '%s'" % self.ns_name)
                    iface.net_ns_fd = self.ns_name
            except KeyError as e:
                # if it is here, the interface was created
                # if it raised this exception means that the interface is already on the correct namespace
                logger.detail("Interface already in new namespace")
                pass
            except Exception:
                logger.error("Error adding tap %s to %s", self.tap_name, self.ns_name)
                logger.error(traceback.format_exc())
                return
            finally:
                ipdb.release()

            logger.temp("*CREATE GW* Setup inner ips")
            ipdb_ns = None
            try:
                ipdb_ns = IPDB(nl=ns)
                # bring local interface up
                with ipdb_ns.interfaces.lo as iface:
                    iface.up()

                with getattr(ipdb_ns.interfaces, self.tap_name) as iface:
                    logger.detail("adding ip to tap '%s'", self.tap_name)
                    if not iface.flags or not (iface.flags & 1):
                        iface.add_ip("192.168.0.2/16")
                        iface.up()
                    else:
                        logger.detail("interface '%s' is already up...", self.tap_name)
            except:
                logger.error("Error adding ip %s to %s?", self.tap_name, self.ns_name)
                logger.error(traceback.format_exc())
                return
            finally:
                if ipdb_ns:
                    ipdb_ns.release()

            # get free ip to use...
            logger.temp("*CREATE GW* Setup gateway ips")
            gw_subnet = self.get_available_network()

            # possible_set = IPSet(IPNetwork("172.16.0.0/24")) - IPSet(["172.16.0.255", "172.16.0.0", "172.16.0.1"]) - curr_ipset
            # if not len(possible_set):
            if not gw_subnet:
                raise IPExhaustedException("No IP available for the gateway (%s)." % self.ns_name)

            # ext_ip = next(iter(possible_set))
            ext_ip = gw_subnet[2]
            logger.detail("chosen external ip: %s", ext_ip)

            logger.temp("*CREATE GW* Create external veth pair")
            # create veth pair
            self.gw_interface, pip = create_veth_pair(self.gw_name, self.ns_name,
                                                      peer_ip="%s/24" % gw_subnet[1], main_ip="%s/24" % ext_ip)

            logger.temp("*CREATE GW* Add iptables rules")
            configure_nat(self.gw_interface, str(gw_subnet))
            configure_nat(self.tap_name, '192.168.0.0/16', namespace=self.ns_name)

            logger.temp("*CREATE GW* Add default routes")
            # add default route
            ipdb_ns = IPDB(nl=NetNS(self.ns_name))
            try:
                logger.detail("add default rules")
                ipdb_ns.routes.add({'dst': "default", "gateway": str(ext_ip)}).commit()
            except NetlinkError as e:
                logger.error("Raised NetlinkError: %s!", e)
                pass
            except Exception as e:
                logger.error(traceback.format_exc())
            finally:
                logger.detail("releasing ns")
                ipdb_ns.release()

    def get_available_network(self):
        for sub in NetworkManager.gw_network.subnet(24):
            if sub not in NetworkManager.gw_network_used:
                NetworkManager.gw_network_used.add(sub)
                return sub
        return None

    def create_libvirt_network_on_process(self, process_function, callback=None):
        exe = "%s -c 'import os; print(os.getcwd())'" % sys.executable
        process_function(exe)

        exe = "%s -m lama.agent.helpers.network_scripts libvirt %s %s %s" % (
            sys.executable, self.app_name, self.controller_ip, self.controller_port
        )
        process_function(exe, cwd=PATH, callback=callback)

    def create_libvirt_network(self):
        self.create_bridge()

        connection = LibvirtConnection()
        network = None
        while not network:
            network = connection.get_network(self.net_name)
            if not network:
                logger.debug("Network not found. Defining...")
                network = connection.define_network(
                    network_type="openvswitch",
                    network_name=self.net_name,
                    bridge_name=self.bridge_name)
                if network:
                    network.setAutostart(True)
                    # TODO: network a wrapper object so we dont have to handle libvirt specific exceptions here
                    try:
                        network.create()
                    except libvirtError as e:
                        if "already active" not in str(e):
                            logger.error("Unexpected exception: %s. Traceback: %s", traceback.format_exc())

        if connection:
            connection.close()

            # def create_gw_access(self):
            #     # create bridge
            #     # create netns
            #     # create veth pair

            # def add_iptables_rules(self, tap_name, int_net, namespace=None):
            #
            #     # args = ["python", "-m", "lama.utils.add_iptables_rules", tap_name, int_net]
            #     # netns_args = []
            #     # if namespace:
            #     #     netns_args = ["ip", "netns", "exec", namespace]
            #     #
            #     # cmd = subprocess.Popen(netns_args + args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            #     # out, err = cmd.communicate()
            #     # if out:
            #     #     logger.warn("Output when applying IPTables NAT rules: %s", out.decode("utf-8"))
            #     # if err:
            #     #     logger.error("Error output when applying IPTables NAT rules: %s", err.decode("utf-8"))
            #
            #     if namespace:
            #         netns.setns(namespace)
            #
            #     iptables.nat_rules(int_net)
            #     iptables.filter_rules(tap_name, int_net)
            #
            # def apply_iptables(self):


def log_stdout():
    root = logging.getLogger()
    root.setLevel(logging.INFO)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    root.addHandler(ch)
    return root


if __name__ == "__main__":
    logger = log_stdout()
    parser = ArgumentParser()
    parser.add_argument('app_name', type=str, help="App name")
    parser.add_argument('-b', dest='bridge', type=str,
                        help="Bridge to connect gateway to (through a tap internal interface and NetNS)")
    parser.add_argument('-s', dest='host', type=str, help="Host where to install the bridge", default="127.0.0.1")
    params = parser.parse_args()

    bridge = NetworkManager.create_bridge_name(
        params.app_name) if "bridge" not in params or not params.bridge else params.bridge

    NetworkManager(params.app_name).create_gw(
        # bridge_name=bridge,
        # ns_name="gw-%s" % params.app_name,
        # tap_name="tapi-%s" % params.app_name,
        # gw_tap_name="tape-%s" % params.app_name
    )

