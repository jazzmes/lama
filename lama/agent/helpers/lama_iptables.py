import logging
import sys
import subprocess
from argparse import ArgumentParser

import iptc

from lama.utils.logging_lama import logger
from config.settings import PATH


def add_rule(rule, chain):
    if not rule in chain.rules:
        chain.append_rule(rule)


def nat_rules(network):
    # iptables -t nat -A POSTROUTING -s 192.168.0.0/16 ! -d 192.168.0.0/16 -j MASQUERADE
    table = iptc.Table(iptc.Table.NAT)
    chain = iptc.Chain(table, "POSTROUTING")

    rule = iptc.Rule()
    rule.src = network
    rule.dst = "!%s" % network
    rule.create_target("MASQUERADE")

    logger.detail("Add NAT rule...")
    add_rule(rule, chain)


def filter_rules(tap, network):
    # iptables -A FORWARD -o vt0 -d 192.168.0.0/16 -m state --state RELATED,ESTABLISHED -j ACCEPT
    # iptables -A FORWARD -i vt0 -s 192.168.0.0/16 -j ACCEPT
    # iptables -A FORWARD -i vt0 -o vt0 -j ACCEPT
    # iptables -A FORWARD -i vt0 -j REJECT
    # iptables -A FORWARD -o vt0 -j REJECT
    table = iptc.Table(iptc.Table.FILTER)
    chain = iptc.Chain(table, "FORWARD")

    rule = iptc.Rule()
    rule.out_interface = tap
    rule.dst = network
    rule.create_target("ACCEPT")
    match = iptc.Match(rule, "state")
    match.state = "RELATED,ESTABLISHED"
    rule.add_match(match)
    logger.detail("Add 1st rule...")
    add_rule(rule, chain)

    rule = iptc.Rule()
    rule.in_interface = tap
    rule.src = network
    rule.create_target("ACCEPT")
    add_rule(rule, chain)

    rule = iptc.Rule()
    rule.in_interface = tap
    rule.out_interface = tap
    rule.create_target("ACCEPT")
    add_rule(rule, chain)

    rule = iptc.Rule()
    rule.in_interface = tap
    rule.create_target("REJECT")
    add_rule(rule, chain)

    rule = iptc.Rule()
    rule.out_interface = tap
    rule.create_target("REJECT")
    add_rule(rule, chain)


def setup_nat(interface, network):
    filter_rules(interface, network)
    nat_rules(network)


# def __configure_nat_with_ns(interface, network, namespace):
#     logger.detail("Configuring NAT iptables rules using namespace: %s" % namespace)
#     logger.detail("PID: %s" % os.getpid())
#     setns(namespace)
#     setup_nat(interface, network)


def configure_nat(interface, network, namespace=None):
    if namespace:
        logger.detail("Creating new process: %s" % __name__)
        # proc = Process(target=__configure_nat_with_ns, args=(interface, network), kwargs={"namespace": namespace})
        # proc.start()
        args = ["ip", "netns", "exec", namespace, "python3",
                "-m", "lama.agent.helpers.lama_iptables", interface, network]
        logger.detail(" ".join(args))
        proc = subprocess.Popen(args, cwd=PATH,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        logger.detail("Created new process: %s" % proc)
        out, err = proc.communicate()
        err = err.decode("utf-8")
        if err:
            logger.error("Err: %s" % err)
        logger.detail("Out!")
    else:
        setup_nat(interface, network)


# def configure_nat(interface, network, namespace=None):
#     if namespace:
#         proc = Process(target=__configure_nat_with_ns, args=(interface, network), kwargs={"namespace": namespace})
#         proc.start()
#     else:
#         setup_nat(interface, network)

def log_stdout():
    root = logging.getLogger()
    root.setLevel(logging.INFO)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    root.addHandler(ch)
    return root


if __name__ == "__main__":
    logger = log_stdout()
    parser = ArgumentParser()
    parser.add_argument('tap', type=str, help="Tap interface name")
    parser.add_argument('network', type=str, help="Network address (ip/prefix).")
    params = parser.parse_args()

    configure_nat(params.tap, params.network)
    # nat_rules(params.network)
    # filter_rules(params.tap, params.network, namespace="ns-test-000001")
