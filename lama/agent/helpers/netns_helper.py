import traceback
from lama.utils.logging_lama import logger
from pyroute2.ipdb import IPDB
from pyroute2.netns import NetNS

__author__ = 'tiago'


class VethCreationException(Exception):
    pass


def create_veth_pair(ifname, ns_name, peer_ip='192.168.0.1/16', main_ip=None, peer_name=None):
    if not peer_name:
        main_iface = "%s-%d" % (ifname, 0)
        ns_iface = "%s-%d" % (ifname, 1)
    else:
        main_iface = ifname
        ns_iface = peer_name

    ipdb = IPDB()
    ipdb_ns = None
    try:
        ipdb_ns = IPDB(nl=NetNS(ns_name))
    except Exception as e:
        logger.debug("Failed to create, retrieve or connect to namespace!")
        ipdb.release()
        if ipdb_ns:
            ipdb_ns.release()
        raise VethCreationException("VETH creation failed!")

    try:
        if main_iface not in ipdb.interfaces:
            ipdb.create(ifname=main_iface, kind="veth", peer=ns_iface).commit()

            with getattr(ipdb.interfaces, ns_iface) as veth_peer:
                veth_peer.net_ns_fd = ns_name

        else:
            logger.debug(" SKIP Creation - Interface exists: %s in %s", main_iface, list(ipdb.interfaces.keys()))

        with getattr(ipdb.interfaces, main_iface) as veth_main:
            if main_ip:
                veth_main.add_ip(main_ip)
            veth_main.up()

        with getattr(ipdb_ns.interfaces, ns_iface) as veth_peer:
            if peer_ip:
                veth_peer.add_ip(peer_ip)
            veth_peer.up()

    except Exception as e:
        logger.error(traceback.format_exc())
        raise VethCreationException("EXCEPTION: %s" % e)
    finally:
        ipdb.release()
        ipdb_ns.release()
    return main_iface, ns_iface