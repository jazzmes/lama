import subprocess
from subprocess import DEVNULL
import psutil
import os
from config.settings import PATH_LOG, PATH
from lama.utils.logging_lama import logger


class Controller(object):

    def __init__(self, app_name, host, nid, port, netns=None):
        self.app_name = app_name
        self.host = host
        self.nid = nid
        self.netns = netns
        self.port = port

        # self.start()

    def start(self):
        if self.is_running():
            return

        args = []
        logger.temp("Launching Controller with netns: %s", self.netns)
        if self.netns:
            args += ["ip", "netns", "exec", self.netns]

        args += [
            "/home/lama/lama_envs/ryu/bin/python",
            "/home/lama/lama_envs/ryu/bin/ryu-manager",
            "--log-file=%s" % os.path.join(PATH_LOG, "controller_%s.log" % self.app_name),
            "--ofp-tcp-listen-port",
            str(self.port),
            "--lama-app",
            self.app_name,
            "--lama-host",
            str(self.host),
            "--lama-nid",
            str(self.nid),
            "lama.ryu.lama_network",
            "--nouse-stderr"
        ]
        logger.detail("cmd line: %s", " ".join(args))
        proc = subprocess.Popen(args, cwd=PATH, close_fds=True, stdout=DEVNULL)
        logger.detail("proc: %s, %d -> %s", proc, proc.pid, proc.returncode)
        logger.detail("Out: %s", proc.stdout)
        logger.detail("Err: %s", proc.stderr)
        return proc.pid

    def is_running(self):
        for proc in psutil.process_iter():
            try:
                if p.name() == "ryu-manager" and self.app_name in proc.cmdline():
                        return True
            except:
                continue
        return False
        # processes = [p for p in psutil.process_iter() if p.name() == "ryu-manager"]
        # found = None
        # for proc in processes:
        #     if self.app_name in proc.cmdline():
        #         return True
        # return found


# class ControllerHelper(object):
#
#     @staticmethod
#     def controller_running(app_name):
#         logger.debug("Checking if controller is running in localhost")
#         processes = [p for p in psutil.process_iter() if p.name() == "ryu-manager"]
#         logger.debug("There are %d controllers running!", len(processes))
#         found = None
#         for proc in processes:
#             logger.debug("Name: %s", proc.name())
#             logger.debug("Cmd:  %s", proc.cmdline())
#             logger.debug("PID:  %d", proc.pid)
#             if app_name in proc.cmdline():
#                 return proc
#             # cmd = proc.cmdline()
#             # try:
#             #     if cmd[cmd.index("--lama-app") + 1] == app_name:
#             #         return proc
#             # except:
#             #     pass
#         # if len(processes):
#         #     raise NotImplementedError()
#         return found
#
#     @staticmethod
#     def start_controller(app_name, nid, host):
#         # ryu-manager --log-file=/var/log/lama/controller_<app_name>.log --app-name <app_name> lama.ryu.lama_network
#         # raise NotImplementedError()
#
#         args = [
#             "ryu-manager",
#             "--log-file=%scontroller_%s.log" % (PATH_LOG, app_name),
#             "--lama-app",
#             app_name,
#             "--lama-host",
#             str(host),
#             "--lama-nid",
#             nid,
#             "lama.ryu.lama_network"
#         ]
#         logger.debug("cmd line: %s", " ".join(args))
#         # proc = subprocess.Popen(["sudo", "ip", "netns", "exec", "app1", "ip", "link"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#         # out, err = proc.communicate()
#         proc = subprocess.Popen(args, cwd=PATH, close_fds=True)
#         logger.debug("proc: %s, %d -> %s", proc, proc.pid, proc.returncode)
#         logger.debug(proc.stdout)
#         return proc.pid