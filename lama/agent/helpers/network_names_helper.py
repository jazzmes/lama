from lama.utils.snippets import shorten_app_name

__author__ = 'tiago'


def create_bridge_name(app_name):
    return shorten_app_name("lbr-%s" % app_name, 15)


def create_tap_name(app_name):
    return shorten_app_name("ltp-%s" % app_name, 15)


def create_gw_name(app_name):
    return shorten_app_name("lgw-%s" % app_name, 13)


def create_ns_name(app_name):
    return "ns-%s" % app_name


def create_network_name(app_name):
    return "lama-net-%s" % app_name

