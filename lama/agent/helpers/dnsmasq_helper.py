#!/usr/bin/python3
from argparse import ArgumentParser
import subprocess
import sys
import logging
import os
from os import makedirs, chown
import signal
from pwd import getpwnam

import psutil
from psutil import Process, NoSuchProcess

from config.settings import PATH_DNSMASQ
from lama.ryu.ovsdb_client import OVSDbClient
from lama.utils.logging_lama import logger
from lama.utils.snippets import shorten_app_name
from .network_helper import NetworkManager
from .netns_helper import VethCreationException, create_veth_pair


__author__ = 'tiago'


# class VethCreationException(Exception):
#     pass


class Dns(object):

    def __init__(self, app_name, netns=None):
        self.app_name = app_name
        self.path = os.path.join(PATH_DNSMASQ, self.app_name)
        self.netns = netns
        self.start()

    def start(self):
        if self.is_running():
            return

        logger.temp("Start DNSMasq - App: %s", self.app_name)

        # make sure directory exists
        try:
            makedirs(self.path)
        except FileExistsError as e:
            pass
        except OSError as e:
            raise Exception("Unable to create directory '%s'" % self.path)

        args = []
        if self.netns:
            args += ["ip", "netns", "exec", self.netns]

        args += [
            "sudo",
            # "/usr/sbin/dnsmasq", "--no-hosts", "--no-resolv", "--strict-order", "--bind-interfaces",
            "/usr/sbin/dnsmasq", "--no-hosts", "--strict-order", "--bind-interfaces",
            "--user=lama",
            "--except-interface", "lo",
            # "--interface=veth-%s-1" % ,
            "--dhcp-range", "192.168.0.0,static,255.255.0.0",
            "--log-facility=%s" % os.path.join(self.path, "dnsmasq.log"),
            "--dhcp-hostsfile=%s" % os.path.join(self.path, "hosts"),
            "--dhcp-leasefile=%s" % os.path.join(self.path, "vlan.leases"),
            # "--dhcp-option=6,0.0.0.0,8.8.8.8",
            "--dhcp-option=6,0.0.0.0",
            "--dhcp-option=3,192.168.0.2",
            "--pid-file=%s" % os.path.join(self.path, "dnsmasq.pid")
        ]
        logger.detail("cmd line: %s", " ".join(args))
        proc = subprocess.Popen(args, close_fds=True)
        logger.info("Started DNS server: proc=%s, pid=%d, retcode=%s", proc, proc.pid, proc.returncode)
        logger.detail(proc.stdout)

        self._current_hosts = {}
        return proc.pid

    def is_running(self):
        pidfile = os.path.join(self.path, "dnsmasq.pid")
        process_name = "dnsmasq"

        logger.detail("Check if dnsmasq is running with pidfile: %s", pidfile)
        if os.path.isfile(pidfile):
            logger.detail("File exists.. opening!")
            # processes = []
            with open(pidfile, "r") as f:
                pid = int(f.read())

            logger.detail("Looking for dnsmasq with pid: %d" % pid)
            try:
                p = Process(pid)
                if p.name() == process_name:
                    return p
            except NoSuchProcess:
                pass

        return False

    def add_host(self, hostname, hostmac, hostip):
        conf_dir = "%s%s" % (PATH_DNSMASQ, self.app_name)
        Dns.create_dir(conf_dir)

        host_file = os.path.join(conf_dir, "hosts")
        # current_hosts = {}
        # try:
        #     with open(host_file, "r") as fp:
        #         lines = fp.read().splitlines()
        #         for line in lines:
        #             mac, ip, name = line.split(",")
        #             current_hosts[mac] = (mac, ip, name)
        # except FileNotFoundError:
        #     pass
        # except Exception as e:
        #     logger.error("Exception when reading host file: %s", e)
        # current_hosts[hostmac] = (hostmac, hostip, hostname)
        self._current_hosts[hostmac] = (hostmac, hostip, hostname)
        with open(host_file, "w") as fp:
            logger.debug("Writing to host_file '%s'" % host_file)
            for m, contents in self._current_hosts.items():
                # logger.debug("Writing host: %s" % str(contents))
                fp.write("%s,%s,%s\n" % contents)

        # signal dnsmasq process to reload file if already running
        proc = self.is_running()
        if not proc:
            logger.warn("Unable to retrieve DNSMasq process")
        else:
            proc.send_signal(signal.SIGHUP)

    def remove_host(self, hostname, hostmac, hostip):
        conf_dir = "%s%s" % (PATH_DNSMASQ, self.app_name)
        Dns.create_dir(conf_dir)

        host_file = os.path.join(conf_dir, "hosts")
        current_hosts = {}
        try:
            with open(host_file, "r") as fp:
                lines = fp.read().splitlines()
                for line in lines:
                    mac, ip, name = line.split(",")
                    current_hosts[mac] = (mac, ip, name)
        except FileNotFoundError:
            # normal
            pass
        except Exception as e:
            logger.error("Exception when reading host file: %s", e)

        deleting_host = current_hosts.get(hostmac)
        if deleting_host == (hostmac, hostip, hostname):
            del current_hosts[hostmac]

            with open(host_file, "w") as fp:
                logger.debug("Writing to host_file '%s'" % host_file)
                for m, contents in current_hosts.items():
                    logger.debug("Writing host: %s" % str(contents))
                    fp.write("%s,%s,%s\n" % contents)

            # signal dnsmasq process to reload file if already running
            proc = self.is_running()
            if not proc:
                logger.warn("Unable to retrieve DNSMasq process")
            else:
                proc.send_signal(signal.SIGHUP)
            return True

        return False

    @staticmethod
    def create_dir(path, user="lama"):
        # make sure directory exists
        try:
            makedirs(path)
            user = getpwnam(user)
            # chmod(conf_dir, S_IRWXG | S_IRWXU | S_IRWXO)
            chown(path, user.pw_uid, user.pw_gid)
        except FileExistsError as e:
            pass
        except OSError as e:
            raise VethCreationException("Error creating directory: %s" % e)


class DNSMasqHelper(object):

    @staticmethod
    def get_dnsmasq_process(process_name="dnsmasq", namespace=None):
        processes = [p for p in psutil.process_iter() if process_name == p.name()]
        logger.debug("There are %d dnsmasq running!", len(processes))
        found = None
        for proc in processes:
            ns = DNSMasqHelper.get_namespace_by_pid(proc.pid)
            if ns:
                ns = ns if isinstance(ns, str) else ns.decode("utf-8")
                logger.debug("type(namespace): %s", type(namespace))
                logger.debug("PID: %d, Namespace: %s, Search: %s (Equal? %s)", proc.pid, ns, namespace, ns == namespace)
                if ns == namespace:
                    found = proc
            else:
                logger.debug("PID: %d", proc.pid)
        return found

    @staticmethod
    def add_host(app_name, hostname, hostmac, hostip):
        conf_dir = "%s%s" % (PATH_DNSMASQ, app_name)
        DNSMasqHelper.create_dir(conf_dir)

        host_file = "%s/hosts" % conf_dir
        current_hosts = {}
        try:
            with open(host_file, "r") as fp:
                lines = fp.read().splitlines()
                for line in lines:
                    mac, ip, name = line.split(",")
                    current_hosts[mac] = (mac, ip, name)
        except FileNotFoundError:
            # normal
            pass
        except Exception as e:
            logger.error("Exception when reading host file: %s", e)

        current_hosts[hostmac] = (hostmac, hostip, hostname)

        with open(host_file, "w") as fp:
            logger.debug("Writing to host_file '%s'" % host_file)
            for m, contents in current_hosts.items():
                logger.debug("Writing host: %s" % str(contents))
                fp.write("%s,%s,%s\n" % contents)

        # signal dnsmasq process to reload file if already running
        proc = DNSMasqHelper.get_dnsmasq_process(namespace=app_name)
        if not proc:
            logger.warn("Unable to retrieve DNSMasq process")
        else:
            proc.send_signal(signal.SIGHUP)

    @staticmethod
    def create_dir(path, user="lama"):
        # make sure directory exists
        try:
            makedirs(path)
            user = getpwnam(user)
            # chmod(conf_dir, S_IRWXG | S_IRWXU | S_IRWXO)
            chown(path, user.pw_uid, user.pw_gid)
        except FileExistsError as e:
            pass
        except OSError as e:
            raise VethCreationException("Error creating directory: %s" % e)

    @staticmethod
    def get_namespace_by_pid(pid):
        cmd = subprocess.Popen(["sudo", "ip", "netns", "identify", str(pid)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = cmd.communicate()
        nses = out.decode("utf-8").splitlines()
        if len(nses) == 1:
            return nses[0]
        else:
            return None

    @staticmethod
    def launch_dnsmasq(namespace):
        conf_dir = "%s%s" % (PATH_DNSMASQ, namespace)

        # make sure directory exists
        DNSMasqHelper.create_dir(conf_dir)

        args = [
            "ip", "netns", "exec", namespace,
            "/usr/sbin/dnsmasq", "--no-hosts", "--no-resolv", "--strict-order", "--bind-interfaces",
            "--user=lama",
            "--except-interface", "lo",
            "--interface=veth-%s-1" % shorten_app_name(namespace, 8),
            "--dhcp-range", "192.168.0.0,static,255.255.0.0",
            "--log-facility=%s/dnsmasq.log" % conf_dir,
            "--dhcp-hostsfile=%s/hosts" % conf_dir,
            "--dhcp-leasefile=%s/vlan.leases" % conf_dir,
            "--dhcp-option=6,0.0.0.0,8.8.8.8",
            "--dhcp-option=3,192.168.0.2"
        ]
        # args = args[0:13]
        logger.debug("cmd line: %s", " ".join(args))
        # proc = subprocess.Popen(["sudo", "ip", "netns", "exec", "app1", "ip", "link"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # out, err = proc.communicate()
        proc = subprocess.Popen(args, close_fds=True)
        logger.debug("proc: %s, %d -> %s", proc, proc.pid, proc.returncode)
        logger.debug(proc.stdout)
        return proc.pid

    # @staticmethod
    # def create_veth_pair(ifname, ns_name, peer_ip='192.168.0.1/16', main_ip=None):
    #     main_iface = "%s%d" % (ifname, 0)
    #     ns_iface = "%s%d" % (ifname, 1)
    #
    #     ipdb = IPDB()
    #     ipdb_ns = None
    #     try:
    #         ipdb_ns = IPDB(nl=NetNS(ns_name))
    #     except Exception as e:
    #         logger.debug("Failed to create, retrieve or connect to namespace!")
    #         ipdb.release()
    #         if ipdb_ns:
    #             ipdb_ns.release()
    #         raise VethCreationException("VETH creation failed!")
    #
    #     try:
    #         if main_iface not in ipdb.interfaces:
    #             ipdb.create(ifname=main_iface, kind="veth", peer=ns_iface).commit()
    #
    #             with getattr(ipdb.interfaces, ns_iface) as veth_peer:
    #                 veth_peer.net_ns_fd = ns_name
    #
    #         else:
    #             logger.debug(" SKIP Creation - Interface exists")
    #
    #         with getattr(ipdb.interfaces, main_iface) as veth_main:
    #             if main_ip:
    #                 veth_main.add_ip(main_ip)
    #             veth_main.up()
    #
    #         with getattr(ipdb_ns.interfaces, ns_iface) as veth_peer:
    #             if peer_ip:
    #                 veth_peer.add_ip(peer_ip)
    #             veth_peer.up()
    #
    #     except Exception as e:
    #         raise VethCreationException("EXCEPTION: %s", traceback.format_exc(e))
    #     finally:
    #         ipdb.release()
    #         ipdb_ns.release()
    #     return main_iface, ns_iface

    @staticmethod
    def start_dnsmasq(host, br_name, namespace):
        # br_name = br_name or "br-%s" % app_name
        proc = DNSMasqHelper.get_dnsmasq_process("dnsmasq", namespace)
        if not proc:
            # create the veth pair
            try:
                short_name = shorten_app_name(namespace, 8)
                veth, peer = create_veth_pair("veth-%s-0" % short_name, namespace, peer_name="veth-%s-1" % short_name)
                # add bridge for the app
                OVSDbClient.create_bridge(host, br_name)
                # add one of veth interfaces to the app bridge
                # OVSDbClient.add_port_to_bridge(host, br_name, veth)
                # if str(host) != "127.0.0.1":
                #     raise NotImplementedError()
                logger.debug("add port %s, %s", br_name, veth)
                OVSDbClient.add_port_to_local_bridge(br_name, veth)
                DNSMasqHelper.launch_dnsmasq(namespace)
                proc = DNSMasqHelper.get_dnsmasq_process("dnsmasq", namespace)
            except VethCreationException as e:
                logger.error("Error creating VETH: %s", e)


def log_stdout():
    root = logging.getLogger()
    root.setLevel(logging.INFO)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    root.addHandler(ch)
    return root

if __name__ == "__main__":
    logger = log_stdout()
    parser = ArgumentParser()
    parser.add_argument('namespace', type=str, help="Namespace to run DNSMasq")
    parser.add_argument('-b', dest='bridge', type=str, help="Bridge to connect DNSMasq to (through a veth pair)")
    parser.add_argument('-s', dest='host', type=str, help="Host where to install the bridge", default="127.0.0.1")
    params = parser.parse_args()

    # DNSMasqHelper.create_veth_pair("veth-%s-" % params.namespace, params.namespace)
    # proc = DNSMasqHelper.get_dnsmasq_process("dnsmasq", params.namespace)
    # if not proc:
    #     DNSMasqHelper.launch_dnsmasq("app1")
    #     proc = DNSMasqHelper.get_dnsmasq_process("dnsmasq", params.namespace)

    bridge = NetworkManager.create_bridge_name(params.namespace) if "bridge" not in params or not params.bridge else params.bridge
    DNSMasqHelper.start_dnsmasq(params.host, bridge, params.namespace)