from netaddr import IPNetwork, AddrFormatError, IPSet

__author__ = 'tiago'


class InvalidIPNetwork(Exception):
    pass


class InvalidSubnet(Exception):
    pass


class DuplicateSubnet(Exception):
    pass


class IPRangeExhausted(Exception):
    pass


class Subnet(object):
    def __init__(self, name, net):
        self.name = name
        self.net = net
        self._used = IPSet()
        self._reserved = IPSet((self.net.broadcast, self.net.network))
        try:
            self._virtual = self.net[-2]
            self._reserved.add(self._virtual)
        except:
            pass

    def get_virtual_ip(self):
        return self._virtual

    def get_ip_set(self):
        return self.net

    def get_ip(self):
        for ip in IPSet(self.net) - self._used:
            if ip not in self._used and ip not in self._reserved:
                self._used.add(ip)
                return ip
        return None

    def remove(self, ip):
        if ip in self._used:
            self._used.remove(ip)

    def reserve(self, ip):
        if ip in self._used:
            return False
        else:
            self._used.add(ip)
            return True

    def reserved(self, ip):
        return ip in self._used

    def used_count(self):
        return len(self._used)


class AddressManager(object):
    def __init__(self, ipnet='192.168.1.0/16', base_subnet_prefix=24, default_subnet=False):
        try:
            self.range = IPNetwork(ipnet)
        except (AddrFormatError, TypeError):
            raise InvalidIPNetwork(ipnet)

        self._base_subnet_size = max(base_subnet_prefix, self.range.prefixlen)

        # need to be persistent state
        # self._in_use = IPSet()
        self._subnets = {}
        self._subnet_used = IPSet()

        if default_subnet:
            self.create_subnet(None)

    def init_with_address(self, subnet_name, ip):
        if subnet_name not in self._subnets:
            for sn in self.range.subnet(self._base_subnet_size):
                if ip in sn:
                    subnet = Subnet(subnet_name, sn)
                    self._subnet_used.add(sn)
                    self._subnets[subnet_name] = subnet

        # self._in_use.add(ip)

    @property
    def size(self):
        return self.range.size

    def get_ip(self, subnet=None):
        if subnet not in self._subnets:
            self.create_subnet(subnet)
        return self._subnets[subnet].get_ip()

    def free(self, ip):
        for name, sn in self._subnets.items():
            if ip in sn.get_ip_set():
                sn.remove(ip)

    def get_cidr(self, subnet=None):
        if subnet in self._subnets:
            return str(self._subnets[subnet].get_ip_set())
        return None

    def create_subnet(self, name):
        if name not in self._subnets:
            for sn in self.range.subnet(self._base_subnet_size):
                if sn not in self._subnet_used:
                    sub = Subnet(name, sn)
                    self._subnet_used.add(sn)
                    self._subnets[name] = Subnet(name, sn)
                    return sub.get_ip_set()
            raise IPRangeExhausted("No space for subnet")
        else:
            raise DuplicateSubnet(name)

    def delete_subnet(self, name):
        if name in self._subnets:
            subnet = IPSet(self._subnets[name].get_ip_set())
            self._subnet_used -= subnet
            # self._in_use -= subnet.get_ip_set()
            del self._subnets[name]
        else:
            raise InvalidSubnet(name)

    def reserve(self, ip):
        for name, sn in self._subnets.items():
            if ip in sn.get_ip_set():
                return sn.reserve(ip)
        return False

    def reserved(self, ip):
        for name, sn in self._subnets.items():
            if ip in sn.get_ip_set():
                return sn.reserved(ip)
        return False

    @property
    def subnet_count(self):
        return len(self._subnets)

    @property
    def used_ip_count(self):
        return sum([sn.used_count() for name, sn in self._subnets.items()])

    def get_virtual_ip(self, subnet_name):
        if subnet_name not in self._subnets:
            return None

        return self._subnets[subnet_name].get_virtual_ip()
