import os
print(os.getcwd())

import traceback
from argparse import ArgumentParser

from netaddr import IPAddress
from libvirt import libvirtError
from lama.utils.logging_lama import logger, LamaLogger
from lama.api.api_libvirt import LibvirtConnection
from lama.ryu.ovsdb_client import OVSDbClient
import lama.agent.helpers.network_names_helper as neth

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'


class NetworkScripts(object):

    def __init__(self, app_name, controller_ip, controller_port, bridge_name=None):
        self._app_name = app_name
        self._bridge_name = bridge_name or neth.create_bridge_name(self._app_name)
        self._tap_name = neth.create_tap_name(self._app_name)
        self._gw_name = neth.create_gw_name(self._app_name)
        self._ns_name = neth.create_ns_name(self._app_name)
        self._net_name = neth.create_network_name(self._app_name)
        self._address = "127.0.0.1"
        self._controller = None
        self._controller_ip = controller_ip
        self._controller_port = controller_port

    def create_libvirt_network(self):
        logger.info("Creating libvirt network")
        self._create_bridge()

        connection = LibvirtConnection()
        network = None
        while not network:
            network = connection.get_network(self._net_name)
            if not network:
                logger.debug("Network not found. Defining...")
                network = connection.define_network(
                    network_type="openvswitch",
                    network_name=self._net_name,
                    bridge_name=self._bridge_name)
                if network:
                    network.setAutostart(True)
                    # TODO: network a wrapper object so we dont have to handle libvirt specific exceptions here
                    try:
                        network.create()
                    except libvirtError as e:
                        if "already active" not in str(e):
                            logger.error("Unexpected exception: %s. Traceback: %s", traceback.format_exc())

        if connection:
            connection.close()

        print("DONE")

    def _create_bridge(self):
        logger.temp("Create bridge: %s", self._bridge_name)
        if not OVSDbClient.bridge_exists(self._address, self._bridge_name):
            if self._controller_ip and self._controller_port:
                logger.temp("create bridge: %s:%s", self._controller_ip, self._controller_port)
                OVSDbClient.create_bridge(self._address, self._bridge_name, controller_ip=self._controller_ip,
                                          controller_port=self._controller_port)
            else:
                logger.error("No controller ip or port set.")
                exit(1)


if __name__ == "__main__":
    # logger.addHandler(logging.StreamHandler(sys.stdout))
    LamaLogger.set_filename(logger, "network_scripts")
    logger.setLevel('DEBUG')
    logger.info("Start network script")

    parser = ArgumentParser()
    subparsers = parser.add_subparsers(dest='action', help='sub-command help')
    libvirt_subparser = subparsers.add_parser('libvirt',
                                              help='Create libvirt network '
                                                   '(includes all necessary underlying network configuration)')
    libvirt_subparser.add_argument('app_name', type=str, help="App name")
    libvirt_subparser.add_argument('cip', type=IPAddress, help="Controller IP address")
    libvirt_subparser.add_argument('cport', type=int, help="Controller port")

    params = parser.parse_args()
    logger.info("Start executing: %s", params)

    if not params.action:
        logger.error("No action requested.")

    if params.action == "libvirt":
        try:
            ns = NetworkScripts(params.app_name, params.cip, params.cport)
            ns.create_libvirt_network()
        except:
            logger.error("Traceback: %s", traceback.format_exc())
            exit(1)

    logger.info("Leaving properly...")