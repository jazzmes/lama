from configparser import ConfigParser, ParsingError, NoSectionError
import os
import shutil
import traceback
import time
from os.path import isfile

from netaddr import IPAddress, IPNetwork

from config.settings import PATH_CONFIG, PROVIDER_AG_WEB_PORT, PROVIDER_AG_PORT, DISPATCHER_AG_PORT, PATH_IMAGE_STORAGE
from lama.agent.base import LamaAgent, AgentAddress, Repeat, EventType
from lama.api.event_log import EventLog, Category
from lama.agent.events import Event
from lama.agent.event_managers import twisted
from lama.agent.state_machine import State, StateMachine, EventNotSupportedByStateException
from lama.utils.logging_lama import logger
from lama.utils.snippets import get_ip_address
from .allocation_manager import AllocationManager
from .app_manager import AppManager
from .instance_manager import InstanceManager, Instance
from .monitoring import Monitoring
from .resource_manager import ResourceManager


__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMA Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class AuthState(State):
    """
    Authentication state: authenticates the app agent at the local provider.
    """
    def __init__(self, agent):
        """
        @type agent:ProviderAgent
        """
        self.agent = agent
        self.recurrent_auth = None
        self.start_auth = None

    def enter(self, event, prev_state):
        # send a message reliably
        event = Event(
            EventType.ProviderAuthRequestEvent,
            self.agent.id,
            self.agent.ip_address,
            self.agent.prefix,
            self.agent.monitoring.local_resources)

        self.start_auth = time.time()
        self.recurrent_auth = self.agent.recurring(
            self.agent.send_event_with_response,
            (self.agent.dispatcher_address, event),
            Repeat(number=3, wait=3), Repeat(number=float("inf"), wait=60))

    def process_event_send_failure(self, ref_event_type, ref_event_type_args, msg, **context):
        logger.info("At state '%s', failed to send event: %s (%s) - %s", self, ref_event_type, ref_event_type_args, msg)

    def process_provider_auth_response_event(self, result, agent_id):
        if result:
            logger.debug("Provider registration confirmed")
            self.agent.set_agent_id(agent_id)
            self.recurrent_auth.stop()
            self.agent.event_log.add_event("provider-%s" % self.agent.ip_address,
                                           Category.Operation,
                                           "Authentication", None,
                                           self.start_auth)
            return Active
        else:
            logger.warning("Provider was not registered")


class Active(State):
    """
    Active state: Starts provider agent's modules, listens to events...
    """

    def __init__(self, agent):
        """
        @type agent:ProviderAgent
        """
        self.agent = agent
        self.recurrent_auth = None

    def enter(self, event, prev_state):
        self.agent.resource_manager.set_local_resources(self.agent.monitoring.local_resources)
        self.agent.start_modules()

    def process_event(self, event, handler=None, **kwargs):
        """
        Replaces base State event handler to be able to look for handler in different modules.
        """
        handled = self.agent.event_handler_multiple(event, **kwargs)
        if not handled:
            try:
                super().process_event(event, **kwargs)
            except EventNotSupportedByStateException:
                logger.warning("Event '%s' is not supported by state '%s'",
                               event.type, self)

    def process_peer_candidate_probe_event(self, agent_id, conditions, **context):
        # get the peers
        candidates = self.agent.resource_manager.get_candidates(5)
        if len(candidates):
            src_address = context.get("address")
            event = Event(EventType.PeerCandidateResponseEvent, agent_id, candidates)
            self.agent.send_datagram(AgentAddress(src_address, PROVIDER_AG_PORT), event)
        else:
            logger.debug("No available candidates: ignoring request!")

    def process_peer_request_event(self, agent_id: str, ip_address: IPAddress, constraints: list, **context):
        src_address = context.get("address")
        if src_address != ip_address:
            logger.warning("Peering request for and address different than the source (src = %s, msg = %s",
                           src_address, ip_address)

        if len(constraints):
            if not self.agent.resource_manager.check_local_constraints(constraints):
                return False

        event = Event(EventType.PeerResponseEvent,
                      self.agent.id,
                      self.agent.ip_address,
                      agent_id,
                      self.agent.resource_manager.get_local_free_resources())
        self.agent.send_datagram(
            AgentAddress(src_address, PROVIDER_AG_PORT),
            event
        )

    def process_web_agent_peers_info(self, **context):
        handler = context.get("response_handler")
        try:
            info = {
                "ip_address": self.agent.address.ip_address,
                "peers": [pa.ip_address for pa in self.agent.resource_manager.get_providers()]
            }
        except:
            # logger.error("Trace: %s", traceback.format_exc())
            if handler:
                handler(False, msg=traceback.format_exc())
        else:
            # logger.warning("Returning info: %s", info)
            if handler:
                handler(True, **info)

    def process_web_agent_info(self, **context):
        handler = context.get("response_handler")
        try:
            info = {
                "ip_address": self.agent.address.ip_address,
                "port": self.agent.address.port,
                "sub_net": str(IPNetwork("%s/%s" % (self.agent.address.ip_address, self.agent.prefix)).cidr),
                "agent_id": self.agent.id,
                "resources": self.agent.resource_manager.get_local_resources().to_pretty_dict(),
                "peers": [pa.info for pa in self.agent.resource_manager.get_providers()],
                "allocations": self.agent.resource_manager.get_local_allocations(),
            }

        except:
            # logger.error("Trace: %s", traceback.format_exc())
            if handler:
                handler(False, msg=traceback.format_exc())
        else:
            # logger.warning("Returning info: %s", info)
            if handler:
                handler(True, **info)

    def process_web_get_provider_events(self, ip_address, start, end, **context):
        handler = context.get("response_handler")
        logger.detail("Getting events @ %s (%s, %s, %s)", self.agent.ip_address, ip_address, start, end)
        # events = None
        try:
            if str(ip_address) == str(self.agent.ip_address):
                events = self.agent.event_log.get_events()
            else:
                raise NotImplementedError("Collecting events from other provider is still not implemented!")
        except:
            # logger.error("Trace: %s", traceback.format_exc())
            if handler:
                handler(False, msg=traceback.format_exc())
        else:
            # logger.warning("Returning info: %s", info)
            if handler:
                handler(True, events=events)

    def send_delete_alerts(self, allocation_to_delete):
        for allocation in allocation_to_delete:
            self.agent.instance_manager.terminate_instance(allocation.app_name, allocation.instance_name)
            self.agent.send_event_reliably(
                AgentAddress(allocation.app_provider_address, PROVIDER_AG_PORT),
                Event(
                    EventType.ProviderDeletedLatentNotification,
                    allocation.app_name,
                    allocation.instance_name
                )
            )
        # raise NotImplementedError("Need to warn prvoider that instance was deleted: %s", allocation_to_delete)

    def process_provider_resource_reservation(self, remote_agent_id, app_name, service_name, vm_name, resource_spec,
                                              state, dependencies=None, **context):
        logger.temp("reservation: %s", (remote_agent_id, app_name, vm_name, resource_spec, state, context))
        remote_provider_address = context.get("address")
        handler = context.get("response_handler")

        # do i have enough resources available?
        logger.temp("PROVIDER BEFORE: %s",
                    self.agent.resource_manager.resource_db.get_provider(self.agent.ip_address).long_str)
        logger.temp("Dependencies received: %s", dependencies)
        result = self.agent.resource_manager.allocate(
                app_name,
                service_name,
                vm_name,
                resource_spec,
                state,
                remote_agent_id,
                remote_provider_address,
                dependencies
        )
        if result is not None:
            logger.temp("PROVIDER AFTER: %s",
                        self.agent.resource_manager.resource_db.get_provider(self.agent.ip_address).long_str)
            event = Event(EventType.ProviderResourceReservationConfirmation, remote_agent_id, app_name, vm_name)
            if handler:
                handler(event)

            if result:
                self.send_delete_alerts(result)
        else:
            event = Event(EventType.ProviderResourceReservationFailure, remote_agent_id, app_name, vm_name,
                          self.agent.resource_manager.get_local_free_resources())
            if handler:
                handler(event)

    def process_provider_resource_deletion(self, remote_agent_id, app_name, vm_name, **context):
        logger.debug("Args: %s", [remote_agent_id, app_name, vm_name, context])

        remote_provider_address = context.get("address")
        handler = context.get("response_handler")

        event = None
        # do i have enough resources available?
        if self.agent.resource_manager.local_delete(
                app_name,
                vm_name,
                remote_agent_id,
                remote_provider_address
        ):
            self.agent.instance_manager.terminate_instance(app_name, vm_name)

            event = Event(EventType.ProviderResourceDeletionConfirmation, remote_agent_id, app_name, vm_name, True)
        else:
            event = Event(EventType.ProviderResourceDeletionConfirmation, remote_agent_id, app_name, vm_name, False)

        if event and handler:
            handler(event)

    def process_provider_resource_change(self, remote_agent_id, app_name, service_name, instance_name, old_state, new_state, **context):
        logger.debug("Args: %s", [remote_agent_id, app_name, service_name, instance_name, old_state, new_state, context])

        # remote_provider_address = context.get("address")
        handler = context.get("response_handler")

        # do i have enough resources available?
        to_delete = self.agent.resource_manager.change_state(
            app_name,
            service_name,
            instance_name,
            # remote_agent_id,
            # remote_provider_address,
            old_state,
            new_state
        )
        if to_delete is not None:
            self.agent.instance_manager.change_state(app_name, instance_name, new_state)
            if to_delete:
                self.send_delete_alerts(to_delete)

            result = True
        else:
            result = False

        event = Event(EventType.ProviderResourceChangeConfirmation, remote_agent_id, app_name, service_name,
                      instance_name, old_state, new_state, result)
        if event and handler:
            handler(event)

    def process_web_image_upload(self, app_name, service_name, instance_name, filename, init, file, **context):
        handler = context.get("response_handler")

        # get allocation and agent
        allocation = self.agent.resource_manager.get_allocation_agent(app_name, service_name, instance_name)
        logger.debug("Agent address: %s", allocation)

        if not os.path.exists(PATH_IMAGE_STORAGE):
            try:
                os.makedirs(PATH_IMAGE_STORAGE)
            except OSError as e:
                logger.error("Unable to create directory '%s' (%s)", PATH_IMAGE_STORAGE, e)

        img_filename = Instance.get_image_filename(PATH_IMAGE_STORAGE, app_name, service_name, init=init)
        if not file:
            # logger.error("Need to use the given filename ('%s') and load file from there!" % filename)
            if not isfile(filename):
                raise FileNotFoundError(filename)

            logger.debug("Moving %s -> %s", filename, img_filename)
            shutil.move(filename, img_filename)
            os.chmod(img_filename, 0o644)
            #
            # raise NotImplementedError("Need to use the given filename ('%s') and load file from there!" % filename)
        else:
            # img_filename = Instance.get_image_filename(app_name, service_name)

            logger.debug("Writing file to filename: %s", img_filename)
            file_stream = open(img_filename, 'wb')
            file_stream.write(file)
            file_stream.close()

        # notify application agent
        self.notify_image_upload(allocation.app_provider_address, app_name, service_name, instance_name, init)

        if handler:
            try:
                handler(True, msg="File Uploaded!")
            except Exception as e1:
                try:
                    handler({'result': True, 'msg': "File Uploaded!"})
                except Exception as e2:
                    logger.error("Using handler: \n%s\n%s", e1, e2)

    def notify_image_upload(self, ip_address, app_name, service_name, instance_name, init):
        event = Event(EventType.AppImageUploadedEvent, app_name, service_name, instance_name, init)
        self.agent.send_event_reliably(AgentAddress(ip_address, PROVIDER_AG_PORT), event)

    def process_notify_host_down(self, ip_address, **context):
        # Not removing for allocation manager for now...
        # self.agent.alloc_manager.remove_provider()
        self.agent.resource_manager.remove_provider(ip_address)


class ProviderAgent(LamaAgent):
    # agent state: app architecture

    # configuration
    auth_max_retries = 3
    auth_short_retry_timeout = 3
    auth_long_retry_timeout = 3

    def __init__(self, dispatcher_address, agent_id=None, filename=None):

        super().__init__()

        self.ip_address, self.prefix = get_ip_address()

        # set logger file for this agent
        if not self.ip_address:
            logger.error("Unable to load IP address. Terminating!")
            exit()

        self.event_log = EventLog(str(self.ip_address))

        self.initialize_log_file("provider_%s" % self.ip_address)
        logger.info("Started Provider Agent")

        # agent information
        self.id = agent_id
        self.config_file = filename

        # relevant addresses
        self.address = AgentAddress(ip=self.ip_address,
                                    port=PROVIDER_AG_PORT)

        self.dispatcher_address = AgentAddress(ip=dispatcher_address,
                                               port=DISPATCHER_AG_PORT)

        # set up a state machine to manage agent's workflow
        self.fsm = StateMachine(AuthState, identifier=self.__class__.__name__, agent=self)
        """:type: StateMachine"""

        # sets agent's modules: each module should have an event handler
        self.monitoring = Monitoring(self)
        self.resource_manager = ResourceManager(self)
        self.alloc_manager = AllocationManager(self)
        self.instance_manager = InstanceManager(self)
        self.app_manager = AppManager(self)

        # configure communication platform and schedule first event (start of the FSM)
        self.set_event_manager_platform(
            twisted.EventManager(),
            initial_function=self.fsm.start,
            event_handler=self.event_handler,
            tcp_port=self.address.port,
            web_port=PROVIDER_AG_WEB_PORT,
            udp_port=self.address.port
        )

        self.response_handler = None

    def start_modules(self):
        self.resource_manager.start()
        self.alloc_manager.start()
        logger.debug("Starting instance manager")
        self.instance_manager.start()
        logger.debug("Starting app manager")
        self.app_manager.start()
        self.monitoring.start()

    def event_handler(self, event, **context):
        logger.info("Received event: %s", str(event)[:200])
        self.fsm.event(event, **context)

    def set_agent_id(self, id):
        self.id = id
        config_changes = {'AGENT': {'id': id}}
        self.update_config(config_changes)

    @classmethod
    def from_config_file(cls, filename=None):
        """
        Create a new provider agent from a config file.
        :param filename: config file of the provider agent.
        """
        generic_filename = PATH_CONFIG + "provider.ini"
        if not filename:
            # try to load default filename
            ip, prefix = get_ip_address()
            ip_filename = PATH_CONFIG + "provider_%s.ini" % ip
            try:
                if not os.path.isfile(ip_filename):
                    raise FileNotFoundError("Config file not found")
                filename = ip_filename
            except FileNotFoundError:
                pass

            if not filename:
                try:
                    if not os.path.isfile(generic_filename):
                        raise FileNotFoundError("Config file not found")
                    filename = generic_filename
                except FileNotFoundError:
                    pass

                if not filename:
                    logger.error("Unable to parse the default config files ('%s', '%s')" % (ip_filename,
                                                                                            generic_filename))
                    exit()

        # [RESOURCES]
        # cpu = 1E9 (cycles per sec)
        # ram = 1GB
        # ...
        config = ConfigParser()
        # inline_comment_prefixes=('#', ';'))
        logger.info("Provider ini file: '%s'" % filename)
        try:
            parsed = config.read(filename)
            if len(parsed) == 0:
                logger.warning("No config files parsed")
                logger.error("ERROR: Bad configuration file: '%s'" % filename)
                exit()

        except ParsingError:
            raise ParsingError("Unable to parse the config file '%s'" % filename)

        if not config.has_section('DISPATCHER'):
            raise NoSectionError("No section named DISPATCHER")

        agent_id = config.get('AGENT', 'id') if config.has_section('AGENT') \
            and config.has_option('AGENT', 'id') else None

        # create new config file if using default global_filename
        if filename == generic_filename:
            with open(ip_filename, 'w') as fp:
                config.write(fp)

            filename = ip_filename

        new_provider = cls(dispatcher_address=config.get('DISPATCHER', 'host'), agent_id=agent_id, filename=filename)

        logger.info("Initialized provider with id = %s" % agent_id)
        return new_provider

    def update_config(self, config_changes):
        config = ConfigParser()
        config.read(self.config_file)

        logger.debug("Writing changes to config file: %s", config_changes)
        for section, option in config_changes.items():
            if not config.has_section(section):
                config.add_section(section)
            for key, value in option.items():
                config.set(section, key, value)

        with open(self.config_file, 'w') as fp:
            config.write(fp)