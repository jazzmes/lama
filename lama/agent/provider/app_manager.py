import json
import os
import traceback
import signal
import subprocess
from multiprocessing import Process

import psutil

from config.settings import PATH_APP_SPEC_FILES, PATH_SCRIPTS_FOLDER
from lama.agent.base import AgentAddress
from lama.utils.codecs import LamaJSONEncoder
from lama.api.event_log import Category
from lama.agent.events import EventType, Event
from lama.utils.lama_collections import PersistentSyncDict
from lama.agent.state_machine import State, StateMachine
from lama.utils import snippets
from lama.utils.enum import Enum
from lama.agent.helpers.network_helper import NetworkManager
from lama.utils.logging_lama import logger
from lama.utils.snippets import set_user
from lama.utils.temp_netns import setns
from .module import LamaModule


__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


# def preexec_fn():
#     os.setgid(1000)
#     os.setuid(1000)


class InitializationState(State):

    def __init__(self, app_manager):
        super().__init__()
        self.app_manager = app_manager

    def enter(self, event, prev_state):
        return ActiveState


class ActiveState(State):

    def __init__(self, app_manager):
        """
        @type app_manager: AppManager
        """
        super().__init__()
        self.app_manager = app_manager
        self.clients = {}

    def enter(self, event, prev_state):
        self.app_manager.verify_apps()

    def process_app_deploy_event(self, app_name, nid, app_spec, **context):
        handler = context.get("response_handler")

        # self.app_manager.get_new_port()
        self.app_manager.agent.event_log.add_event("provider-%s" % self.app_manager.agent.ip_address,
                                                   Category.Operation,
                                                   "AppCreation", "App: %s" % app_name)

        # register app or save configuration
        app = self.app_manager.register_app(nid, app_spec)
        logger.info("Start app agent - app_spec: %s", app_spec)
        if handler:
            logger.detail("Sending response")
            if app:
                logger.detail("Sending ACCEPT response")
                dispatcher_event = Event(
                    EventType.AppDeployAcceptEvent,
                    app_name,
                    0
                )
            else:
                logger.detail("Sending REFUSE response")
                dispatcher_event = Event(
                    EventType.AppDeployRefuseEvent,
                    app_name
                )

            handler(dispatcher_event)
        else:
            logger.warn("No handler!!!")

        # launch app
        if app and app.status == AppStatus.Created:
            # ATTENTION: cannot run long processes like launch before sending response to dispatcher
            # Because we are using twisted the function would run before it sent the message!!!
            self.app_manager.agent.event_manager.schedule(0.002, app.launch, self.app_manager.agent.ip_address)

    def process_app_auth_request_event(self, app_name, pid, port, **context):
        logger.detail("Args: %s", [app_name, pid, port, context])
        address = context.get("address")
        response_handler = context.get("response_handler")

        result = self.app_manager.authenticate_app(app_name, pid, address, port)
        event = Event(EventType.AppAuthResponseEvent,
                      True if result else False,
                      app_name if result else None)

        if result:
            self.app_manager.agent.event_log.add_event("provider-%s" % self.app_manager.agent.ip_address,
                                                       Category.Operation,
                                                       "AppAuthentication", "App: %s" % app_name)

        if response_handler:
            response_handler(event)
        else:
            self.app_manager.agent.send_event_reliably(address, event)

    def process_web_app_agent_info(self, app_name, **context):
        handler = context.get("response_handler")
        if app_name in self.app_manager.apps:
            self.clients[(app_name, EventType.AppInfoResponseEvent)] = handler
            result = self.app_manager.send_event_to_app_with_response(
                app_name,
                Event(EventType.AppInfoQueryEvent))
            if not result:
                self.process_app_info_response_event(app_name, {})

    def process_web_app_status_data(self, app_name, options, **context):
        handler = context.get("response_handler")
        logger.temp("app_name=%s apps=%s", app_name, self.app_manager.apps)
        if app_name in self.app_manager.apps:
            self.clients[(app_name, EventType.AppStatusDataResponseEvent)] = handler
            logger.temp(self.clients)
            result = self.app_manager.send_event_to_app_with_response(
                app_name,
                Event(EventType.AppStatusDataQueryEvent, options))
            if not result:
                self.process_app_status_data_response_event(app_name, {})

    def process_web_app_monitor_data(self, app_name, options, **context):
        handler = context.get("response_handler")
        logger.temp("process_web_app_monitor_data:handler:%s", handler)
        if app_name in self.app_manager.apps:
            self.clients[(app_name, EventType.AppMonitorDataResponseEvent)] = handler
            logger.temp(self.clients)
            result = self.app_manager.send_event_to_app_with_response(
                app_name,
                Event(EventType.AppMonitorDataQueryEvent, options))
            if not result:
                self.process_app_monitor_data_response_event(app_name, {})

    def process_app_monitor_data_response_event(self, app_name, data, **context):
        handler = self.clients.get((app_name, EventType.AppMonitorDataResponseEvent))
        logger.temp("process_app_monitor_data_response_event:handler:%s", handler)
        logger.temp(self.clients)
        if handler:
            handler(len(data) > 0, **data)
            del self.clients[(app_name, EventType.AppMonitorDataResponseEvent)]

    def process_app_info_response_event(self, app_name, app_info, **context):
        handler = self.clients.get((app_name, EventType.AppInfoResponseEvent))
        if handler:
            handler(len(app_info) > 0, **app_info)
            del self.clients[(app_name, EventType.AppInfoResponseEvent)]

    def process_app_status_data_response_event(self, app_name, data, **context):
        handler = self.clients.get((app_name, EventType.AppStatusDataResponseEvent))
        logger.temp("process_app_status_data_response_event:handler:%s", handler)
        logger.temp(self.clients)
        if handler:
            handler(len(data) > 0, **data)
            del self.clients[(app_name, EventType.AppStatusDataResponseEvent)]

    def process_web_app_create_image(self, app_name, service_name, size, init, **context):
        handler = context.get("response_handler")
        logger.debug("DEBUG ASYNC ISSUE: Web app create image: %s", [app_name, service_name, size, init])
        if app_name in self.app_manager.apps:
            self.clients[(app_name, EventType.AppCreateImageResponseEvent)] = handler
            result = self.app_manager.send_event_to_app_with_response(
                app_name,
                Event(EventType.AppCreateImageEvent, service_name, size, init)
            )
            if not result:
                self.process_app_create_image_response_event(app_name, None, None)

    def process_app_create_image_response_event(self, app_name, service_name, instances, **context):
        key = (app_name, EventType.AppCreateImageResponseEvent)
        handler = self.clients.get(key)
        if handler:
            response = {
                "service_name": service_name,
                "instances": instances
            }
            logger.debug("DEBUG ASYNC ISSUE: Web app create image response: %s", [app_name, service_name, instances])
            handler(service_name is not None, **response)
            del self.clients[key]

    def process_web_add_instance(self, app_name, service_name, **context):
        handler = context.get("response_handler")
        if app_name in self.app_manager.apps:
            event = Event(EventType.AppAddInstanceEvent, app_name, service_name)
            self.clients[(app_name, event.event_id, EventType.AppAddInstanceResponseEvent)] = handler
            result = self.app_manager.send_event_to_app_with_response(
                app_name,
                event
            )
            if not result:
                self.process_app_add_instance_response_event(app_name, event.event_id, False, "Unknown App IP address")

    def process_app_add_instance_response_event(self, app_name, event_id, result, reason, **context):
        key = (app_name, event_id, EventType.AppAddInstanceResponseEvent)
        handler = self.clients.get(key)
        logger.detail("Handler: %s => %s", key, handler)
        if handler:
            response = {
                "msg": reason
            }
            logger.detail(response)
            handler(result, **response)
            del self.clients[key]

    def process_web_start_instance(self, app_name, instance_name, **context):
        # logger.detail("Starting instance: '%s' of '%s'", instance_name, app_name)
        handler = context.get("response_handler")
        if app_name in self.app_manager.apps:
            self.clients[(app_name, EventType.AppStartInstanceResponseEvent)] = handler
            result = self.app_manager.send_event_to_app_with_response(
                app_name,
                Event(EventType.AppStartInstanceEvent, app_name, instance_name)
            )
            if not result:
                self.process_app_start_instance_response_event(app_name, False, "Unknown App IP address")

    def process_app_start_instance_response_event(self, app_name, result, reason, **context):
        key = (app_name, EventType.AppStartInstanceResponseEvent)
        handler = self.clients.get(key)
        logger.detail("Clients: %s => %s", self.clients, key)
        if handler:
            response = {
                "msg": reason
            }
            logger.detail(response)
            handler(result, **response)
            del self.clients[key]

    def process_web_start_app(self, app_name, **context):
        handler = context.get("response_handler")
        # logger.detail("@ app_manager :: web_start_app")
        if app_name in self.app_manager.apps:
            self.clients[(app_name, EventType.AppStartResponseEvent)] = handler
            result = self.app_manager.send_event_to_app_with_response(
                app_name,
                Event(EventType.AppStartEvent, app_name)
            )
            logger.temp("Response result: %s", result)
            if not result:
                self.process_app_start_response_event(app_name, False, "Unknown App")
        # forward request to app ?

    def process_app_start_response_event(self, app_name, result, reason, **context):
        key = (app_name, EventType.AppStartResponseEvent)
        handler = self.clients.get(key)
        logger.detail("Clients: %s => %s", self.clients, key)
        if handler:
            response = {
                "msg": reason
            }
            logger.detail(response)
            handler(result, **response)
            del self.clients[key]

    def process_web_get_app_events(self, app_name, **context):
        handler = context.get("response_handler")
        if app_name in self.app_manager.apps:
            result = self.app_manager.send_event_to_app_with_response(
                app_name,
                Event(EventType.AppEventsQueryEvent))
            self.clients[(app_name, EventType.AppEventsResponseEvent)] = handler
            if not result:
                self.process_app_events_response_event(app_name, None)

    def process_app_events_response_event(self, app_name, events, **context):
        handler = self.clients.get((app_name, EventType.AppEventsResponseEvent))
        if handler:
            handler(events is not None, events=events)
            del self.clients[(app_name, EventType.AppEventsResponseEvent)]

    def process_web_change_workload(self, app_name, kwargs, **context):
        logger.detail("Processing change workload event - %s - args: %s", app_name, kwargs)
        handler = context.get("response_handler")
        logger.detail(self.app_manager.apps)
        if app_name in self.app_manager.apps:
            self.clients[(app_name, EventType.AppChangeWorkloadResponseEvent)] = handler
            result = self.app_manager.send_event_to_app_with_response(
                app_name,
                Event(EventType.AppChangeWorkloadEvent, app_name, kwargs)
            )
            if not result:
                self.process_app_start_instance_response_event(app_name, False, "Unknown App IP address")

    def process_app_change_workload_response_event(self, app_name, result, text, **context):
        key = (app_name, EventType.AppChangeWorkloadResponseEvent)
        handler = self.clients.get(key)
        logger.detail("Clients: %s => %s", self.clients, key)
        if handler:
            # logger.detail(response)
            handler(result, **{"msg": text})
            del self.clients[key]

    def process_provider_start_instance_response_event(self, app_name, service_name, instance_name,
                                                       active, msg, **context):
        logger.detail("Processing ProviderStartInstanceResponseEvent: %s",
                    [app_name, service_name, instance_name, active, msg])
        if app_name in self.app_manager.apps:
            self.app_manager.send_event_to_app(
                app_name,
                Event(EventType.ProviderStartInstanceResponseEvent,
                      app_name, service_name, instance_name,active, msg)
            )

    def process_app_image_uploaded_event(self, app_name, service_name, instance_name, init, **context):
        if app_name in self.app_manager.apps:
            self.app_manager.send_event_to_app(
                app_name,
                Event(EventType.AppImageUploadedEvent, app_name, service_name, instance_name, init)
            )


class AppStatus(Enum):
    Created = 1
    Launched = 2
    Active = 3


class App(object):

    def __init__(self, app_name, provider_address, nid, controller_port, spec_file):
        # , local=True):
        # self.local = local
        self.app_name = app_name
        self.nid = nid
        self.spec_file = spec_file
        self.status = AppStatus.Created
        self.pid = None
        self.address = None
        self.port = 0
        logger.temp("New app with controller_port=%s", controller_port)
        self.net = NetworkManager(app_name, controller_port, controller_ip=str(provider_address))
        # self.controller = None

    def __str__(self):
        return "App['%s' @ PID=%s, port=%s with status %s]" % (self.app_name, self.pid, self.port, self.status)

    def kill(self):
        # we do not use transport because if the application agent was already
        #  running, the transport to the old process was not established
        # otherwise: transport.signalProcess('KILL')
        logger.debug("KILLing app: pid: %s, signal: %s", self.pid, signal.SIGQUIT)
        raise NotImplementedError()
        os.kill(self.pid, signal.SIGQUIT)

    def verify(self):
        if not self.pid:
            logger.detail("No PID!")
            return False

        proc = None
        try:
            proc = psutil.Process(self.pid)
        except psutil.NoSuchProcess:
            pass

        if proc:
            logger.detail("Verifying %s at pid %s. Running? %s", proc, self.pid, proc.is_running())
            if proc.is_running():
                logger.detail("Proc is running...")
                if ("run_application" in proc.name()
                    or ("python" in proc.name()
                        and any("run_application" in s for s in proc.cmdline()))):
                    logger.detail("...and is an app!")
                    # restart
                    if not self.status == AppStatus.Active:
                        # TODO: should ping the process to check if everything is ok
                        self.status = AppStatus.Active
                    return True

        return False

    def launch(self, provider_address):
        logger.temp("Creating gateway: app = %s, pa = %s", self.app_name, provider_address)
        # create gateway: namespace + netns
        if self.status == AppStatus.Created:
            logger.temp("Creating gateway")
            self.net.create_gw()
            logger.temp("Creating controller")
            self.net.create_controller(provider_address, self.nid)
            logger.temp("Start agent")
            self.start_app_agent(provider_address)

    # def launch_agent(self, provider_address):
    def launch_agent(self, provider_address):
        logger.temp("Launching app in new process")
        exe = os.path.join(PATH_SCRIPTS_FOLDER, "run_application.py")
        args = [exe, str(provider_address), str(self.nid), self.spec_file, str(self.port), str(self.net.controller_ip), str(self.net.controller_port)]

        cmd = None
        try:
            setns(self.net.ns_name)
            # self.set_user()

            logger.temp("LAUNCH: Launching agent: %s" % args)
            cmd = subprocess.Popen(args, close_fds=True, preexec_fn=set_user)
            # cmd = subprocess.Popen(args, close_fds=True)
        except:
            logger.detail("LAUNCH: Error launching process (%s). Trace: %s",
                        [self.app_name, args], traceback.format_exc())
            # return None

        if cmd:
            logger.detail("LAUNCH: the new pid: %s" % cmd.pid)

    def start_app_agent(self, provider_address):

        logger.detail("Launching app in new process")
        proc = Process(target=self.launch_agent, args=(provider_address,))
        proc.start()
        logger.detail("Leaving the start_app_Agent function")


        # setns(self.net.ns_name)
        #
        # exe = os.path.join(PATH_SCRIPTS_FOLDER, "run_application.py")
        # args = [exe, str(self.nid), self.spec_file, str(self.port)]
        # logger.info("Launching agent: %s", args)
        # try:
        #     proc = subprocess.Popen(args, close_fds=True, preexec_fn=lambda: self.__change_user())
        # except:
        #     logger.error("Error launching process (%s). Trace: %s", [self.app_name, args], traceback.format_exc())
        #     return None
        #
        # if proc.pid:
        #     self.pid = proc.pid
        #     self.status = AppStatus.Launched
        #     logger.info("Launched application agent for App: '%s' at port %s => PID: %s",
        #                 self.app_name, self.port, self.pid)
        # else:
        #     return None
        #     # raise NotImplementedError("Application was not launched: no pid.")


class IdleState(State):
    pass


class AppManager(LamaModule):

    def __init__(self, agent):
        super().__init__(agent)

        self.fsm = StateMachine(InitializationState, identifier=self.__class__.__name__, app_manager=self)
        self.agent.add_handler(self.fsm.event)

        self.apps = PersistentSyncDict("applications", App)
        # TODO: Not RESTART proof
        self.app_controller_ports = list(range(16633, 26633))

        logger.info("App Manager State:\n%s", self)

    def start(self):
        self.fsm.start()

    def __str__(self):
        str = "" if len(self.apps) else "Provider has not app agents!"
        for app in self.apps.values():
            str += "%s\n" % app
        return str

    def save_spec_to_file(self, spec):
        folder = PATH_APP_SPEC_FILES
        if folder[-1] != "/":
            folder += "/"
        try:
            os.makedirs(folder)
        except FileExistsError:
            pass

        if not spec.name:
            return None

        filename = "%s%s.json" % (folder, snippets.escape_name(spec.name))
        try:
            with open(filename, "w") as fp:
                d = spec.to_dict()
                # logger.debug(d)
                s = json.dumps(d, cls=LamaJSONEncoder)
                # logger.debug(s)
                fp.write(s)
        except:
            logger.error("Type: %s", type(spec))
            logger.error("Error writing spec to file '%s'", filename)
            logger.debug("Detail: " + traceback.format_exc())
            return None

        return filename

    def get_unique_controller_port(self):
        try:
            return self.app_controller_ports.pop(0)
        except IndexError:
            logger.error("No ports available")
            return None

    def register_app(self, nid, spec):

        if spec.name not in self.apps:
            filename = self.save_spec_to_file(spec)
            controller_port = self.get_unique_controller_port()
            app = App(spec.name, self.agent.ip_address, nid, controller_port, filename)
            self.apps.add(spec.name, app)

        else:
            app = self.apps.get(spec.name)
            logger.detail("App name = %s, Status = %s", app.app_name, app.status)
        return app

    def verify_apps(self):
        logger.detail("App agents loaded")
        if len(self.apps):
            logger.detail("App agents registered as running:\n %s", self.apps)
        else:
            logger.detail("No app agents running:")

        pids = []
        launching = []
        # TODO: contact all ports...
        logger.detail("Apps: %s", self.apps)
        for name, app in self.apps.items():
            logger.detail("Verifying app '%s':", name)
            if app.status in [AppStatus.Launched, AppStatus.Active]:
                if not app.verify():
                    logger.detail("... not running!")
                    launching.append(name)
                    app.pid = None
                    app.status = AppStatus.Created
                else:
                    pids.append(app.pid)
                    logger.detail("App '%s' is running on pid %s!", name, app.pid)

        ghost_pids = []
        for proc in psutil.process_iter():
            try:
                if "run_application" in proc.name()\
                        or ("python" in proc.name() and any("run_application" in s for s in proc.cmdline())):
                    if proc.pid not in pids:
                        ghost_pids.append(proc.pid)
                        proc.kill()
            except psutil.NoSuchProcess:
                pass

        for name in launching:
            app = self.apps.get(name)
            logger.detail("App '%s' is not running on pid %s! Restarting...", name, app.pid)
            app.launch(self.agent.ip_address)
            logger.detail("App re-launched: %s", app)
            pids.append(app.pid)

        if len(ghost_pids) > 0:
            logger.error("Ghost Agents :: Pids %s at %s do not have a matching CREATED/ACTIVE agent registry! Killed!",
                         ghost_pids, self.agent.ip_address)
            # raise GhostAgentException("Pids %s at %s do not have a matching agent registry! Killed",
            #                           ghost_pids, self.agent.ip_address)

    def authenticate_app(self, app_name, pid, address, port):
        if app_name in self.apps:
            app = self.apps.get(app_name)
            self.apps.update(app_name, status=AppStatus.Active, pid=pid, address=address, port=port)
            logger.info("App agent '%s' with pid %d is running at %s:%d.", app.app_name, app.pid, app.address, app.port)
            return app

        return None

    def send_event_to_app(self, app_name, event):
        app = self.apps.get(app_name)
        self.agent.send_event_reliably(
            AgentAddress(ip=app.address, port=app.port),
            event
        )

    def send_event_to_app_with_response(self, app_name, event):
        app = self.apps.get(app_name)
        if not app:
            logger.error("App '%s' does not exist", app_name)
            return False
        elif not app.address:
            logger.warn("App '%s' does not have IP address (not authenticated yet?)", app_name)
            return False
        logger.debug("Send event '%s' to app '%s' @ port %s", event, app_name, app.port)
        self.agent.send_event_with_response(
            AgentAddress(ip=app.address, port=app.port),
            event
        )
        return True
    # def get_new_port(self):
    #     # have a range of ports
    #     rng = set(range(20013, 20063))
    #
    #     # get a list of used ports
    #     used = set({app.port for app in self.apps.values()})
    #
    #     # eliminate whats taken: compute difference
    #     # decide on a port: minimum of list
    #     port = min(rng.difference(used))
    #     return port