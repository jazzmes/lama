import sys
import random
import traceback
import time
from datetime import datetime, timedelta

from netaddr import IPSet, IPNetwork, IPAddress

import config.exp_constraints as constraints
from config.settings import TIMEOUT_PROBES, PEER_QUERY_SIMUL_NUMBER, PROVIDER_AG_PORT
from lama.agent.base import AgentAddress, Repeat, RecurringFunction
from lama.api.event_log import Category
from lama.agent.events import Event, EventType
from lama.agent.provider.module import LamaModule
from lama.agent.state_machine import State, StateMachine
from lama.utils.enum import Enum
from lama.utils.logging_lama import logger


__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class NoCandidatesException(Exception):

    def __init__(self):
        super().__init__("Candidates list is empty!")


class NoProbesException(Exception):

    def __str__(self):
        return "NoProbesException:: No probes were generated!"


class ActiveState(State):

    def __init__(self, peer_manager):
        """
        @type peer_manager: PeerManager
        """
        self.peer_manager = peer_manager

    def process_provider_resource_subscribe_updates(self, agent_id, **context):
        self.peer_manager.add_subscriber(context.get('address'))

    def process_provider_resource_cancel_updates(self, agent_id, **context):
        self.peer_manager.remove_subscriber(context.get('address'))


class SteadyState(ActiveState):
    """
    Agent only responds to requests.
     -> CandidateSearchState: when constraints change and are not met, if there are no peer candidates
     -> SteadyState: when constraints change and are not met, if there are peer candidates
    """
    def enter(self, event, prev_state):
        # check conditions
        logger.debug("Steady provider list: %s", self.peer_manager.peers)

    def process_start_peer_search_event(self, conditions):
        self.peer_manager.set_conditions(conditions)
        if self.peer_manager.probing_mode == ProbingModes.KB_PROBING \
                and not self.peer_manager.peers.has_candidates():
            return CandidateSearchState
        else:
            return PeerSearchState


class CandidateSearchState(ActiveState):
    """
    Agent repeatedly sends requests for candidates.
     -> SteadyState: when database constraints are met
     -> PeerSearchState: when adds new candidates
    """
    def __init__(self, peer_manager):
        """
        @type peer_manager: PeerManager
        """
        super().__init__(peer_manager)

        self.probe_levels = [
            # self.send_to_forced_peers,
            self.send_to_peers,
            # self.send_local_broadcast,
            self.send_to_dispatcher
        ]

        self.probe_level = 0
        self.recurrent_probing = None
        self.start_candidate_search = None

    def enter(self, event, prev_state):
    #     self.process_candidate_probe_timeout()
    #
    # def process_candidate_probe_timeout(self):
        self.recurrent_probing = self.peer_manager.agent.recurring(
            self.probe,
            (),
            Repeat(number=len(self.probe_levels)*1, wait=0.2), Repeat(number=float("inf"), wait=5))

        self.start_candidate_search = time.time()

    def probe(self):
        total_probes = None
        for i in range(len(self.probe_levels)):
            try:
                # total_probes = None
                current = (self.probe_level + i) % len(self.probe_levels)
                total_probes, exhausted = self.probe_levels[current](self.peer_manager.conditions)
                if exhausted:
                    self.probe_level = (self.probe_level + i + 1) % len(self.probe_levels)
                break
            except NotImplementedError:
                _, _, exc_traceback = sys.exc_info()
                logger.warning("No callback set for function '%s' (%s)." %
                               (self.probe_levels[current].__name__, traceback.format_tb(exc_traceback)[-1]))
            except NoProbesException as e:
                logger.debug("Probing method did not generate probes. Trying next method!")
            except Exception as inst:
                logger.error("%r ::: Unable to run the function '%s'. Trace: %s" %
                             (inst, self.probe_levels[current].__name__, traceback.format_exc()))

        # TODO: consider going back to this solutions to vary interval based on number of packets sent
        # # wait for 60 seconds if no probes were sent...
        # timeout = TIMEOUT_PROBES * total_probes if total_probes else 60
        #
        # self.peer_manager.agent.event_manager.schedule(timeout, self.peer_manager.fsm.event,
        #                                                Event(EventType.CandidateProbeTimeout))

    def send_local_broadcast(self, conditions):

        assert self.peer_manager.agent.ip_address, "The local IP is not set!"

        logger.info("Sending local broadcast.")
        net = IPNetwork("{}/{}".format(self.peer_manager.agent.ip_address, self.peer_manager.agent.prefix))

        # send own id or id request
        self.peer_manager.send_peer_req(
            AgentAddress(ip=net.broadcast, port=self.peer_manager.agent.address.port),
            conditions)

        # single option: exhausted
        return 1, True

    def send_to_peers(self, conditions):
        logger.info("Sending probe to current peers")

        contacted_peers = self.peer_manager.peers.candidate_quarantine

        peers = self.peer_manager.current_external_peers - contacted_peers.ip_set
        if len(peers):
            logger.detail("Has peers: %s (#=%s)", peers, len(peers))
            peers = random.sample(set(peers), min(len(peers), PEER_QUERY_SIMUL_NUMBER))

            for ip in peers:
                self.peer_manager.send_peer_req(
                    AgentAddress(IPAddress(ip), self.peer_manager.agent.address.port),
                    conditions)
                contacted_peers.add(ip)
        else:
            logger.detail("Has no peers: %s", peers)
            raise NoProbesException()

        return len(peers), False

    # def send_to_forced_peers(self, conditions):
    #     logger.info("Sending probe to forced peers (experiment run purposes)")
    #     try:
    #         forced_peers = IPSet(getattr(constraints, 'FORCE_PEERS', {}).get(str(self.peer_manager.agent.address.ip_address), {}))
    #     except Exception as e:
    #         logger.error("Bad forced peers format: %s", getattr(constraints, 'FORCE_PEERS', {}))
    #         raise e
    #     logger.debug("Forced peers: %s", forced_peers)
    #     forced_peers -= self.peer_manager.current_external_peers
    #     logger.debug("Not added forced_peers: %s", forced_peers)
    #     self.peer_manager.add_peer_candidates(forced_peers)
    #
    #     return len(forced_peers), len(forced_peers) == 0

    def send_to_dispatcher(self, conditions):
        logger.info("Sending to dispatcher.")

        try:
            dispatcher_address = self.peer_manager.agent.dispatcher_address
        except:
            raise NotImplementedError("No address to central server.")

        self.peer_manager.send_peer_req(dispatcher_address, conditions)

        # single option: exhausted
        return 1, True

    def process_peer_candidate_response_event(self, agent_id, candidates):
        if agent_id != self.peer_manager.agent.id:
            logger.error("Message with wrong agent id: '%s'", agent_id)
            return

        if len(candidates) > 0:
            self.peer_manager.add_peer_candidates(candidates)
            if self.peer_manager.peers.has_candidates():
                logger.info("Peer Manager has candidates: %s", self.peer_manager.peers.candidate_pool)
                self.recurrent_probing.stop()
                self.peer_manager.agent.event_log.add_event("provider-%s" % self.peer_manager.agent.ip_address,
                                                            Category.Operation,
                                                            "CandidateSearch", len(candidates),
                                                            self.start_candidate_search)
                return PeerSearchState

        else:
            logger.debug("No candidates received: ignoring!")

    def process_stop_peer_search_event(self):
        if self.recurrent_probing:
            self.recurrent_probing.stop()
        return SteadyState


class PeerSearchState(ActiveState):
    """
    Agent repeatedly sends requests for new peers.
     -> SteadyState: when database constraints are met
     -> CandidateSearchState: when runs out of peer candidates
    """
    def __init__(self, peer_manager):
        """
        @type peer_manager: PeerManager
        """
        super().__init__(peer_manager)

        self.timeout_eid = None
        self.start_peer_search = None

    def enter(self, event, prev_state):
        self.start_peer_search = time.time()
        self.send_peer_request()

    def get_forced_peers_list(self):
        try:
            forced_peers = IPSet(
                getattr(constraints, 'FORCE_PEERS', {}).get(str(self.peer_manager.agent.address.ip_address), {}))
        except Exception as e:
            logger.error("Bad forced peers format: %s", getattr(constraints, 'FORCE_PEERS', {}))
            raise e

        forced_peers -= self.peer_manager.current_external_peers
        logger.debug("Currently not added forced peers: %s", forced_peers)
        return forced_peers

    def send_peer_request(self):
        if self.peer_manager.probing_mode == ProbingModes.RANDOM_PROBING:
            raise NotImplementedError()
        else:
            self.timeout_eid = None
            candidate_ip = None
            forced_peers = self.get_forced_peers_list()
            if forced_peers:
                candidate_ip = next(iter(forced_peers))
            elif self.peer_manager.peers.has_candidates():
                candidate_ip = self.peer_manager.peers.pop_random_peer_candidate()

            if candidate_ip:
                event = Event(EventType.PeerRequestEvent,
                              self.peer_manager.agent.id,
                              self.peer_manager.agent.address.ip_address,
                              self.peer_manager.conditions)
                self.peer_manager.agent.send_datagram(
                    AgentAddress(candidate_ip, PROVIDER_AG_PORT),
                    event
                )

                timeout = TIMEOUT_PROBES
                self.timeout_eid = self.peer_manager.agent.event_manager.schedule(timeout, self.peer_manager.fsm.event,
                                                                                  Event(EventType.PeerRequestTimeout, candidate_ip))
            else:
                return CandidateSearchState

    def process_peer_request_timeout(self, candidate_ip):
        # TODO: add limited quarantine for the first few seconds of operation
        # self.peer_manager.peers.quarantine_candidate(candidate_ip)
        return self.send_peer_request()

    def process_stop_peer_search_event(self):
        self.peer_manager.agent.event_manager.cancel(self.timeout_eid)

        self.peer_manager.agent.event_log.add_event("provider-%s" % self.peer_manager.agent.ip_address,
                                                    Category.Operation,
                                                    "PeerSearch",
                                                    None,
                                                    self.start_peer_search)

        return SteadyState


class ProbingModes(Enum):
    KB_PROBING = 1
    RANDOM_PROBING = 2


class PeerManager(LamaModule):

    def __init__(self, agent):
        super().__init__(agent)

        self.fsm = StateMachine(SteadyState, identifier=self.__class__.__name__, peer_manager=self)

        self.peers = Peers()
        self.conditions = None
        self.probing_mode = ProbingModes.KB_PROBING

        self.agent.add_handler(self.fsm.event)

        self._subscribed_peers = set()
        self._need_to_update = True
        self._last_update = datetime.now()
        self._min_update_delay = timedelta(milliseconds=750)
        self._recurring_update = None

    @property
    def current_external_peers(self):
        # excluding self for peering effects
        return IPSet(self.agent.resource_manager.get_current_peers(True))

    def start(self):
        self._recurring_update = RecurringFunction(self.agent.event_manager.schedule, self.run_update_subscriber_peers,
                                                   tuple(), Repeat(number=float("inf"), wait=1), Repeat(number=float("inf"), wait=1))
        self._recurring_update.run()

        self.fsm.start()

    def start_searching(self, conditions):
        self.fsm.event(
            Event(EventType.StartPeerSearchEvent, conditions)
        )

    def stop_searching(self):
        self.fsm.event(
            Event(EventType.StopPeerSearchEvent)
        )

    def add_subscriber(self, ip_address):
        self._subscribed_peers.add(str(ip_address))

    def remove_subscriber(self, ip_address):
        self._subscribed_peers.remove(str(ip_address))

    def update_subscriber_peers(self):
        logger.temp("Need to send update to subscriber peers: %s", self._subscribed_peers)
        self._need_to_update = True

    def run_update_subscriber_peers(self):
        # logger.temp("need=%s, now=%s, last=%s, delay=%s", self._need_to_update, datetime.now(), self._last_update, self._min_update_delay)
        if self._need_to_update and (datetime.now() - self._last_update > self._min_update_delay):
            self._need_to_update = False
            self._last_update = datetime.now()
            # logger.temp("Sending update to subscriber peers: %s", self._subscribed_peers)
            # schedule update
            for ip_address in self._subscribed_peers:
                event = Event(EventType.ProviderResourceUpdate, self.agent.id,
                              self.agent.resource_manager.get_local_free_resources())
                self.agent.send_datagram(AgentAddress(ip_address, PROVIDER_AG_PORT), event)
            # pass
        return False

    def subscribe_peer(self, agent_ip_address):
        event = Event(EventType.ProviderResourceSubscribeUpdates, self.agent.id)
        self.agent.send_event_reliably(AgentAddress(agent_ip_address, PROVIDER_AG_PORT), event)

    def set_conditions(self, conditions):
        self.conditions = conditions

    def send_peer_req(self, address, conditions):
        self.agent.send_datagram(address, Event(EventType.PeerCandidateProbeEvent, self.agent.id, conditions))

    def add_peer_candidates(self, candidates):
        """
        Add received candidates to list
        :rtype : returns the number of candidates added
        :param candidates: IPSet to be used as peer candidates
        """
        self.peers.add_candidates(candidates - self.agent.resource_manager.get_current_peers())

    def get_candidates(self, n=float("inf")):
        return self.peers.get_candidates(n)

    def stop(self):
        self._recurring_update.stop()


class Peers(object):

    def __init__(self):
        self.candidate_pool = IPSet()
        self.candidate_quarantine = Quarantine()
        # self.peer_pool = IPSet()
        # self.peer_quarantine = Quarantine()

    # def __str__(self):
    #     return "::Peer IPs (#P/C = %s/%s, #PQ/CQ = %s/%s): %s" % (
    #         len(self.peer_pool),
    #         len(self.candidate_pool),
    #         len(self.peer_quarantine),
    #         len(self.candidate_quarantine),
    #         self.peer_pool
    #     )

    def __str__(self):
        return "Candidate IPs (#C/#Q) = %s/%s" % (len(self.candidate_pool), len(self.candidate_quarantine))

    def add_candidates(self, ip_set):
        """
        @type ip_set:IPSet
        """
        # filter out self.peer and self.quarantine from peer_set
        # fip_set = ip_set - self.peer_pool - self.candidate_quarantine._ip_set
        fip_set = ip_set - self.candidate_quarantine.ip_set

        # add peer_set to candidates
        logger.debug("Adding %s to %s", fip_set, self.candidate_pool)
        self.candidate_pool |= fip_set
        logger.debug("Result %s", self.candidate_pool)

    def get_candidates(self, n=float("inf")):
        # TODO: should i include quarantined candidates?
        candidates = IPSet()
        if len(self.candidate_pool) >= n:
            for ip in random.sample(set(self.candidate_pool), n):
                candidates.add(ip)
        else:
            candidates |= self.candidate_pool

        return candidates

    def pop_random_peer_candidate(self):
        if not len(self.candidate_pool):
            raise NoCandidatesException()

        ip = IPAddress(random.sample(set(self.candidate_pool), 1)[0])
        self.candidate_pool.remove(ip)
        return ip

    # def add_peer(self, ip):
    #     self.peer_pool.add(ip)

    def has_candidates(self):
        return len(self.candidate_pool) > 0

    # def quarantine_peer(self, ip):
    #     self.peer_quarantine.add(ip)

    def quarantine_candidate(self, ip):
        self.candidate_quarantine.add(ip)


class Quarantine(object):

    def __init__(self, timeout=60):
        self._ip_set = IPSet()
        self._timestamp = {}
        self._timeout = timeout

    def add(self, ip):
        self._ip_set.add(ip)
        self._timestamp[ip] = datetime.now()
        assert len(self._ip_set) == len(self._timestamp)

    def remove(self, ip):
        self._ip_set.remove(ip)
        if ip in self._timestamp:
            del self._timestamp[ip]
        assert len(self._ip_set) == len(self._timestamp)

    def __len__(self):
        return len(self._timestamp)

    def _clear(self):
        limit_time = datetime.now() - timedelta(seconds=self._timeout)
        expired = [ip for ip, t in self._timestamp.items() if t < limit_time]
        for ip in expired:
            del self._timestamp[ip]
            self._ip_set.remove(ip)

    @property
    def size(self):
        return self.__len__()

    def __str__(self):
        return "Quarantined IPs::%s" % self._ip_set

    @property
    def ip_set(self):
        self._clear()
        return self._ip_set