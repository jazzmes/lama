from lama.utils.logging_lama import logger

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class DependencyError(Exception):

    def __init__(self, module, dep_module):
        self.module = module
        self.dep_module = dep_module

    def __str__(self):
        return "DependencyError:: Module '%s' depends on '%s'" % (self.module, self.dep_module)


class LamaModule(object):
    """
    Base class to define agent's modules.
    A module should have the capability to:
     - receive and process events.
     - access to agent and it's other modules
    """
    def __init__(self, agent):
        """
        @type agent: ProviderAgent
        """
        super(LamaModule, self).__init__()
        self.agent = agent

    def set_callback(self, key, cb):
#         self.logger.debug("Setting callback")
        if not key.startswith("on_"):
            logger.warning("Function '%s' is not a callback.", key)
            return

        if key in self.__dict__:
            self.__dict__[key] = cb
        else:
            logger.error("No callback: %s" % key)

    def add_handler(self, handler):
        try:
            self.agent.add_handler(handler)
        except AttributeError:
            logger.error("Unable to register module '%s' event handler", self.__class__.__name__)