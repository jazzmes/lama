from lama.agent.base import AgentAddress
from lama.agent.events import Event, EventType
from lama.utils.lama_collections import PersistentSyncDict
from lama.agent.provider.module import LamaModule
from lama.agent.provider.resource_manager import ResourceSearchAlgorithms
from lama.agent.state_machine import State, StateMachine
from lama.utils.enum import Enum
from lama.utils.logging_lama import logger

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class InitializationState(State):

    def __init__(self, alloc_manager):
        super().__init__()
        self.alloc_manager = alloc_manager

    def enter(self, event, prev_state):
        return ActiveState


class ActiveState(State):

    def __init__(self, alloc_manager):
        """
        @type alloc_manager: AllocationManager
        """
        super().__init__()
        self.alloc_manager = alloc_manager

    def enter(self, event, prev_state):
        pass

    def process_app_resource_request(self, app_name, req_spec, partial, **context):
        logger.temp("Processing allocation request for app '%s' with spec type %s", app_name, req_spec)

        if app_name not in self.alloc_manager.allocation_processes:
            aprocess = AllocationProcess(self.alloc_manager.agent, app_name)
            self.alloc_manager.allocation_processes[app_name] = aprocess
        else:
            aprocess = self.alloc_manager.allocation_processes[app_name]

        aprocess.refresh(req_spec, partial)

    def process_provider_resource_reservation_confirmation(self, agent_id, app_name, vm_name, **context):

        if app_name in self.alloc_manager.allocation_processes:
            aprocess = self.alloc_manager.allocation_processes[app_name]
            aprocess.confirm_allocation(vm_name)
        else:
            raise NotImplementedError()

    def process_provider_resource_reservation_failure(self, agent_id, app_name, vm_name, resources, **context):
        logger.temp("Processing provider reservation failurefor app '%s' - instance '%s' - resources: %s",
                    app_name, vm_name, resources)
        if app_name in self.alloc_manager.allocation_processes:
            aprocess = self.alloc_manager.allocation_processes[app_name]
            ip_address = context.get("address")
            self.alloc_manager.agent.resource_manager.delete_allocation(ip_address, agent_id, app_name, vm_name)
            self.alloc_manager.agent.resource_manager.update_provider_resources(agent_id, ip_address, resources)
            aprocess.cancel_allocation(vm_name)
        else:
            raise NotImplementedError()

    def process_provider_terminate_instance_event(self, app_name, instance_name, ip, mac, **context):
        logger.temp("Processing termination request for app '%s' - instance '%s'", app_name, instance_name)
        process = self.alloc_manager.allocation_processes.get(app_name)
        if not process:
            msg = "No allocation process found for app '%s'" % app_name
            self.alloc_manager.agent.app_manager.send_event_to_app(
                app_name,
                Event(
                    EventType.ProviderTerminateInstanceResponseEvent,
                    app_name,
                    instance_name,
                    False,
                    msg
                )
            )
            logger.warn(msg)
            return

        process.terminate(instance_name)

    def process_provider_change_instance_state_event(self, app_name, service_name, instance_name, old_state, new_state, **context):
        # adjust the allocations
        # run the start instance in the instance_manager
        process = self.alloc_manager.allocation_processes.get(app_name)
        if not process:
            msg = "No allocation process found for app '%s'" % app_name
            self.alloc_manager.agent.app_manager.send_event_to_app(
                app_name,
                Event(
                    EventType.ProviderChangeInstanceStateResponseEvent,
                    app_name,
                    service_name,
                    instance_name,
                    False,
                    msg
                )
            )
            logger.warn(msg)
            return

        process.change_instance_state(service_name, instance_name, old_state, new_state)

    def process_provider_resource_deletion_confirmation(self, agent_id, app_name, vm_name, result, **context):

        if app_name in self.alloc_manager.allocation_processes:
            aprocess = self.alloc_manager.allocation_processes[app_name]
            aprocess.confirm_deletion(vm_name, result)
        else:
            raise NotImplementedError()

    def process_provider_resource_change_confirmation(self, agent_id, app_name, service_name, vm_name, old_state, new_state, result, **context):

        if app_name in self.alloc_manager.allocation_processes:
            aprocess = self.alloc_manager.allocation_processes[app_name]
            aprocess.confirm_change_state(service_name, vm_name, old_state, new_state, result)
        else:
            raise NotImplementedError()

    def process_provider_deleted_latent_notification(self, app_name, vm_name, **context):
        logger.debug("Args: %s", [app_name, vm_name, context])
        if app_name in self.alloc_manager.allocation_processes:
            aprocess = self.alloc_manager.allocation_processes[app_name]
            aprocess.remote_deletion(vm_name)


class UnknownVMException(Exception):
    def __init__(self, vm_name):
        self.vm_name = vm_name

    def __str__(self):
        return "VM '%s' does not exist!" % self.vm_name


class AllocationStatus(Enum):
    Idle = 1
    Selected = 2
    Reserved = 3
    Allocated = 4
    Deleting = 5
    Active = 6
    ChangingState = 7


class VMAllocation(object):
    def __init__(self, name, status, provider, resources, state, service_name):
        self.name = name
        self.status = status
        self.provider = provider
        self.resources = resources
        self.state = state
        self.service_name = service_name

    def __str__(self):
        return "VM Allocation @ %s" % self.provider


class Allocation(object):

    def __init__(self, name):
        self.app_name = name

        self.vms = {}
        self.dependencies = {}
        self.constraints = {}

# self.__initialize_status()

    # def __initialize_status(self):
    #     for vm in self.app_spec.get_vms():
    #         self.vms[vm.name] = VMAllocation(status=AllocationStatus.Idle, provider=None)

    def add_dependencies(self, dependencies):
        self.dependencies.update(dependencies)

    def add_constraints(self, constraints):
        self.constraints.update(constraints)

    def add_vm(self, vm):
        """
        @type vm:lama.agent.app_arch.VirtualNode
        """
        if vm.name not in self.vms:
            self.vms[vm.name] = VMAllocation(
                name=vm.name,
                status=AllocationStatus.Idle,
                provider=None,
                resources=vm.resources,
                state=vm.state,
                service_name=vm.service_name
            )
        else:
            raise NotImplementedError("Duplicate VM request")

    def set_vm_selected(self, vm_name, provider):
        if not vm_name in self.vms:
            raise UnknownVMException(vm_name)

        vm = self.vms.get(vm_name)
        vm.status = AllocationStatus.Selected
        vm.provider = provider

    def set_vm_status(self, vm_name, status):
        self.vms[vm_name].status = status

    def set_vm_provider(self, vm_name, provider):
        self.vms[vm_name].provider = provider

    def get_vm_status(self, vm_name):
        vm_alloc = self.vms.get(vm_name)
        if vm_alloc:
            return vm_alloc.status
        return None

    def get_vm_provider(self, vm_name):
        vm_alloc = self.vms.get(vm_name)
        if vm_alloc:
            return vm_alloc.provider
        return None

    def get_vm_resources(self, vm_name):
        vm_alloc = self.vms.get(vm_name)
        if vm_alloc:
            return vm_alloc.resources
        return None

    def get_vm(self, vm_name):
        return self.vms.get(vm_name)

    def del_vm(self, vm_name):
        del self.vms[vm_name]

    def get_idle_vms(self):
        logger.temp("status: %s", [v.status for v in self.vms.values()])
        return [v for v in self.vms.values() if v.status == AllocationStatus.Idle]

    @property
    def number_of_idle_vms(self):
        return sum(1 for v in self.vms.values() if v.status == AllocationStatus.Idle)

    @property
    def number_of_vms(self):
        return len(self.vms)

    # @property
    # def number_of_unconfirmed_vms(self):
    #     return sum(1 for v in self.vms.values() if v.status == AllocationStatus.Allocated)

    def __str__(self):
        s = ", ".join(
            ["%s:%s:%s" % (k, v.status.name, v.provider.ip_address if v.provider else None) for k, v in self.vms.items()]
        )
        return "Allocation [%s :: {%s}]" % (self.app_name, s)

    def __iter__(self):
        return self.vms.__iter__()


class AllocationProcess(object):
    """
    This class manages the process of requesting and confirming allocations using a FSM.
    """
    def __init__(self, agent, app_name,
                 cb_app_allocated=None, cb_app_failure=None,
                 cb_vm_allocated=None,
                 algorithm=ResourceSearchAlgorithms.greedy_reservation):
        """
        @type agent:lama.agent.provider.ProviderAgent
        """
        self.agent = agent
        self.app_name = app_name

        if app_name not in self.agent.alloc_manager.allocation_info:
            # logger.temp("Add allocation for app '%s'", app_name)
            self.agent.alloc_manager.allocation_info.add(app_name, Allocation(app_name))

        # self.fsm = {}
        self.cb_app_allocated = cb_app_allocated
        self.cb_app_failure = cb_app_failure
        self.cb_vm_allocated = cb_vm_allocated
        self.allocation_algorithm = algorithm

        # self._resource_spec_not_allocated = AppVirtualSpec(app_name)

    def needs_allocation(self):
        alloc = self.agent.alloc_manager.allocation_info.get(self.app_name)
        logger.temp("needs alloc - app=%s, number_of_vms=%s, idle_vms=%s",
                    self.app_name,
                    alloc.number_of_vms,
                    alloc.number_of_idle_vms)

        return alloc.number_of_idle_vms > 0

    def refresh(self, resource_spec=None, partial=True):
        """
        @type resource_spec: lama.agent.app_arch.AppVirtualSpec
        """
        # TODO: for now all allocations are partial...
        alloc_info = self.agent.alloc_manager.allocation_info.get(self.app_name)
        if resource_spec:
            for vm in resource_spec.get_vms():
                alloc_info.add_vm(vm)
            alloc_info.add_dependencies(resource_spec.dependencies)
            alloc_info.add_constraints(resource_spec.constraints)

        # no delete operations
        logger.detail("Starting AllocationProcess:refresh")
        # logger.temp(" === Resource DB (Before Reserve) == \n%s\n\n", self.agent.resource_manager.resource_db)
        # logger.temp(" Before Reserving: free-vcpus: %s", self.agent.resource_manager.resource_db.get_num_of_free_vcpus())
        # logger.temp(" Constraints: %s", resource_spec.constraints)

        logger.info("Req_spec: %s", alloc_info)
        logger.info("Not allocated: %s constraints: %s", alloc_info.number_of_idle_vms, alloc_info.constraints)
        logger.temp("DB before: %s", self.agent.resource_manager.resource_db)
        not_allocated_vms = alloc_info.get_idle_vms()
        if alloc_info.number_of_idle_vms > 0:
            # logger.temp("Not allocated: %s", not_allocated_vms)
            local_allocations = self.agent.resource_manager.reserve(
                self.app_name,
                not_allocated_vms,
                constraints=alloc_info.constraints,
                dependencies=alloc_info.dependencies,
                algorithm=self.allocation_algorithm,
                partial=partial
            )

            logger.temp("Chosen providers: %s", local_allocations)
            if local_allocations:

                # for vm in resource_spec.get_vms():
                for vm in not_allocated_vms:
                    # if vm.name in self.agent.alloc_manager.allocation_info.get(self.app_name):
                    #     # this is called every time that the App agent
                    #     # sends a AppResourceRequest > sent by adjust_allocations...
                    #     logger.temp("VM '%s' already in '%s'",
                    #                 vm.name, self.agent.alloc_manager.allocation_info)
                    # else:
                    if vm.name in local_allocations:
                        target_provider = local_allocations[vm.name]["provider"]
                        logger.temp("setting allocation for vm: %s (prov: %s, local: %s)", vm.name, target_provider.agent_id, self.agent.id)
                        alloc_info.set_vm_selected(vm.name, target_provider)

                        if target_provider.agent_id == self.agent.id:
                            logger.temp("Local allocation")
                            # own agent: is confirmed...
                            self.process_successful_allocation(self.app_name, vm.name)
                        else:
                            event = Event(EventType.ProviderResourceReservation,
                                          self.agent.id,
                                          self.app_name,
                                          vm.service_name,
                                          vm.name,
                                          vm.resources,
                                          vm.state,
                                          alloc_info.dependencies.get(vm.name))
                            logger.temp("Sending event: %s", event)
                            self.agent.send_event_with_response(
                                AgentAddress(target_provider.ip_address, target_provider.port), event)

                    # TODO: cannot sync all of it at same time...
                    self.agent.alloc_manager.allocation_info.sync()
            else:
                logger.warning("No allocations made!")
                # raise NotImplementedError()
        logger.temp("DB AFTER: %s", self.agent.resource_manager.resource_db)

        return True

    def confirm_allocation(self, vm_name):

        if vm_name in self.agent.alloc_manager.allocation_info.get(self.app_name):
            self.process_successful_allocation(self.app_name, vm_name)

    def cancel_allocation(self, vm_name):
        if vm_name in self.agent.alloc_manager.allocation_info.get(self.app_name):
            self.process_failed_allocation(self.app_name, vm_name)

    def process_successful_allocation(self, app_name, vm_name):
        self.agent.alloc_manager.allocation_info.get(self.app_name).set_vm_status(vm_name, AllocationStatus.Allocated)
        self.agent.alloc_manager.allocation_info.sync()
        logger.detail("Sending VM allocation confirmation to app...")

        vm = self.agent.alloc_manager.allocation_info.get(self.app_name).get_vm(vm_name)
        logger.info("Successful Allocation: %s:%s @ %s", app_name, vm_name, vm.provider.ip_address)
        # send event to app agent
        self.agent.app_manager.send_event_to_app(
            app_name,
            Event(
                EventType.VMAllocatedEvent,
                vm_name,
                vm.provider.ip_address,
                vm.resources
            )
        )

        # raise event locally (provider)
        self.agent.event_handler(
            Event(
                EventType.VMAllocatedEvent,
                vm_name,
                vm.provider.ip_address,
                vm.resources
            )
        )
        # # noinspection PyBroadException
        # try:
        #     self.cb_vm_allocated(
        #         app_name, vm_name,
        #         self.allocation_info.vms[vm_name].resources,
        #         self.allocation_info.vms[vm_name].provider
        #     )
        # except:
        #     if self.cb_vm_allocated:
        #         logger.debug("Error running callback 'on_vm_allocation_complete': %s", traceback.format_exc())

        # logger.detail("self.allocation_info.number_of_unallocated_vms = %s", self.agent.alloc_manager.allocation_info.get(self.app_name).number_of_unallocated_vms)
        # if not self.agent.alloc_manager.allocation_info.get(self.app_name).number_of_unallocated_vms:
        #     logger.detail("Sending APP allocation confirmation to app...")
        #     self.agent.app_manager.send_event_to_app(
        #         app_name,
        #         Event(EventType.AppAllocatedEvent)
        #     )

            # logger.info("Successful App allocation: app: %s,", app_name)
            # # noinspection PyBroadException
            # try:
            #     self.cb_app_allocated(
            #         app_name
            #     )
            # except:
            #     if self.cb_app_allocated:
            #         logger.debug("Error running callback 'on_app_allocation_complete': %s", traceback.format_exc())

    def process_failed_allocation(self, app_name, vm_name):
        logger.info("Failed allocation: app: %s, vm: %s", app_name, vm_name)
        self.agent.alloc_manager.allocation_info.get(self.app_name).set_vm_status(vm_name, AllocationStatus.Idle)
        self.agent.alloc_manager.allocation_info.sync()
        self.refresh()

    def terminate(self, instance_name):
        logger.temp("Terminating/Deleting instance '%s'", instance_name)
        app_ainfo = self.agent.alloc_manager.allocation_info.get(self.app_name)
        if not app_ainfo:
            logger.warn("No allocation info for app '%s'", self.app_name)
            return False

        ainfo = app_ainfo.get_vm(instance_name)
        if not ainfo:
            logger.warn("No allocation info for instance '%s:%s'", self.app_name, instance_name)
            return False

        app_ainfo.set_vm_status(instance_name, AllocationStatus.Deleting)

        target_provider = app_ainfo.get_vm_provider(instance_name)
        if not target_provider:
            logger.warn("No provider info for instance '%s:%s'", self.app_name, instance_name)
            return False

        event = Event(EventType.ProviderResourceDeletion,
                      self.agent.id,
                      self.app_name,
                      instance_name)

        logger.detail("Sending event: %s", event)
        self.agent.send_event_with_response(
            AgentAddress(target_provider.ip_address, target_provider.port), event)

    def confirm_deletion(self, vm_name, result):

        if vm_name in self.agent.alloc_manager.allocation_info.get(self.app_name):
            if result:
                self.process_successful_deletion(vm_name)
            else:
                self.process_failed_deletion(vm_name)
        else:
            raise NotImplementedError("VM '%s' not found! What to do? Is this a duplicate?", vm_name)

        logger.temp("RESOURCE DB: %s", self.agent.resource_manager.resource_db)

    def remote_deletion(self, instance_name):
        logger.temp("Processing remote deletion - instance '%s'", instance_name)
        ainfo = self.agent.alloc_manager.allocation_info.get(self.app_name)
        if ainfo:
            # delete from resource manager
            self.agent.resource_manager.free(
                self.app_name, {instance_name: {"provider": ainfo.get_vm_provider(instance_name)}}
            )

            # delete the allocation info
            ainfo.del_vm(instance_name)

            # notify app
            self.agent.app_manager.send_event_to_app(
                self.app_name,
                Event(
                    EventType.ProviderDeletedLatentNotification,
                    self.app_name,
                    instance_name,
                )
            )

        else:
            raise NotImplementedError("Allocation info for '%s' not found! What to do?", instance_name)

        logger.temp("RESOURCE DB: %s", self.agent.resource_manager.resource_db)

    def process_successful_deletion(self, instance_name):
        logger.temp("Processing successful deletion - instance '%s'", instance_name)
        ainfo = self.agent.alloc_manager.allocation_info.get(self.app_name)

        # delete from resource manager
        self.agent.resource_manager.free(
            self.app_name, {instance_name: {"provider": ainfo.get_vm_provider(instance_name)}}
        )

        # delete the allocation info
        ainfo.del_vm(instance_name)

        # send response
        self.agent.app_manager.send_event_to_app(
            self.app_name,
            Event(
                EventType.ProviderTerminateInstanceResponseEvent,
                self.app_name,
                instance_name,
                True,
                None
            )
        )

    def process_failed_deletion(self, vm_name):
        logger.warn("Processing failed deletion - instance '%s'", vm_name)
        raise NotImplementedError("What to do if deletion fails?")

    def change_instance_state(self, service_name, instance_name, old_state, new_state):
        logger.temp("Change instance '%s' state: %s -> %s", instance_name, old_state, new_state)
        app_ainfo = self.agent.alloc_manager.allocation_info.get(self.app_name)
        if not app_ainfo:
            logger.warn("No allocation info for app '%s'", self.app_name)
            return False

        ainfo = app_ainfo.get_vm(instance_name)
        if not ainfo:
            logger.warn("No allocation info for instance '%s:%s'", self.app_name, instance_name)
            return False

        app_ainfo.set_vm_status(instance_name, AllocationStatus.ChangingState)

        target_provider = app_ainfo.get_vm_provider(instance_name)
        if not target_provider:
            logger.warn("No provider info for instance '%s:%s'", self.app_name, instance_name)
            return False

        # change the allocations: active <-> latent
        # app_ainfo.change_vm_state(old_state, new_state)

        event = Event(EventType.ProviderResourceChange,
                      self.agent.id,
                      self.app_name,
                      service_name,
                      instance_name,
                      old_state,
                      new_state)

        logger.detail("Sending event: %s", event)
        self.agent.send_event_with_response(
            AgentAddress(target_provider.ip_address, target_provider.port), event)

    def confirm_change_state(self, service_name, instance_name, old_state, new_state, result):
        logger.temp("Processing successful deletion - instance '%s'", instance_name)
        logger.temp("Result: %s", result)
        try:
            app_ainfo = self.agent.alloc_manager.allocation_info.get(self.app_name)
            ainfo = app_ainfo.get_vm(instance_name)
            # delete from resource manager
            self.agent.resource_manager.change_state(
                self.app_name, service_name, instance_name, old_state, new_state,
                last_provider_ip=ainfo.provider.ip_address
            )
            r = True
        except:
            r = False

        logger.temp("RESOURCE DB: %s", self.agent.resource_manager.resource_db)
        self.agent.app_manager.send_event_to_app(
            self.app_name,
            Event(
                EventType.ProviderChangeInstanceStateResponseEvent,
                self.app_name,
                service_name,
                instance_name,
                result,
                msg=None
            )
        )


class AllocationManager(LamaModule):

    def __init__(self, agent):
        super().__init__(agent)

        self.fsm = StateMachine(InitializationState, identifier=self.__class__.__name__, alloc_manager=self)
        self.agent.add_handler(self.fsm.event)

        # self.active_allocations = PersistentSyncDict("local_app_allocations", Allocation)
        self.allocation_processes = {}
        self.allocation_info = PersistentSyncDict("allocations", Allocation)

        logger.info("Allocation Manager State:\n%s", self)

    def start(self):
        self.fsm.start()

    def __str__(self):
        str = "" if len(self.allocation_info) else "Provider does not have allocations!"
        for a in self.allocation_info.values():
            str += "%s\n" % a
        return str

    def refresh_allocations(self):
        logger.temp("Refreshing allocations...")
        for app_name, process in self.allocation_processes.items():
            if process.needs_allocation():
                process.refresh(partial=True)

    def get_remote_hosts_and_resource_types(self):

        allocs = {}
        # for each allocation,
        for app_name, alloc in self.allocation_info.items():
            # get host... merge resources
            for vm_name, vm_alloc in alloc.vms.items():
                # assume ProviderEntry
                # assert vm_alloc.provider == ProviderEntry, "Unexpected type: %s" % type(vm_alloc.provider)

                ip = vm_alloc.provider.ip_address
                if ip in allocs:
                    allocs[ip].update(vm_alloc.resources.types)
                else:
                    allocs[ip] = vm_alloc.resources.types
        return allocs


    # def get_allocation(self, app_name, instance_name):
    #     logger.temp("Getting allocation for %s:%s", app_name, instance_name)
    #     logger.temp("Allocations: %s", self.allocation_info)
    #     app_allocs = self.allocation_info.get(app_name)
    #     if app_allocs:
    #         return app_allocs.get_vm_resources(instance_name)
    #     return None
# Allocation fsm
