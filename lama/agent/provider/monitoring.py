import socket
from configparser import ConfigParser
import traceback
from config.settings import FORCE_RESOURCE_SCAN

from lama.agent.provider.module import LamaModule
from lama.agent.event_managers.emredis import LamaPubSub
from lama.scripts.system_parser import SystemParser
from lama.agent.specs.resource import ResourceSet, DiskSpec, CpuSpec, RamSpec, NetSpec
from lama.agent.state_machine import State, StateMachine
from lama.utils.logging_lama import logger
from lama.api.rrd_wrapper import RrdWrapper


__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class MonitoringBaseState(State):

    def __init__(self, monitoring):
        super().__init__()
        self.monitoring = monitoring

    def enter(self, event, prev_state):
        logger.debug("WARNING: subscribing is disabled for now - testing with independent script")
        return
        # for ip, resources in self.monitoring.agent.alloc_manager.get_remote_hosts_and_resource_types().items():
        #     logger.detail("Host with allocation: %s", ip)
        #     if ip not in self.monitoring.redis:
        #         subs = self.monitoring.subscribe_remote_metric(ip, resources)
        #         logger.detail("Subscriptions: %s", subs)

    def process_vm_allocated_event(self, vm_name, provider_ip, resources, **context):
        logger.warning("WARNING: subscribing is disabled for now - testing with independent script")
        return

        # logger.detail("MonDebug :: Processing allocation event at Monitoring module")
        # self.monitoring.subscribe_remote_metric(provider_ip, resources.types)

    def process_web_get_plugin_list(self, app_name, entity_type, name, **context):
        handler = context.get("response_handler")
        try:
            logger.detail("Processing plugin list request: %s", (app_name, entity_type, name, context))
            # entity type can be physical or virtual (VM)
            if entity_type == "physical":
                plugin_list = PmRrdEntity.get_plugins(self.monitoring.rrd, self.monitoring.agent.ip_address, app_name, name)
            elif entity_type == "virtual":
                plugin_list = VmRrdEntity.get_plugins(self.monitoring.rrd, self.monitoring.agent.ip_address, app_name, name)

            else:
                raise NotImplementedError("Unsupported entity_type")

            logger.detail("Plugins: %s", plugin_list)

        except:
            # logger.error("Trace: %s", traceback.format_exc())
            if handler:
                handler(False, msg=traceback.format_exc())
        else:
            # logger.warning("Returning data usinghandler: %s", handler)
            if handler:
                if plugin_list:
                    handler(True, **{"plugins": plugin_list})
                else:
                    handler(False, msg="Entity '%s' not found" % name)

    def process_web_get_plugin_data(self, app_name, entity_type, name, plugin, start, end, resolution, **context):
        handler = context.get("response_handler")
        try:
            logger.detail("Processing plugin data request: %s", (app_name, entity_type, name, plugin, start, end, resolution, context))

            if entity_type == "physical":
                data = PmRrdEntity.get_data(self.monitoring.rrd,
                                            self.monitoring.agent.ip_address,
                                            app_name, name, plugin, start, end, resolution)
            elif entity_type == "virtual":
                data = VmRrdEntity.get_data(self.monitoring.rrd,
                                            self.monitoring.agent.ip_address,
                                            app_name, name, plugin, start, end, resolution)
            else:
                raise NotImplementedError("Unsupported entity_type")
        except:
            # logger.error("Trace: %s", traceback.format_exc())
            if handler:
                handler(False, msg=traceback.format_exc())
        else:
            # logger.warning("Returning data usinghandler: %s", handler)
            if handler:
                if data:
                    handler(True, **{"data": data})
                else:
                    handler(False, msg="Entity/Plugin '%s/%s' not found." % (name, plugin))


class RrdEntity(object):

    @staticmethod
    def get_plugins(rrd, ip_address, app_name, entity):
        raise NotImplementedError()

    @staticmethod
    def get_data(rrd, ip_address, app_name, entity, plugin, start, end, resolution):
        raise NotImplementedError()


class VmRrdEntity(RrdEntity):

    @staticmethod
    def get_metric(name):
        try:
            return name.index("-")
        except:
            return None

    @staticmethod
    def get_plugins(rrd, ip_address, app_name, entity):
        # return rrd.get_plugins(host="%s_%s" % (app_name, entity))
        metrics = rrd.get_metrics(host="%s_%s" % (app_name, entity))
        logger.detail("Metrics: %s", metrics)
        subplugins = list(set(m[:VmRrdEntity.get_metric(m)] for m in metrics))

        logger.detail("Subplugins: %s", subplugins)
        return subplugins

    @staticmethod
    def get_data(rrd, ip_address, app_name, entity, plugin, start, end, resolution):
        entity = "%s_%s" % (app_name, entity)
        logger.detail("Plugin: %s, Metrics: %s", plugin, rrd.get_metrics(host=entity))
        metrics = [m for m in rrd.get_metrics(host=entity) if m.startswith(plugin + "-") or m == plugin]
        plugin = rrd.get_plugins(entity)[0]
        logger.detail("Asking data for: %s --> %s --> %s", entity, plugin, metrics)
        return rrd.get_data(entity, plugin, metrics=metrics, start=start, end=end, resolution=resolution)


class PmRrdEntity(RrdEntity):

    @staticmethod
    def get_plugins(rrd, ip_address, app_name, entity):
        # try ip:
        plugin_list = None
        if str(ip_address) == str(entity):
            plugin_list = rrd.get_plugins(host=entity)
            if not plugin_list:
                # try hostname:
                try:
                    hostname = socket.gethostname()
                    plugin_list = rrd.get_plugins(host=hostname)
                    if not plugin_list:
                        plugin_list = rrd.get_plugins(host="localhost")
                except socket.herror as e:
                    logger.warn("Failed to get hostname")

        return plugin_list

    @staticmethod
    def get_data(rrd, ip_address, app_name, entity, plugin, start, end, resolution):
        data = rrd.get_data(entity, plugin, start=start, end=end, resolution=resolution)
        logger.detail("Addresses: ag=%s, name=%s", ip_address, entity)
        if not data and str(ip_address) == str(entity):
            try:
                hostname = socket.gethostname()
                logger.detail("Trying with hostname: %s", hostname)
                data = rrd.get_data(hostname, plugin, start=start, end=end, resolution=resolution)
            except socket.herror as e:
                logger.warn("Failed to get hostname")

        return data


class Monitoring(LamaModule):

    def __init__(self, agent):
        super().__init__(agent)

        try:
            force = True if FORCE_RESOURCE_SCAN else False
        except:
            force = False

        self.local_resources = System.scan_resources(self.agent.config_file, force=force)
        self.local_redis = LamaPubSub(name=str(self.agent.ip_address))
        self.redis = {
            self.agent.ip_address: self.local_redis
        }

        self.fsm = StateMachine(MonitoringBaseState, identifier=self.__class__.__name__, monitoring=self)
        self.rrd = RrdWrapper()
        self.agent.add_handler(self.fsm.event)

    def start(self):
        self.fsm.start()

    def subscribe_remote_metric(self, ip, resource_set):
        logger.detail("MonDebug :: subscribe: %s", [ip, resource_set])
        if ip not in self.redis:
            self.redis[ip] = LamaPubSub(name=str(ip), host=ip)

        for rtype in resource_set:
            # as we are setting the channels, we only need to apply to one resource of each type
            self.redis[ip].subscribe_by_resource(rtype, callback=self.handle_remote_metric)

        logger.detail("MonDebug :: [%s] Subscribed? %s", ip, self.redis[ip].subscribed)
        if self.redis[ip].subscribed and not self.redis[ip].running:
            # call in thread
            self.agent.event_manager.call_in_thread(self.redis[ip].start)

        logger.detail("MonDebug :: Subscriptions: %s", self.redis)

    def handle_remote_metric(self, host, message):
        logger.temp("MonDebug :: Received remote metric from %s: %s", host, message)

        # reformat remote channel to local channel: [ip]:[metric]/[attribute]
        self.local_redis.publish("%s:%s" % (host, message["channel"]), message["data"])

    def close(self):
        for r in self.redis.values():
            r.stop()


class CollectdConfig(object):

    def __init__(self, config_file="/etc/collectd.conf"):
        self.config_file = config_file
        raise NotImplementedError()

    def configure_plugin(self, plugin, **options):
        # read config
        # erase None ones (explicitly deleted)
        # add or modified the ones specified
        # maintain the rest
        raise NotImplementedError()

    # specific metric (deal with LAMA-Collectd adaptation)
    def configure_disk(self):
        raise NotImplementedError()

    def configure_memory(self):
        raise NotImplementedError()

    def configure_cpu(self):
        raise NotImplementedError()

    def configure_network(self):
        raise NotImplementedError()


class System(object):

    @staticmethod
    def scan_resources(config_file, force=False):
        """
        Scans local resources information, saves and writes it to config file.
        """
        config = ConfigParser()
        config.read(config_file)

        # read resources from config file
        if force or not config.has_section('RESOURCES'):
            if config.has_section('RESOURCES'):
                config.remove_section('RESOURCES')
            config.add_section('RESOURCES')
            sp = SystemParser()
            logger.info("Parsing disk info...")
            disk_nfo = sp.parse_disk_info(test_speed=True)
            logger.info("Parsing CPU info...")
            cpu_nfo = sp.parse_cpu_mem_info()
            logger.info("Parsing memory info...")
            mem_nfo = sp.parse_mem_info()
            logger.info("Parsing network info...")
            net_nfo = sp.parse_network_info()

            rs = ResourceSet()
            # logger.debug("Disk Info: %s" % disk_nfo)
            for disk in disk_nfo:
                # logger.debug("Disk: %s" % disk)
                rs.add_resource_spec(
                    DiskSpec(
                        disk_size=int(disk['Available'] * 0.8),
                        rd_speed=int(disk['mean_buffer_read']),
                        wr_speed=int(disk['mean_write']),
                        device=disk['LogDev'],
                        name=disk['Name']
                    )
                )
            rs.add_resource_spec(
                CpuSpec(
                    n_processors=cpu_nfo['CPUs'],
                    processor_speed=cpu_nfo['Frequency']
                )
            )
            rs.add_resource_spec(
                RamSpec(
                    size=mem_nfo['total']
                )
            )
            for net in net_nfo:
                rs.add_resource_spec(
                    NetSpec(
                        tx_bw=net['speed'],
                        rx_bw=net['speed']
                    )
                )

            for k, v in rs.to_config().items():
                config.set('RESOURCES', k, v)

            logger.debug("Config: %s", config)
            logger.info("Writing configuration to file: %s", config_file)
            with open(config_file, 'w') as configfile:
                config.write(configfile)

        return ResourceSet(
            dict((key, value) for key, value in config.items('RESOURCES'))
        )

