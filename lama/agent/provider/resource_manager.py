import copy
import traceback

from functools import partial
import random
from datetime import datetime

from netaddr import IPAddress, IPSet

import config.exp_constraints as exp_constraints
from config import settings
from config.settings import PROVIDER_AG_PORT
from lama.agent.app.app_arch import VirtualNode, InstanceState
from lama.agent.define import FailureDomain
from lama.utils.lama_collections import PersistentSyncDict
from lama.agent.provider.module import LamaModule
from .peer_manager import PeerManager
from lama.agent.specs.resource import ResourceSet, CpuSpec
from lama.agent.state_machine import State, StateMachine
from lama.utils.enum import Enum
from lama.utils.logging_lama import logger

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class InitializationState(State):
    def __init__(self, resource_manager):
        """
        @type resource_manager: ResourceManager
        """
        self.resource_manager = resource_manager

    def enter(self, event, prev_state):
        # add first basic constraint (list size)
        self.resource_manager.add_constraint(
            'number_of_providers',
            (
                ConstraintTypes.LAMBDA,
                self.resource_manager.resource_db.create_condition_number_of_providers(
                    settings.DEFAULT_PROVIDER_NUM_SHORTLIST
                )
            )
        )
        self.resource_manager.add_constraint(
            'number_of_free_vcpus',
            (
                ConstraintTypes.LAMBDA,
                self.resource_manager.resource_db.create_condition_number_of_free_vcpus(
                    settings.DEFAULT_PROVIDER_VCPUS_NUM_SHORTLIST
                )
            )
        )
        self.resource_manager.peer_manager.start()
        return ActiveState


class ActiveState(State):
    def __init__(self, resource_manager):
        """
        @type resource_manager: ResourceManager
        """
        self.resource_manager = resource_manager

    def enter(self, event, prev_state):
        provider_conditions = self.resource_manager.get_provider_conditions()
        self.resource_manager.peer_manager.start_searching(provider_conditions)

    def process_peer_response_event(self, agent_id: str,
                                    ip_address: IPAddress,
                                    req_agent_id: str,
                                    resources: ResourceSet,
                                    **context):
        if req_agent_id != self.resource_manager.agent.id:
            logger.warning("Receved Peer response for request from another agent (mine=%s, req=%s",
                           self.resource_manager.agent.id, req_agent_id)

        self.resource_manager.resource_db.add_provider(
            agent_id, ip_address, PROVIDER_AG_PORT, resources
        )

        logger.debug("Resource DB - %s", self.resource_manager.resource_db)
        self.resource_manager.peer_manager.peers.candidate_quarantine.add(ip_address)

        self.resource_manager.peer_manager.subscribe_peer(ip_address)

        if self.resource_manager.check_constraints():
            return self.resource_manager.peer_manager.stop_searching()

        self.resource_manager.agent.alloc_manager.refresh_allocations()

    def process_provider_resource_update(self, agent_id, resources, **context):
        ip_address = context.get('address')
        logger.info("Updating resources: id=%s, ip=%s, resources=%s", agent_id, ip_address, resources)
        self.resource_manager.update_provider_resources(agent_id, ip_address, resources)


class ResourceSearchAlgorithms(object):
    def __init__(self, db):
        self.db = db

    def greedy_reservation(self, app_name, instances, app_provider_id, app_provider_ip, constraints=None,
                           dependencies=None, partial=True):
        dependencies = dependencies or {}
        allocations = {}

        exclude_providers = set()
        # logger.detail(" *LOCAL* Start greedy allocation")
        for instance in instances:
            provider = self.db.reserve_allocation(app_name, instance.service_name,
                                                  instance.name, instance.resources,
                                                  instance.state, app_provider_id, app_provider_ip,
                                                  constraints=constraints,
                                                  dependencies=dependencies.get(instance.name),
                                                  filter_out=exclude_providers)
            if provider:
                allocations[instance.name] = {"provider": provider}
                logger.debug(" *LOCAL* reservation: %s -> %s", instance.name, provider)
                avoid_duplicates = getattr(exp_constraints, 'FORCE_AVOID_DUPLICATE_PROVIDERS_SAME_ALLOCATION', False)
                if avoid_duplicates:
                    exclude_providers.add(str(provider.ip_address))
            else:
                logger.debug(" *FAILED* reservation: %s", instance.name)
                if not partial:
                    for k, a in allocations.items():
                        self.db.delete_allocation(a["provider"].ip_address, a["provider"].agent_id, app_name, k)
                    return None
        # allocation status will hold information about how many services were allocated

        logger.debug("Allocations: %s", allocations)
        # logger.debug("Success allocating ALL servers")

        return allocations


class ConstraintTypes(Enum):
    LAMBDA = 1


class ResourceManager(LamaModule):
    def __init__(self, agent):
        super().__init__(agent)

        self.resource_db = ResourceDatabase(self)
        self.peer_manager = PeerManager(agent)

        self.fsm = StateMachine(InitializationState, identifier=self.__class__.__name__, resource_manager=self)
        self.agent.add_handler(self.fsm.event)

        # module info
        self.constraints = {}
        self.checking_function = {
            ConstraintTypes.LAMBDA: self.check_lambda_constraint
        }

        logger.info("Resource Manager State:")
        logger.info(" * Resource DB state: \n%s", self.resource_db)

    def start(self):
        self.fsm.start()

    def set_local_resources(self, resource_set):
        self.resource_db.add_provider(
            self.agent.id,
            self.agent.address.ip_address,
            self.agent.address.port,
            resource_set)

    def get_local_resources(self):
        return self.resource_db.get_provider_resources(self.agent.ip_address)

    def get_local_free_resources(self):
        return self.resource_db.get_provider_free_resources(self.agent.ip_address)

    def add_constraint(self, name, constraint):
        self.constraints[name] = constraint

    def check_constraints(self):
        if not len(self.constraints):
            logger.info("No constraints added.")
            return True

        for name, constraint in self.constraints.items():
            # noinspection PyBroadException
            logger.debug("Checking constraint '%s': %s", name, constraint)
            logger.debug("Function: %s", self.checking_function[constraint[0]])
            try:
                if not self.checking_function[constraint[0]](constraint[1]):
                    logger.info("Constraint %s not satisfied! [%s]", name, constraint[0].name)
                    return False
            except:
                logger.error("Ignoring constraint! Error on constraint: %s ::: Trace: %s" %
                             (constraint[0], traceback.format_exc()))

        logger.info("All constraints satisfied.")
        return True

    def check_lambda_constraint(self, constraint):
        return self.resource_db.check_condition(constraint)

    def get_provider_conditions(self):
        logger.warning("This function was not implemented yet! Returning empty condition list.")
        return ()

    def get_local_allocations(self):
        return self.resource_db.get_local_allocations()

    def get_providers(self, provider_filter=None):
        return self.resource_db.get_providers(provider_filter)

    def get_current_peers(self, exclude_self=False) -> IPSet:
        return self.resource_db.get_current_peers(exclude_self)

    def get_candidates(self, n=float("inf")):
        candidates = self.get_current_peers()
        logger.detail("candidates: %s", candidates)

        # TODO: what is faster? assign and compare? or always do len?
        if len(candidates) > n:
            candidates = IPSet(random.sample(set(candidates), n))
            logger.detail("candidates: %s", candidates)

        elif len(candidates) < n:
            left = n - len(candidates)
            candidates |= self.peer_manager.get_candidates(left)
            logger.detail("candidates: %s", candidates)

        return candidates

    def reserve(self, app_name, instances, constraints=None, dependencies=None, algorithm=ResourceSearchAlgorithms.greedy_reservation, partial=True):
        result = algorithm(ResourceSearchAlgorithms(self.resource_db), app_name, instances, self.agent.id,
                           self.agent.ip_address, constraints=constraints, dependencies=dependencies, partial=partial)
        if not self.check_constraints():
            self.peer_manager.start_searching(self.get_provider_conditions())
        return result

    def update_provider_resources(self, aid, address, resource_spec):
        self.resource_db.update_provider_free_resources(aid, address, resource_spec)
        if not self.check_constraints():
            self.peer_manager.start_searching(self.get_provider_conditions())

    def free(self, app_name, allocations):
        for k, a in allocations.items():
            self.resource_db.delete_allocation(a["provider"].ip_address, a["provider"].agent_id, app_name, k)

    def delete_allocation(self, prov_addr, prov_id, app_name, instance_name):
        self.resource_db.delete_allocation(prov_addr, prov_id, app_name, instance_name)

    def allocate(self, app_name, service_name, instance_name, resource_spec, state, last_provider_id, last_provider_ip,
                 dependencies=None):
        return self.resource_db.allocate(app_name, service_name, instance_name, resource_spec, state, last_provider_id,
                                         last_provider_ip, dependencies)

    def local_delete(self, app_name, instance_name, last_provider_id, last_provider_ip):
        return self.resource_db.local_delete(app_name, instance_name, last_provider_id, last_provider_ip)

    def get_allocation(self, app_name, service_name, instance_name):
        return self.resource_db.get_allocation(app_name, service_name, instance_name)

    def get_allocation_agent(self, app_name, service_name, instance_name):
        return self.resource_db.get_allocation_agent(app_name, service_name, instance_name)

    def change_state(self, app_name, service_name, instance_name, old_state, new_state, last_provider_ip=None):
        return self.resource_db.change_state(app_name, service_name, instance_name, old_state, new_state,
                                             last_provider_ip)

    def remove_provider(self, ip_address):
        self.resource_db.get_provider(ip_address)
        self.peer_manager.remove_subscriber(ip_address)
        self.resource_db.delete_provider(ip_address)


class AllocationEntry(object):
    """
    Class used by resource manager to hold information about an allocation made to an app.
    """

    def __init__(self, app_name, instance_name, spec, app_provider_id, app_provider_address, dependencies=None):
        """

        :param app_name: App ID (typically app name)
        :param instance_name: The name of the vm to which the resources were allocated
        :param spec: Specification of the allocated resources.
        :param app_provider_id: ID of the provider where the app (last known) is located
        :param app_provider_address: IP address of the app (last known) to which the resources were allocated
        """

        # logger.debug("add allocation entry: %s", (app_name, instance_name, spec, app_provider_id, app_provider_address))
        self.app_name = app_name
        self.instance_name = instance_name
        self.spec = spec
        self.app_provider_id = app_provider_id
        self.app_provider_address = app_provider_address
        self.dependencies = dependencies or {}

    def __str__(self):
        return "%s:%s @ %s ::: Spec: %s" % (self.app_name, self.instance_name, self.app_provider_address, self.spec)
        # def to_dict(self):
        #     d = self.__dict__.copy()
        #     if d['spec']:
        #         d['spec'] = d['spec'].to_dict()
        #     return d

    def to_dict(self):
        d = copy.deepcopy(self.__dict__)
        d["spec"] = self.spec.to_pretty_dict()
        return d

    def is_dependent(self, domain, value_set):
        return not self.dependencies.get(domain, set()).intersection(value_set)

    def depends_on(self, domain, value_set):
        return value_set in self.dependencies.get(domain, set())


class ProviderEntry(object):
    """
    Class to hold a information about a provider and keep track of available and allocated resources.
    """

    def __init__(self, aid, ip_address, port, spec):
        """
        Initializes a provider entry.

        :param aid: provider agent's id
        :param ip_address: provider agent's ip address
        :param port: provider agent's tcp port
        :param spec: provider agent's specification (resource information)
        """
        self.agent_id = aid
        self.ip_address = ip_address
        self.port = port

        # variable to keep track of resources
        self._spec = spec
        self._free_spec = copy.deepcopy(spec)
        # keeps track of all allocation made in this provider: dictionary of AllocationEntry
        self._allocations = {}
        self._latent_allocations = {}

        currtime = datetime.now()
        self._added_on = currtime
        self._updated_on = currtime

        self._latent_per_dependency = {}
        self._latent_instances_per_dependency = {}

    @property
    def long_str(self):
        """
        Long string representation of the provider entry.
        """
        stringer = ""
        stringer += "\n  ***  Provider @ %s:%d [ID: %s]" % (self.ip_address, self.port, self.agent_id)
        stringer += "\n\t- Spec: %s" % (self._spec.to_dict() if self._spec else "")
        stringer += "\n\t- Added: %s\n\t- Last Updated: %s" % (self._added_on, self._updated_on)
        if len(self._allocations):
            stringer += "\n\t- Allocations:\n\t =>  %s" % ("\n\t =>  ".join([str(a) for a in self._allocations.values()]))
        if len(self._latent_allocations):
            stringer += "\n\t- Latent:\n\t =>  %s" % ("\n\t =>  ".join([str(a) for a in self._latent_allocations.values()]))
        return stringer

    def __str__(self):
        """
        Single-ling string representation of the provider entry.
        """
        return "Provider [id: %s, ip: %s:%s]" % (self.agent_id, self.ip_address, self.port)

    def has_resources_available(self, resource_set):
        if self._free_spec.includes(resource_set):
            return True

        return False

    def update_free_resources(self, resources):
        self._free_spec = copy.deepcopy(resources)

    def adjust_allocations(self):
        deleted_allocs = []
        # logger.temp("Latent per dependency: %s", self._latent_per_dependency)
        # logger.temp("Latent instances: %s", self._latent_instances_per_dependency)
        # for key, allocation in self._latent_allocations.items():
        #     logger.temp("%s -> %s", key, allocation)

        del_keys = set()
        for dep, resource_set in self._latent_per_dependency.items():
            logger.temp("Check: dep=%s latent=%s", dep, resource_set)
            while self._latent_allocations and not self._free_spec.includes(resource_set) \
                    and self._latent_instances_per_dependency.get(dep):
                logger.temp("Overload: dep=%s", dep)
                instance_key = self._latent_instances_per_dependency.get(dep).pop()
                logger.temp("Remove: %s", instance_key)
                alloc = self._latent_allocations.get(instance_key)
                deleted_allocs.append(alloc)

                self._delete_dependent_allocations(alloc)
                del self._latent_allocations[instance_key]
                if instance_key in self._allocations:
                    del self._allocations[instance_key]
                if not self._latent_instances_per_dependency.get(dep):
                    del_keys.add(dep)
        for k in del_keys:
            del self._latent_instances_per_dependency[k]
        return deleted_allocs

    def allocate(self, app_name, service_name, instance_name, resource_set, state, provider_id, provider_address,
                 dependencies=None):
        logger.debug("Trying to allocate %s:%s: %s", app_name, instance_name, resource_set)
        key = (app_name, instance_name)
        if key in self._allocations:
            raise NotImplementedError("Error? the VM is already allocated!")

        deleted_allocs = []
        if state == InstanceState.Active:
            if self._free_spec.includes(resource_set):
                self._free_spec.subtract(resource_set)

                self._allocations[key] = AllocationEntry(app_name, instance_name, resource_set, provider_id,
                                                         provider_address)

                deleted_allocs = self.adjust_allocations()
                #
                # logger.temp("Latent per dependency: %s", self._latent_per_dependency)
                # logger.temp("Latent instances: %s", self._latent_instances_per_dependency)
                # for key, allocation in self._latent_allocations.items():
                #     logger.temp("%s -> %s", key, allocation)
                #
                # for dep, resource_set in self._latent_per_dependency.items():
                #     logger.temp("Check: dep=%s latent=%s", dep, resource_set)
                #     while self._latent_allocations and not self._free_spec.includes(resource_set):
                #         logger.temp("Overload: dep=%s", dep)
                #         instance_key = self._latent_instances_per_dependency.get(dep)[-1]
                #         logger.temp("Remove: %s", instance_key)
                #         alloc = self._latent_allocations.get(instance_key)
                #         deleted_allocs.append(alloc)
                #
                #         self._delete_dependent_allocations(alloc)
                #         del self._latent_allocations[instance_key]
                #         if instance_key in self._allocations:
                #             del self._allocations[instance_key]

                #
                # for each latent:
                #     is there enough on active?
                #         if not add to list to de-allocate
                #
                # return list... instead of true
                # logger.debug("TEST::Pickling...")
                # logger.debug("TEST::Original entry : %s", self.allocations)
                # pickled_str = pickle.dumps(self)
                # unpickled_obj = pickle.loads(pickled_str)
                # logger.debug("TEST::Unpickled entry: %s", unpickled_obj.allocations)

                return deleted_allocs
        elif state == InstanceState.Warm or state == InstanceState.Hot:
            logger.temp("Instance state: %s", state.name)
            # warm: this instance needs only disk space to be "actually allocated"
            # hot: this instance needs disk space and memory to be "actually allocated"
            # raise NotImplementedError()

            # check if free spec include this resource + all the latent resources of same app_name:service_name
            # Note: need to go back and check where service_name is
            # TODO: change for dependencies for each instance
            # latent = self._latent_allocations.setdefault((app_name, service_name), {})
            if key in self._latent_allocations:
                raise NotImplementedError("Error? the latent VM is already allocated")

            logger.temp("Latent allocation: %s", self._latent_allocations.get(key))
            logger.temp("Resource set: %s", resource_set)
            logger.temp("Dependencies: %s", dependencies)

            # total_latent = ResourceSet()
            # for sk, allocs in self._latent_allocations.items():
            #     for ik, v in latent.items():
            #         for domain, entities in dependencies.items():
            #             logger.temp("Check dependency [%s] %s", domain, entities)
            #             if v.is_dependent(domain, entities):
            #                 logger.temp("\t DEPENDENT")
            #                 total_latent.join(v.spec)
            #
            # logger.temp("Total latent: %s", total_latent)
            # simult_spec = resource_set + total_latent
            # logger.temp("Total necessary allocation: %s", simult_spec)

            enough_resources = True
            new_resource_set_per_dep = {}
            if dependencies:
                for domain, entities in dependencies.items():
                    for entity in entities:
                        k = (domain, app_name, entity) if domain == FailureDomain.Instance else (domain, entity)
                        logger.temp("Check dependency: dep=%s", k)

                        resource_set_per_dep = self._latent_per_dependency.get(k, ResourceSet())
                        tmp = resource_set_per_dep + resource_set
                        new_resource_set_per_dep[k] = tmp
                        if not self._free_spec.includes(tmp):
                            logger.temp("Resource set does not fit: dep=%s total=%s", k, tmp)
                            enough_resources = False
                            break

            # TODO: Check constraints...
            # if self._free_spec.includes(simult_spec):
            if enough_resources:
                # update per dependency
                self._latent_per_dependency.update(new_resource_set_per_dep)
                for domain, entities in dependencies.items():
                    for entity in entities:
                        # FIXME: this is a hack to avoid overlapping instance names from different apps
                        if domain == FailureDomain.Instance:
                            self._latent_instances_per_dependency.setdefault((domain, app_name, entity), []).append(key)
                        else:
                            self._latent_instances_per_dependency.setdefault((domain, entity), []).append(key)
                # if so... create two allocation entries: one for active and another for latent
                # warm instance: disk goes in the active, hot instance: disk and memory go in the active
                active_res = ("DiskSpec",) if state == InstanceState.Warm else ("DiskSpec", "RamSpec")
                active_spec, latent_spec = resource_set.split(*active_res)
                logger.temp("After split: active_spec=%s latent_spec: %s", active_spec, latent_spec)

                # add to both allocations
                self._allocations[key] = AllocationEntry(
                    app_name,
                    instance_name,
                    active_spec,
                    provider_id,
                    provider_address
                )

                self._latent_allocations[key] = AllocationEntry(
                    app_name,
                    instance_name,
                    latent_spec,
                    provider_id,
                    provider_address,
                    dependencies
                )
                logger.temp("Active alloc: %s", self._allocations.get(key))
                logger.temp("Latent alloc: %s", self._latent_allocations.get(key))
                # TODO EDIT return []
                return deleted_allocs

        else:
            logger.error("State '%s' for the instance is not valid!", state)

        logger.detail("returning False")
        # TODO EDIT return None instead of False
        return None

    def get_allocation(self, app_name, service_name, instance_name):
        """
        Returns the sum of active or latent allocations
        :param app_name:
        :param instance_name:
        :return:
        """
        logger.debug("checking provider: %s, allocs keys: %s", self.ip_address, list(self._allocations.keys()))

        logger.debug("Allocations: %s", self._allocations)
        logger.debug("Latent: %s", self._latent_allocations)
        key = (app_name, instance_name)
        alloc = self._allocations.get(key)
        latent = self._latent_allocations.get(key)
        if alloc:
            logger.debug("Alloc found: %s", alloc)
            return alloc.spec + latent.spec if latent else alloc.spec
        else:
            return latent.spec if latent else None

    def get_allocation_entry(self, app_name, service_name, instance_name):
        key = (app_name, instance_name)
        alloc = self._allocations.get(key)
        return alloc

    def _delete_dependent_allocations(self, allocation):
        for domain, entities in allocation.dependencies.items():
            for entity in entities:
                k = (domain, allocation.app_name, entity) if domain == FailureDomain.Instance else (domain, entity)
                self._latent_per_dependency[k].subtract(allocation.spec)

    def delete(self, app_name, instance_name, provider_id=None):
        # TODO: add check for provider id
        key = (app_name, instance_name)
        if key in self._allocations:
            ae = self._allocations[key]
            self._free_spec.add(ae.spec)
            del self._allocations[key]

            if key in self._latent_allocations:
                self._delete_dependent_allocations(self._latent_allocations.get(key))
                del self._latent_allocations[key]

            # logger.debug("Free spec: %s", self._free_spec)
            return True

        logger.warn("No key %s in allocations", key)
        return False

    def change_state(self, app_name, service_name, instance_name, old_state, new_state):
        key = (app_name, instance_name)
        logger.debug("Provider entry before: %s", self.long_str)
        if new_state.value < old_state.value:
            logger.debug("Going upward")
            # upward
            if key in self._latent_allocations:
                if new_state == InstanceState.Active:
                    ae = self._allocations[key]
                    le = self._latent_allocations[key]
                    self._free_spec.subtract(le.spec)
                    ae.spec.join(le.spec)
                    self._delete_dependent_allocations(self._latent_allocations.get(key))
                    del self._latent_allocations[key]
                    return self.adjust_allocations()
                elif new_state == InstanceState.Hot:
                    le = self._latent_allocations[key]
                    ae = self._allocations[key]

                    active_spec, latent_spec = le.spec.split("DiskSpec", "RamSpec")
                    self._free_spec.subtract(active_spec)
                    ae.spec.join(active_spec)
                    le.spec = latent_spec
                    return self.adjust_allocations()
                else:
                    logger.error("Invalid state transition: %s -> %s", old_state, new_state)
            else:
                logger.error("No latent allocation. There should be some!!!")
        else:
            logger.debug("Going downward")
            # downward
            key = (app_name, instance_name)
            if new_state == InstanceState.Hot or new_state == InstanceState.Warm:
                ae = self._allocations[key]
                active_res = ("DiskSpec",) if new_state == InstanceState.Warm else ("DiskSpec", "RamSpec")
                active_spec, latent_spec = ae.spec.split(*active_res)

                self._free_spec.add(latent_spec)
                ae.spec = active_spec

                if old_state == InstanceState.Active:
                    self._latent_allocations[key] = AllocationEntry(app_name, instance_name, latent_spec,
                                                  ae.app_provider_id, ae.app_provider_address)
                else:
                    if key in self._latent_allocations:
                        self._latent_allocations[key].spec.join(latent_spec)
                    else:
                        logger.error("No latent allocation. There should be some!!!")
                return []
            else:
                logger.error("Invalid state transition: %s -> %s", old_state, new_state)

        logger.debug("Provider entry after: %s", self.long_str)
        return None

    @property
    def free_resources(self):
        return self._free_spec

    @property
    def allocations(self):
        return self._allocations

    @property
    def info(self):
        return {
            "agent_id": self.agent_id,
            "ip_address": self.ip_address,
            "port": self.port,
            "resources": self._spec.to_dict()
        }

    @property
    def resources(self):
        return self._spec


class ResourceDatabase(object):
    # todo verify execution

    def __init__(self, resource_manager):
        """
        @type agent: lama.agent.provider.ProviderAgent
        """
        self.resource_manager = resource_manager
        # self._providers = {}
        self._providers_by_ip = PersistentSyncDict("resource_db", ProviderEntry)
        # """:type dict[str, ProviderEntry]"""

    def add_provider(self, aid, address, port, resource_spec):
        provider = ProviderEntry(aid, address, port, resource_spec)

        self._providers_by_ip.add(str(address), provider)
        logger.info("Resource DB :: Added: %s", provider)

        if aid == self.resource_manager.agent.id:
            self.resource_manager.peer_manager.update_subscriber_peers()

    def update_provider_free_resources(self, aid, ip_address, resource_spec):
        provider = self._providers_by_ip.get(str(ip_address))
        if not provider:
            logger.warn("Trying to update provider not in the DB [address=%s]", ip_address)
            return
        provider.update_free_resources(resource_spec)

    def get_provider_resources(self, ip_address):
        # if agent_id and agent_id in self._providers:
        #     return self._providers[agent_id].resources
        key = str(ip_address)
        if key and key in self._providers_by_ip:
            return self._providers_by_ip.get(str(ip_address)).resources
        else:
            return None

    def get_provider_free_resources(self, ip_address):
        assert (IPAddress(ip_address))
        # if agent_id and agent_id in self._providers:
        #     return self._providers[agent_id].free_resources
        key = str(ip_address)
        if key and key in self._providers_by_ip:
            # return self._providers_by_ip[ip_address].free_resources()
            prov = self._providers_by_ip.get(str(ip_address))
            return prov.free_resources
        else:
            return None

    def get_local_allocations(self):
        allocs = []
        for provider in [p for p in self._providers_by_ip.values() if self.resource_manager.agent.id == p.agent_id]:
            allocs += [v.to_dict() for v in provider.allocations.values()]
        logger.debug("ResourceDB :: Allocations: %s", allocs)
        return allocs

    def get_provider(self, ip_address):
        return self._providers_by_ip.get(str(ip_address))

    def get_providers(self, filter_providers=None):
        if filter_providers:
            return [val for key, val in self._providers_by_ip.items() if key not in filter_providers]

        return self._providers_by_ip.values()

    def get_current_peers(self, exclude_self=False) -> IPSet:
        ipset = IPSet([p.ip_address for p in self._providers_by_ip.values()])
        if exclude_self:
            ipset.remove(self.resource_manager.agent.ip_address)
        return ipset

    def initialize_database(self):
        pass

    @staticmethod
    def create_condition_number_of_providers(n):
        # TODO: transform this method from create_condition_[condition](args) into create_condition(ConditionType, args)
        return lambda providers: len(providers) >= n

    @staticmethod
    def create_condition_number_of_free_vcpus(n):
        def test_free_cpus(providers, n):
            vcpus = 0
            for p in providers.values():
                for cpu in p.free_resources.get(CpuSpec):
                    vcpus += cpu.n_processors
            logger.info("number of free vcpus: %s (threshold: %s)", vcpus, n)
            return vcpus >= n

        return partial(test_free_cpus, n=n)

    def get_num_of_free_vcpus(self):
        vcpus = 0
        for p in self._providers_by_ip.values():
            for cpu in p.free_resources.get(CpuSpec):
                vcpus += cpu.n_processors
        return vcpus

    def check_condition(self, callback):
        res = callback(self._providers_by_ip)
        return res

    def find_allocation(self, spec):
        logger.debug("ResourceDB :: Finding allocation for spec: %s", spec)
        for agent_ip, prov in self._providers_by_ip.items():
            assert isinstance(prov, ProviderEntry)
            logger.debug("\t * Searching in  provider '%s': %s", prov.ip_address, prov.free_resources)

            if prov.has_resources_available(spec):
                return prov

        return None

    def _process_constraints(self, instance_name, spec):
        spec_constraints = []
        logger.temp("spec: %s", spec)
        for ctype, constraint in spec.get(instance_name, []):
            if ctype == 'physical':
                spec_constraints.append(constraint)
        return spec_constraints

    def reserve_allocation(self, app_name, service_name, instance_name, spec, state, app_provider_id, app_provider_ip,
                           constraints=None, dependencies=None, filter_out=None):
        filter_out = (filter_out or set()) | getattr(exp_constraints, 'FORCE_NO_ALLOC_PROVIDERS', set())
        constraints = constraints or {}

        force_providers = set(getattr(exp_constraints, 'FORCE_ALLOCATIONS', {}).get(app_name, {}).get('allocations', {}).get(instance_name, ()))

        spec_constraints = set(self._process_constraints(instance_name, constraints))
        if force_providers and not force_providers.difference(spec_constraints):
            raise Exception("Contraint problem: force=%s, spec=%s", force_providers, spec_constraints)

        logger.temp("Force providers: %s", force_providers)
        logger.temp("Spec providers: %s", spec_constraints)

        arr = []
        for prov_ip in force_providers:
            if prov_ip in self._providers_by_ip and prov_ip not in spec_constraints:
                    arr.append((prov_ip, self._providers_by_ip.get(prov_ip)))

        arr += [
            (agent_ip, prov)
            for agent_ip, prov in self._providers_by_ip.items()
            if str(agent_ip) not in filter_out and str(agent_ip) not in force_providers and str(agent_ip) not in spec_constraints
        ]

        # randomize
        random.shuffle(arr)

        logger.debug("Reserve allocation for '%s:%s' - spec: %s", app_name, instance_name, spec)
        logger.debug("#provider=%s ips=%s", len(arr), [agent_ip for agent_ip, _ in arr])
        for agent_ip, prov in arr:
            # assert isinstance(prov, ProviderEntry), \
            #     "The list of providers is not provider entry: %s (%r)" % (prov, prov)
            # logger.debug("\t * Searching in  provider '%s': %s", prov.ip_address, prov.free_resources)

            result = prov.allocate(app_name, service_name, instance_name, spec, state, app_provider_id, app_provider_ip, dependencies=dependencies)
            if result is not None:
                # logger.debug("Resources reserved locally! Free: %s", prov.free_resources)
                self._providers_by_ip.sync()
                if app_provider_id == self.resource_manager.agent.id:
                    self.resource_manager.peer_manager.update_subscriber_peers()

                return prov
            else:
                logger.debug("Could not allocate resources for %s:%s:%s on %s", app_name, service_name, instance_name,
                             prov.ip_address)

        return None

    def allocate(self, app_name, service_name, instance_name, resource_spec, state, last_provider_id=None,
                 last_provider_ip=None, dependencies=None):
        if str(self.resource_manager.agent.ip_address) not in self._providers_by_ip:
            return False
        # logger.warning("======= ALLOCATION =========")
        result = self._providers_by_ip.get(str(self.resource_manager.agent.ip_address)).allocate(
            app_name,
            service_name,
            instance_name,
            resource_spec,
            state,
            last_provider_id,
            last_provider_ip,
            dependencies=dependencies
        )
        # TODO EDIT esult can ne a list of instances to remove...
        if result is not None:
            self.resource_manager.peer_manager.update_subscriber_peers()

            # logger.debug("SYNCED!!!")
            self._providers_by_ip.sync()
            return result
        # logger.warning("======= ALLOCATION =========")
        return None

    def local_delete(self, app_name, instance_name, last_provider_id, last_provider_ip):
        logger.debug("Local delete: %s:%s", app_name, instance_name)
        pentry = self._providers_by_ip.get(str(self.resource_manager.agent.ip_address))
        logger.debug("Provider Entry: %s", pentry)
        if not pentry:
            logger.warn("No local provider address")
            return False

        result = pentry.delete(
            app_name, instance_name, provider_id=last_provider_id
        )
        if result:
            self.resource_manager.peer_manager.update_subscriber_peers()
            self._providers_by_ip.sync()
            return True
        return False

    def delete_allocation(self, prov_addr, prov_id, app_name, instance_name):
        result = self._providers_by_ip.get(str(prov_addr)).delete(app_name, instance_name, provider_id=prov_id)
        if result:
            if prov_id == self.resource_manager.agent.id:
                self.resource_manager.peer_manager.update_subscriber_peers()
            self._providers_by_ip.sync()
            return True
        return False

    def get_allocation(self, app_name, service_name, instance_name):
        logger.debug("Get allocation to %s:%s *** keys: %s", app_name, instance_name,
                     list(self._providers_by_ip.keys()))
        for prov in self._providers_by_ip.values():
            rset = prov.get_allocation(app_name, service_name, instance_name)
            if rset:
                logger.debug("Returning: %s", rset)
                return rset
        return None

    def get_allocation_agent(self, app_name, service_name, instance_name):
        pentry = self._providers_by_ip.get(str(self.resource_manager.agent.ip_address))
        return pentry.get_allocation_entry(app_name, service_name, instance_name)

    def __str__(self):
        stringer = "Providers:"
        stringer += "\n" if len(self._providers_by_ip) else " no provider in DB!"
        for provider in self._providers_by_ip.values():
            #             logger.debug("Resources: %s", provider['spec'].res_dict())
            stringer += provider.long_str
        return stringer

    def change_state(self, app_name, service_name, instance_name, old_state, new_state, prov_addr=None):
        if not prov_addr:
            prov_addr = self.resource_manager.agent.ip_address
        logger.debug("Local change state: %s:%s:%s", app_name, service_name, instance_name)
        pentry = self._providers_by_ip.get(str(prov_addr))
        logger.debug("Provider Entry: %s", pentry)
        if not pentry:
            logger.warn("No local provider address")
            return None

        result = pentry.change_state(
            app_name, service_name, instance_name, old_state, new_state
        )
        if result is not None:
            self._providers_by_ip.sync()

        return result

    def delete_provider(self, ip_address):
        self._providers_by_ip.delete(str(ip_address))
