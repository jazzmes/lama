from functools import partial
import shutil
from os import path, remove
import traceback
from libvirt import libvirtError
from datetime import datetime
from netaddr.ip import IPNetwork
from paramiko import SFTPClient, Transport, SSHClient, AutoAddPolicy
from config.settings import PATH_IMAGE_POOL, PATH_IMAGE_STORAGE, PROVIDER_AG_PORT
from lama.agent.app.app_arch import InstanceState
from lama.agent.base import AgentAddress
from lama.agent.event_managers.twisted import run_async_static, run_in_thread
from lama.agent.events import Event, EventType
from lama.agent.provider.module import LamaModule
from lama.agent.state_machine import State, StateMachine
from lama.utils.enum import Enum
from lama.agent.helpers.network_helper import NetworkManager
from lama.utils.logging_lama import logger
from lama.api.api_libvirt import LibvirtConnection

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class InitializationState(State):
    def __init__(self, instance_manager):
        super().__init__()
        self.instance_manager = instance_manager

    def enter(self, event, prev_state):
        self.instance_manager.create_pool()
        return ActiveState


# noinspection PyBroadException
class ActiveState(State):
    def __init__(self, instance_manager):
        """
        @type instance_manager: InstanceManager
        """
        super().__init__()
        self.instance_manager = instance_manager

    def enter(self, event, prev_state):
        pass

    def process_provider_start_instance_event(self, app_name, service_name, vm_name, controller_ip, controller_port,
                                              mac, image_location, init,
                                              state, extra_nic_mac=None, **context):
        logger.temp("About to start VM: %s",
                      [app_name, service_name, vm_name, image_location, controller_ip, mac, state, extra_nic_mac])
        logger.debug("Context: %s", context)

        # self.instance_manager.agent.run_async(
        #     partial(self._start_instance, app_name, service_name, vm_name, controller_ip, controller_port,
        #             mac, image_location, init, state, extra_nic_mac, **context),
        #     lambda result: logger.temp("[VM:%s] done start instance (async... should be fast)", vm_name)
        # )
        # run_in_thread(
        #     self._start_instance,
        #     app_name, service_name, vm_name, controller_ip, controller_port,
        #     mac, image_location, init, state, extra_nic_mac, **context
        # )
        self._start_instance(app_name, service_name, vm_name, controller_ip, controller_port,
                    mac, image_location, init, state, extra_nic_mac, **context)

    def _start_instance(self, app_name, service_name, vm_name, controller_ip, controller_port,
                        mac, image_location, init, state, extra_nic_mac=None, **context):
        # should "authenticate", i.e. guarantee there is an allocation for this VM
        # at the same time we need to get the resources for the VM
        logger.debug("NOT IMPLEMENTED: should 'authenticate', i.e. guarantee there is an allocation for this VM")
        logger.debug("NOT IMPLEMENTED: at the same time we need to get the resources for the VM")

        nman = self.instance_manager.networks.get(app_name)
        if not nman:
            if app_name in self.instance_manager.agent.app_manager.apps:
                logger.temp("Found network/app in app manager: not creating -> loading")
                # nman = self.instance_manager.agent.app_manager.add_remote_app(app_name).net
                nman = self.instance_manager.agent.app_manager.apps.get(app_name).net
                # make sure bridge exists
                # nman.create_bridge()
                self.instance_manager.networks[app_name] = nman
            else:
                logger.temp("Did not find network/app in app manager: creating")
                nman = NetworkManager(app_name, controller_port, controller_ip=controller_ip)
                self.instance_manager.networks[app_name] = nman
            nman.create_libvirt_network()
            # have nman and instance
            #     nman.create_libvirt_network_on_process(
            #         self.instance_manager.agent.event_manager.run_on_process,
            #         callback=partial(self._upload_image, app_name, service_name, vm_name, instance, image_location, extra_nic_mac, **context)
            #     )
        else:
            logger.debug("Found network/app in instance manager: not creating")

        key = Instance.get_key(app_name, vm_name)
        logger.temp("Instances: %s", self.instance_manager.instances)
        if key in self.instance_manager.instances:
            instance = self.instance_manager.instances.get(key)
            logger.temp("Instance found")
            # logger.temp("status=%s target_state=%s", instance.status, instance.target_state)
            self.instance_manager.change_state(app_name, vm_name, new_state=state)
            self.process_provider_start_instance_event_deploy(app_name, service_name, vm_name,
                                                              instance, context.get("address"),
                                                              extra_nic_mac=extra_nic_mac)

        else:
            # get resources
            res_spec = self.instance_manager.agent.resource_manager.get_allocation(app_name, service_name, vm_name)
            logger.debug("Resource spec: %s", res_spec)
            # create instance
            instance = Instance(app_name, service_name, vm_name, init=init,
                                image_location=image_location, network=nman.net_name, mac_address=mac,
                                res_spec=res_spec, target_state=state)
            self.instance_manager.instances[key] = instance

            # def _upload_image(self, app_name, service_name, vm_name, instance, image_location, extra_nic_mac, **context):
            if image_location != self.instance_manager.agent.ip_address:
                logger.info("Image is at remote provider: %s", image_location)
                self.instance_manager.agent.run_async(
                    partial(instance.get_image, instance.get_image_storage_filename(), instance.get_local_pool_image_filename(),
                            image_location),
                    lambda result: self.process_provider_start_instance_event_deploy(app_name, service_name, vm_name,
                                                                                     instance, context.get("address"),
                                                                                     extra_nic_mac=extra_nic_mac)
                )

            else:
                self.instance_manager.agent.run_async(
                    partial(instance.get_local_image),
                    lambda result: self.process_provider_start_instance_event_deploy(app_name, service_name, vm_name,
                                                                                     instance, context.get("address"),
                                                                                     extra_nic_mac=extra_nic_mac)
                )

    def process_provider_start_instance_event_deploy(self, app_name, service_name, vm_name, instance, client_address,
                                                     extra_nic_mac=None):
        # deploy: this function will verify instance status, request image if necessary
        try:
            req_spec = self.instance_manager.agent.resource_manager.get_allocation(app_name, service_name, vm_name)
            logger.debug("Instance is still allocated? %s", req_spec is not None)
            if req_spec:
                logger.debug("Checking instance: %s", vm_name)
                check_result = instance.check()
                if check_result:
                    logger.debug("Instance already deployed: %s", vm_name)
                    self.process_provider_start_instance_event_respond(
                        {"active": True, "msg": "Instance '%s' was already at the desired state: %s!" % (vm_name, instance.target_state.name)},
                        app_name, service_name, vm_name, instance, client_address
                    )
                else:
                    logger.debug("Deploying instance: %s", vm_name)
                    self.instance_manager.agent.run_async(
                        instance.deploy,
                        self.process_provider_start_instance_event_respond,
                        func_args=(extra_nic_mac,),
                        cb_args=(
                            app_name, service_name, vm_name, instance, client_address
                        )
                    )
            else:
                self.instance_manager.terminate_instance(app_name, vm_name)

        except Exception as e:
            logger.error("Error while deploying instance: %s", traceback.format_exc())

            # if handler:
            #     handler(result["active"], msg=result["msg"])

    def process_provider_start_instance_event_respond(self, result, app_name, service_name, vm_name, instance,
                                                      client_address):
        # deploy: this function will verify instance status, request image if necessary
        try:
            logger.temp("Result of deploy: %s", result)
            event = Event(EventType.ProviderStartInstanceResponseEvent, app_name, service_name, vm_name,
                          result["active"], result["msg"])
            # check if client address is local?
            if client_address in NetworkManager.gw_network:
                self.instance_manager.agent.app_manager.send_event_to_app(app_name, event)
            else:
                client_address = AgentAddress(ip=client_address, port=PROVIDER_AG_PORT)
                self.instance_manager.agent.send_event_reliably(
                    client_address, event
                )
        except Exception as e:
            logger.error("Error while sending instance deployment respnose: %s", traceback.format_exc())

            # if handler:
            #     handler(result["active"], msg=result["msg"])


class UnknownVMException(Exception):
    def __init__(self, vm_name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.vm_name = vm_name

    def __str__(self):
        return "VM '%s' does not exist!" % self.vm_name


class InvalidInstanceAction(Exception):
    def __init__(self, vm_name, action, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.vm_name = vm_name
        self.action = action

    def __str__(self):
        return "Invalid action '%s' for instance '%s'" % (self.action, self.vm_name)


class InstanceStatus(Enum):
    NotDeployed = 1
    Idle = 2
    Warm = 3
    Hot = 4
    Active = 5


class InstanceManager(LamaModule):
    def __init__(self, agent):
        super().__init__(agent)

        self.fsm = StateMachine(InitializationState, identifier=self.__class__.__name__, instance_manager=self)
        self.agent.add_handler(self.fsm.event)
        self.instances = {}
        self.networks = {}

    def start(self):
        self.fsm.start()

    def create_pool(self, conn=None):
        logger.debug("Connect to libvirt")
        connection = conn if conn else LibvirtConnection()

        # TODO: have the folder created on installation
        # or else we ned to run this as sudo
        # try:
        #     makedirs(PATH_IMAGE_POOL)
        # except OSError as exception:
        #     if exception.errno != errno.EEXIST:
        #         logger.error("Unable to create images directory: %s", traceback.format_exc())
        logger.debug("Get storage pool")
        # check if lama pool exists
        pool = connection.get_storage_pool("lama-images")
        if not pool:
            logger.debug("Create storage pool")
            connection.create_storage_pool("lama-images", PATH_IMAGE_POOL)
        else:
            logger.debug("Set autostart")
            if not pool.autostart():
                pool.setAutostart(True)
            if not pool.isActive():
                pool.create()

    def terminate_instance(self, app_name, instance_name):
        key = Instance.get_key(app_name, instance_name)
        instance = self.instances.get(key)
        if not instance:
            logger.debug("Terminate Request FAIL - Instance '%s:%s' does not exist", app_name, instance_name)
            return False

        instance.terminate()
        del self.instances[key]
        return True

    def change_state(self, app_name, instance_name, new_state):
        key = Instance.get_key(app_name, instance_name)
        instance = self.instances.get(key)
        if not instance:
            logger.debug("Change state fail - Instance '%s:%s' does not exist", app_name, instance_name)
            return False

        instance.change_state(new_state)
        return True


class Instance(object):

    def __init__(self, app_name, service_name, vm_name, image_location,
                 network="default", init=False, mac_address=None, res_spec=None,
                 target_state=InstanceState.Active):
        self.status = InstanceStatus.NotDeployed
        self.target_state = target_state

        self.app_name = app_name
        self.service_name = service_name
        self.vm_name = vm_name
        self.network_name = network
        self.mac_address = mac_address
        self.res_spec = res_spec
        self._transient = False

        # network information
        # self.host = host
        # self.controller_ip = controller_ip
        # self.controller_port = controller_port

        self.image_location = image_location
        self.init = init

    def check(self):
        instance_name = self.get_instance_name()
        connection = LibvirtConnection()
        instance = connection.get_domain(instance_name)
        r = False
        if instance:
            logger.debug("Check instance: %s", instance_name)
            if instance.isActive():
                logger.debug("Instance is active")
                r = True
                if self.target_state == InstanceState.Active:
                    logger.debug("Trying to resume anyway, just in case (status=%s, target=%s)",
                                 self.status, self.target_state)
                    instance.resume()

            elif self.target_state == InstanceState.Hot or self.target_state == InstanceState.Warm:
                logger.debug("Instance is NOT active but target state is: %s", self.target_state.name)
                r = True

        connection.close()
        return r

    def change_state(self, new_state):
        # logger.info("[%s] Current state should be %s. Current NOT verifying", old_state)
        if self._transient:
            logger.error("Instance is in a transient state!")

        self.target_state = new_state
        # self.deploy()

    def deploy(self, extra_nic_mac=False, max_attempts=100):
        self._transient = True

        last_result = None
        n = 1
        while n < max_attempts:
            connection = None
            try:

                instance_name = self.get_instance_name()
                logger.info("[%s:%s] About to deploy '%s'", self.app_name,
                                        self.vm_name, instance_name)

                logger.temp("[%s:%s] Connecting to Libvirt", self.app_name, self.vm_name)
                connection = LibvirtConnection()

                instance = connection.get_domain(instance_name)
                actions = []
                active = False
                if not instance:
                    # make sure we have the image file in place:
                    img_file = self.get_local_pool_image_filename()
                    if not path.isfile(img_file):
                        logger.warning("[%s:%s] Image file not found: '%s'. Unable to deploy instance...", self.app_name, self.vm_name, img_file)
                        return {"active": False,
                                "msg": "Image file not found: '%s'. Unable to deploy instance..." % img_file}

                    logger.temp("[%s:%s] Defining instance...", self.app_name, self.vm_name)

                    # defines the instance
                    instance = connection.define_instance(instance_name, img_file, network=self.network_name,
                                                          mac=self.mac_address, extra_nic_mac=extra_nic_mac,
                                                          res_spec=self.res_spec)
                    logger.temp("[%s:%s] Instance defined!", self.app_name, self.vm_name)
                    actions.append("Instance was defined.")
                else:
                    logger.warning("[%s:%s] Instance '%s' was already defined!", self.app_name, self.vm_name, instance_name)
                    actions.append("Instance was already defined.")

                if not instance:
                    logger.warn("[%s:%s] Unable to define VM (define VM)!", self.app_name, self.vm_name)
                    self._transient = False
                    return {"result": False, "active": False, "msg": "Unable to define VM!"}

                if self.target_state == InstanceState.Active or self.target_state == InstanceState.Hot:
                    if instance.isActive():
                        logger.warn("[%s:%s] Instance '%s' was already created!", self.app_name, self.vm_name, instance_name)
                        actions.append("Instance was already defined.")
                    else:
                        ts = datetime.now()
                        logger.temp("[%s:%s] Create instance...", self.app_name, self.vm_name)
                        # create
                        r = instance.create()
                        actions.append("Instance was created")
                        logger.temp("[%s:%s] Created instance: %s (time: %s)", self.app_name, self.vm_name, r, datetime.now() - ts)

                    active = bool(instance.isActive())
                    instance.setAutostart(True)

                    if active:
                        self.status = InstanceStatus.Active

                    if self.target_state == InstanceState.Hot:
                        logger.info("[%s:%s] Hot instance: will suspend VM in 60 seconds", self.app_name, self.vm_name)
                        # FIXME should not use it directly
                        run_async_static(
                            partial(self.suspend_instance, instance_name),
                            timeout=60
                        )
                        # instance.suspend()
                        self.status = InstanceStatus.Hot

                else:
                    if instance.isActive():
                        instance.shutdown()
                        instance.setAutostart(False)
                        active = bool(instance.isActive())
                        if active:
                            logger.warn("[%s:%s] Instance is still active. Need to wait for shutdown? Or try destroy?", self.app_name, self.vm_name)
                            instance.destroy()
                            logger.info("[%s:%s] Instance '%s' - Instance was destroyed.", self.app_name, self.vm_name, instance_name)
                        else:
                            logger.info("[%s:%s] Instance '%s' - Instance was shutdown.", self.app_name, self.vm_name, instance_name)

                    else:
                        logger.info("[%s:%s] Instance '%s' - Target State: %s - Not starting!", self.app_name, self.vm_name, instance_name,
                                    self.target_state.name)

                    self.status = InstanceStatus.Warm

                self._transient = False
                return {"result": True, "active": active, "msg": " ".join(actions)}
            except libvirtError as e:
                logger.warn("[%s:%s] Libvirt error: %s. Traceback:\n%s", self.app_name, self.vm_name, e, traceback.format_exc())
                n += 1
                logger.warn("[%s:%s] Retrying...", self.app_name, self.vm_name)
                last_result = {"active": False,
                        "msg": "Error deploying instance: %s" % traceback.format_exc()}
            except Exception as e:
                logger.error("[%s:%s] Traceback: %s", self.app_name, self.vm_name, traceback.format_exc())
                self._transient = False
                return {"active": False,
                        "msg": "Error deploying instance: %s" % traceback.format_exc()}

            finally:
                logger.temp("[%s:%s] Leaving deploy!", self.app_name, self.vm_name)
                if connection:
                    connection.close()
        self._transient = False
        return last_result

    def suspend_instance(self, name):
        connection = None
        try:
            connection = LibvirtConnection()
            instance = connection.get_domain(name)
            if instance:
                logger.info("Suspend instance: %s", instance)
                instance.suspend()
                logger.debug("TEST After Suspension: active=%s", instance.isActive())
            else:
                logger.info("Instance '%s' must have been deleted", name)
        except Exception as e:
            logger.error("[%s] Traceback: %s", self.vm_name, traceback.format_exc())
        finally:
            if connection:
                    connection.close()

    def get_instance_name(self):
        return "lama_%s_%s" % (self.app_name, self.vm_name)

    def get_local_image(self):
        src = self.get_image_filename(PATH_IMAGE_STORAGE, self.app_name, self.service_name, init=self.init)
        dst = self.get_local_pool_image_filename()
        logger.info("Copying local image: %s -> %s", src, dst)
        shutil.copyfile(src, dst)
        logger.info("Done copying: %s -> %s", src, dst)

    def start(self):
        raise NotImplementedError()

    def suspend(self):
        raise NotImplementedError()

    def shutdown(self):
        raise NotImplementedError()

    def migrate(self):
        raise NotImplementedError()

    def terminate(self, max_attempts=3):
        logger.debug("Terminating instance - status: %s", self.status)
        n = 1
        connection = True
        instance_name = self.get_instance_name()
        while n < max_attempts:
            try:
                connection = LibvirtConnection()
                instance = connection.get_domain(instance_name)

                if not instance:
                    logger.warn("Instance '%s' not found!", instance_name)
                    return False

                if instance.isActive():
                    # shutdown instance
                    instance.destroy()

                # undefine instance
                instance.undefine()

                return True

            except libvirtError as e:
                logger.warn("[%s] Libvirt error: %s. Traceback:\n%s", self.vm_name, e, traceback.format_exc())
                n += 1
                logger.warn("[%s] Retrying...", self.vm_name)
            except Exception as e:
                logger.error("[%s] Traceback: %s", self.vm_name, traceback.format_exc())
                return False
            finally:
                if connection:
                    connection.close()

            # remove local image
            filename = self.get_local_pool_image_filename()
            logger.debug("Deleting local image file: %s", filename)
            try:
                remove(filename)
            except OSError as e:
                logger.warn("Unable to delete image file: %s", e)
            except:
                logger.error("Unable to delete image file: %s", traceback.format_exc())

        return False

    @staticmethod
    def get_key(app_name, vm_name):
        return app_name, vm_name

    @staticmethod
    def get_image_filename(folder_path, app_name, service_name, init=False):
        return path.join(folder_path, "%s_img%s_%s.qcow2" % (app_name, '_init' if init else '', service_name))

    def get_image_storage_filename(self):
        return self.get_image_filename(PATH_IMAGE_STORAGE, self.app_name, self.service_name, init=self.init)

    def get_local_pool_image_filename(self):
        return path.join(PATH_IMAGE_POOL, "%s_%s.qcow2" % (self.app_name, self.vm_name))

    @staticmethod
    def log_progress(status, transferred, total):
        try:
            interval = status["interval"]
            last = status["last"]
            perc = int(transferred * 100 / total)
            if perc >= last:
                logger.temp("Tranferred: %s/%s - %d%%", transferred, total, perc)
                status["last"] = last + interval
        except:
            logger.error("Traceback: %s", traceback.format_exc())

    @staticmethod
    def get_image(storage_filename, local_filename, location, attempts=10):
        # ssh = SSHClient()
        # ssh.load_system_host_keys()
        # ssh.set_missing_host_key_policy(AutoAddPolicy())
        # ssh.connect(str(location), username="lama", password="lA&maD3plo!y")
        # tp = ssh.get_transport()
        # tp.sock.getsockname()
        # fn = self.get_image_storage_filename()

        # logger.detail("Connected?")
        # local_fn = ntpath.basename(storage_filename)

        logger.info("Trying to get image file: '%s' @ %s -> '%s", storage_filename, location, local_filename)

        transport = None
        sftp = None
        for attempt in range(attempts):
            try:
                ssh = SSHClient()
                ssh.set_missing_host_key_policy(AutoAddPolicy())
                ssh.connect(str(location), username="lama", password="lA&maD3plo!y", timeout=10)
                sftp = ssh.open_sftp()

                # transport = Transport((str(location), 22))
                # transport.connect(username="lama", password="lA&maD3plo!y")
                # sftp = SFTPClient.from_transport(transport)
                status = {"last": 0, "interval": 5}
                # logger.debug("storage_filename=%s, target=%s", storage_filename, local_filename)
                sftp.get(storage_filename, local_filename, callback=lambda tr, to: Instance.log_progress(status, tr, to))
                logger.info("Done copying: %s -> %s", storage_filename, local_filename)
                # ssh.close()
                return
            except:
                logger.error("Attempt %d failed. Traceback: %s", attempt, traceback.format_exc())
            finally:
                if sftp:
                    sftp.close()
                if transport:
                    transport.close()
