import os.path
import logging
import sys
import getpass
from config.settings import PATH_LOG, LOG_LEVEL

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"

TEMP_LOG_LEVEL = 15
DETAIL_LOG_LEVEL = 5


# define new levels for:
# - temp: temporary development messages: high level that are intended to be removed
# - detail: very detailed debug messages: lower level below dbeug

def temp(self, message, *args, **kws):
    if self.isEnabledFor(TEMP_LOG_LEVEL):
        self._log(TEMP_LOG_LEVEL, message, args, **kws)


def detail(self, message, *args, **kws):
    if self.isEnabledFor(DETAIL_LOG_LEVEL):
        self._log(DETAIL_LOG_LEVEL, message, args, **kws)

try:
    logging.TEMP
    logging.DETAIL
except:
    logging.addLevelName(TEMP_LOG_LEVEL, "TEMP")
    logging.addLevelName(DETAIL_LOG_LEVEL, "DETAIL")
    logging.Logger.temp = temp
    logging.Logger.detail = detail


class LogColors(object):
    """
    Enum that defines color codes to be used at a terminal.
    """
    red = '\033[91m'
    green = '\033[92m'
    yellow = '\033[93m'
    blue = '\033[94m'
    magenta = '\033[95m'
    cyan = '\033[96m'
    white = '\033[97m'
    red_on_white = '\033[101m\033[97m'
    end_color = '\033[0m'


class ColorFormatter(logging.Formatter):
    """
    Formatter that defines the messages for Lama logs using colors.
    """
    COLORS = {
        logging.DEBUG: LogColors.green,
        logging.INFO: LogColors.blue,
        logging.WARNING: LogColors.magenta,
        logging.ERROR: LogColors.red,
        logging.CRITICAL: LogColors.red_on_white,
        TEMP_LOG_LEVEL: LogColors.cyan,
        DETAIL_LOG_LEVEL: LogColors.yellow
    }

    def format(self, record):

        try:
            default = self._style._fmt
            style = self.COLORS.get(record.levelno, LogColors.white)

            self._style._fmt = style \
                               + "%(asctime)s : %(levelname)5.5s : %(module)14.14s::%(funcName)20.20s[%(lineno)3.3s]" \
                               + LogColors.end_color + " : %(message)s"

            res = super().format(record)
            self._style._fmt = default
        except:
            # python2
            default = self._fmt

            style = self.COLORS.get(record.levelno, LogColors.white)

            self._fmt = style + \
                        "%(asctime)s : %(levelname)5.5s : %(module)14.14s::%(funcName)20.20s[%(lineno)3.3s]" \
                        + LogColors.end_color + " : %(message)s"

            res = super(ColorFormatter, self).format(record)
            self._fmt = default

        return res


class SimpleFormatter(logging.Formatter):
    def format(self, record):
        """
        Format without colors.

        :param record: record containing the message to log
        """

        default = self._fmt
        self._fmt = "%(asctime)s : %(levelname)s : %(module)s::%(funcName)s [%(lineno)d] : %(message)s"
        result = logging.Formatter.format(self, record)
        self._fmt = default
        return result


class LamaLogger(object):
    """
    Class that creates the logger file for LAMA.
    Logs are placed in a filename with the input name provided.
    """

    loggers = dict()

    LAMA_LOG_PATH = PATH_LOG

    @staticmethod
    def get_logger(name, filename=None, level=None):
        """
        Get a logger and configure log file.

        :param name: name of the logger
        :param filename: filename to log file (an extension .log will be added if not present)
        :param level: the minimum level of messages to log
        :return: returns the logger object
        """

        if name in LamaLogger.loggers:
            return LamaLogger.loggers[name]

        log = logging.getLogger(name)
        if level:
            log.setLevel(level)
        else:
            try:
                settings = __import__('settings', fromlist=['config'])
            except ImportError:
                log.setLevel("TEMP")
                # logger.warn("Set level to temp")
            else:
                log.setLevel(settings.LOG_LEVEL)

        #         streamh = logging.StreamHandler()
        #         streamh.setFormatter(ColorFormatter())
        #         logger.addHandler(streamh)

        if not filename:
            filename = name

        LamaLogger.set_filename(log, filename)
        LamaLogger.loggers[name] = log

        return log

    @staticmethod
    def set_filename(log, filename, stdout=False):
        """
        Defines the file handler of the log object.

        :param log: log object
        :param filename: name of the log file
        """

        for handler in log.handlers:
            log.removeHandler(handler)

        if not filename.lower().endswith('.log'):
            filename += '.log'

        if not os.path.isabs(filename):
            filename = LamaLogger.LAMA_LOG_PATH + filename

        fileh = logging.FileHandler(filename, mode='w')
        fileh.setFormatter(ColorFormatter())

        log.addHandler(fileh)

        if stdout:
            consoleh = logging.StreamHandler(sys.stdout)
            consoleh.setFormatter(ColorFormatter())
            log.addHandler(consoleh)

        log.propagate = False

    @staticmethod
    def enable_stdout(log):
        consoleh = logging.StreamHandler(sys.stdout)
        consoleh.setFormatter(ColorFormatter())
        log.addHandler(consoleh)


user = getpass.getuser()
# print("USER: %s" % user)
try:
    logger = LamaLogger.get_logger("general-%s" % user, level=LOG_LEVEL)
    logger.warn("Set level to %s", LOG_LEVEL)
except ImportError:
    logger = LamaLogger.get_logger("general-%s" % user, level="INFO")
    logger.warn("Set level to default: %s", logger.getEffectiveLevel())

# logger.detail("shows: %s", DETAIL_LOG_LEVEL)
# logger.debug("shows: %s", logging.DEBUG)
# logger.info("shows: %s", logging.INFO)
# logger.temp("shows: %s", TEMP_LOG_LEVEL)
# logger.warn("shows: %s", logging.WARN)
# logger.error("shows: %s", logging.ERROR)
