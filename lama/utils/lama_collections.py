import shelve
from os import path, makedirs
from config.settings import PATH_PERSISTENCE
from lama.utils.logging_lama import logger

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMA Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class NonPersistentItemException(Exception):
    pass


class WrongObjectTypeException(Exception):
    pass


class Persistent(object):
    pass


class PersistentSyncDict(Persistent):

    def __init__(self, name, obj_type):
        # if not issubclass(obj_type, Persistent):
        #     raise NonPersistentItemException("Class '%s' is not subclass of lama_collections.Persistent" % obj_type)
        self.name = name
        folder = PATH_PERSISTENCE
        if folder[-1] != "/":
            folder += "/"

        try:
            makedirs(folder)
        except:
            pass

        logger.temp("Path: %s", path.join(folder, "%s.psd" % name))
        self.d = shelve.open(path.join(folder, "%s.psd" % name), writeback=True)
        self.obj_type = obj_type

    def add(self, oid, obj):
        if not isinstance(obj, self.obj_type):
            raise WrongObjectTypeException("Item should be of class '%s' (is '%s')." % (self.obj_type, obj.__class__))
        self.d[oid] = obj

    def get(self, oid):
        return self.d.get(oid, None)

    def delete(self, oid):
        del self.d[oid]

    def update(self, oid, **changes):
        try:
            obj = self.d[oid]
        except KeyError as e:
            raise KeyError(oid)

        # check attributes
        for k in changes.keys():
            if not hasattr(obj, k):
                logger.error("PersistentListAccessError :: Unable to access attribute '%s'", k)
                raise AttributeError("Wrong attribute '%s'" % k)

        # update
        for k, v in changes.items():
            setattr(obj, k, v)

        # persistent update
        self.d.sync()

    def __str__(self):
        entries = []
        for oid, o in self.d.items():
            entries.append("   %s:   (%s)" % (oid, ", ".join(["%s = %s" % (k, v) for k, v in o.__dict__.items()])))
        if entries:
            return "\n".join(entries)
        else:
            return "{}"

    def close(self):
        self.d.close()

    def __iter__(self):
        return self.d.__iter__()

    def __setitem__(self, *args, **kwargs):
        raise NotImplementedError()

    def values(self):
        return self.d.values()

    def keys(self):
        return self.d.keys()

    def items(self):
        return self.d.items()

    def __len__(self):
        return len(self.d)

    def sync(self):
        logger.debug(" Bypassing sync...")
        # TODO: TEMPORARY Bypassing sync for experiments
        # logger.debug(" ** BEF ** Persistent: %s\n\t%s", self.name, self)
        # self.d.sync()
        # logger.debug(" ** AFT ** Persistent: %s\n\t%s", self.name, self)


class PersistentWrapper(object):

    def __init__(self, default_obj, filename):
        self.__shelve = shelve.open(filename, writeback=True)
        try:
            logger.detail("Loading: '%s'", filename)
            self.obj = self.__shelve["obj"]
            logger.detail("Got object: '%s/%r'", self.obj, self.obj)
            assert isinstance(self.obj, default_obj.__class__)
            logger.detail("Loaded App Arch state from file: '%s'", filename)
            self.new = False
        except (KeyError, AssertionError):
            self.__shelve["obj"] = default_obj
            self.obj = self.__shelve["obj"]
            logger.detail("New %r from config file", self.obj)
            self.new = True
        self.filename = filename
        self.save()

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return "%s[%s]" % (self.__class__, self.obj.__class__)

    def __getattr__(self, item):
        if item == 'obj':
            raise AttributeError
        else:
            return getattr(self.obj, item)

    def save(self):
        logger.detail("SYNC! %s to %s", self.__shelve, self.filename)
        self.__shelve["obj"] = self.obj
        # self.__shelve.sync()

    def close(self):
        self.__shelve.close()
