from json.encoder import JSONEncoder
import datetime
import numpy as np

from netaddr.ip import IPAddress, IPNetwork

from lama.agent.app.app_arch import AppSpec
from lama.utils.enum import Enum
from lama.utils.logging_lama import logger

__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class LamaJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, IPAddress) or isinstance(obj, IPNetwork) or isinstance(obj, datetime.datetime):
            return str(obj)
        elif isinstance(obj, Enum):
            return obj.name
        elif isinstance(obj, AppSpec):
            return obj.to_dict()
        elif hasattr(obj, "jsonify"):
            return obj.jsonify()
        elif isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()

        try:
            res = super().default(obj)
        except Exception as e:
            logger.error("Unable to encode: (%s) %s", type(obj), obj)
            raise e
        return res
