import string
import re
import netifaces
from math import pow
import traceback
import pwd
import grp
import os
from netaddr import IPNetwork
from netaddr.ip import IPAddress
from netaddr.ip.sets import IPSet

from lama.utils.logging_lama import logging, LogColors, logger


__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMAs Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


def get_ip_addresses():

    ipset = IPSet()

    interfaces = netifaces.interfaces()
    for i in interfaces:
        iface = netifaces.ifaddresses(i).get(netifaces.AF_INET)
        if iface:
            for j in iface:
                ipset.add(j['addr'])
    return ipset


def get_ip_address():
    # CAREFUL this is a first version, with minimal functionality
    # will not cover all cases

    ints = list()

    interfaces = netifaces.interfaces()
    for i in interfaces:
        ifa = {"name": i}
        iface = netifaces.ifaddresses(i).get(netifaces.AF_INET)
        if iface:
            for j in iface:
                ifa["address"] = j['addr']
                ip = IPNetwork(j['addr'], j['netmask'])
                ifa["prefix"] = ip.prefixlen
        ints.append(ifa)

    #print results
    ip_address = None
    prefix = None
    for i in ints:
        out = "Interface found: %s" % (i["name"])
        if "address" in i:
            out += " : %s/%s" % (i["address"], i["prefix"])
            if i["address"] == "127.0.0.1":
                out += " : local"
            elif i["address"][:7] == "169.254":
                out += " : self_assigned"
            else:
                out += " : " + LogColors.red + "ACTIVE" + LogColors.end_color
                if ip_address is None:
                    ip_address = i["address"]
                    prefix = i["prefix"]
        else:
            out += " : no_ip/virtual"

        logging.debug(out)

    return IPAddress(ip_address), prefix


def escape_name(name):

    parsed_name = name.lower()
    valid_chars = "-_%s%s" % (string.ascii_letters, string.digits)
    for ch in parsed_name:
        if ch not in valid_chars:
            parsed_name = parsed_name.replace(ch, '_')
            
    return parsed_name


def shorten_app_name(name, length):
    parts = name.split("-")
    try:
        int(parts[-1])
        if len(parts[-1]) > length:
            logger.warn("Suffix too large '%s' truncated!" % parts[-1])
            return ("%s" % parts[-1])[len(parts[-1]) - length:]
        elif len(parts[-1]) == length:
            return parts[-1]
        elif len(parts[-1]) == length - 1:
            return "-%s" % parts[-1]
        else:
            return "%s-%s" % ("-".join(parts[0:-1])[0:length-len(parts[-1]) - 1], parts[-1])

    except:
        return "%s" % (name[0:length])


def set_user(user="lama", group=None):
    try:
        u = pwd.getpwnam(user)
        if group:
            g = grp.getgrnam(group)
            os.setgid(g.gr_gid)
        else:
            os.setgid(u.pw_gid)
        os.setuid(u.pw_uid)
    except:
        return


class UnitRep(object):
    
    LETTER_MULTIPLIERS = 'kMGTPEZY'
    VAL_REGEXP = ('([-+]?(?:[0-9]*\.?[0-9]+)(?:[eE][-+]?[0-9]+)?)([%s])([a-zA-Z]*\/?[a-zA-Z]+)' % LETTER_MULTIPLIERS)
    
    @staticmethod
    def val2str(value, unit='B'):
        
        step = 1024 if unit is 'B' else 1000
        
        value = float(value)
        i = 0
        while value >= step:
            value /= step
            i += 1
        
        valstr = ("%0.2f" % value).rstrip('0').rstrip('.')
        if i > 0:
            return "%s%s%s" % (valstr, UnitRep.LETTER_MULTIPLIERS[i - 1], unit)
                              
        return "%s%s" % (valstr, unit)  

    @staticmethod
    def str2val(valstr):
        try:
            return float(valstr)
        except:
            pass
        
        res = re.findall(UnitRep.VAL_REGEXP, valstr)
#         try:
        assert len(res) == 1
        res = res[0]
        assert len(res) <= 3
        val = float(res[0]) 
        ind = UnitRep.LETTER_MULTIPLIERS.find(res[1])
        step = 1024 if len(res) > 2 and (res[2] == 'B' or res[2].startswith('B/') or res[2] == 'Bps') else 1000
        
        val *= pow(step, ind + 1)
#         except:
#             raise AttributeError("Badly formatted string: %s", valstr)
        
        return val
    
    
class MethodNameConversion(object):
    
    CAMELCASING_RE = re.compile('((?<=[a-z0-9])[A-Z]|(?!^)[A-Z](?=[a-z]))')
    UNDERSCORE_RE = re.compile('_')

    @staticmethod
    def camelcase2underscore(camel):
        return MethodNameConversion.CAMELCASING_RE.sub(r'_\1', camel).lower()

    @staticmethod
    def underscore2camelcase(underscore):
        return ''.join(MethodNameConversion.UNDERSCORE_RE.sub(" ", underscore).title().split(' '))


class Enum(object):
    """Generic enumeration.

    Derive from this class to define new enumerations.

    """
    def __new__(cls, value):
        # all enum instances are actually created during class construction
        # without calling this method; this method is called by the metaclass'
        # __call__ (i.e. Color(3) ), and by pickle
        if type(value) is cls:
            # For lookups like Color(Color.red)
            return value
        # by-value search for a matching enum member
        # see if it's in the reverse mapping (for hashable values)
        try:
            if value in cls._value2member_map_:
                return cls._value2member_map_[value]
        except TypeError:
            # not there, now do long search -- O(n) behavior
            for member in cls._member_map_.values():
                if member.value == value:
                    return member
        raise ValueError("%s is not a valid %s" % (value, cls.__name__))

    def __repr__(self):
        return "<%s.%s: %r>" % (
                self.__class__.__name__, self._name_, self._value_)

    def __str__(self):
        return "%s.%s" % (self.__class__.__name__, self._name_)

    def __dir__(self):
        added_behavior = [m for m in self.__class__.__dict__ if m[0] != '_']
        return (['__class__', '__doc__', '__module__', 'name', 'value'] +
                added_behavior)

    def __format__(self, format_spec):
        # mixed-in Enums should use the mixed-in type's __format__, otherwise
        # we can get strange results with the Enum name showing up instead of
        # the value

        # pure Enum branch
        if self._member_type_ is object:
            cls = str
            val = str(self)
        # mix-in branch
        else:
            cls = self._member_type_
            val = self.value
        return cls.__format__(val, format_spec)

    def __hash__(self):
        return hash(self._name_)

    def __reduce_ex__(self, proto):
        return self.__class__, (self._value_, )

    # DynamicClassAttribute is used to provide access to the `name` and
    # `value` properties of enum members while keeping some measure of
    # protection from modification, while still allowing for an enumeration
    # to have members named `name` and `value`.  This works because enumeration
    # members are not set directly on the enum class -- __getattr__ is
    # used to look them up.

    @property
    def name(self):
        """The name of the Enum member."""
        return self._name_

    @property
    def value(self):
        """The value of the Enum member."""
        return self._value_


