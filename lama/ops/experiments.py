#!/usr/bin/env python3

"""This module allows defining a scenario, running an experiment and collecting and analyzing statistics."""

from argparse import ArgumentParser, ArgumentTypeError
from functools import partial

from collections import deque
import random
import signal
import subprocess
import sys

from lama.generators.app_generators import ResourceSetFactory
from os.path import exists
import traceback
import json
from time import strftime, gmtime
from config.settings import PATH_GML, PATH
from lama.ops.apps import AppType
from lama.ops.appcontrol import AppLaunch, InvalidAppException, AppOperations
from lama.scripts.lama_interface import LamaAPI
from twisted.internet import reactor
from twisted.web.resource import Resource
from twisted.web.server import Site, NOT_DONE_YET
from lama.utils.logging_lama import logger, LamaLogger

__author__ = 'tiago'


class AppDistribution(object):

    def __init__(self, probs, apps):
        self.probs, self.apps = probs, apps

    @staticmethod
    def factory(type_="Uniform", *args):
        if type_ == "Uniform":
            return UniformAppDistribution()
        elif type_ == "Custom":
            return CustomAppDistribution(*args)

    def random_app(self):
        n = random.random()
        v = None
        for k, v in zip(self.probs, self.apps):
            if n < k:
                return v
        return v


class UniformAppDistribution(AppDistribution):
    """Default distribution.
    """

    def __init__(self):
        apps = sorted([c.__name__ for c in AppType.__subclasses__()])
        probs = [(p + 1) * 1/len(apps) for p in range(len(apps))]
        super().__init__(probs, apps)


class CustomAppDistribution(AppDistribution):

    def __init__(self, custom_dist):
        probs = list(custom_dist.values())
        apps = list(custom_dist.keys())
        probs, apps = (list(t) for t in zip(*sorted(zip(probs, apps))))

        tot = sum(probs)
        cumul = 0
        for a in apps:
            cumul += custom_dist[a]
            probs.append(cumul/tot)
        super().__init__(probs, apps)


class Experiment(object):
    """Base class to define experiments."""

    def __init__(self, dispatcher, n_apps=None, distr=None, name=None, runtime=10):
        self.apps_launched = True
        self.distr = distr
        self.apps = self.select_apps(n_apps)
        self.apps_to_launch = deque(self.apps)
        self.name = name or self.auto_name()
        self.dispatcher = dispatcher
        self.runtime = runtime

    def select_apps(self, n):
        if not self.distr:
            return []

        apps = [self.distr.random_app() for _ in range(n)]
        if apps:
            self.apps_launched = False
        return apps

    def launch_apps(self):
        logger.debug("Launch app from experiment: %s", self.name)

        app_name = self.apps_to_launch.popleft()
        if len(self.apps_to_launch):
            self.launch_app(app_name, cb_finished=lambda x: self.launch_apps())
        else:
            self.launch_app(app_name, cb_finished=lambda x: self.__apps_launched())

    def launch_app(self, app_name, app_type=None, flavor=None, cb_finished=None, make_unique=True, config=None):
        if config is None:
            config = {}
        logger.info("Launch app: name=%s, type=%s, flavor=%s", app_name, app_type, flavor)
        app_type = app_type or app_name
        try:
            filename = "%s/%s.gml" % (PATH_GML, app_type)
            app = AppType(gml_file=filename)
        except:
            raise InvalidAppException(app_type)
        try:
            suffix = "%06d" % app.app_nid if make_unique else None
            launch = AppLaunch(gml_file=app.gml, suffix=suffix, app_name=app_name, flavor=flavor, config=config)
            launch.cb_created = launch.create_and_upload_images
            launch.cb_images_uploaded = launch.start
            if cb_finished:
                launch.cb_started = lambda: cb_finished(launch.app_name)
            else:
                logger.debug("No callback for after setup/start the app")

            launch.create_app(self.dispatcher)
        except InvalidAppException as e:
            raise e
        except Exception as e:
            logger.warn("Unable to launch app with input argument '%s'", app_name)
            try:
                logger.error("Traceback: %s", traceback.format_exc())
            except:
                logger.warn("Unable to get traceback! Exception: %s", e)
            raise InvalidAppException(app_name)

        return launch.app_name, launch.provider

    def __apps_launched(self):
        self.apps_launched = True

    def start(self):
        reactor.callLater(self.runtime, self.stop)
        reactor.callWhenRunning(self.launch_apps)

        from twisted.python import log
        log.startLogging(sys.stdout)

        reactor.run()

    def __can_terminate(self):
        return self.apps_launched

    def hard_stop(self, signal, frame):
        logger.debug("Hard st op (runtime was %s): apps launched: %s", self.runtime, self.apps_launched)
        reactor.stop()

    def stop(self, first=True):
        if first:
            logger.debug("Stopping experiment (runtime was %s): apps launched: %s", self.runtime, self.apps_launched)
        if self.__can_terminate():
            reactor.stop()
        else:
            reactor.callLater(1, self.stop, first=False)

    def auto_name(self):
        # return "%s_Exp_%dApps_%s" % (
        #     datetime.now().strftime("%Y%m%d%H%M%S"),
        #     len(self.apps),
        #     self.distr.__class__.__name__
        # )
        return "Exp - %d Apps - %s" % (
            len(self.apps),
            self.distr.__class__.__name__
        )

    def launch_client(self, appname, config):
        api = LamaAPI(self.dispatcher, appname)
        return api.change_workload_clients(config)


class OperationResponse(object):
    def __init__(self, complete=True, result=False, msg="", **kwargs):
        self.complete = complete
        self.result = result
        self.msg = msg
        self.__dict__.update(kwargs)

    def as_json(self, encoding="UTF-8"):
        j = json.dumps(self.__dict__)
        return j.encode(encoding) if encoding else j


class ExperimentOperator(object):

    def __init__(self, dispatcher):
        self.__processors = {
            "start-app": self.process_start_app,
            "check-status": self.process_check_status,
            "launch-client": self.process_launch_client,
            "add-instance": self.process_add_instance,
            "instance-info": self.process_instance_info
        }
        self.exp = Experiment(dispatcher)
        self.app_status = {}
        self.app_info = {}

    def process_request(self, action, cb_completed=None, **kwargs):
        logger.temp("process request: action=%s, kwargs=%s", action, kwargs)
        if action not in self.__processors:
            return OperationResponse(msg="Invalid action.")

        try:
            return self.__processors[action](cb_completed=cb_completed, **kwargs)
        except TypeError as e:
            logger.error("Exception: %s" % traceback.format_exc())
            return OperationResponse(msg="Bad arguments: %s" % e)

    def convert_to_lama_flavor(self, flavor):
        if isinstance(flavor, ResourceSetFactory.Level):
            return flavor

        if isinstance(flavor, str):
            try:
                if flavor.startswith("m1."):
                    return getattr(ResourceSetFactory.Level, "OS_%s" % flavor[3:].upper())
            except:
                logger.error("Unable to convert flavor: %s", traceback.format_exc())

        logger.warn("Unable to retrieve flavor: %s", flavor)
        return None

    def process_start_app(self, appname, apptype, flavor=None, make_unique=True, config=None, cb_completed=None):
        if config is None:
            config = {}
        try:
            # translate flavor:
            if flavor:
                flavor = self.convert_to_lama_flavor(flavor)

            # final_app_name = self.exp.launch_app(appname, app_type=apptype, flavor=flavor, cb_finished=self.process_app_deployed, make_unique=make_unique)
            logger.debug("Start app")
            final_app_name, provider = self.exp.launch_app(appname, app_type=apptype, flavor=flavor,
                                                           cb_finished=partial(self.process_app_deployed,
                                                                               cb_completed=cb_completed),
                                                           make_unique=make_unique,
                                                           config=config)
            self.app_status[final_app_name] = False
            self.app_info[final_app_name] = provider[0]
            # if cb_completed:
            #     self.app_status[final_app_name] = False
            #     return None
            # else:
            logger.debug("Started app - name=%s, provider=%s", final_app_name, provider)
            output = OperationResponse(result=True, complete=False, msg="App launched with name: %s" % final_app_name)
            output.appname = final_app_name
            output.provider = provider[0]
            return output
        except InvalidAppException as e:
            logger.debug("Traceback: %s", traceback.format_exc())
            return OperationResponse(complete=True, result=False, msg="Invalid app: %s" % e)
        except:
            logger.error("Unexpected exception: %s" % traceback.format_exc())
            return OperationResponse(msg="Not implemented!")

    def process_add_instance(self, appname, service_name, cb_completed=None):
        logger.info("Add instance: [app=%s, service=%s]", appname, service_name)
        app_op = AppOperations(appname)
        instance_name = app_op.add_instance(service_name)
        logger.info("Add instanceed: [app=%s, service=%s, instance=%s]", appname, service_name, instance_name)
        if instance_name:
            return OperationResponse(result=True, msg=instance_name)
        else:
            return OperationResponse(msg='No instance returned.')

    def process_instance_info(self, appname, instance_name, cb_completed=None):
        app_op = AppOperations(appname)
        instance = app_op.get_instance_info(instance_name)
        if instance:
            return OperationResponse(result=True, msg=instance)
        else:
            return OperationResponse(msg='No instance returned.')

    def process_app_deployed(self, appname, cb_completed=None):
        logger.info("App deployed: %s %s", appname, "(unknown)" if appname not in self.app_status else "")
        self.app_status[appname] = True
        if cb_completed:
            if appname:
                cb_completed(OperationResponse(result=True, msg="App launched with name: %s" % appname, appname=appname))
            else:
                cb_completed(OperationResponse(msg="App launched failed"))

    def process_check_status(self, appname, cb_completed=None):
        if appname in self.app_status:
            return OperationResponse(complete=self.app_status[appname], result=True, msg="Status updated @ %s" % strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                     provider = self.app_info[appname])

        return OperationResponse(msg="App not found: %s" % appname)

    def process_launch_client(self, appname, config, cb_completed=None):
        results = self.exp.launch_client(appname, config)
        if results:
            # logger.info(results)
            # if isinstance(results, tuple):
            #     logger.info("tuple")
            #     result, msg = results
            #     return OperationResponse(result=result, msg=msg)
            # elif isinstance(results, bool):
            #     logger.info("bool")
            #     return OperationResponse(result=results, msg="No message returned.")
            # elif isinstance(results, str):
            #     logger.info("str")
            #     return OperationResponse(result=True, msg=results)
            if isinstance(results, dict):
                result = results["result"] if "result" in results else False
                msg = results["msg"] if "msg" in results else "%s" % results
                return OperationResponse(result=result, msg=msg)
            return OperationResponse(result=False, msg="Unkown: %s" % results)
        return OperationResponse(result=False, msg="Failed for unknown reason")


class ExperimentServer(Resource):
    isLeaf = True
    allowedMethods = ('POST', )
    cls_launching = []
    cls_launched = []

    def __init__(self, dispatcher):
        super().__init__()
        self.launching = []
        self.launched = []
        self.processor = ExperimentOperator(dispatcher)

        logger.info("Experiment server listening to requests...")

    # def process_response(self, data):
    #     resp = None
    #     try:
    #         resp = json.dumps(data)
    #     except Exception as e:
    #         logger.info("Exception json.dumps: %s/%r" % (e, e))
    #         resp = json.dumps({"result": False, "error": "JSON error", "original": str(data)})
    #     finally:
    #         assert resp, "ERROR: Unable to compute response. Not replying to client!"
    #         resp = resp.encode("UTF-8")
    #
    #     logger.info("Send response to client: %s" % resp)
    #     return resp

    def render_OPTIONS(self, request):
        request.setHeader(b'Access-Control-Allow-Origin', '*')
        request.setHeader(b'Access-Control-Allow-Headers', 'Content-Type, X-CSRFToken')
        request.finish()
        return NOT_DONE_YET

    # noinspection PyPep8Naming
    def render_POST(self, request):
        request.setHeader(b'Access-Control-Allow-Origin', '*')
        request.setHeader(b'Access-Control-Allow-Methods', 'POST')
        request.setHeader(b'Access-Control-Allow-Headers', 'Content-Type, X-CSRFToken')
        request.setHeader(b'Access-Control-Max-Age', 2520)  # 42 hours
        request.setHeader(b'Content-type', 'application/json')

        result = False
        msg = "Unexpected processing! You should not see this!"
        try:

            contenttype = str(request.getHeader(b'content-type'))
            if contenttype.find('application/json') >= 0:

                data = json.loads(request.content.getvalue().decode("UTF-8"))
                logger.info("Received new request from %s:", request.getClientIP())
                logger.info("\tApp name:%s" % data.get('appname'))
                logger.info("\tApp type:%s" % data.get('apptype'))
                logger.info("\tAction:%s" % data.get('action'))

                if "action" not in data:
                    return OperationResponse(msg="No action set.").as_json()

                try:
                    # logger.info("Accept request: %s" % data)
                    output = self.processor.process_request(cb_completed=partial(self.async_response, request), **data)
                    logger.debug("Output of command: %s", output)
                    if output:
                        return output.as_json()
                    else:
                        logger.warn("Not done yet: request=%s", request)
                        return NOT_DONE_YET

                except Exception as e:
                    logger.warn("Unexpected problem: %s" % traceback.format_exc())
                    return OperationResponse(msg="Unexpected problem: %r" % e).as_json()

            else:
                logger.warn("Received non-JSON request: %s. Ignoring..." % request)
                return OperationResponse(msg="Received non-JSON request: %s. Ignoring..." % request).as_json()
        except:
            logger.warn(traceback.format_exc())
            return OperationResponse(msg="Received non-JSON request: %s. Ignoring..." % request).as_json()

    def async_response(self, request, result):
        logger.debug("async_response: result=%s, request=%s", result, request)
        if request:
            try:
                request.write(result.as_json())
                request.finish()
            except RuntimeError:
                logger.warn(" * it looks like response was sent before *")


    @classmethod
    def start(cls, dispatcher, port, standalone=True):
        web = Site(cls(dispatcher))
        # noinspection PyUnresolvedReferences
        reactor.listenTCP(port, web)
        if standalone:
            reactor.run()


# custom args
def arg_distribution(argstr):
    fields = argstr.split(",")
    if len(fields) % 2:
        raise ArgumentTypeError()
    distr = dict()
    tot = 0
    apps = [c.__name__ for c in AppType.__subclasses__()]
    for i in range(0, len(fields), 2):
        frq = float(fields[i])
        app = fields[i+1]
        if app not in apps:
            filename = "%s/%s.gml" % (PATH_GML, app)
            if not exists(filename):
                raise ArgumentTypeError("No app named '%s' or filename '%s'" % (app, filename))
        tot += frq
        distr[app] = frq

    return {k: v/tot for k, v in distr.items()}


def call_stop(signal, frame):
    logger.debug("called stop")
    exit()


def launch_experiment_server(dispatcher_ip=None):
    args = ["python3", "-m", "lama.ops.experiments", "--server"]
    if dispatcher_ip:
        args += ["--da", str(dispatcher_ip)]

    try:
        logger.info("Launching experiment server: %s" % args)
        subprocess.Popen(args, close_fds=True, cwd=PATH)
    except:
        logger.error("Error launching experiments (%s). Trace: %s", args, traceback.format_exc())


if __name__ == "__main__":
    # signal.signal(signal.SIGINT, call_stop)
    # signal.pause()

    parser = ArgumentParser(description="Create and run an experiment. "
                                        "Default distribution is uniform accross all available app types.")
    parser.add_argument('--server', dest='server',
                        help="Start experiment server (all other options are ignored except for port)",
                        action='store_true')
    parser.add_argument('-p', type=int, dest='port', help="Port experiment server listens to", default=21000)

    parser.add_argument('-a', dest="apps", help="List available apps.", action='store_true')
    parser.add_argument('-n', type=int, help="Number of apps to deploy", default=1)
    parser.add_argument('-s', type=int, dest="seed", help="Experiment seed (reproducible results).")
    parser.add_argument('--name', type=str, dest="name", help="Custom name for the experiment")
    parser.add_argument('-t', type=int, dest="runtime", help="Custom experiment runtime", default=10)
    parser.add_argument('--da', type=str, dest='dispatcher', help='IP Address of the dispatcher.',
                        default="10.1.1.7")
    # parser.add_argument('--al', action='store_true', help="List app types (other arguments will be ignored).")
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-d', type=arg_distribution,
                       help="Specify a distribution (frequency will be normalized): -d frq1,app1,frq2,app2,...")
    cmdargs = parser.parse_args()

    LamaLogger.set_filename(logger, "experiments")

    if cmdargs.server:
        ExperimentServer.start(cmdargs.dispatcher, cmdargs.port)
    else:
        if cmdargs.apps:
            app_lst = [c.__name__ for c in AppType.__subclasses__()]
            logger.info("Available Apps: %s" % ", ".join(app_lst))
            exit()

        if cmdargs.seed:
            random.seed(cmdargs.seed)

        dist = AppDistribution.factory("Custom", cmdargs.d) if cmdargs.d else AppDistribution.factory("Uniform")
        exp = Experiment(cmdargs.dispatcher, cmdargs.n, dist, name=cmdargs.name, runtime=cmdargs.runtime)
        signal.signal(signal.SIGINT, exp.hard_stop)
        logger.info("Start experiment server [seed=%s]", cmdargs.seed)
        exp.start()


