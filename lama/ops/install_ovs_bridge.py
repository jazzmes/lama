from os import listdir, readlink, sysconf
from os.path import isfile, join, islink, abspath, basename, dirname
import subprocess
from sys import stderr
from datetime import datetime
from shutil import copy

__author__ = 'tiago'


class RunCommandError(Exception):
    pass


class OpenvSwitchWrapper(object):
    IFCFG_PATH = "/etc/sysconfig/network-scripts/ifcfg-"

    @staticmethod
    def run_cmd(cmd, ignore_err=False):
        cmd = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = cmd.communicate()
        if out:
            out = out.decode("utf-8")
        elif err:
            if ignore_err:
                print("Ignoring err: %s" % err.decode("utf-8"))
            else:
                raise RunCommandError(err.decode("utf-8"))
        return out

    @staticmethod
    def get_ovs_br_list():
        out = OpenvSwitchWrapper.run_cmd("ovs-vsctl list-br")
        return out.splitlines()

    @staticmethod
    def create_ovs_br(iface=None, ovsbr_name=None):
        if not iface and not ovsbr_name:
            stderr.write("Create OVS bridge :: One of two arguments (iface, ovsbr_name) must be specified")
        ovsbr_name = ovsbr_name or "br-%s" % iface
        OpenvSwitchWrapper.run_cmd("ovs-vsctl add-br %s" % ovsbr_name)

    @staticmethod
    def get_ovs_br_properties(iface, ovsbr):
        return {
            "DEVICE": ovsbr,
            "TYPE": "OVSBridge",
            # "BOOTPROTO": "dhcp",
            "ONBOOT": "yes",
            "DEVICETYPE": "ovs",
            "OVSBOOTPROTO": "dhcp",
            "OVSDHCPINTERFACES": iface["name"],
            "DELAY": 0,
            "NM_CONTROLLED": "no",
            "HOTPLUG": "no",
            "MACADDR": iface["address"],
            "OVS_EXTRA": "\"set bridge %s other-config:hwaddr=$MACADDR\"" % ovsbr
        }

    @staticmethod
    def get_phy_iface_properties(iface, ovsbr):
        return {
            "DEVICE": iface["name"],
            "ONBOOT": "yes",
            "TYPE": "OVSPort",
            "DEVICETYPE": "ovs",
            "OVS_BRIDGE": ovsbr,
            "BOOTPROTO": "none",
            # "HWADDR": iface["address"],
            # "MTU": 1600,
            "NM_CONTROLLED": "no"
        }

    @staticmethod
    def backup(file):
        # get current date
        cur_date = datetime.now().strftime("%Y%m%d%H%M%S")
        # get base folder
        base_dir = dirname(file)
        bfile = "bkp.%s.%s" % (cur_date, basename(file))
        copy(file, join(base_dir, bfile))

    @staticmethod
    def configure_bridge(iface, ovsbr):
        ovsbr_filename = "%s%s" % (OpenvSwitchWrapper.IFCFG_PATH, ovsbr)
        phyiface_filename = "%s%s" % (OpenvSwitchWrapper.IFCFG_PATH, iface["name"])

        if isfile(ovsbr_filename):
            OpenvSwitchWrapper.backup(ovsbr_filename)
        if isfile(phyiface_filename):
            OpenvSwitchWrapper.backup(phyiface_filename)

        with open(ovsbr_filename, "w") as fp:
            fp.write("# File create by LAMA script (%s)\n" % basename(__file__))
            for key, value in OpenvSwitchWrapper.get_ovs_br_properties(iface, ovsbr).items():
                fp.write("%s=%s\n" % (key, value))

        with open(phyiface_filename, "w") as fp:
            fp.write("# File create by LAMA script (%s)\n" % basename(__file__))
            for key, value in OpenvSwitchWrapper.get_phy_iface_properties(iface, ovsbr).items():
                fp.write("%s=%s\n" % (key, value))

    @staticmethod
    def start_openvswitch():
        # disable and stop NetworkManager
        print("Stop NetworkManager")
        out = OpenvSwitchWrapper.run_cmd("systemctl stop NetworkManager")
        if out:
            print(out)
        out = OpenvSwitchWrapper.run_cmd("systemctl disable NetworkManager")
        if out:
            print(out)

        # enable and start network
        print("Start network.service")
        out = OpenvSwitchWrapper.run_cmd("systemctl enable network.service", ignore_err=True)
        if out:
            print(out)
        out = OpenvSwitchWrapper.run_cmd("systemctl start network.service")
        if out:
            print(out)

        # enable and start openvswitch
        print("Start openvswitch")
        out = OpenvSwitchWrapper.run_cmd("systemctl enable openvswitch", ignore_err=True)
        if out:
            print(out)
        out = OpenvSwitchWrapper.run_cmd("systemctl start openvswitch")
        if out:
            print(out)


def get_physical_interfaces():
    net_dev_path = "/sys/class/net"
    net_dev_status_path = "operstate"
    net_dev_address_path = "address"
    devs = []
    for f in listdir(net_dev_path):
        file = join(net_dev_path, f)
        if islink(file):
            dev_dir = abspath(join(net_dev_path, readlink(file)))
            if "virtual" not in dev_dir:
                try:
                    with open(join(dev_dir, net_dev_status_path), 'r') as fp:
                        status = fp.readline().strip()
                except FileNotFoundError:
                    status = "N/A"

                try:
                    with open(join(dev_dir, net_dev_address_path), 'r') as fp:
                        address = fp.readline().strip()
                except FileNotFoundError:
                    address = "N/A"

                dev = basename(dev_dir)
                devs.append({"name": dev, "status": status, "address": address})
                # print("Interface %s is %s" % (dev, status))
    return devs


def test():
    # use first found physical interface
    iface = None
    for iface in get_physical_interfaces():
        if iface["status"] == "up":
            break

    # print("%s: %s" % (iface, status))

    if iface:
        # start openvswitch
        OpenvSwitchWrapper.start_openvswitch()

        # get ovs bridges
        brs = OpenvSwitchWrapper.get_ovs_br_list()

        # if does not exist create
        ovsbr_name = "br-%s" % iface["name"]
        if ovsbr_name not in brs:
            OpenvSwitchWrapper.create_ovs_br(iface["name"], ovsbr_name)

            brs = OpenvSwitchWrapper.get_ovs_br_list()
        print(brs)

        if ovsbr_name in brs:
            # configure bridge
            OpenvSwitchWrapper.configure_bridge(iface, ovsbr_name)

            # should verify ports and connection and connectivity

    else:
        print("Could not find an active interface!")


if __name__ == "__main__":
    test()
