__author__ = 'tiago'


class AppType(object):

    _APP_COUNTER = 0

    def __init__(self, gml_file):
        self.gml = gml_file
        AppType._APP_COUNTER += 1
        self.app_nid = AppType._APP_COUNTER


class Rubbos(AppType):
    gml_file = "lama/ops/app_gml/rubbos-nolb.gml"

    def __init__(self):
        super().__init__(Rubbos.gml_file)


class Cmart(AppType):
    gml_file = "lama/ops/app_gml/cmart-2-tier-sql.gml"

    def __init__(self):
        super().__init__(Cmart.gml_file)

class EmptyApp(AppType):

    def __init__(self):
        super().__init__(None)