from lxml import etree

__author__ = 'tiago'

xmlstr = '<domain type="kvm">' \
         '  <name>test</name>' \
         '  <uuid>422b5d84-78b8-4270-849e-581e331c7ace</uuid>' \
         '  <memory>1048576</memory>' \
         '  <currentMemory>1048576</currentMemory>' \
         '  <vcpu>1</vcpu>' \
         '  <os>' \
         '    <type arch="x86_64">hvm</type>' \
         '    <boot dev="hd"/>' \
         '  </os>' \
         '  <features>' \
         '    <acpi/>' \
         '    <apic/>' \
         '    <pae/>' \
         '  </features>' \
         '  <cpu mode="custom" match="exact">' \
         '    <model>Penryn</model>' \
         '  </cpu>' \
         '  <clock offset="utc">' \
         '    <timer name="rtc" tickpolicy="catchup"/>' \
         '    <timer name="pit" tickpolicy="delay"/>' \
         '    <timer name="hpet" present="no"/>' \
         '  </clock>' \
         '  <on_poweroff>destroy</on_poweroff>' \
         '  <on_reboot>restart</on_reboot>' \
         '  <on_crash>restart</on_crash>' \
         '  <devices>' \
         '    <emulator>/usr/bin/qemu-kvm</emulator>' \
         '    <disk type="file" device="disk">' \
         '      <driver name="qemu" type="qcow2"/>' \
         '      <source file="/var/lib/libvirt/images/lama/rubbos_img_apache.qcow2"/>' \
         '      <target dev="hda" bus="ide"/>' \
         '    </disk>' \
         '    <controller type="usb" index="0" model="ich9-ehci1"/>' \
         '    <controller type="usb" index="0" model="ich9-uhci1">' \
         '      <master startport="0"/>' \
         '    </controller>' \
         '    <controller type="usb" index="0" model="ich9-uhci2">' \
         '      <master startport="2"/>' \
         '    </controller>' \
         '    <controller type="usb" index="0" model="ich9-uhci3">' \
         '      <master startport="4"/>' \
         '    </controller>' \
         '    <interface type="bridge">' \
         '      <source bridge="ovs-bridge"/>' \
         '      <mac address="52:54:00:3e:6a:54"/>' \
         '    </interface>' \
         '    <input type="mouse" bus="ps2"/>' \
         '    <console type="pty"/>' \
         '  </devices>' \
         '</domain>'

root = etree.fromstring(xmlstr)
interfaces = root.findall(".//devices/interface[@type='bridge']")
print(interfaces)
for iface in interfaces:
    src = iface.find(".//source")
    if src is not None and src.get("bridge") and src.get("bridge").startswith("ovs"):
        etree.SubElement(iface, "virtualport", type='openvswith')


print(etree.tostring(root).decode("utf-8"))
