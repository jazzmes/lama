graph [
    name "rubbos"
    label "RuBBoS (MySQL)"
    directed 1
    connector rubbos
    strategy resilience_graph

    node [
        id client
        label "Client Generator"
        image "../rubbos-client.qcow2"
        subtype "client"
        user "lama"
        password "lama"
    ]

    node [
        id apache
        label "Apache Web Tier"
        image "../rubbos-apache.qcow2"
        scaling auto
        scaling_port 80
        user "lama"
        password "lama"
    ]

    edge [
        source client
        target apache
        label "Client Flow"
        type client
    ]

    node [
        id "mysql"
        label "MySQL"
        image "../rubbos-mysql.qcow2"
        user "lama"
        password "lama"
    ]

    edge [
        source apache
        target mysql
        label "DB Flow"
        type client
    ]

]