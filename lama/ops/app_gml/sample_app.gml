graph [
    name "3_tier_app_gen"
    label "3-Tier App"
    directed 1

    node [
        id web
        label "Web Tier"
        image "/Users/tiago/Downloads/utopic-server-cloudimg-amd64-disk1.img"
    ]

    node [
        id app
        label "App Tier"
        image "/Users/tiago/Downloads/utopic-server-cloudimg-amd64-disk1.img"
    ]

    node [
        id db
        label "DB Tier"
        image "/Users/tiago/Downloads/utopic-server-cloudimg-amd64-disk1.img"
    ]

    edge [
        source web
        target app
        label "Web-App Flow"
    ]

    edge [
        source app
        target db
        label "App-DB Flow"
    ]

]