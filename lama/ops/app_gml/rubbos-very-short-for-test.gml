graph [
    name "rubbos"
    label "RuBBoS (no LB)"
    directed 1
    connector rubbos

    node [
        id apache
        label "Apache Web Tier"
        image "/home/lama/vm_images/rubbos/rubbos-apache-tc.qcow2"
        subtype "client"
        scaling auto
        scaling_port 80
        # scaling "auto" will create a load balancer node before
        user "lama"
        password "llama"
    ]

    node [
        id client
        label "Client Generator"
        image "/home/lama/vm_images/rubbos/rubbos-client-tc.qcow2"
        user "lama"
        password "llama"
    ]

    edge [
        source client
        target apache
        label "Client Flow"
        type client
    ]

]