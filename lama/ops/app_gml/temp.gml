graph [
    name "cmart"
    label "CMART deployment"
    directed 1

    node [
        id client
        label "Client Generator"
        image "/home/lama/cmart_images/new/cmart-client.qcow2"
        subtype "client"
        connector "CmartAppConnector"
        user "root"
        password "Cm4rt!"
        config '{"full_url1": "http://%(entry_node)s:80/cmart-1"}'
    ]

    node [
        id lb
        label "Load Balancer"
        image "/home/lama/cmart_images/new/cmart-lb.qcow2"
    ]

    node [
        id tomcat
        label "Tomcat App Server"
        image "/home/lama/cmart_images/new/cmart-tomcat.qcow2"
    ]

    node [
        id mysql
        label "MySQL DB Server"
        image "/home/lama/cmart_images/new/cmart-mysql.qcow2"
    ]

    edge [
        source client
        target lb
        label "Client Flow"
    ]

    edge [
        source lb
        target tomcat
        label "LB-Tomcat Flow"
    ]

    edge [
        source tomcat
        target mysql
        label "App-SQLDB Flow"
    ]
]