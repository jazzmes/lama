graph [
    name "cirros"
    label "CirrOS (no LB)"
    directed 1
    connector cirros
    manual 1

    node [
        id cirros
        label "Simple CirrOS instance"
        image "/home/lama/vm_images/cirros.qcow2"
        user "cirros"
        password "cubswin:)"
    ]

]