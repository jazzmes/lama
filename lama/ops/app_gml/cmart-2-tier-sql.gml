graph [
    name "cmart"
    label "CMART deployment"
    directed 1

    node [
        id client
        label "Client Generator"
        image "/home/lama/cmart_images/Client.qcow2"
    ]

    node [
        id tomcat
        label "Tomcat App Server"
        image "/home/lama/cmart_images/Application.qcow2"
    ]

    node [
        id mysql
        label "MySQL DB Server"
        image "/home/lama/cmart_images/SQL.qcow2"
    ]

    edge [
        source client
        target tomcat
        label "App-SQLDB Flow"
    ]

    edge [
        source tomcat
        target mysql
        label "App-SQLDB Flow"
    ]

]