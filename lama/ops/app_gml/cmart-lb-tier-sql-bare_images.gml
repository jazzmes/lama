graph [
    name "cmart"
    label "CMART deployment"
    directed 1

    node [
        id client
        label "Client Generator"
        image "/home/lama/vm_images/lama-base-cirros.qcow2"
        subtype "client"
        connector "CmartAppConnector"
        user "cirros"
        password "cubswin:)"
    ]

    node [
        id lb
        label "Load Balancer"
        image "/home/lama/vm_images/lama-base-cirros.qcow2"
    ]

    node [
        id tomcat
        label "Tomcat App Server"
        image "/home/lama/vm_images/lama-base-cirros.qcow2"
    ]

    node [
        id mysql
        label "MySQL DB Server"
        image "/home/lama/vm_images/lama-base-cirros.qcow2"
    ]

    edge [
        source client
        target lb
        label "Client Flow"
    ]

    edge [
        source lb
        target tomcat
        label "LB-Tomcat Flow"
    ]

    edge [
        source tomcat
        target mysql
        label "App-SQLDB Flow"
    ]
]