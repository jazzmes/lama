graph [
    name "test"
    label "Test (Minimal cirros images)"
    directed 1

    node [
        id tier1
        label "Tier 1"
        image "/home/lama/vm_images/lama-base-cirros.qcow2"
    ]

    node [
        id tier2
        label "Tier 2"
        image "/home/lama/vm_images/lama-base-cirros.qcow2"
    ]

    edge [
        source tier1
        target tier2
        label "Flow 1_2"
    ]

]