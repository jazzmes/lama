graph [
    name "rubbos"
    label "RuBBoS (no LB)"
    directed 1
    connector rubbos

    node [
        id client
        subtype client
        label "Client Generator"
        image "/home/lama/vm_images/rubbos/rubbos-client-tc.qcow2"
        user "lama"
        password "llama"
    ]

    node [
        id apache
        label "Apache Web Tier"
        image "/home/lama/vm_images/rubbos/rubbos-apache-tc.qcow2"
        scaling auto
        scaling_port 80
        # scaling "auto" will create a load balancer node before
        user "lama"
        password "llama"
    ]

    edge [
        source client
        target apache
        label "Client Flow"
        type client
    ]

    node [
        id "mysqlc-man"
        label "MySQL Cluster Manager"
        image "/home/lama/vm_images/rubbos/rubbos-mysqlc-man.qcow2"
        image_init "/home/lama/vm_images/rubbos/rubbos-mysqlc-man.init.qcow2"
        prefix_data_nodes "mysqlc-data"
        prefix_api_nodes "mysqlc-sql"
        config "{'DataMemory': '1G', 'IndexMemory': '200M'}"
        user "lama"
        password "llama"
    ]

    node [
        id "mysqlc-data"
        label "MySQL Cluster Data"
        image "/home/lama/vm_images/rubbos/rubbos-mysqlc-data.qcow2"
        image_init "/home/lama/vm_images/rubbos/rubbos-mysqlc-data.init.qcow2"
#        scaling_min_instances 2
#        scaling_increment 2
        resource_ram "2GB"
    ]

    node [
        id "mysqlc-sql"
        label "MySQL Cluster SQL"
        image "/home/lama/vm_images/rubbos/rubbos-mysqlc-sql.qcow2"
        image_init "/home/lama/vm_images/rubbos/rubbos-mysqlc-sql.init.qcow2"
        scaling auto
        scaling_port 3306
        resource_ram "1GB"
    ]

    edge [
        source apache
        target "mysqlc-sql"
        label "DB Cluster Flow"
        type client
    ]

    edge [
        source "mysqlc-sql"
        target "mysqlc-data"
        label "DB SQL Data Flow"
    ]

    edge [
        source "mysqlc-man"
        target "mysqlc-sql"
        label "DB Manage Flow - SQL"
    ]

    edge [
        source "mysqlc-man"
        target "mysqlc-data"
        label "DB Manage Flow - Data"
    ]

]

