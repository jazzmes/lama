from functools import partial
from time import sleep
from networkx.readwrite.gml import read_gml
import os
import traceback
from os.path import join
from requests_toolbelt.multipart import MultipartEncoder
from config.settings import DISPATCHER_AG_WEB_PORT, PROVIDER_AG_WEB_PORT, PROVIDER_AG_PORT
from config.settings_dispatcher import MIDDLEWARE_SERVICES_FOLDER
from lama.agent.app.app_arch import AppLogicalSpec
from lama.agent.event_managers.twisted import FileProtocolFactory
from lama.agent.specs.resource import ResSpec
from lama.generators.app_generators import ResourceSetFactory
import requests
import json
from argparse import ArgumentParser
import ntpath
from lama.utils.logging_lama import logger

__author__ = 'tiago'


class InvalidAppException(Exception):
    pass


class Provider(object):

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port


class AppOperations(object):

    dispatcher = '10.1.1.12'
    dispatcher_port=DISPATCHER_AG_WEB_PORT

    def __init__(self, app_name):
        self._app_name = app_name
        self._provider = None

    def get_app_provider(self):
        url = 'http://%s:%s/' % (self.dispatcher, self.dispatcher_port)
        try:
            response = requests.post(url, json={'action': 'app_info', 'app_name': self._app_name}, timeout=3).json()
        except: # (requests.ConnectTimeout, requests.ReadTimeout, requests.ConnectionError) as e:
            logger.info("[%s] Connection to dispatcher failed (%s:%s)!", self._app_name, self.dispatcher, self.dispatcher_port)
            logger.error("[%s] Traceback: %s", self._app_name, traceback.format_exc())
            return

        self._provider = Provider(response.get('app', {}).get('provider'), response.get('app', {}).get('provider_port'))
        return self._provider

    def add_instance(self, service_name):
        if not self._provider:
            self.get_app_provider()

        url = 'http://%s:%s/' % (self._provider.ip, self._provider.port)
        try:
            response = requests.post(url,
                                     json={
                                         'action': 'add_instance',
                                         'app_name': self._app_name,
                                         'service_name': service_name
                                     },
                                     timeout=3).json()
        except: # (requests.ConnectTimeout, requests.ReadTimeout, requests.ConnectionError) as e:
            logger.info("[%s] Connection to provider failed (%s:%s)!", self._app_name, self._provider.ip, self._provider.port)
            logger.error("[%s] Traceback: %s", self._app_name, traceback.format_exc())
            return
        logger.debug("Response: %s", response.get('msg'))
        return response.get('msg')

    def get_instance_info(self, instance_name):
        if not self._provider:
            self.get_app_provider()

        url = 'http://%s:%s/' % (self._provider.ip, self._provider.port)
        try:
            response = requests.post(url,
                                     json={
                                         'action': 'instance_info',
                                         'app_name': self._app_name,
                                         'instance_name': instance_name
                                     },
                                     timeout=3).json()
        except (requests.ConnectTimeout, requests.ReadTimeout, requests.ConnectionError) as e:
            logger.info("[%s] Connection to provider failed (%s:%s)!", self._app_name, self._provider.ip, self._provider.port)
            return
        logger.debug("[%s] Response: %s", self._app_name, response.get('msg'))
        return response.get('msg')


class AppLaunch(object):

    def __init__(self, gml_file=None, suffix=None, app_name=None, flavor=ResourceSetFactory.Level.SMALL, config=None):
        self.spec = None
        """:type : AppLogicalSpec"""
        self.app_name = app_name
        self.graph = None
        self.provider = None
        self.suffix = suffix
        self.flavor = flavor
        self.app_config = config

        if gml_file:
            self.read_app_gml(gml_file)
        else:
            raise NotImplementedError("Currently only support GML")

        if not self.spec:
            raise NotImplementedError("No spec defined")

        self.cb_created = None
        self.cb_images_uploaded = None
        self.cb_started = None

        self.tgt_uploads = 0
        self.cnt_uploaded = 0
        self.uploads = []

    def read_app_gml(self, gml_file):
        logger.info("[%s] Read GML (%s)", self.app_name, gml_file)
        # load graph from file (GML) or generate random
        try:
            self.graph = read_gml(gml_file)
        except:
            raise InvalidAppException(gml_file)

        # create app spec
        resource_gen = ResourceSetFactory()
        if not self.app_name:
            self.app_name = self.graph.name

        if self.suffix:
            self.app_name = "%s-%s" % (self.app_name, self.suffix)

        # update with dynamic options
        self.graph.graph.update(self.app_config)

        self.spec = AppLogicalSpec(self.app_name, **self.graph.graph)
        logger.info("[%s] Final name: %s", self.app_name, self.app_name)
        tgt_middleware_subs = {}
        middleware_services = {}
        logger.info(self.graph.nodes())
        for k, n in self.graph.nodes_iter(data=True):
            # args = {k: v for k, v in n.items() if k not in {"id", "subtype", "image", "label"}}
            args = {k: v for k, v in n.items() if k not in {"id", "image", "label"}}
            scaling = n.get("scaling")
            if scaling == "auto":
                port = n.get("scaling_port")
                if not port:
                    port = 80
                    args["scaling_port"] = port
                else:
                    try:
                        port = int(port)
                        assert port > 0
                    except Exception:
                        raise InvalidAppException("Scaling port (scaling_port)"
                                                  " must be a positive integer (value received: %s)", port)

                mw_name = "lb-%s" % n['id']
                self.spec.add_service(
                    mw_name, resource_gen.fixed_flavor(ResourceSetFactory.Level.SMALL),
                    subtype="lb",
                    connector='LbConnector'
                )
                tgt_middleware_subs[n['id']] = mw_name
                # if port == 80:
                #     middleware_services[mw_name] = "lb-apache"
                # else:
                middleware_services[mw_name] = "lb"

            if not self.flavor:
                self.flavor = ResourceSetFactory.Level.SMALL
            resources = resource_gen.fixed_flavor(flavor=self.flavor)

            # set non-default resources
            for label, value in args.items():
                if label.startswith("resource_"):
                    _, res_type = label.split("_")
                    res = ResSpec.from_key_args(res_type, value)
                    resources.replace_resource_spec(res)
            logger.debug("[%s] Add service: node=%s, resources=%s, args=%s", self.app_name, n['id'], resources, args)
            self.spec.add_service(n['id'], resources, **args)

        for s, t, e in self.graph.edges_iter(data=True):
            if t in tgt_middleware_subs and "type" in e and e['type'] == "client":
                self.spec.add_flow(s, tgt_middleware_subs[t])
            else:
                self.spec.add_flow(s, t)

        for t, s in tgt_middleware_subs.items():
            self.spec.add_flow(s, t)
            self.graph.add_node(
                s,
                id=s,
                subtype=middleware_services[s],
                image=join(MIDDLEWARE_SERVICES_FOLDER, "support-%s.qcow2" % middleware_services[s])
            )

        logger.debug("[%s] App Spec:", self.app_name)
        logger.debug("[%s] ---------------", self.app_name)
        logger.debug("[%s] %s", self.app_name, self.spec)
        logger.debug("[%s] ---------------", self.app_name)

    def create_app(self, dispatcher, dispatcher_port=DISPATCHER_AG_WEB_PORT):
        # j = json.loads(spec.to_json().decode("utf-8"))
        # logger.info(json.dumps(j, indent=4, sort_keys=True))

        # TODO: request new app using spec...
        base_url = 'http://%s:%s/' % (dispatcher, dispatcher_port)

        client = requests.session()

        # url = base_url + "/monitor/app_create/"
        # client.get(url)
        # csrftoken = client.cookies['csrftoken']
        logger.debug("[%s] Url: %s", self.app_name, base_url)
        payload = {'action': 'app_create', 'app_spec': json.loads(self.spec.to_json().decode("utf-8"))} #, "csrfmiddlewaretoken": csrftoken}
        headers = {'content-type': 'application/json'} #, 'X-CSRFToken': csrftoken}
        try:
            r = client.post(base_url, data=json.dumps(payload), headers=headers, cookies=dict(client.cookies),
                            timeout=5)
        except requests.Timeout as e:
            logger.info("Connection to dispatcher (%s:%s) timed out!" % (dispatcher, dispatcher_port))
            client.close()
            return

        # logger.info("\n *** Response *** \n")
        json_resp = json.loads(r.text)
        # logger.info("Result: %s\nMessage: %s" % (json_resp["result"], json_resp["msg"] if "msg" in json_resp else "Success!!"))
        if json_resp["result"]:
            if "app_name" in json_resp and "provider" in json_resp and "port" in json_resp:
                logger.info("[%s] App created [location=%s]", self.app_name, json_resp["provider"])
            else:
                logger.warn("[%s] Error creating [resp=%s]", self.app_name, json.dumps(json_resp, indent=4, sort_keys=True))
        client.close()

        if "provider" not in json_resp:
            logger.warn("[%s] No 'provider' returned for the application!", self.app_name)
            return

        self.provider = (json_resp["provider"], PROVIDER_AG_WEB_PORT)

        # if not "app_name" in json_resp:
        #     logger.info("No 'app_name' returned for the application!")
        #     return

        attempts = 50
        # logger.info("Get app info...")
        logger.info("[%s] Wait for app info", self.app_name)
        resp = self.get_app_info()
        while attempts > 0 and not resp:
            sleep(1)
            resp = self.get_app_info()
            attempts -= 1
        logger.info("[%s] resp=%s", self.app_name, resp)
        if resp and self.cb_created:
            self.cb_created()

    def set_provider(self, provider, provider_port=PROVIDER_AG_WEB_PORT):
        self.provider = (provider, provider_port)

    def get_app_provider(self, dispatcher, dispatcher_port=DISPATCHER_AG_WEB_PORT):
        app_name = self.spec.name

        provider_port = PROVIDER_AG_WEB_PORT
        url = 'http://%s:%s/' % (dispatcher, dispatcher_port)
        payload = {'action': 'app_info', 'app_name': app_name}
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(url, data=json.dumps(payload), headers=headers, timeout=5)
        except (requests.exceptions.ConnectTimeout, requests.exceptions.ReadTimeout) as e:
            logger.warn("[%s] Connection to dispatcher (%s:%s) timed out!", self.app_name, dispatcher, dispatcher_port)
            return None

        json_resp = json.loads(r.text)

        if not "result" in json_resp or not json_resp["result"]:
            logger.info("[%s] Unable to get app info!", self.app_name)

        if not "app" in json_resp or not json_resp["app"]:
            logger.warn("[%s] Unable to get app info: %s", self.app_name, json_resp)
        else:
            self.provider = (json_resp["app"]["provider"], json_resp["app"]["provider_port"])

    def get_app_info(self):
        # sleep(1)
        if not self.spec:
            logger.info("Invalid spec")
            return None

        app_name = self.spec.name

        if not self.provider:
            raise NotImplementedError("Make sure it fails before if there is not provider")

        provider, provider_port = self.provider
        url = 'http://%s:%s/' % (provider, provider_port)
        payload = {'action': 'app_agent_info', 'app_name': app_name}
        headers = {'content-type': 'application/json'}
        # while True:
        json_resp = None
        try:
            logger.debug("[%s] Trying to get app info from %s:%s", self.app_name, *self.provider)
            r = requests.post(url, data=json.dumps(payload), headers=headers, timeout=5)
        except (requests.exceptions.ConnectionError, requests.exceptions.ConnectTimeout, requests.exceptions.ReadTimeout) as e:
            logger.debug("[%s] Connection to provider (%s:%s) timed out!", self.app_name, provider, provider_port)
            return None
                # sleep(1)
                # return None
        else:
            json_resp = json.loads(r.text)
            if not json_resp["result"]:
                logger.debug("[%s] No app info was returned!", self.app_name)
                return None

            logger.info("[%s] Agent running [provider=%s, agent=%s:%s]",
                        self.app_name, provider, json_resp["agent"]["ip_address"], json_resp["agent"]["port"])
        # logger.info([n["name"] for n in json_resp["nodes"] if n["type"] == "logical"])

        # logger.info(json.dumps(json_resp, indent=4, sort_keys=True))
        return json_resp

    def create_and_upload_images(self):

        app_name = self.spec.name

        for k, n in self.graph.nodes_iter(data=True):
            # logger.info(n)
            if "id" not in n:
                logger.warn("[%s] Service '%s' has not 'id' field", self.app_name, n)
                return

            if "image" not in n:
                logger.warn("[%s] An image is not specified for service '%s'", self.app_name, n['id'])
                return

            if not os.path.isfile(n['image']):
                logger.warn("[%s] The image file '%s' for service '%s' does not exist!", self.app_name, n['image'], n['id'])
                return

        uploads = []
        for k, n in self.graph.nodes_iter(data=True):
            instance, addr = self.create_image(app_name, n['id'], n['image'])
            if addr:
                uploads.append((app_name, n['id'], instance, addr, n['image']))

            try:
                img_init = n['image_init']
            except KeyError:
                # logger.info("Raised key error...")
                pass
            else:
                logger.debug("[%s] Creating init event so...", self.app_name)
                addr = self.create_image(app_name, n['id'], img_init, init=True)
                if addr:
                    uploads.append((addr, app_name, n['id'], img_init, True))

        self.cnt_uploaded = 0
        self.tgt_uploads = len(uploads)
        self.uploads = uploads

        if len(uploads):
            self.upload_file(*uploads.pop())
        # for upload in uploads:
        #     self.upload_file(*upload)

    def create_image(self, app_name, service_name, filename, init=False):
        file_size = os.stat(filename).st_size

        logger.info("[%s] Create image for %s:%s (size: %s)", self.app_name, app_name, service_name, file_size)
        url = 'http://%s:%s/' % self.provider
        payload = {
            'action': 'app_create_image',
            'app_name': app_name,
            'service_name': service_name,
            'size': file_size,
            'init': init
        }

        headers = {'content-type': 'application/json'}
        done = False
        attempts = 0
        r = None
        while attempts < 3 and not done:
            try:
                r = requests.post(url, data=json.dumps(payload), headers=headers, timeout=10)
                done = True
            except (requests.exceptions.ConnectTimeout, requests.exceptions.ReadTimeout) as e:
                logger.info("[%s] Connection to provider (%s:%s) timed out!", self.app_name, *self.provider)
                attempts += 1
                # return

        if not done:
            logger.info("[%s] ERROR: Unable to upload image to provider (%s:%s)!", self.app_name, *self.provider)
            return None, None

        if r:
            json_resp = json.loads(r.text)
            if "result" in json_resp and json_resp["result"]:
                logger.info("[%s] Image service @ %s", self.app_name, json_resp["instances"])
            else:
                logger.info("[%s] Error ** JSON: %s", self.app_name, json.dumps(json_resp, indent=4, sort_keys=True))

            # logger.info("\nSleep 5 seconds before uploading images...\n")
            # sleep(5)
            if json_resp["result"]:
                try:
                    instance = list(json_resp["instances"].items())[0]
                    return instance
                except:
                    logger.info("[%s] Unable to get address: %s", self.app_name, json_resp)
                    return None, None

        return None, None

    def upload_file(self, app_name, service_name, instance_name, addr, image_file, init=False):
        # logger.info("Uploading...")
        # input("Press enter to upload...")
        logger.info("[%s] Upload File Args: %s", self.app_name, (service_name, instance_name, addr, image_file, init))
        http_upload = False
        if http_upload:
            url = 'http://%s:%s/' % (addr[0], addr[1])
            payload = {
                'action': 'image_upload',
                'app_name': app_name,
                'service_name': service_name,
                'instance_name': instance_name,
                'filename': ntpath.basename(image_file),
                'init': init,
                'file': open(image_file, 'rb')}

            logger.info("[%s] Uploading to %s...", self.app_name, url)
            try:
                m = MultipartEncoder(fields=payload)
                r = requests.post(url, data=m, headers={'Content-Type': m.content_type}, timeout=300)
            # except (requests.exceptions.ConnectTimeout, requests.exceptions.ReadTimeout) as e:
            except:
                logger.info("[%s] Connection to provider (%s:%s) timed out. Traceback: %s", self.app_name, addr[0], addr[1], traceback.format_exc())
                return

            jr = json.loads(r.text)
            logger.info("[%s] JSON: %s", self.app_name, json.dumps(jr, indent=4, sort_keys=True))
        else:
            logger.info("[%s] Uploading %s(init=%s) image with twisted to %s...", self.app_name, service_name, init, addr[0])
            self.send_file(addr[0], "image_upload", app_name, service_name, instance_name, image_file, init)

    def upload_finished(self):
        self.cnt_uploaded += 1
        logger.info("[%s] uploaded = %d of %d", self.app_name, self.cnt_uploaded, self.tgt_uploads)
        if len(self.uploads):
            logger.info("[%s] Upload next:", self.app_name)
            self.upload_file(*self.uploads.pop())
        else:
            logger.info("[%s] All uploaded", self.app_name)
            if self.cb_images_uploaded:
                self.cb_images_uploaded()
        # if self.cb_images_uploaded and self.cnt_uploaded >= self.tgt_uploads:
        #     self.cb_images_uploaded()

    def upload_failed(self, provider, action, app, service, instance, filename, init=False):
        logger.debug("[%s] Uploaded file failed. Retrying... ", self.app_name)
        self.send_file(provider, action, app, service, instance, filename, init=init)

    def send_file(self, provider, action, app, service, instance, filename, init=False):
        logger.debug("[%s] send file: %s", self.app_name, instance)
        FileProtocolFactory.start(
            filename, action, app, service, instance, provider, PROVIDER_AG_PORT,
            init=init,
            cb_finished=self.upload_finished,
            cb_failed=partial(
                self.upload_failed, provider, action, app, service, instance, filename,
                init=init
            )
        )

        # FileProtocolFactory.start(filename, action, app, service, instance, provider, PROVIDER_AG_PORT,
        #                           init=init, cb_finished=self.upload_finished)

    def start(self):

        if not self.provider:
            logger.info("[%s] No provider set!", self.app_name)
            return

        json_app_info = self.get_app_info()
        # logger.info("JSON: %s" % json.dumps(json_app_info, indent=4, sort_keys=True))

        if not json_app_info:
            logger.info("[%s] Unable to retrieve app info!", self.app_name)
            return

        n = 0
        logger.info("[%s] Starting app in %d seconds", self.app_name, n)
        sleep(n)

        # start all instances
        url = 'http://%s:%s/' % self.provider
        payload = {'action': 'start_app', 'app_name': self.spec.name}
        headers = {'content-type': 'application/json'}
        r = None
        while not r:
            try:
                logger.info("[%s] Starting app '%s' (provider: '%s')", self.app_name, self.spec.name, url)
                r = requests.post(url, data=json.dumps(payload), headers=headers, timeout=5)
                logger.info("[%s] \n *** Response *** \n", self.app_name)
                json_resp = json.loads(r.text)
                logger.info("[%s] JSON: %s", self.app_name, json.dumps(json_resp, indent=4, sort_keys=True))
            except requests.ConnectionError as e:
                logger.info("[%s] Connection to provider (%s:%s) failed! Retrying...", self.app_name, *self.provider)
                sleep(1)
                # return None
            except requests.Timeout as e:
                logger.info("[%s] Connection to provider (%s:%s) timed out! Retrying...", self.app_name, *self.provider)
                sleep(1)
                # return None
            except Exception as e:
                logger.info("[%s] Unknown failure when starting VM at provider (%s): %s - %s", self.app_name,
                            self.provider, e, traceback.format_exc())
                break

        logger.info("[%s] Start callback: %s", self.app_name, self.cb_started)
        if self.cb_started:
            logger.info("[%s] Started callback: %s", self.app_name, self.cb_started)
            self.cb_started()


# TODO: input arguments:
#   * -r, random app
#   * -s, app_style (obsolete if not random): what type of appliaction should be generated
#   * --gid, generator id to be included in the apps name (for identification)
#   * -d, dispatcher address...

if __name__ == "__main__":

    parser = ArgumentParser(description="Launch an App")
    parser.add_argument('action', help='Action', choices=['create', 'info', 'start', 'stop'])
    parser.add_argument('-f', type=str, dest='graph_file',
                        help='Read graph from GML file (if a file is not set, a random app will be generated).')
    parser.add_argument('--da', type=str, dest='dispatcher', help='IP Address of the dispatcher.',
                        default="10.1.1.7")
    parser.add_argument('--dp', type=int, dest='port', help='Port of the dispatcher.',
                        default=20001)
    parser.add_argument('--pa', type=str, dest='provider', help='IP Address of the provider.')
    parser.add_argument('-a', type=str, dest='app_name', help='Name of the application!')

#     parser.add_argument('-t', type=str, dest='app_type', choices=['mtier', '3tier'])
#     parser.add_argument('--gid', type=str, dest='gen_id',
#                         description='Id of the generator (Included in the app name (random apps)')
#     parser.add_argument('n', type=str, dest='app_name', description='App name')
#
    args = parser.parse_args()

    launch = AppLaunch(gml_file=args.graph_file)
    if args.action == "create":
        launch.create_app(args.dispatcher)
        launch.create_and_upload_images()

    elif args.action == "info":
        if not args.provider:
            logger.info("No provider address specified!")
        elif not args.app_name:
            logger.info("No app name provided!")
        else:
            launch.get_app_info()
    elif args.action == "start":
        if not args.dispatcher and not args.provider:
            logger.info("No dispatcher nor provider address specified!")
        else:
            if args.dispatcher:
                launch.get_app_provider(args.dispatcher)
            else:
                launch.set_provider(args.provider)
            launch.start()
    else:
        raise NotImplementedError("Action: %s" % args.action)
