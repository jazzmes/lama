import os
import ntpath

from twisted.internet.defer import Deferred
from twisted.protocols.basic import FileSender, LineReceiver
from config.settings import PROVIDER_AG_PORT
from lama.agent.event_managers.twisted import FileSpec, FileProtocolFactory
from twisted.internet.protocol import ClientFactory
from twisted.internet import reactor


__author__ = 'tiago'


# class FileIOClient(ObjectProtocol): #LineReceiver):
class FileIOClient(LineReceiver):
    """ file sender """

    def __init__(self, app, service, path, controller):
        """ """
        self.app = app
        self.service = service
        self.path = path
        self.controller = controller

        self.infile = open(self.path, 'rb')
        self.insize = os.stat(self.path).st_size

        self.result = None
        self.completed = False

        self.controller.file_sent = 0
        self.controller.file_size = self.insize

    # def clean(self, data):
    #     """ """
    #     self.transport.unregisterProducer()
    #     self.transport.loseConnection()
    #     return data

    def _monitor(self, data):
        """ """
        self.controller.file_sent += len(data)
        self.controller.total_sent += len(data)

        # Check with controller to see if we've been cancelled and abort
        # if so.
        if self.controller.cancel:
            print('FileIOClient._monitor Cancelling')

            # Need to unregister the producer with the transport or it will
            # wait for it to finish before breaking the connection
            self.transport.unregisterProducer()
            self.transport.loseConnection()

            # Indicate a user cancelled result
            self.result = False

        return data

    def cbTransferCompleted(self, lastsent):
        """ """
        print("Transfer completed!!")
        self.completed = True
        self.transport.loseConnection()

    def connection_made(self):
        """ """
        print("Connection made")
        base_filename = ntpath.basename(self.path)
        fs = FileSpec(self.app, self.service, base_filename, self.insize)

        self.transport.write(fs)

        print("File spec sent")
        sender = FileSender()
        sender.CHUNK_SIZE = 2 ** 16
        d = sender.beginFileTransfer(self.infile, self.transport, self._monitor)
        d.addCallback(self.cbTransferCompleted)
        print("At the end of sending file")

    def clientConnectionFailed(self, connector, reason):
        print("proto: %s", self.proto)
        print("Connection failed: %s", reason)
        # logger.detail("abort")
        self.clean()
        reactor.stop()

    def connectionLost(self, reason):
        """
            NOTE: reason is a twisted.python.failure.Failure instance
        """
        super().connectionLost(self, reason)
        print(' - connectionLost\n  * ' + reason.getErrorMessage())
        print(' * finished with %s' % self.path)
        self.infile.close()
        reactor.stop()


class FileIOClientFactory(ClientFactory):
    """ file sender factory """
    protocol = FileIOClient

    def __init__(self, app, service, path):
        """ """
        self.app = app
        self.service = service
        self.path = path
        self.controller = type('test',
                               (object,),
                               {'cancel': False, 'total_sent': 0, 'completed': Deferred()})

    def clientConnectionFailed(self, connector, reason):
        """ """
        print("Client connection failed")
        ClientFactory.clientConnectionFailed(self, connector, reason)
        self.controller.completed.errback(reason)

    def buildProtocol(self, addr):
        """ """
        print(" + Building protocol")
        p = self.protocol(self.app, self.service, self.path, self.controller)
        p.factory = self
        print(" + Done building protocol")
        return p

    def clientConnectionLost(self, connector, reason):
        print("proto: %s", self.proto)
        print("Connection lost: %s", reason)
        reactor.stop()


# def send_file(provider, app, service, filename):
#     """ helper for file transmission """
#     f = FileIOClientFactory(app, service, filename)
#     reactor.connectTCP(provider, PROVIDER_AG_PORT, f)
#     reactor.run()
#     return True


def send_file(provider, app, service, filename):
    print("File transfer client %s" % [app, service, filename, provider, PROVIDER_AG_PORT])
    FileProtocolFactory.start(filename, app, service, provider, PROVIDER_AG_PORT)


if __name__ == "__main__":
    send_file(
        "10.1.1.8",
        "app1",
        "service1.1",
        # "/home/lama/vm_images/rubbos/rubbos-apache-tc.qcow2"
        "/home/lama/lama/persistence/providers.psd"
    )