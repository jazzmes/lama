import glob
import json
import rrdtool
from pprint import pprint
from os import path
from time import time
from config.settings import PATH_RRD
from lama.utils.logging_lama import logger

__author__ = 'tiago'


class RrdWrapper(object):
    MetadataUpdateInterval = 300  # seconds
    CleanUpdateInterval = 3600
    ExpirationTime = "1w"

    def __init__(self):
        self.metadata = None
        self.metadata_last_update = None

        self.clean_last_update = None
        self.filter_plugins = {}

    def get_hosts(self):
        self.get_metadata()
        return list(self.metadata.keys())

    def get_metrics(self, host):
        self.get_metadata()
        if host not in self.metadata:
            return []

        return [metric for plugin, metrics in self.metadata[host].items() for metric in metrics]

    def get_plugins(self, host, filter=True):
        self.get_metadata()
        if host not in self.metadata:
            return []
        logger.debug("All Plugins: %s", self.metadata[host].keys())
        logger.detail("Host: %s", host)
        logger.detail("Filter: %s", self.filter_plugins)
        logger.debug("Filter: %s", self.filter_plugins.get(host, set()))
        logger.debug("Result: %s", set(self.metadata[host].keys()) - self.filter_plugins.get(host, set()))

        return list(set(self.metadata[host].keys()) - self.filter_plugins.get(host, set()))

    def get_metadata(self):
        if not self.metadata_last_update or (self.metadata_last_update + RrdWrapper.MetadataUpdateInterval) <= time():
            self.update_metadata()
            self.metadata_last_update = time()

            self.clean_metadata()

        return self.metadata

    def clean_metadata(self):
        logger.detail("Cleaning metadata")
        if not self.clean_last_update or (self.clean_last_update + RrdWrapper.CleanUpdateInterval) <= time():
            self.clean_last_update = time()

        for host, plugins in self.metadata.items():
            for plugin, metric in plugins.items():
                data = self.read_data(host, plugin, metric, start=("-" + RrdWrapper.ExpirationTime), resolution=3600)
                # print(not any(any(any(series) for series in d["values"]) for d in data.values()))
                    # for series in d["values"]:
                    #     print(series)
                    #     print(any(series))
                    #     # print([v is None for v in d["values"]])
                    #     print(any([v is not None for v in d["values"]]))

                if not any(any(any(series) for series in d["values"]) for d in data.values()):
                    if host not in self.filter_plugins:
                        self.filter_plugins[host] = set()
                    self.filter_plugins[host].add(plugin)
                # else:
                #     print("MAINTAIN")

    def get_data(self, host, plugin, metrics=None, start='-1h', resolution=10, end=None):
        self.get_metadata()
        return self.read_data(host, plugin, metrics, start, resolution, end)

    def read_data(self, host, plugin, metrics=None, start='-1h', resolution=10, end=None):
        logger.detail("read_data: %s", (host, plugin, metrics, start, resolution, end))
        if host not in self.metadata:
            return []
        if plugin not in self.metadata[host]:
            return []
        if metrics and not isinstance(metrics, list):
            metrics = [metrics]

        #  if metric is defined, then we return an array for each metric
        available_metrics = set(self.metadata[host][plugin])
        requested_metrics = [m for m in metrics if m in available_metrics] if metrics else available_metrics

        args = ["--start", str(start), "--resolution", str(resolution)]
        if end:
            args.extend(["--end", str(end)])
        data = {}
        for m in requested_metrics:
            # TODO: guess units per metric?
            rrd_file = str(path.join(PATH_RRD, host, plugin, "%s.rrd" % m))
            # result = rrdtool.fetch(rrd_file, )
            rperiod, rmetrics, rdata = rrdtool.fetch(*([rrd_file, "AVERAGE"] + args))
            # logger.detail(rperiod)
            # logger.detail(rmetrics)
            # logger.detail(rdata)
            s, e, r = rperiod
            data[m] = {
                "start": s, "end": e, "res": r,
                "series": list(rmetrics),
                "values": list(map(list, zip(*rdata)))
            }
        return data

    def get_json_data(self, host, plugin, metrics=None, start='-1h', resolution=10, end=None):
        data = self.get_data(host, plugin, metrics, start, resolution, end)
        return json.dumps(data)

    def update_metadata(self):
        # raise NotImplementedError()

        rrd_directory = path.abspath(PATH_RRD)
        # From base directory: host/plugin/instance.rrd
        self.metadata = {}
        # Host --> Plugin -> Metric
        for name in glob.glob("%s/*/*/*.rrd" % rrd_directory):
            name = name.replace("%s/" % rrd_directory, '')
            host, plugin = path.split(path.dirname(name))
            instance, _ = path.splitext(path.basename(name))
            info = self.metadata.get(host, {})
            instances = info.get(plugin, [])
            instances.append(instance)
            info[plugin] = instances
            self.metadata[host] = info


if __name__ == "__main__":
    rw = RrdWrapper()
    # pprint(rw.get_hosts())
    # pprint(rw.get_plugins(host="localhost"))
    # pprint(rw.get_metrics(host="localhost"))
    # pprint(rw.get_data("e8500-2013-381c.localdomain", plugin="cpu-0", metrics="cpu-idle"))
    # pprint(rw.get_data("e8500-2013-381c.localdomain", plugin="cpu-0"))
    # pprint(rw.get_json_data("e8500-2013-381c.localdomain", plugin="cpu-0"))
    # rw.get_metadata()
    # pprint(rw.get_data("e8500-2013-381c.localdomain", plugin="cpu-0", start=))