import subprocess
import libvirt
from lxml import etree
import traceback
import uuid

__author__ = 'tiago'

CHECK_FEATURES = ["acpi", "apic", "pae", "privnet", "hap", "viridian", "eoi", "hyperv_vapic",
                  "hyperv_relaxed", "hyperv_spinlocks", "hyperv_spinlocks_retries"]


# create xml
class LibvirtError(Exception):
    pass


def create_instance(conn):
    # sample xml...
    f = open('lama/scripts/sample.xml', 'r')
    xml = f.read()
    f.close()
    # print(conn)
    # print(dir(conn))

    dom = conn.defineXML(xml)

    res = dom.create()
    print(res)
    conn.close()


def get_first_element(root, xpath):
    try:
        return root.xpath("%s[1]" % xpath)[0]
    except:
        print("ERROR - Unable to get first '%s' from '%s'. Trace: %s" % (xpath, root, traceback.format_exc()))
        return None


def process_cpu_capabilities(dom_root, cpu_root):
    dom_cpu_root = etree.SubElement(dom_root, "cpu", {"mode": "custom", "match": "exact"})
    for e in cpu_root:
        if e.tag in ["model", "vendor"]:
            se = etree.SubElement(dom_cpu_root, e.tag, dict(e.attrib))
            se.text = e.text
        elif e.tag == "feature":
            attrs = dict(e.attrib)
            attrs.update({"policy": "require"})
            etree.SubElement(dom_cpu_root, e.tag, attrs)


def add_timer(clock_root, name, **attrs):
    attributes = {"name": name}
    attributes.update(attrs)
    etree.SubElement(clock_root, "timer", attributes)


def add_clock(dom_root):
    dom_clock_root = etree.SubElement(dom_root, "clock", {"offset": "utc"})
    add_timer(dom_clock_root, "rtc", tickpolicy="catchup")
    add_timer(dom_clock_root, "pit", tickpolicy="delay")
    add_timer(dom_clock_root, "hpet", tickpolicy="False")


def add_features(dom_root, guest):
    features_root = etree.SubElement(dom_root, "features")
    t = etree.ElementTree(guest)
    guest_features = t.xpath("/guest/features/*")
    for f in guest_features:
        if f.tag in CHECK_FEATURES:
            etree.SubElement(features_root, f.tag)


def check_guest(capabilities, os_type=None, arch=None):
    guests = capabilities.xpath("/capabilities/guest")
    for guest in guests:
        tguest = etree.ElementTree(guest)
        try:
            if os_type:
                x = tguest.xpath("/guest/os_type[contains(text(),'%s')]" % os_type)
                if not len(x):
                    continue
            if arch:
                x = tguest.xpath("/guest/arch[@name='%s']" % arch)
                if not len(x):
                    continue
            return guest
        except:
            continue
    return None


def add_devices(dom_root, guest, arch, qcow2_path):
    dev_root = etree.SubElement(dom_root, "devices")

    # emulator (support for kvm only)
    try:
        emul = etree.ElementTree(guest).xpath("/guest/arch[@name='%s']/domain[@type='kvm']/emulator" % arch)[0]
    except:
        raise LibvirtError("Unable to find emulator path")

    etree.SubElement(dev_root, "emulator").text = emul.text

    disk_root = etree.SubElement(dev_root, "disk", {"type": "file", "device": "disk"})
    etree.SubElement(disk_root, "driver", {"name": "qemu", "type": "qcow2"})
    etree.SubElement(disk_root, "source", {"file": qcow2_path})
    etree.SubElement(disk_root, "target", {"dev": "hda", "bus": "ide"})

    etree.SubElement(dev_root, "input", {"type": "mouse", "bus": "ps2"})
    etree.SubElement(dev_root, "graphics", {"type": "vnc", "port": "-1"})
    etree.SubElement(dev_root, "console", {"type": "pty"})
    video_root = etree.SubElement(dev_root, "video")
    etree.SubElement(video_root, "model", {"type": "cirrus"})


def create_default_xml(conn):

    # get capabilities
    caps_xml = conn.getCapabilities()
    caps_root = etree.fromstring(caps_xml)

    # domain root
    dom_root = etree.Element("domain")

    # currently kvm only
    dom_root.set("type", "kvm")

    # get random uuid
    euuid = etree.SubElement(dom_root, "uuid")
    euuid.text = str(uuid.uuid4())

    print(etree.tostring(caps_root).decode("UTF-8"))
    # get host features
    host = get_first_element(caps_root, "/capabilities/host")
    for e in host:
        if e.tag == "cpu":
            process_cpu_capabilities(dom_root, e)

    # clock
    add_clock(dom_root)

    # power options
    etree.SubElement(dom_root, "on_poweroff").text = "destroy"
    etree.SubElement(dom_root, "on_reboot").text = "restart"
    etree.SubElement(dom_root, "on_crash").text = "restart"

    # check if guest hvm exists (unmodified)
    opt_os_type = "hvm"
    opt_os_arch = "i686"
    guest = check_guest(caps_root, opt_os_type, opt_os_arch)
    if guest is None:
        raise LibvirtError("No guest with required capability!")

    os_root = etree.SubElement(dom_root, "os")
    etree.SubElement(os_root, "type", {"arch": opt_os_arch}).text = opt_os_type
    # default boot from hd
    etree.SubElement(os_root, "boot", {"dev": "hd"})

    # print(guest)
    # print(etree.tostring(guest, pretty_print=True).decode("UTF-8"))

    add_features(dom_root, guest)

    add_devices(dom_root, guest, opt_os_arch)

    # if host is not None:
    #     print(element_str(host))
    #     cpu_root = get_first_element(host, "/capabilities/host/cpu")
    #     if cpu_root is not None:
    #         print(element_str(cpu_root))
    #         tree = etree.ElementTree(host)
    #         print(tree.getpath(cpu_root))
    #         print(tree.xpath(tree.getpath(cpu_root)))

    print("\n\nFINAL XML:\n\n%s" % etree.tostring(dom_root, pretty_print=True).decode("UTF-8"))


def get_xml(name, image):
    client_args = [
        '--name', name,
        '--vcpus', '1',
        '--ram', '1024',
        '--os-type=linux',
        '--disk', 'path=%s' % image,
        '--force',
    ]
    default_args = [
        '--print-xml',
        '--cpu', 'host',
        '--vnc',
        '--import',
        '--connect', 'qemu:///system']

    args = ["virt-install"] + default_args + client_args
    print("Arguments: %s" % args)

    cmd = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = cmd.communicate()
    out = out.decode("utf-8")

    if err:
        print("Error (%s)" % err)
    if out:
        try:
            etree.fromstring(out)
            print("VALID XML:\n%s" % out)
        except:
            print("Out (%s)" % out)


conn = libvirt.open('qemu:///system')
# create_instance(conn)
# create_default_xml(conn)
get_xml("test_app_1", "/home/lama/lama/image_files/auto-app-000001_img_tier-0.img")


# XML
# ROOT<domain type="kvm">
# OPTION  <name>test_app_2</name>
#   <uuid>dbd87387-60c3-4523-b501-517b76cb647b</uuid>
# OPTION   <memory>1048576</memory>
# OPTION   <currentMemory>1048576</currentMemory>
# OPTION   <vcpu>1</vcpu>
#   <os>
#     <type arch="x86_64">hvm</type>
#     <boot dev="hd"/>
#   </os>
# CAPABILITIES
#   <features>
#     <acpi/>
#     <apic/>
#     <pae/>
#   </features>
# CAPABILITIES
#   <cpu mode="custom" match="exact">
#     <model>Opteron_G2</model>
#     <vendor>AMD</vendor>
#     <feature policy="require" name="vme"/>
#     <feature policy="require" name="mmxext"/>
#     <feature policy="require" name="fxsr_opt"/>
#     <feature policy="require" name="cr8legacy"/>
#     <feature policy="require" name="ht"/>
#     <feature policy="require" name="3dnowprefetch"/>
#     <feature policy="require" name="3dnowext"/>
#     <feature policy="require" name="extapic"/>
#     <feature policy="require" name="cmp_legacy"/>
#     <feature policy="require" name="3dnow"/>
#   </cpu>
# ??? anyway to get the defaults? or is it hard-coded? check virt-install
#   <clock offset="utc">
#     <timer name="rtc" tickpolicy="catchup"/>
#     <timer name="pit" tickpolicy="delay"/>
#     <timer name="hpet" present="no"/>
#   </clock>
# DEFAULT
#   <on_poweroff>destroy</on_poweroff>
#   <on_reboot>restart</on_reboot>
#   <on_crash>restart</on_crash>
#   <devices>
# DEFAULT
#     <emulator>/usr/bin/qemu-kvm</emulator>
# OPTION: only support qcow2 for now
#     <disk type="file" device="disk">
#       <driver name="qemu" type="qcow2"/>
#       <source file="/var/lib/libvirt/images/Application.qcow2"/>
#       <target dev="hda" bus="ide"/>
#     </disk>
# REMOVE (no controllers)
#     <controller type="usb" index="0" model="ich9-ehci1"/>
#     <controller type="usb" index="0" model="ich9-uhci1">
#       <master startport="0"/>
#     </controller>
#     <controller type="usb" index="0" model="ich9-uhci2">
#       <master startport="2"/>
#     </controller>
#     <controller type="usb" index="0" model="ich9-uhci3">
#       <master startport="4"/>
#     </controller>
# ??? check virt-install
#     <interface type="network">
#       <source network="default"/>
#       <mac address="52:54:00:58:4d:d4"/>
#     </interface>
# DEFAULT (lets leave it...)
#     <input type="mouse" bus="ps2"/>
# DEFAULT (or access to the machine)
#     <graphics type="vnc" port="-1"/>
#     <console type="pty"/>
#     <video>
#       <model type="cirrus"/>
#     </video>
#   </devices>
# </domain>

    # <cpu>
    #   <arch>x86_64</arch>
    #   <model>Opteron_G2</model>
    #   <vendor>AMD</vendor>
    #   <topology sockets="1" cores="2" threads="1"/>
    #   <feature name="3dnowprefetch"/>
    #   <feature name="cr8legacy"/>
    #   <feature name="extapic"/>
    #   <feature name="cmp_legacy"/>
    #   <feature name="3dnow"/>
    #   <feature name="3dnowext"/>
    #   <feature name="fxsr_opt"/>
    #   <feature name="mmxext"/>
    #   <feature name="ht"/>
    #   <feature name="vme"/>
    # </cpu>

