from libvirt import virDomain, libvirtError
from lxml import etree
import subprocess
import libvirt
import psutil
import grp
import os
import traceback
from lama.agent.specs.resource import RamSpec, CpuSpec
from lama.utils.logging_lama import logger

__author__ = 'tiago'


class LibvirtException(Exception):
    pass


class XMLException(Exception):

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "Invalid XML: %s" % self.msg


class LibvirtConnection(object):

    def __init__(self):
        self.conn = libvirt.open('qemu:///system')

    # def define_instance(self, xml=None, xml_path=None) -> virDomain:
    #     if not xml and xml_path:
    #         # sample xml...
    #         f = open('lama/scripts/sample.xml', 'r')
    #         xml = f.read()
    #         f.close()
    #     elif not xml:
    #         raise XMLException("Should provider either a XML string or a XML file path.")
    #
    #     dom = self.conn.defineXML(xml)
    #     return dom

    # create - creates and starts the VM

    def close(self):
        self.conn.close()

    def define_instance(self, name, image_path, xml=None, network=None, mac=None, extra_nic_mac=None, res_spec=None):
        if not xml:
            args = {
                "network": network,
                "mac": mac,
                "extra_nic_mac": extra_nic_mac
            }
            if res_spec:
                # args["vpcus"] = res_spec.get_vcpus()
                logger.temp("res spec: %s", res_spec)
                try:
                    r = res_spec.get(CpuSpec)
                    if len(r):
                        r = r[0]
                        args["vcpus"] = int(r.n_processors)
                except:
                    logger.warn("Error getting CPU resource spec: %s", traceback.format_exc())

                # args["ram"] = res_spec.get_ram()
                try:
                    r = res_spec.get(RamSpec)
                    if len(r):
                        r = r[0]
                        # ram in MB
                        args["ram"] = int(r.size / 1048576)
                except:
                    logger.warn("Error getting RAM resource spec: %s", traceback.format_exc())

                logger.warn("SETTING CUSTOM RESOURCES: %s", res_spec)

            logger.temp("args to create XML: %s", args)
            xml = self.get_xml(name, image_path, **args)

        logger.detail("XML:\n%s" % xml)
        dom = self.conn.defineXML(xml)
        return dom

    def define_network(self, network_name, network_type, bridge_name=None):
        if network_type == "openvswitch":
            network = etree.Element("network")

            name = etree.Element("name")
            name.text = network_name
            network.append(name)

            network.append(etree.Element("forward", mode="bridge"))
            network.append(etree.Element("virtualport", type="openvswitch"))
            network.append(etree.Element("bridge", name=bridge_name))
            xml = etree.tostring(network).decode("utf-8")
            try:
                net = self.conn.networkDefineXML(xml)
            except libvirtError as e:
                if "already exists" in str(e):
                    net = self.get_network(network_name)
                else:
                    logger.error("Unexpected error: %s. Traceback: %s", e, traceback.format_exc())
                    net = None
            return net
        else:
            raise NotImplementedError()

    def get_storage_pool(self, name):
        try:
            pool = self.conn.storagePoolLookupByName(name)
            return pool
        except libvirtError as e:
            pass

        return None

    def create_storage_pool(self, name, path):
        # if not create...
        # <pool type='dir'>
        #   <name>tmp</name>
        #   <target>
        #     <path>/home/lama/tmp</path>
        #     <permissions>
        #       <mode>0755</mode>
        #       <owner>-1</owner>
        #       <group>-1</group>
        #     </permissions>
        #   </target>
        # </pool>

        user_id = str(os.getuid())
        group_id = str(grp.getgrnam("lamagrp").gr_gid)
        mode = "0755"

        root = etree.Element("pool")
        root.set("type", "dir")
        ename = etree.Element("name")
        ename.text = name
        root.append(ename)
        target = etree.Element("target")
        root.append(target)
        epath = etree.Element("path")
        epath.text = path
        target.append(epath)
        perms = etree.Element("permissions")
        target.append(perms)
        emode = etree.Element("mode")
        emode.text = mode
        perms.append(emode)
        owner = etree.Element("owner")
        owner.text = user_id
        perms.append(owner)
        group = etree.Element("group")
        group.text = group_id
        perms.append(group)

        logger.debug("Create pools storage: %s", etree.tostring(root).decode("utf-8"))

        storage = None
        try:
            storage = self.conn.storagePoolDefineXML(etree.tostring(root).decode("utf-8"))
        except Exception as e:
            logger.error("Unable to create pool: %", etree.tostring(root).decode("utf-8"))
            logger.error("Exception: %s: %s", e, traceback.format_exc())

        if storage:
            try:
                storage.setAutostart(True)
                storage.create()
            except Exception as e:
                logger.error("Unable to create pool: %", etree.tostring(root).decode("utf-8"))
                logger.error("Exception: %s: %s", e, traceback.format_exc())

    def restore_instance(self, image_path):
        return self.conn.restore(image_path)

    # def start_instance(self, dom: virDomain):
    #     res = dom.create()
    #     return res
    #
    # def reboot_instance(self):
    #     raise NotImplementedError()
    #
    # def suspend_instance(self, dom: virDomain):
    #     return dom.suspend()

    # snapshot
    # suspend: suspends VM but still uses memory
    # save: suspends VM and save memory to file - does it release the memory?
    # resume: resume a suspended VM

    @staticmethod
    def get_xml(name, image_path, vcpus=1, ram=1024, os_type='linux', network="default", mac=None, extra_nic_mac=None):
        """
        With no arguments retrieves a XML string for a default VM configuration.
        :return:
        """
        attempt = 1
        max_attempts = 3
        done = False
        out = None
        while not done:
            network_str = network
            if mac:
                network_str += ",mac=%s" % mac

            client_args = [
                '--name', name,
                '--vcpus', str(vcpus),
                '--ram', str(ram),
                '--os-type=%s' % os_type,
                '--network', 'network=%s' % network_str,
                '--disk', 'path=%s,device=disk,bus=virtio,format=qcow2' % image_path,
                # '--disk', image_path,
                '--force',
            ]
            if extra_nic_mac:
                client_args += ['--network', "network=%s,mac=%s" % (network, extra_nic_mac)]

            default_args = [
                '--print-xml',
                '--cpu', 'host',
                '--vnc',
                '--import',
                '--connect', 'qemu:///system']

            args = ["virt-install"] + default_args + client_args

            cmd = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = cmd.communicate()
            out = out.decode("utf-8")

            if err:
                if attempt >= 3:
                    raise LibvirtException(err)
                attempt += 1
                logger.warn("Failed to launch: %s", err)
            else:
                done = True

        if out:
            try:
                etree.fromstring(out)
            except Exception as e:
                raise XMLException(str(e))

        # set up the network
        root = etree.fromstring(out)
        interfaces = root.findall(".//devices/interface[@type='network']")
        print(interfaces)
        for iface in interfaces:
            src = iface.find(".//source")
            if src is not None and src.get("network") and src.get("network").startswith("ovs"):
                etree.SubElement(iface, "model", type='virtio')
        out = etree.tostring(root).decode("utf-8")
        return out

    def get_domain(self, name):
        try:
            return self.conn.lookupByName(name)
        except libvirtError as e:
            return None

    def get_network(self, name):
        try:
            return self.conn.networkLookupByName(name)
        except libvirtError as e:
            return None


def get_memory_usage():
    return psutil.phymem_usage().used


def get_metric():
    return "M=%s" % get_memory_usage()


def print_debug(msg):
    print("%s\t%s" % (msg, get_metric()))

if __name__ == "__main__":
    instancename = "TestInstance"
    imagein = "/var/lib/libvirt/images/lama/Application.qcow2"
    # imagein = "/home/lama/lama/image_files/auto-app-000001_img_tier-0.img"
    fileout = "vm_save"

    print_debug("Start testing LibVirt")
    connection = LibvirtConnection()

    instance = connection.get_domain(instancename)

    if instance:
        print_debug("Instance '%s' is already defined!" % instancename)
    else:
        print_debug("Deploying instance...")
        # defines the instance
        instance = connection.deploy_instance(instancename, imagein)

    if instance.isActive():
        print_debug("Instance '%s' is already active!" % instancename)
    else:
        print_debug("Activate instance...")
        # create
        instance.create()

    # confirm if it is active
    active = instance.isActive()
    print_debug("After CREATE - Is instance active? %s" % active)

    if active:
        # suspend
        input("Will suspend :: press Enter to continue...")
        print_debug("Suspend instance...")
        instance.suspend()
        print_debug("After SUSPEND - Is instance active? %s" % instance.isActive())

        # resume
        input("Will resume :: press Enter to continue...")
        print_debug("Resume instance...")
        instance.resume()
        print_debug("After RESUME - Is instance active? %s" % instance.isActive())

        # save
        input("Will save :: press Enter to continue...")
        print_debug("Save instance...")
        instance.save(fileout)
        print_debug("After SAVE - Is instance active? %s" % instance.isActive())

        # restore
        input("Will restore :: press Enter to continue...")
        print_debug("Restore instance...")
        connection.restore_instance(fileout)
        print_debug("After RESTORE - Is instance active? %s" % instance.isActive())

        # stop
        input("Will shutdown :: press Enter to continue...")
        print_debug("Shutdown instance...")
        instance.shutdown()
        print_debug("After SHUTDOWN - Is instance active? %s" % instance.isActive())

    # undefine/delete
    input("Will delete :: press Enter to continue...")
    print_debug("Delete instance...")
    print_debug("After DELETE - Is instance active? %s" % instance.isActive())
    instance.undefine()

    # destroy
    print_debug("Destroy instance object...")
    instance.destroy()
