from time import time

from lama.utils.lama_collections import PersistentSyncDict
from lama.utils.enum import Enum
from lama.utils.logging_lama import logger


__author__ = 'tiago'


class CategoryNotFoundException(Exception):
    pass


class AttributeException(Exception):
    pass


class Category(Enum):
    Operation, Management, Failure = range(1, 4)


# class Category(object):
#
#     class Operation(Enum):
#         Authenticated = range(1, 2)
#
#     class Management(Enum):
#         StartInstance, StopInstance, MigrateInstance, StartMigrateInstance, EndMigrateInstance,\
#             AppRequestReceived, ReceivingFile = range(1, 8)
#
#     class Failure(Enum):
#         HostCrash = range(1, 2)


class Event(object):

    def __init__(self, source, entity, category, event_title, description, start=None):
        self.source = source
        self.category = category
        self.event_title = event_title
        self.entity = entity
        self.description = description
        self.end = int(time() * 1000)
        self.start = int(start * 1000) if start else self.end

    def jsonify(self):
        return self.__dict__


class EventLog(object):

    def __init__(self, source):
        self.source = source
        self.events = PersistentSyncDict("events-%s" % source, Event)
        self.index = len(self.events)

    def add_event(self, target, category, event_title, description=None, start=None):
        logger.debug("Added event to LOG: %s", (target, category, event_title, description, start))
        self.events.add(str(self.index), Event(self.source, target, category, event_title, description, start))
        self.index = len(self.events)

    def get_events(self, sort_field="end", reverse=False):
        try:
            return sorted(self.events.values(), key=lambda o: getattr(o, sort_field), reverse=reverse)
        except AttributeError:
            raise

    def get_events_with_metadata(self, sort_field="end"):
        return {
            "source": self.source,
            "events": self.get_events(sort_field)
        }

