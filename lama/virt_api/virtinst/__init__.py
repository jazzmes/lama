# Copyright (C) 2013, 2014 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301 USA.

from lama.virt_api.virtcli import cliconfig, cliutils
stable_defaults = cliconfig.stable_defaults
cliutils.setup_i18n()


from lama.virt_api.virtinst import util
from lama.virt_api.virtinst import support

from lama.virt_api.virtinst.osxml import OSXML
from lama.virt_api.virtinst.domainfeatures import DomainFeatures
from lama.virt_api.virtinst.domainnumatune import DomainNumatune
from lama.virt_api.virtinst.domainblkiotune import DomainBlkiotune
from lama.virt_api.virtinst.domainmemorytune import DomainMemorytune
from lama.virt_api.virtinst.domainmemorybacking import DomainMemorybacking
from lama.virt_api.virtinst.domainresource import DomainResource
from lama.virt_api.virtinst.clock import Clock
from lama.virt_api.virtinst.cpu import CPU, CPUFeature
from lama.virt_api.virtinst.seclabel import Seclabel
from lama.virt_api.virtinst.pm import PM
from lama.virt_api.virtinst.idmap import IdMap

from lama.virt_api.virtinst import capabilities as CapabilitiesParser
from lama.virt_api.virtinst.interface import Interface, InterfaceProtocol
from lama.virt_api.virtinst.network import Network
from lama.virt_api.virtinst.nodedev import NodeDevice
from lama.virt_api.virtinst.storage import StoragePool, StorageVolume

from lama.virt_api.virtinst.device import VirtualDevice
from lama.virt_api.virtinst.deviceinterface import VirtualNetworkInterface
from lama.virt_api.virtinst.devicegraphics import VirtualGraphics
from lama.virt_api.virtinst.deviceaudio import VirtualAudio
from lama.virt_api.virtinst.deviceinput import VirtualInputDevice
from lama.virt_api.virtinst.devicedisk import VirtualDisk
from lama.virt_api.virtinst.devicehostdev import VirtualHostDevice
from lama.virt_api.virtinst.devicechar import (VirtualChannelDevice,
                                 VirtualConsoleDevice,
                                 VirtualParallelDevice,
                                 VirtualSerialDevice)
from lama.virt_api.virtinst.devicevideo import VirtualVideoDevice
from lama.virt_api.virtinst.devicecontroller import VirtualController
from lama.virt_api.virtinst.devicewatchdog import VirtualWatchdog
from lama.virt_api.virtinst.devicefilesystem import VirtualFilesystem
from lama.virt_api.virtinst.devicesmartcard import VirtualSmartCardDevice
from lama.virt_api.virtinst.deviceredirdev import VirtualRedirDevice
from lama.virt_api.virtinst.devicememballoon import VirtualMemballoon
from lama.virt_api.virtinst.devicetpm import VirtualTPMDevice
from lama.virt_api.virtinst.devicerng import VirtualRNGDevice
from lama.virt_api.virtinst.devicepanic import VirtualPanicDevice

from lama.virt_api.virtinst.installer import (ContainerInstaller, ImportInstaller,
                                LiveCDInstaller, PXEInstaller, Installer)

from lama.virt_api.virtinst.distroinstaller import DistroInstaller

from lama.virt_api.virtinst.guest import Guest
from lama.virt_api.virtinst.cloner import Cloner
from lama.virt_api.virtinst.snapshot import DomainSnapshot

from lama.virt_api.virtinst.connection import VirtualConnection
