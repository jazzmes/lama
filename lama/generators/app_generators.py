"""
Created on Feb 14, 2014

@author: Tiago
"""
from lama.agent.app.app_arch import AppLogicalSpec
import lama.agent.specs.resource as rspec
from lama.agent.specs.resource import ResourceSet


class AppGenerator(object):
    """
    Generates applications graphs with resource requirements.
    TODO: start simple with limited options.

    It should generate: logical graph of the application, initial VM (or ?) requirements, logical SLAs
    """

    app_number = 1

    def __init__(self):
        """
        Constructor.
        """
        self.resource_gen = ResourceSetFactory() 

    def generate(self): 
        app = AppLogicalSpec("auto-app-%06d" % AppGenerator.app_number)
        return app


class MultiTierAppGenerator(AppGenerator):
    
    def __init__(self):
        super(MultiTierAppGenerator, self).__init__()
        self.tiers = list()
        self.global_goal = list()
    
    def add_tier(self, name):
        self.tiers.append(name)
        
    def add_global_goal(self, tier, metric, goal):
        self.global_goal.append((tier, metric, goal))
        
    def generate(self):
        app = super(MultiTierAppGenerator, self).generate()
        for i in range(len(self.tiers)):
            app.add_service(self.tiers[i], self.resource_gen.fixed_flavor(ResourceSetFactory.Level.MEDIUM))
            if i > 0:
                app.add_flow(self.tiers[i-1], self.tiers[i], ResourceSet()) #, {"type" : Flow})
                
        return app


class Simple3TierAppGenerator(MultiTierAppGenerator):
    
    def __init__(self):
        super(Simple3TierAppGenerator, self).__init__()
        self.nr_tiers = 3

    def generate(self):
        for i in range(self.nr_tiers):
            self.add_tier("tier-%d" % i)
        
        self.add_global_goal("tier-0", "ResponseTime", "300ms")

        return super(Simple3TierAppGenerator, self).generate()
        

class ResourceSetFactory(object):
    
    class Level:
        SMALL = 1
        MEDIUM = 2
        LARGE = 3
        OS_TINY = 101
        OS_SMALL = 102
    
    def fixed_flavor(self, flavor):
        rs = rspec.ResourceSet()
        
        if flavor is ResourceSetFactory.Level.SMALL:
            rs.add_resource_spec(rspec.CpuSpec(1, "1GHz"))
            rs.add_resource_spec(rspec.RamSpec("990MB")) # machines do not have 100% of the advertised memory
            rs.add_resource_spec(rspec.DiskSpec("5GB", "1MBps", "1MBps"))
            rs.add_resource_spec(rspec.NetSpec("5Mbps", "5Mbps"))
        elif flavor is ResourceSetFactory.Level.MEDIUM:
            rs.add_resource_spec(rspec.CpuSpec(2, "1GHz"))
            rs.add_resource_spec(rspec.RamSpec("2GB"))
            rs.add_resource_spec(rspec.DiskSpec("10GB", "2MBps", "2MBps"))
            rs.add_resource_spec(rspec.NetSpec("20Mbps", "20Mbps"))
        elif flavor is ResourceSetFactory.Level.LARGE:
            rs.add_resource_spec(rspec.CpuSpec(4, "1GHz"))
            rs.add_resource_spec(rspec.RamSpec("4GB"))
            rs.add_resource_spec(rspec.DiskSpec("20GB", "8MBps", "4MBps"))
            rs.add_resource_spec(rspec.NetSpec("100Mbps", "100Mbps"))
        elif flavor is ResourceSetFactory.Level.OS_TINY:
            rs.add_resource_spec(rspec.CpuSpec(1, "1GHz"))
            rs.add_resource_spec(rspec.RamSpec("512MB"))
            rs.add_resource_spec(rspec.DiskSpec("1GB", "1MBps", "1MBps"))
        elif flavor is ResourceSetFactory.Level.OS_SMALL:
            rs.add_resource_spec(rspec.CpuSpec(1, "1GHz"))
            rs.add_resource_spec(rspec.RamSpec("2GB"))
            rs.add_resource_spec(rspec.DiskSpec("20GB", "1MBps", "1MBps"))
        else:
            return None
        
        return rs

