#!/usr/bin/python
from twisted.internet.error import ConnectionDone
from twisted.internet import protocol, reactor

from lama.utils.logging_lama import logger
from lama.agents.protocols.base import ObjectTxRx


# lama_path = os.path.abspath(os.path.join(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]), '../../'))
# sys.path.append(lama_path)


class AppDeployReqClient(ObjectTxRx):
    """Send an application request."""
    
    def __init__(self, spec, func):
        self.spec = spec
        self.save_response = func
    
    def connectionMade(self):
        """Send application deploy request."""

        logger.debug("App Spec: %s", self.spec)
        # logger.debug("1. name is unicode: %s", isinstance(self.spec.name, unicode))
        # logger.debug("1. name is str: %s", isinstance(self.spec.name, str))

        self.sendObject(AppDeployRequest(self.spec))
#         data = pickle.dumps()
#         self.transport.write(data)
    
    def objectReceived(self, message):

#         message = pickle.loads(data)
        logger.debug("Message type: %s", type(message))
        logger.debug("Message: %s", message)
        if not isinstance(message, AppDeployResponse):
            logger.info("BAD RESPONSE from server!")
            self.response = None
        elif message.status is AppDeployResponse.Status.ACCEPTED:
            logger.info("Application ACCEPTED")
            self.save_response(message)
        elif message.status is AppDeployResponse.Status.DUPLICATE:
            logger.warn("Application already exists")
            self.save_response(message)
        else:
            logger.error("Application was refused!")
            self.save_response(message)

        self.transport.loseConnection()
    
    def connectionLost(self, reason):
        logger.info("connection lost")


class AppDeployReqFactory(protocol.ClientFactory):
    
    def __init__(self, spec):
        self.spec = spec
        self.reason = None
        self.response = None

    def buildProtocol(self, addr):
        return AppDeployReqClient(self.spec, self.set_response)

    def set_response(self, response):
        self.response = response

    def clientConnectionFailed(self, connector, reason):
        logger.info("Connection to dispatcher failed!")
        reactor.stop()
        self.reason = reason

    def clientConnectionLost(self, connector, reason):
        # logger.debug("Traceback: %s", traceback.format_exc())
        logger.info("Connection to dispatcher lost (reason %s)!" % reason)
        logger.info("type of response: %s (%r)", type(reason), reason)
        logger.info("type of connector: %s (%r)", type(connector), connector)
        reactor.stop()

        if reason.check(ConnectionDone):
            self.reason = "Closed cleanly"
        else:
            self.reason = str(reason)

        logger.info("Reason: %s" % self.reason)


def launch_app(spec, server, port):
    factory = AppDeployReqFactory(spec)
    reactor.connectTCP(server, port, factory)
    reactor.run()
    return factory.response, factory.reason


# def main(spec, server, port):
#     factory = AppDeployReqFactory(spec)
#     reactor.connectTCP(server, port, factory)
#     reactor.run()
#     return factory.error, factory.reason
#
# if __name__ == '__main__':
#     parser = ArgumentParser(description='Send a request to dispatcher in order to deploy an application in the DC.')
#     parser.add_argument('-f', dest='spec_file', help='Path to the application\'s logical specification.')
#     parser.add_argument('-s', dest='server_ip', help='Dispatcher\'s IP address.')
#     parser.add_argument('-p', dest='server_port', type=int, help='Dispatcher\'s port.')
#
#     args = parser.parse_args()
#
#     if args.spec_file:
#         with open (args.spec_file, "r") as f:
#             app_data = f.read().replace('\n', '')
#         spec = AppLogicSpec.from_json(app_data)
#     else:
#         ag = Simple3TierAppGenerator()
#         spec = ag.generate()
#         pprint(spec)
#         logger.info("Example node: %s" % spec.get_node("tier-1"))
#     main(spec, args.server_ip, args.server_port)
