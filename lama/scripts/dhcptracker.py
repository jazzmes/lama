from time import sleep
# from twisted.internet.process import Process
from twisted.internet import reactor
# from twisted.internet.protocol import ProcessProtocol
import os
import subprocess
# import threading
from collections import deque

__author__ = 'tiago'


# class TcpdumpProcess(ProcessProtocol):
#
#     def connectionMade(self):
#         print("@connectionMade")
#
#     def outReceived(self, data):
#         print("@outReceived")
#         print(data)
#
#     def errReceived(self, data):
#         print("@errReceived")
#         print(data)
#
#     def processExited(self, reason):
#         print("@processExited")
#         print(reason)
#         reactor.stop()
#
#
# if __name__ == "__main__":
#
#     unknown_macs = set("52:54:00:98:2f:47")
#     mac_ip = {}
#
#     iface = "em1"
#     dump_cmd = "sudo tcpdump -vvven -i %s port 67 or port 68" % iface
#     # dump_cmd = "ls -la"
#     # dump_cmd = "sudo tcpdump -i %s -vvv -s 1500 ((port 67 or port 68) and (udp[8:1] = 0x1))" % iface
#     args = dump_cmd.split()
#     processProtocol = TcpdumpProcess()
#     # reactor.spawnProcess(processProtocol, args[0], args=args)
#                      # env={'HOME': os.environ['HOME']}, path,
#                      # uid, gid, usePTY, childFDs)
#     os.execvpe(args[0], args, {})
#     # reactor.run()

class AsynchronousFileReader(object):
    """
    Helper class to implement asynchronous reading of a file
    in a separate thread. Pushes read lines on a queue to
    be consumed in another thread.
    http://stefaanlippens.net/python-asynchronous-subprocess-pipe-reading
    """

    def __init__(self, fd, queue):
        assert isinstance(queue, deque)
        assert callable(fd.readline)
        # threading.Thread.__init__(self)
        self._fd = fd
        self._queue = queue

    def run(self):
        """The body of the tread: read lines and put them on the queue."""
        print("reading")
        # sleep(30)
        # for line in self._fd.readline():
        #     self._queue.append(line)
        for line in iter(self._fd.readline, b''):
            if line:
                self._queue.append(line)
            # print("line: %s" % line)
        #     self._queue.append(line)
        print("leaving")

    # def eof(self):
    #     """Check whether there is no more content to expect."""
    #     return not self.is_alive() and self._queue

    @staticmethod
    def start(stream, queue):
        print("start")
        AsynchronousFileReader(stream, queue).run()


class TcpdumpProcess(object):

    def __init__(self, cmd, poll_interval=1, event_manager=reactor):
        self.cmd = cmd
        self.proc = None
        self.poll_interval = poll_interval
        self.event_manager = event_manager
        self.check_event = None

        self.stdout_queue = deque()
        self.stderr_queue = deque()
        self.stdout_reader = None
        self.stderr_reader = None

    def start(self):
        self.proc = subprocess.Popen(dump_cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # print("Call stdout thread")
        t1 = reactor.callInThread(AsynchronousFileReader.start, self.proc.stdout, self.stdout_queue)
        t2 = reactor.callInThread(AsynchronousFileReader.start, self.proc.stderr, self.stderr_queue)
        # self.stdout_reader = AsynchronousFileReader(self.proc.stdout, self.stdout_queue)
        # self.stdout_reader.start()

        self.check()

    def check(self):
        print("Checking...")

        if not self.proc.poll():
            print("Polled... %d , %d" % (len(self.stdout_queue), len(self.stderr_queue)))
            stderr_lines = []
            while self.stdout_queue:
                stderr_lines.append(self.stdout_queue.popleft())
            print("STDERR output: \n" + stderr_lines.join('\n'))
            # while self.stdout_queue:
            #     print("Getting...")
            #     line = self.stdout_queue.popleft()
            #     print("Received line on standard output: " + repr(line))

            # Show what we received from standard error.
            while self.stderr_queue:
                print("Getting...")
                line = self.stderr_queue.popleft()
                print("Received line on standard error: " + repr(line))

            self.check_event = self.event_manager.callLater(self.poll_interval, self.check)

    def terminate(self):
        if self.check_event:
            self.check_event.cancel()
        if self.proc:
            self.proc.terminate()

        self.stderr_reader.join()
        self.stdout_reader.join()

        self.proc.stdout.close()
        self.proc.stderr.close()


if __name__ == "__main__":

    iface = "em1"
    dump_cmd = "sudo tcpdump -vvven -i %s" % iface
    dump_cmd = "sudo tcpdump -vvven -i %s port 67 or port 68" % iface

    mac_ip = {}
    unknown_macs = {"52:54:00:98:2f:47"}

    p = TcpdumpProcess(dump_cmd, poll_interval=1, event_manager=reactor)
    p.start()
    reactor.run()
    #
    # print("launching process")
    # p = subprocess.Popen(dump_cmd.split(), bufsize=0, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # print("launched process")
    # print(p.stdout)
    # while p.poll() and len(unknown_macs):
    #     print("read")
    #     print(p.stdout.read(10))
    #     print("sleep")
    #     sleep(1)
    #     # os.read(p.stdout, 10)
    # p.terminate()
