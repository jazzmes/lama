#!/usr/bin/python3

import os
import sys
import inspect

lama_path = os.path.abspath(
    os.path.join(os.path.abspath(
        os.path.split(
            inspect.getfile(inspect.currentframe())
        )[0]), '../../'))
sys.path.append(lama_path)

from lama.utils.logging_lama import logger, LamaLogger
LamaLogger.set_filename(logger, "app_temp")

from lama.agent.app.app import AppAgent

argc = len(sys.argv)
if argc < 4:
    logger.error("Script takes 4 possible arguments: provider_ip, network_id, spec_filename and port ")
    quit()

logger.detail(sys.argv)
pa = AppAgent(*sys.argv[1:])
pa.start()

