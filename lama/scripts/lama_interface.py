import json
import requests
import logging

from config.settings import DISPATCHER_AG_WEB_PORT, PROVIDER_AG_WEB_PORT

__author__ = 'tiago'


class LamaAPI(object):

    def __init__(self, dispatcher, app_name, dispatcher_port=DISPATCHER_AG_WEB_PORT):
        self.dispatcher = (dispatcher, dispatcher_port)
        self.app_name = app_name
        self.provider = None
        self.provider_port = None

    def change_workload_clients(self, config):
        if not self.provider:
            self.get_app_provider()

        if self.provider:
            resp = self.provider_request("change_workload", **{
                'app_name': self.app_name,
                'kwargs': {
                    'config': config
                }
            })
            if not resp:
                logging.warning("No confirmation for change_workload")
                return False
            else:
                logging.info(resp)
                return resp

    def change_workload_by_file(self, workload_file):
        if not self.provider:
            self.get_app_provider()

        if self.provider:
            raise NotImplementedError()

    def get_app_provider(self):
        resp = self.dispatcher_request("app_info", **{
            'app_name': self.app_name
        })
        try:
            self.provider = (resp["app"]["provider"], resp["app"]["provider_port"])
            logging.debug("Provider: %s:%s", resp["app"]["provider"], resp["app"]["provider_port"])
            return True
        except:
            logging.warning("Unable to get provider info for app '%s'! Did you launch the app?", self.app_name)
        return False

    def start_app(self):
        if not self.provider:
            self.get_app_provider()

        resp = self.provider_request("start_app", **{
            'app_name': self.app_name,
        })
        if not resp:
            logging.warning("No confirmation for start_app")
        else:
            logging.info(resp)

    def provider_request(self, action, **kwargs):
        url = 'http://%s:%s/' % (self.provider[0], PROVIDER_AG_WEB_PORT)
        return self.make_request(url, action, **kwargs)

    def dispatcher_request(self, action, **kwargs):
        url = 'http://%s:%s/' % self.dispatcher
        return self.make_request(url, action, **kwargs)

    def make_request(self, url, action, **kwargs):
        payload = kwargs
        payload.update({'action': action})
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(url, data=json.dumps(payload), headers=headers, timeout=5)
        except requests.Timeout as e:
            logging.error("Connection to '%s' timed out (%s)!", url, e)
            return None
        except requests.ConnectionError as e:
            logging.error("Error on connection to '%s' (%s)!", url, e)
            return None

        if r.status_code == 200:
            return r.json()
        else:
            return None
