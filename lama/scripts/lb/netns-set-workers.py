#! /usr/bin/env python
from argparse import ArgumentParser
import subprocess

__author__ = 'tiago'

if __name__ == "__main__":

    parser = ArgumentParser(description="Configure number of worker nodes (remote script)")
    parser.add_argument('netns', help='Namespace', type=str)
    parser.add_argument('host', help='LB IP address', type=str)
    parser.add_argument('user', help='Username', type=str)
    parser.add_argument('pwd', help='Password (CHANGE THIS PLEASE! :( )', type=str)
    parser.add_argument('nworkers', help='Number of workers', type=int)
    args = parser.parse_args()


    args = [
        "ip", "netns", "exec", args.netns,
        "lama/scripts/lb/remote-set-workers.py",
        args.host,
        args.user,
        args.pwd,
        str(args.nworkers)
    ]
    proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = proc.communicate()
    out = str(out.decode("utf-8"))
    err = str(err.decode("utf-8"))
    if err:
        print("ERROR :: %s" % err)
    if out:
        print("Output returned: %s" % out)

