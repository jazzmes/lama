#! /usr/bin/env python
import subprocess
from argparse import ArgumentParser, ArgumentTypeError

__author__ = 'tiago'


def set_workers(filename, nworkers, cluster_name, target_prefix, port, target_numbers=None):
    prefix = [
        "ServerName localhost",
        "ProxyRequests Off",
        "<Proxy \*>",
        "\tOrder deny,allow",
        "\tDeny from all",
        "</Proxy>",
        "",
        # "<Proxy balancer://cmart>",
        "<Proxy balancer://%s>" % cluster_name,
        ""
    ]
    # worker_line = "\tBalancerMember http://tomcat-%04d:8080/"
    worker_line = "\tBalancerMember http://%s-%04d:%d/"
    suffix = [
        # "BalancerMember http://tomcat-0001:8080/",
        "\tProxySet lbmethod=byrequests",
        "</Proxy>",
        "",
        "ProxyPass /balancer-manager !",
        # "ProxyPass / balancer://cmart/",
        "ProxyPass / balancer://%s/" % cluster_name,
        "",
        "<Location /balancer-manager>",
        "\tSetHandler balancer-manager",
        "</Location>",
        ""
    ]

    with open(filename, "w") as fp:
        fp.write("\n".join(prefix))
        for i in target_numbers or range(1, nworkers + 1):
            if not worker_line.endswith("\n"):
                worker_line += "\n"
            fp.write(worker_line % (target_prefix, i, port))
        fp.write("\n".join(suffix))

    # relaunch apacher
    try:
        cmd = subprocess.Popen(["service", "httpd", "graceful"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except OSError as e:
        print("Unable to restart apacher...")
    else:
        out, err = cmd.communicate()
        out = str(out.decode("utf-8"))
        err = str(err.decode("utf-8"))
        if err:
            print("Error returned: %s" % err)
        if out:
            print("Output returned: %s" % out)

    try:
        cmd = subprocess.Popen(["/root/balancer-manage.py", "-l"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = cmd.communicate()
        print(str(out.decode("utf-8")))
    except Exception as e:
        print("Unable to list workers (%s)." % e)


def csv_int(val):
    return csv(val, int)


def csv_str(val):
    return csv(val, str)


def csv(val, vartype):
    try:
        return tuple([vartype(v) for v in val.split(",")])
    except:
        raise ArgumentTypeError("Bad CSV of type %s" % vartype)


if __name__ == "__main__":

    parser = ArgumentParser(description="Configure number of worker nodes")
    parser.add_argument('filename', help='Apache configuration file', type=str)
    parser.add_argument('cluster_name', help='The name of the cluster (e.g. rubbos-lb-apache)', type=str)
    parser.add_argument('target_prefix', help='The prefix of the instances to be balanced', type=str)
    parser.add_argument('--p', dest='port', help='Port of the service', type=int, default=80)

    wk_group = parser.add_mutually_exclusive_group(required=True)
    wk_group.add_argument('--ids', dest="target_ids", help='CSV string with the ids of the target vms', type=csv_int, default=None)
    wk_group.add_argument('--n', dest="nworkers", help='Number of workers', type=int, default=None)
    args = parser.parse_args()
    set_workers(
        args.filename,
        args.nworkers or len(args.target_ids),
        cluster_name=args.cluster_name,
        target_prefix=args.target_prefix,
        port=args.port,
        target_numbers=args.target_ids
    )
