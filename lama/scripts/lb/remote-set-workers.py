#! /usr/bin/env python
from argparse import ArgumentParser
from paramiko.client import SSHClient, AutoAddPolicy

__author__ = 'tiago'


# def remote_set_workers(host, user, password):
#     try:
#         session = pxssh.pxssh()
#         session.login(host, user, password=password)
#
#         session.sendline("/root/set-workers.py /usr/share/apache2/conf.d/lb.conf %d" % args.nworkers)
#         session.prompt()
#         print("".join(session.before.splitlines(True)[1:]))
#
#         session.close()
#     except Exception as e:
#         raise e

class TestSetWorkers(object):
    def __init__(self, ip, user, password, workers):
        self.ip = ip
        self.user = user
        self.password = password
        self.workers = workers
        self.client = SSHClient()

    def connect_to_vm(self):
        self.client = SSHClient()
        self.client.set_missing_host_key_policy(AutoAddPolicy())
        print("Connection to %s with user %s" % (self.ip, self.user))
        self.client.connect(self.ip, username=self.user, password=self.password, timeout=5)

    def exec(self, cmd):
        print("Exec command: %s" % cmd)
        print("Client: %s" % self.client)
        stdin, stdout, stderr = self.client.exec_command(cmd, get_pty=True)
        out = err = ""
        print("Looping...")
        while not stdout.channel.exit_status_ready():
            if stdout.channel.recv_ready():
                out = stdout.channel.recv(1024).decode("utf-8")
            if stderr.channel.recv_ready():
                err = stderr.channel.recv(1024).decode("utf-8")
        out += "\n".join(stdout.readlines())
        err += "\n".join(stderr.readlines())
        print("Out: %s" % out)
        print("Err: %s" % err)
        return out, err

    def close(self):
        self.client.close()

    def run(self):
        self.connect_to_vm()
        self.exec("ls -la")
        self.exec("sudo ls -la")
        s = ",".join([str(i) for i in range(abs(self.workers))])
        print("Cmd: /root/set-workers.py /usr/share/apache2/conf.d/lb.conf cluster_lb-apache apache --ids %s" % s)
        self.exec("/root/set-workers.py /usr/share/apache2/conf.d/lb.conf cluster_lb-apache apache --ids %s" % s)
        self.close()


if __name__ == "__main__":
    parser = ArgumentParser(description="Configure number of worker nodes (remote script)")
    parser.add_argument('host', help='LB IP address', type=str)
    parser.add_argument('user', help='Username', type=str)
    parser.add_argument('pwd', help='Password (CHANGE THIS PLEASE! :()', type=str)
    parser.add_argument('-w', dest='nworkers', help='Number of workers', type=int, default=1)
    args = parser.parse_args()

    TestSetWorkers(args.host, args.user, password=args.pwd, workers=args.nworkers).run()
    # remote_set_workers(args.host, args.user, password=args.pwd)
