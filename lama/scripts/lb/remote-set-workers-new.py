#! /usr/bin/env python
from argparse import ArgumentParser
import pxssh

__author__ = 'tiago'

def set_workers(session, filename, nworkers):
    prefix = [
        "ProxyRequests Off",
        "<Proxy \*>",
        "\tOrder deny,allow",
        "\tDeny from all",
        "</Proxy>",
        "",
        "<Proxy balancer://cmart>",
        ""
    ]
    worker_line = "\tBalancerMember http://tomcat-%04d:8080/"
    suffix = [
        # "BalancerMember http://tomcat-0001:8080/",
        "\tProxySet lbmethod=byrequests",
        "</Proxy>",
        "",
        "ProxyPass /balancer-manager !",
        "ProxyPass / balancer://cmart/",
        "",
        "<Location /balancer-manager>",
        "\tSetHandler balancer-manager",
        "</Location>",
        ""
    ]

    with open(filename, "w") as fp:
        fp.write("\n".join(prefix))
        for i in range(0, nworkers):
            if not worker_line.endswith("\n"):
                worker_line += "\n"
            fp.write(worker_line % i)
        fp.write("\n".join(suffix))

    # copy file
    # restart apache
    # list workers

    # relaunch apacher
    try:
        cmd = subprocess.Popen(["service", "httpd", "graceful"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except OSError as e:
        print("Unable to restart apacher...")
    else:
        out, err = cmd.communicate()
        out = str(out.decode("utf-8"))
        err = str(err.decode("utf-8"))
        if err:
            print("Error returned: %s" % err)
        if out:
            print("Output returned: %s" % out)

    try:
        cmd = subprocess.Popen(["./balancer-manage.py", "-l"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = cmd.communicate()
        print(str(out.decode("utf-8")))
    except Exception as e:
        print("Unable to list workers (%s)." % e)

if __name__ == "__main__":

    parser = ArgumentParser(description="Configure number of worker nodes (remote script)")
    parser.add_argument('host', help='LB IP address', type=str)
    parser.add_argument('user', help='Username', type=str)
    parser.add_argument('pwd', help='Password (CHANGE THIS PLEASE! :()', type=str)
    parser.add_argument('nworkers', help='Number of workers', type=int)
    args = parser.parse_args()

    try:
        session = pxssh.pxssh()
        session.login(args.host, args.user, password=args.pwd)

        session.sendline("./set-workers.py /usr/share/apache2/conf.d/lb.conf %d" % args.nworkers)
        session.prompt()
        print("".join(session.before.splitlines(True)[1:]))

        session.close()
    except Exception as e:
        print("ERROR::%s" % e)