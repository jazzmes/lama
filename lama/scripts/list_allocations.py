import sys
import requests
import json
import logging
from netaddr import IPAddress
from config.settings import DISPATCHER_AG_WEB_PORT, PROVIDER_AG_WEB_PORT

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

logger = logging.getLogger(__name__)


def configure_logger(level):
    logger.setLevel(level)
    ch = logging.StreamHandler(sys.stdout)
    # formatter = logging.Formatter('%(asctime)s - %(name)s %(levelname)s - %(message)s')
    formatter = logging.Formatter('%(asctime)s: %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)


def make_request(request_url, action, **kwargs):
    payload = kwargs
    payload.update({'action': action})
    headers = {'content-type': 'application/json'}
    try:
        r = requests.post(request_url, data=json.dumps(payload), headers=headers, timeout=5)
    except requests.Timeout as e:
        logger.error("Connection to '%s' timed out (%s)!", url, e)
        return None
    except requests.ConnectionError as e:
        logger.error("Error on connection to '%s' (%s)!", url, e)
        return None

    if r.status_code == 200:
        return r.json()
    else:
        return None


configure_logger(logging.DEBUG)
dispatcher_address = '10.1.1.12'
dispatcher_port = DISPATCHER_AG_WEB_PORT

red = '\033[91m'
green = '\033[92m'
yellow = '\033[93m'
blue = '\033[94m'
end_color = '\033[0m'

url_fmt = 'http://{}:{}/'
url = url_fmt.format(dispatcher_address, dispatcher_port)
result = make_request(url, 'provider_agent_clusters')
# logger.info(result)


provider_ips = sorted(IPAddress(provider.get('name')) for provider in result.get('providers'))
for provider in provider_ips:
    print("\tprovider={}".format(provider))
    result = make_request(url_fmt.format(provider, PROVIDER_AG_WEB_PORT), 'agent_info')
    allocations = result.get('allocations')
    if not allocations:
        print("\t\t---")
    else:
        print(
            "\t\t{}{:<14}{:<20}{:>3}{:>8}{}".format(
                blue,
                "App",
                "Instance",
                "CPU",
                "RAM",
                end_color
            )
        )
        for allocation in allocations:
            cpu = int((allocation.get('spec', {}).get('CPU', []) or [{}])[0].get('Number of Processors', 0)) or None
            ram = (allocation.get('spec', {}).get('RAM', [{}]) or [{}])[0].get('Size', None)
            # print(allocation)
            print(
                "\t\t{}{:<14}{:<20}{}{:>3}{:>8}".format(
                    '' if cpu and ram else (red if ram else yellow),
                    allocation.get('app_name'),
                    allocation.get('instance_name'),
                    '' if cpu and ram else end_color,
                    cpu or '-',
                    ram or '-'
                )
            )
