import sys
import subprocess
import re
import getpass

import libvirt
import numpy
try:
    from lama.utils.logging_lama import logger
except ImportError:
    pass


__author__ = "Tiago Carvalho"
__copyright__ = "Copyright 2014, LAMA Project"

__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Tiago Carvalho"
__email__ = "tcarvalho@cmu.edu"


class SystemParser(object):
    """
    Parses the system for resource information (Disk space and IO, CPU, Memory, Network)
    """

    BYTE_MULTIPLIERS = {'k': 1024, 'm': 1024 * 1024, 'g': 1024 * 1024 * 1024, 't': 1024 * 1024 * 1024 * 1024}
    BIT_MULTIPLIERS = {'k': 1E3, 'm': 1E6, 'g': 1E9, 't': 1E12}

    LETTER_MULTIPLIERS = 'kMGTPEZY'

    DISK_TOTAL_STATS = 3

    def __init__(self):
        pass

    def val2str(self, value, unit='B'):
        step = 1024 if unit is 'B' else 1000

        value = float(value)
        i = 0
        while value >= step:
            value /= step
            i += 1

        valstr = ("%0.2f" % value).rstrip('0').rstrip('.')
        if i > 0:
            return "%s%s%s" % (valstr, self.LETTER_MULTIPLIERS[i - 1], unit)

        return "%s%s" % (valstr, unit)

    def get_disk_device_name(self, partition):
        prefixes = ["/dev/sd", "/dev/hd"]
        for prefix in prefixes:
            match = re.search('(%s[a-zA-Z]+)\d+' % prefix.replace("/", "\/"), partition)
            if match:
                return match.group(1)

        return partition

    def parse_disk_info(self, path='/home', test_speed=False, output=None):
        """
        Retrieve information about disk info and test speed. Gets current partition.
        TODO: should get partitions explicitly define in configuration or find some logic to select partitions to use (partitions with a minimal size??). 
        """
        if output:
            print("\nExtracting disk info...")
        # note: subprocess.call blocks while subprocess.Popen doesn't
        cmd = subprocess.Popen(["df", "-P", path], stdout=subprocess.PIPE)
        out, err = cmd.communicate()
        out = out.decode("utf-8")
        out = out.splitlines()
        keys = out.pop(0).split()
        # could have used -B 1 to get 1-blocks but the option is not available on all versions
        blocks = int(next(k.split("-")[0] for k in keys if k.endswith("-blocks")))

        partitions = list()
        for line in out:
            raw = dict(zip(keys, line.split()))
            part = dict()
            part["Total"] = int(raw["%s-blocks" % blocks]) * blocks
            part["Available"] = int(raw["Available"]) * blocks
            part["Used"] = int(raw["Used"]) * blocks
            part["Filesystem"] = raw["Filesystem"]
            part["Device"] = self.get_disk_device_name(part["Filesystem"])
            partitions.append(part)

            # get logical device for mapper
            # TODO: very error prone (tested on fedora)
            if part["Device"].startswith("/dev/mapper"):
                logger.debug("Partition: %s", part)
                cmd_line = "%sdmsetup info %s" % ('' if getpass.getuser() == "root" else 'sudo ', part["Device"])
                cmd = subprocess.Popen(cmd_line.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = cmd.communicate()
                result = re.search('Major, minor: *\d*, *(\d*)', out.decode("UTF-8"))
                # logger.temp("Partition: %s", part)
                # if err:
                #     logger.temp("Error:  %s", err.decode("UTF-8"))
                # logger.temp("Output: %s", out.decode("UTF-8"))
                # logger.temp("Result: %s", result)
                part["LogDev"] = "/dev/dm%s" % result.group(1)

                cmd = subprocess.Popen(["dmsetup", "splitname", part["Device"]], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = cmd.communicate()
                out = out.decode("UTF-8")
                logger.debug("Output: %s", out.splitlines())
                out = out.splitlines().pop()
                logger.debug("Output: %s", out)
                result = re.search('\S* +(\S*) *\S*', out)
                # result = re.search('(?:\S+: *(\S+))', out.decode("UTF-8"))
                if err:
                    logger.debug("Error:  %s", err.decode("UTF-8"))
                logger.debug("Result: %s", result)
                part["Name"] = result.group(1)
                logger.debug("Partition: %s", part)

        if output:
            for part in partitions:
                print(" * Partition: %s (Device: %s)" % (part["Filesystem"], part["Device"]))
                for k in part.keys():
                    try:
                        print("    - %s: %s" % (k, self.val2str(part[k])))
                    except ValueError:
                        pass

        if test_speed:
            for part in partitions:
                # test write speed

                if output:
                    print("")
                    print(" * Testing device read speed...")
                    sys.stdout.write("   ")

                device = part["Device"]

                c_rlist = list()
                b_rlist = list()
                for attempt in range(self.DISK_TOTAL_STATS):
                    if output:
                        sys.stdout.write('.')
                        sys.stdout.flush()
                    logger.debug(["sudo", "hdparm", "-Tt", device])
                    cmd_line = "%shdparm -Tt %s" % ('' if getpass.getuser() == "root" else 'sudo ', device)
                    cmd = subprocess.Popen(cmd_line.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    # cmd = subprocess.Popen(["sudo", "hdparm", "-Tt", device], stdout=subprocess.PIPE)
                    out, err = cmd.communicate()
                    out = out.decode("utf-8")

                    match = re.findall('\s(\d*.\d+)\s([k|m|g|t])b/sec', out.lower())
                    if match:
                        cache = int(float(match[0][0]) * self.BYTE_MULTIPLIERS[match[0][1]])
                        buffer = int(float(match[1][0]) * self.BYTE_MULTIPLIERS[match[1][1]])

                        c_rlist.append(cache)
                        b_rlist.append(buffer)

                if output:
                    print("")

                c_mean_read = numpy.mean(c_rlist)
                c_sdev_read = numpy.std(c_rlist)
                b_mean_read = numpy.mean(b_rlist)
                b_sdev_read = numpy.std(b_rlist)
                part['mean_cache_read'] = c_mean_read
                part['sdev_cache_read'] = c_sdev_read
                part['mean_buffer_read'] = b_mean_read
                part['sdev_buffer_read'] = b_sdev_read

                if output:
                    print("    - Average Read speed (cache):   %s/sec (stddev: %s)" % (self.val2str(c_mean_read),
                                                                                       self.val2str(c_sdev_read)))
                    print("    - Average Read speed (buffer):  %s/sec (stddev: %s)" % (self.val2str(b_mean_read),
                                                                                       self.val2str(b_sdev_read)))

                    print(" * Testing device write speed...")

                wlist = list()
                for attempt in range(self.DISK_TOTAL_STATS):

                    cmd = subprocess.Popen(["dd", "if=/dev/zero", "of=output.xpto", "conv=sync", "bs=8k", "count=128k"],
                                           stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    out, err = cmd.communicate()
                    err = err.decode("utf-8")
                    subprocess.Popen(["rm", "-f", "output.xpto"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

                    match = re.findall('\s(\d*.\d+)\s([k|m|g|t])b/s', err.lower())
                    if match:
                        cache = int(float(match[0][0]) * self.BYTE_MULTIPLIERS[match[0][1]])
                        wlist.append(cache)

                mean_write = numpy.mean(wlist)
                sdev_write = numpy.std(wlist)
                part['mean_write'] = mean_write
                part['sdev_write'] = sdev_write

                if output:
                    print("")
                    print("    - Average Write speed:  %s/sec (stddev: %s)" % (self.val2str(mean_write), self.val2str(sdev_write)))

        return partitions

    def parse_cpu_mem_info(self, output=None):

        if output:
            print("\nExtracting cpu and memory info...")
        conn = libvirt.openReadOnly(None)

        # Example:
        # CPU model:           x86_64
        # CPU(s):              1
        # CPU frequency:       2294 MHz
        # CPU socket(s):       1
        # Core(s) per socket:  1
        # Thread(s) per core:  1
        # NUMA cell(s):        1
        # Memory size:         1019020 KiB
        cpu_keys = ['Model', 'RAM', 'CPUs', 'Frequency', 'Sockets', 'Cores', 'Threads', 'NUMA']
        cpu_info = conn.getInfo()
        conn.close()
        sysinfo = dict(zip(cpu_keys, cpu_info))

        sysinfo['RAM'] *= 1024
        sysinfo['Frequency'] *= 1e6

        if output:
            for key, value in sysinfo.items():
                if key is 'RAM':
                    print(" * %s: %s" % (key, self.val2str(value)))
                elif key is 'Frequency':
                    print(" * %s: %s" % (key, self.val2str(value, unit='Hz')))
                else:
                    print(" * %s: %s" % (key, value))
        return sysinfo

    def parse_mem_info(self, output=True):

        if output:
            print("\nExtracting memory info...")
        conn = libvirt.openReadOnly(None)

        mem_info = conn.getMemoryStats(libvirt.VIR_NODE_MEMORY_STATS_ALL_CELLS, 0)
        mem_info.update((k, v * 1024) for k, v in mem_info.items())
        conn.close()

        if output:
            for key, value in mem_info.items():
                print(" * %s: %s" % (key.capitalize(), self.val2str(value)))

        return mem_info

    def parse_network_info(self, output=True):

        if output:
            print("\nExtracting interfaces...")
        conn = libvirt.openReadOnly(None)

        ifaces = conn.listAllInterfaces(libvirt.VIR_CONNECT_LIST_INTERFACES_ACTIVE)
        conn.close()

        interfaces = []
        for iface in ifaces:
            cmd = subprocess.Popen(["ethtool", iface.name()], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = cmd.communicate()
            out = out.decode("utf-8")

            print(out)

            match = re.search('speed:\s?(\d+)([k,m,g,t])b\/s.+duplex:\s?(half|full)', out.lower(), re.DOTALL)
#             match = re.search('speed:\s?(\d+)([k,m,g,t])b\/s', out.lower())
            if match:
                speed = int(match.group(1)) * self.BIT_MULTIPLIERS[match.group(2)]
                mode = match.group(3)
                interfaces.append({"name": iface.name(), "speed": speed, "mode": mode})

                if output:
                    print(" * Name: %s, Speed: %sps, Mode: %s" % (iface.name(), self.val2str(speed, unit='b'), mode))

        return interfaces


if __name__ == "__main__":

#     if os.geteuid() != 0:
#         exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")

    sp = SystemParser()
    sp.parse_disk_info(test_speed=True, output=True)
    sp.parse_cpu_mem_info(output=True)
    sp.parse_mem_info(output=True)
    sp.parse_network_info(output=True)
    print("")
