#!/usr/bin/python3

import os
import sys
import inspect
from logging import DEBUG

lama_path = os.path.abspath(
    os.path.join(os.path.abspath(
        os.path.split(
            inspect.getfile(inspect.currentframe())
        )[0]), '../../'))
sys.path.append(lama_path)

from lama.utils.logging_lama import logger
from lama.agent.provider.provider import ProviderAgent

config_file = None

argc = len(sys.argv)
if argc > 2:
    logger.error("ERROR - First and only (optional) argument should be <config_file> (type: str)")
    quit()
    
elif argc > 1:
    config_file = sys.argv[1]
    if not sys.path.isfile(config_file):
        logger.error("ERROR - File '%s' does not exist or cannot be read." % config_file)
        quit()

if config_file:
    logger.info("Launching provider with config file '%s'" % str(config_file))
else:
    logger.info("Launching provider with no inline config file.")

pa = ProviderAgent.from_config_file(config_file)
pa.start()

