import json
import traceback
from argparse import ArgumentParser, ArgumentTypeError
from lama.agent.app.app_arch import AppArch, InstanceState
from lama.agent.app.comm import LamaRedisQueue, Command
from lama.agent.event_managers.emredis import LamaKeyStore

__author__ = 'Tiago'


def instances_number(a):
    try:
        a, h, w = a.split(',')
        return int(a), int(h), int(w)
    except:
        raise ArgumentTypeError("Specify number of instances as a list of ints: <active>,<hot>,<warm>")

# read argument
parser = ArgumentParser()
parser.add_argument('app_name', type=str, help="Application name")
parser.add_argument('service_name', type=str, help="Service name")
parser.add_argument('num_instances', type=instances_number, help="Target number of instances")
parser.add_argument('-a', dest="check_app", help="Do not check application", action="store_false")

args = parser.parse_args()
print(args)
app = None
service = None
num_instances = [0, 0, 0]
if args.check_app:
    # get app
    try:
        redis = LamaKeyStore()
        redis.get("app:%s" % args.app_name)
        d = redis.get("app:%s" % args.app_name)
        d = json.loads(d)
        app = AppArch.from_dict(d)
    except Exception as e:
        print("Unable to retrieve app '%s'. Traceback: %s" % (args.app_name, traceback.format_exc()))
        exit(1)

    service = app.get_service(args.service_name)
    if not service:
        print("Unable to retrieve service '%s'. Available services: %s" % (args.service_name, app.get_services()))
        exit(1)

    num_instances = tuple(len(service.get_instances(states=(state,))) for state in (InstanceState.Active, InstanceState.Hot, InstanceState.Warm))
    if args.num_instances == num_instances:
        print("Service '%s:%s' already has %s instances." % (args.app_name, args.service_name, args.num_instances))
        exit(1)

# connect to redis
queue = LamaRedisQueue("%s:monitor:main" % args.app_name)
cmd = Command(queue.change_instances(args.service_name, *args.num_instances))
if num_instances:
    print("Sent order to change instances of %s:%s from %s to %s" %
          (args.app_name, args.service_name, num_instances, args.num_instances))
else:
    print("Sent order to change instances of %s:%s to %s" %
          (args.app_name, args.service_name, args.num_instances))
