from config.settings import PATH_IMAGE_POOL
from lama.virt_api import virt_install
from lama.virt_api.virtinst import cli
from lama.utils.logging_lama import logger

__author__ = 'tiago'

cmd_line = "--name test_app_1 --cpu host --vcpus 1 --ram 1024 --os-type=linux" \
           " --disk path=%sApplication.qcow2 --vnc --import" % PATH_IMAGE_POOL
print(cmd_line)
args = cmd_line.split()
print("Arguments: %s" % args)


class LibvirtAPIError(Exception):
    pass


def create(args, conn=None):
    # cli.earlyLogging()
    options = virt_install.parse_args(args)

    # Default setup options
    options.quiet = options.xmlstep or options.xmlonly or options.quiet

    cli.setupLogging("virt-install", options.debug, options.quiet)

    virt_install.check_cdrom_option_error(options)

    cli.set_force(options.force)
    cli.set_prompt(options.prompt)

    parsermap = cli.build_parser_map(options)
    if cli.check_option_introspection(options, parsermap):
        return 0

    if conn is None:
        conn = cli.getConnection(options.connect)

    if options.xmlstep not in [None, "1", "2", "3", "all"]:
        logger.error("--print-step must be 1, 2, 3, or all")
        raise LibvirtAPIError("--print-step must be 1, 2, 3, or all")

    print(".")
    guest = virt_install.build_guest_instance(conn, options, parsermap)
    print(".")
    continue_inst = guest.get_continue_inst()

    if options.xmlstep or options.xmlonly or options.dry:
        xml = virt_install.xml_to_print(guest, continue_inst,
                           options.xmlonly, options.xmlstep, options.dry)
        if xml:
            logger.info("XML: %s", xml)
            raise NotImplementedError()
            # print_stdout(xml, do_force=True)
    else:
        virt_install.start_install(guest, continue_inst, options)
        print(3)

    return 0


create(args)