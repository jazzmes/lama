#!/usr/bin/python3
import os
import sys
import inspect

lama_path = os.path.abspath(
    os.path.join(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]), '../../'))
sys.path.append(lama_path)

from lama.agent.dispatcher.dispatcher import DispatcherAgent

# print("Launching dispatcher...")
DispatcherAgent().start()
