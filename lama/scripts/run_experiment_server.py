#!/usr/bin/python3

import os
import sys
import inspect
from lama.ops.experiments import AppDistribution, Experiment

lama_path = os.path.abspath(
    os.path.join(os.path.abspath(
        os.path.split(
            inspect.getfile(inspect.currentframe())
        )[0]), '../../'))
sys.path.append(lama_path)

from lama.utils.logging_lama import logger

logger.info("Launching experiment server.")

dist = AppDistribution.factory("Uniform")
exp = Experiment("10.1.1.7", 1, dist, name=cmdargs.name, runtime=cmdargs.runtime)
signal.signal(signal.SIGINT, exp.hard_stop)
exp.start()