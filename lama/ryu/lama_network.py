import traceback
from collections import namedtuple
from functools import partial


from ryu.base.app_manager import RyuApp
from ryu.cfg import CONF
from ryu.controller import ofp_event
from ryu.controller.handler import set_ev_cls, MAIN_DISPATCHER, CONFIG_DISPATCHER
from ryu.lib.ovs.bridge import OVSBridge
from ryu.lib.packet import ethernet, packet
from ryu.topology import api as topo_api
from ryu.topology import event as topo_event
from netaddr import EUI

from lama.agent.helpers import network_names_helper as neth
from lama.ryu.ovsdb_client import OVSDbClient
from lama.utils.logging_lama import logger, LamaLogger

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


LamaLogger.set_filename(logger, '/var/log/lama/lama_controller_{}.log'.format(CONF['lama']['app']))


class LamaSwitch(RyuApp):

    OvsdbConf = namedtuple('OvsdbConf', ['ovsdb_timeout'])
    OvsdbPort = 6640

    def __init__(self, *args, **kwargs):
        super(LamaSwitch, self).__init__(*args, **kwargs)

        self._app_name = CONF['lama']['app']
        self._app_tun_id = int(CONF['lama']['nid'])

        self._switch_to_ip = {}
        self._mac_to_port = {}
        self._tunnel_ports = {}
        self._switch_ports = {}

        self._permanent_flood_rules = {}

    def dump_ports(self):
        str = "\n"
        for dpid, ports in self._mac_to_port.items():
            str += "\t%s:".format(dpid)
            for mac, port in ports.items():
                str += "\t%s > %s\n".format(mac, port)

        return str

    def update_ports(self, switch_ip):
        logger.debug("app_name=%s, ip=%s", self._app_name, switch_ip)
        local_ports, remote_ports, gw = OVSDbClient.get_ports(
            bridge_name=neth.create_bridge_name(self._app_name),
            ovs_ip=switch_ip
        )

        logger.debug("Update ports:")
        logger.debug("\tlocal ports:  %s", local_ports)
        logger.debug("\tremote ports: %s", remote_ports)
        logger.debug("\tdhcp:         %s", gw)

        ports = {}
        for port in local_ports.values():
            if 0 < port['ofport'] < 65000:
                ports[port['ofport']] = {'remote': False}

        for port in remote_ports.values():
            if 0 < port['ofport'] < 65000:
                ports[port['ofport']] = {'remote': True}

        return ports

    def add_flow(self, datapath, priority, match, actions, buffer_id=None, idle_timeout=0, hard_timeout=0):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst, idle_timeout=idle_timeout,
                                    hard_timeout=hard_timeout)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst, idle_timeout=idle_timeout,
                                    hard_timeout=hard_timeout)
        datapath.send_msg(mod)

    # def create_tunnel(self, datapath, tunnel_name, tunnel_local_ip, tunnel_peer_ip, tunnel_type='vxlan'):
    def create_tunnel(self, src_switch, dst_switch, tunnel_type='vxlan'):

        local_ip = src_switch.dp.socket.getpeername()[0]
        remote_ip = dst_switch.dp.socket.getpeername()[0]

        ovsdb_addr = "tcp:%s:%d" % (self._switch_to_ip[src_switch.dp.id], LamaSwitch.OvsdbPort)
        tunnel_name = "vt-%s-%s" % (self._app_tun_id, '.'.join(str(remote_ip).split('.')[-2:]))

        # "tcp:10.1.1.8:6640"
        c = LamaSwitch.OvsdbConf(ovsdb_timeout=10)
        bridge = OVSBridge(c, src_switch.dp.id, ovsdb_addr)
        try:
            bridge.br_name = neth.create_bridge_name(self._app_name)
        except:
            logger.warn("Failed to create bridge. Traceback: %s", traceback.format_exc())
            return False

        try:
            logger.debug("New tunnel %s -> %s: name=%s, type=%s, key=%s",
                         local_ip, remote_ip, tunnel_name, tunnel_type, self._app_tun_id)
            bridge.add_tunnel_port(tunnel_name, tunnel_type, local_ip, remote_ip, self._app_tun_id)
        except:
            logger.warn("Failed to add tunnel. Traceback: %s", traceback.format_exc())
            return False

        return True

    def get_flood_ports(self, datapath, in_port):
        dpid = datapath.id
        logger.debug("Find flood ports: dpid=%s in_port=%s", dpid, in_port)
        switch_ip = self._switch_to_ip.get(dpid)
        ports = []
        if switch_ip:
            sw_port = self._switch_ports.get(dpid, {}).get(in_port)
            if not sw_port:
                self._switch_ports[dpid] = self.update_ports(switch_ip)
                sw_port = self._switch_ports.get(dpid, {}).get(in_port)

            if sw_port:
                logger.temp("ps=%s, sw_port=%s", self._switch_ports.get(dpid), sw_port)
                in_remote = sw_port.get('remote')

                # if in port is remote
                if not in_remote:
                    ports = [datapath.ofproto.OFPP_FLOOD]
                else:
                    ports = [port_no for port_no, props in self._switch_ports.get(dpid).items()
                             if not props.get('remote')]
            else:
                logger.warn("Could not find port: in_port=%s, sw_ip=%s", in_port, switch_ip)
        return ports

    @set_ev_cls(topo_event.EventSwitchEnter)
    def switch_enter_handler(self, ev):
        logger.debug(type(ev.switch))
        logger.debug(ev.switch)

        # local_dpid = ev.datapath.id
        for src_switch in topo_api.get_all_switch(self):
            addr = src_switch.dp.socket.getpeername()[0]
            logger.debug("Switch: ip=%s, id = %s, of_ver=%s,%s)", addr,
                         src_switch.dp.id, src_switch.dp.ofproto.__name__, src_switch.dp.ofproto.OFP_VERSION)
            self._switch_to_ip[src_switch.dp.id] = addr

            tunnels = self._tunnel_ports.setdefault(src_switch.dp.id, {})
            for dst_switch in topo_api.get_all_switch(self):
                if src_switch.dp.id != dst_switch.dp.id:
                    if dst_switch.dp.id not in tunnels:
                        if dst_switch.dp.id not in tunnels:
                            tunnels[dst_switch.dp.id] = self.create_tunnel(
                                src_switch, dst_switch
                            )

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]

        logger.debug("OFPSwitchFeatures dpid=%s", ev.msg.datapath.id)
        self.add_flow(datapath, 0, match, actions)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        try:
            if ev.msg.msg_len < ev.msg.total_len:
                logger.debug("packet truncated: only %s of %s bytes", ev.msg.msg_len, ev.msg.total_len)

            msg = ev.msg
            datapath = msg.datapath
            ofproto = datapath.ofproto
            parser = datapath.ofproto_parser
            in_port = msg.match['in_port']

            pkt = packet.Packet(msg.data)
            eth = pkt.get_protocols(ethernet.ethernet)[0]

            dst = eth.dst
            src = eth.src

            dpid = datapath.id

            logger.info("Packet recv: %s", pkt)
            self._mac_to_port.setdefault(dpid, {})

            # self.logger.info("packet in %s %s %s %s", dpid, src, dst, in_port)

            # learn a mac address to avoid FLOOD next time.
            self._mac_to_port[dpid][src] = in_port
            logger.debug("Updated port [%s] %s -> %s", dpid, src, in_port)

            # out_ports = self.get_ports(datapath, dst, in_port)
            flood = False
            if dst in self._mac_to_port[datapath.id]:
                out_ports = [self._mac_to_port[datapath.id][dst]]
            else:
                flood = True
                out_ports = self.get_flood_ports(datapath, in_port)
                logger.warn("Flood packet: in_port=%s, dst=%s, ports=%s", in_port, dst, out_ports)

            # check if it is a 'special' address that we can permanently setup rules for
            idle_timeout = 0
            permanent_flood = False
            logger.temp("The multicast bit: %s", EUI(dst)[0] & 1)
            if EUI(dst)[0] & 1:
                logger.temp("Permanent flood...")
                # if multicast or broadcast
                permanent_flood = True
                idle_timeout = 30

            if out_ports:
                actions = [parser.OFPActionOutput(out_port) for out_port in out_ports]

                logger.temp("Permanent flood? %s", permanent_flood)
                if not flood or permanent_flood:
                    # install a flow to avoid packet_in next time
                    match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
                    # verify if we have a valid buffer_id, if yes avoid to send both
                    # flow_mod & packet_out
                    if msg.buffer_id != ofproto.OFP_NO_BUFFER:
                        logger.debug('buffer_id != OFP_NO_BUFFER')
                        logger.warn("Install flow: dp=%s, prio=%s, match=%s, actions=%s, buf=%s",
                                    datapath, 1, match, actions, msg.buffer_id)
                        self.add_flow(datapath, 1, match, actions, msg.buffer_id, idle_timeout=idle_timeout)
                    else:
                        logger.debug('buffer_id == OFP_NO_BUFFER')
                        logger.warn("Install flow: dp=%s, prio=%s, match=%s, actions=%s, buf=%s",
                                    datapath, 1, match, actions, None)
                        self.add_flow(datapath, 1, match, actions, idle_timeout=idle_timeout)

                    logger.temp("Permanent flood? %s", permanent_flood)
                    if permanent_flood:
                        logger.temp("Add permanent rule to %s: %s", dpid, (in_port, dst))
                        switch_permanent_flood_rules = self._permanent_flood_rules.setdefault(dpid, set())
                        switch_permanent_flood_rules.add((in_port, dst))

                data = None
                if msg.buffer_id == ofproto.OFP_NO_BUFFER:
                    data = msg.data

                logger.debug("Send actions: %s", actions)
                out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                          in_port=in_port, actions=actions, data=data)
                datapath.send_msg(out)

            # no ports: it can happen in case of flooding a packet from a remote port
            # before local ports have been added
        except:
            logger.error("Traceback: %s", traceback.format_exc())

    def delete_rules(self, datapath, **match_args):
        logger.warn("Delete permanent flood rules for local switch %s: %s", datapath.id, match_args)

        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        match = parser.OFPMatch(**match_args)
        mod = datapath.ofproto_parser.OFPFlowMod(
            datapath=datapath, match=match, cookie=0,
            command=ofproto.OFPFC_DELETE,
            out_port=ofproto.OFPP_ANY,
            out_group=ofproto.OFPG_ANY
        )
        datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPPortStatus, MAIN_DISPATCHER)
    def port_status_handler(self, ev):
        msg = ev.msg
        dp = msg.datapath
        ofp = dp.ofproto

        if msg.reason == ofp.OFPPR_ADD:
            reason = 'ADD'
            ports = self._switch_ports.setdefault(dp.id, {})
            ports[msg.desc.port_no] = {'remote': msg.desc.name.startswith('vt-')}
        elif msg.reason == ofp.OFPPR_DELETE:
            reason = 'DELETE'
        elif msg.reason == ofp.OFPPR_MODIFY:
            reason = 'MODIFY'
        else:
            reason = 'unknown'

        logger.temp("Permanent rules: %s", self._permanent_flood_rules.get(dp.id))
        logger.temp("Permanent rules: %s", self._permanent_flood_rules.get(dp.id, set()))

        for in_port, dst in self._permanent_flood_rules.get(dp.id, set()):
            logger.temp("Will delete: in_port=%s, eth_dst=%s", in_port, dst)
            self.delete_rules(dp, in_port=in_port, eth_dst=dst)
        self._permanent_flood_rules.get(dp.id, set()).clear()
        logger.debug('OFPPortStatus received: reason=%s desc=%s', reason, msg.desc)

