import json
import traceback
from logging import FileHandler, Formatter

from lama.agent.event_managers.emredis import LamaKeyStore
from lama.ryu.ovsdb_client import OVSDbClient
from ryu.base.app_manager import RyuApp
from ryu.cfg import CONF
from ryu.controller import ofp_event
from ryu.controller.handler import set_ev_cls, MAIN_DISPATCHER, CONFIG_DISPATCHER
from ryu.lib.ovs.bridge import OVSBridge
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import arp
from ryu.lib.packet import ipv4
from ryu.lib.packet import udp
from ryu.ofproto.ether import ETH_TYPE_ARP, ETH_TYPE_IP
from ryu.ofproto.inet import IPPROTO_UDP
from ryu.ofproto.ofproto_parser import namedtuple
from ryu.topology import api as topo_api
from ryu.topology import event as topo_event
import lama.agent.helpers.network_names_helper as neth
from lama.utils.logging_lama import logger, LamaLogger


__author__ = 'tiago'


# print("REGISTERING!")
#
# CONF = cfg.CONF
#
# CONF.register_cli_opts([
# cfg.BoolOpt('observe-links', default=False,
# help='observe link discovery events.'),
#     cfg.BoolOpt('install-lldp-flow', default=True,
#                 help='link discovery: explicitly install flow entry '
#                      'to send lldp packet to controller'),
#     cfg.BoolOpt('explicit-drop', default=True,
#                 help='link discovery: explicitly drop lldp packet in')
# ])

# logger = logging.getLogger(__name__)logger
LamaLogger.set_filename(logger, '/var/log/lama/lama_controller_{}.log'.format(CONF['lama']['app']))
logger.debug("Handlers: %s", logger.handlers)

class LamaSwitch(RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
    OvsdbConf = namedtuple('OvsdbConf', ['ovsdb_timeout'])
    OvsdbPort = 6640
    DHCPServerIP = "192.168.0.1"

    def __init__(self, *args, **kwargs):
        super(LamaSwitch, self).__init__(*args, **kwargs)
        self.app_name = CONF['lama']['app']
        self.agent_ip = CONF['lama']['host']
        self.app_tun_id = int(CONF['lama']['nid'])
        logger.debug("Start LamaSwitch : %s : %s : %s" % (self.app_name, self.agent_ip, self.app_tun_id))
        self.ip_to_port = {}
        # key, value structures to be later stored in redis
        # self.agent_ip = "10.1.1.8"
        # self.app_tun_id = 100
        # self._app_macs = {
        # "52:54:00:0f:dc:1b": {"mac": "52:54:00:0f:dc:1b", "ip": "192.168.1.2", "host": "10.1.1.8"},
        # "52:54:00:f9:0a:e2": {"mac": "52:54:00:f9:0a:e2", "ip": "192.168.1.3", "host": "10.1.1.8"},
        # "52:54:00:fb:29:6c": {"mac": "52:54:00:fb:29:6c", "ip": "192.168.1.4", "host": "10.1.1.9"},
        # "52:54:00:48:c8:20": {"mac": "52:54:00:48:c8:20", "ip": "192.168.1.5", "host": "10.1.1.11"},
        # }

        # self._app_ips = {
        # "192.168.1.1": {"mac": None, "ip": "192.168.1.1", "host": "10.1.1.8"},
        # "192.168.1.2": {"mac": "52:54:00:0f:dc:1b", "ip": "192.168.1.2", "host": "10.1.1.8"},
        # "192.168.1.3": {"mac": "52:54:00:f9:0a:e2", "ip": "192.168.1.3", "host": "10.1.1.8"},
        # "192.168.1.4": {"mac": "52:54:00:fb:29:6c", "ip": "192.168.1.4", "host": "10.1.1.9"},
        # "192.168.1.5": {"mac": "52:54:00:48:c8:20", "ip": "192.168.1.5", "host": "10.1.1.11"},
        # }

        # redis keys : app_name:mac:<mac> => "<mac>-<ip>-<host>"
        # redis keys : app_name:ip:<ip> => "<mac>-<ip>-<host>"
        # read on demand...
        # Connect to redis...
        self.db = LamaKeyStore()

        self.local_ports = {}
        self.remote_ports = {}
        self.gw = {}
        self.local_port_list = {}

        self.switches = {}

    def get_by_mac(self, mac):
        # TODO: Consider caching...
        d = self.db.get_dict("%s:%s" % (self.app_name, mac))
        return {k.lower(): v if v is None else v.lower() for k, v in d.items()}

    def get_by_ip(self, ip):
        # TODO: Consider caching...
        d = self.db.get_dict("%s:%s" % (self.app_name, ip))
        return {k.lower(): v if v is None else v.lower() for k, v in d.items()}

    # def close(self):
    #     try:
    #         self.db.close()
    #     except:
    #         pass

    # def OBSOLETE_add_flow(self, datapath, priority, match, actions, buffer_id=None, table_id=None):
    #     ofproto = datapath.ofproto
    #     parser = datapath.ofproto_parser
    #
    #     inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
    #                                          actions)]
    #     args = {"datapath": datapath, "priority": priority, "match": match, "instructions": inst}
    #     if buffer_id:
    #         args["buffer_id"] = buffer_id
    #     if table_id:
    #         args["table_id"] = table_id
    #     mod = parser.OFPFlowMod(**args)
    #     datapath.send_msg(mod)

    def del_flows_by_port(self, datapath, port=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        match = parser.OFPMatch(in_port=port)
        mod = datapath.ofproto_parser.OFPFlowMod(
            datapath=datapath, match=match, cookie=0,
            command=ofproto.OFPFC_DELETE,
            out_port=ofproto.OFPP_ANY,
            out_group=ofproto.OFPG_ANY
        )
        logger.debug(" | ** Delete flow: %s", match)
        datapath.send_msg(mod)

        match = parser.OFPMatch(
            tunnel_id=self.app_tun_id
        )
        logger.debug(" | ** Delete flow: %s", match)
        mod = datapath.ofproto_parser.OFPFlowMod(
            datapath=datapath, match=match, cookie=0,
            command=ofproto.OFPFC_DELETE,
            table_id=1,
            out_port=port,
            out_group=ofproto.OFPG_ANY
        )
        datapath.send_msg(mod)

        dpid = datapath.id
        switch_ip = self.switches.get(dpid, (None, None))[0]

        if self.local_ports.get(switch_ip):
            for k, p in self.local_ports.get(switch_ip).items():
                logger.debug(" | \t * local:  %s: %s = %s", k, p, p.get('ofport') != port)
            self.local_ports[switch_ip] = {k: p
                                           for k, p in self.local_ports.get(switch_ip).items() or []
                                           if p.get('ofport') != port}

        if self.remote_ports.get(switch_ip):
            for k, p in self.remote_ports.get(switch_ip).items():
                logger.debug(" | \t * remote:  %s: %s = %s", k, p, p.get('ofport') != port)

            # not pretty:
            self.remote_ports[switch_ip] = {k: p
                                            for k, p in self.remote_ports.get(switch_ip).items() or []
                                            if p.get('ofport') != port}

    def add_flow(self, datapath, priority, match, actions, instructions=None, buffer_id=None, table_id=None):
        # if table_id != 0 or priority != 10:
        #     return
        logger.debug("add_flow: action=%s, instructions=%s", actions, instructions)
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = []
        if instructions:
            inst += instructions
        if actions:
            inst += [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                                  actions)]
        args = {"datapath": datapath, "priority": priority, "match": match, "instructions": inst}
        if buffer_id:
            args["buffer_id"] = buffer_id
        if table_id:
            args["table_id"] = table_id
        mod = parser.OFPFlowMod(**args)
        logger.debug(" | ** Add flow: %s", args)
        # logger.debug("mod = %s", mod)
        # logger.debug("mode = %s", json.dumps(mod.to_jsondict(), ensure_ascii=True, indent=3, sort_keys=True))
        datapath.send_msg(mod)

    def add_forwarding_flow(self, datapath, match, actions):
        self.add_flow(datapath, 500, match, actions, table_id=1)

    #     logger.debug("add_flow %s", match)
    #     ofproto = datapath.ofproto
    #
    #     mod = datapath.ofproto_parser.OFPFlowMod(
    #         datapath=datapath, match=match, cookie=0, table=1,
    #         command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0,
    #         priority=ofproto.OFP_DEFAULT_PRIORITY,
    #         flags=ofproto.OFPFF_SEND_FLOW_REM, actions=actions)
    #     datapath.send_msg(mod)

    def mark_packet(self, datapath, in_port):
        logger.debug("Mark packet: tun=%s, port=%s", self.app_tun_id, in_port)
        parser = datapath.ofproto_parser
        if in_port >= 0:
            self.add_flow(datapath, 100, parser.OFPMatch(in_port=in_port),
                          [parser.OFPActionSetField(tunnel_id=self.app_tun_id)],
                          instructions=[parser.OFPInstructionGotoTable(1)], table_id=0)
        else:
            logger.warn("Negative port - Flow not added")

    def _update_ports(self, switch_ip):
        local_ports, remote_ports, gw = OVSDbClient.get_ports(
            bridge_name=neth.create_bridge_name(self.app_name), ovs_ip=switch_ip)

        self.local_port_list[switch_ip] = set()
        # logger.debug(" | \t# local ports received = %s", len(local_ports))
        for k, v in local_ports.iteritems():
            # logger.debug(" | \tk=%s, v=%s", k, v)
            if "ofport" in v:
                try:
                    self.local_port_list[switch_ip].add(v['ofport'])
                except Exception as e:
                    logger.warn("Unable to add port from switch %s: %s => %s", switch_ip, k, v)

        if "ofport" in gw:
            logger.debug(" | \t * add DHCP port: switch=%s, oport=%s", switch_ip, gw["ofport"])

        logger.debug(" Ports updated:")
        logger.debug("\tlocal ports:  %s", local_ports)
        logger.debug("\tremote ports: %s", remote_ports)
        logger.debug("\tdhcp:         %s", gw)
        self.local_ports[switch_ip] = local_ports
        self.remote_ports[switch_ip] = remote_ports
        self.gw[switch_ip] = gw

    def _get_local_port(self, datapath, mac):
        logger.debug(" | Get Local: Search port for %s", mac)
        dpid = datapath.id
        switch_ip = self.switches[dpid][0]
        logger.debug(" | Switch IP: %s, Local ports: %s", switch_ip, self.local_ports)
        logger.debug(" | Switch ports: %s", self.local_ports.get(switch_ip, {}))

        if switch_ip not in self.local_ports or mac not in self.local_ports[switch_ip]:
            self._update_ports(switch_ip)
        logger.debug(" | Port: %s", self.local_ports.get(switch_ip, {}).get(mac))
        return self.local_ports.get(switch_ip, {}).get(mac, {}).get('ofport')

    def _get_local_port_by_name(self, datapath, name):
        logger.debug(" | Get Local: Search port for %s", name)
        dpid = datapath.id
        switch_ip = self.switches[dpid][0]
        logger.debug(" | Switch IP: %s, Local ports: %s", switch_ip, self.local_ports)
        if switch_ip not in self.local_ports or name not in self.local_ports[switch_ip]:
            self._update_ports(switch_ip)
        try:
            return self.local_ports[switch_ip].get(name)["ofport"]
        except:
            return None

    def _get_remote_port(self, datapath, ip):
        dpid = datapath.id
        switch_ip = self.switches[dpid][0]
        logger.debug("Remote ports: %s - Switch IP: %s", self.remote_ports, switch_ip)
        if switch_ip not in self.remote_ports or ip not in self.remote_ports[switch_ip]:
            self._update_ports(switch_ip)

        if ip not in self.remote_ports[switch_ip]:
            logger.debug("Tunnel does not exist. Create VXLAN tunnel %s - %s", switch_ip, ip)
            res = self._create_vxlan_tunnel(datapath, switch_ip, ip)
            if res:
                self._update_ports(switch_ip)

        try:
            return self.remote_ports[switch_ip].get(ip)["ofport"]
        except:
            return None

    def _get_gw_port(self, dpid):
        switch_ip = self.switches[dpid][0]
        if switch_ip not in self.gw:
            self._update_ports(switch_ip)
        try:
            return self.gw[switch_ip]["ofport"]
        except:
            logger.debug("No port found: %s", self.gw)
            return None

    def is_local_port(self, switch_addr, port):
        if switch_addr not in self.local_port_list:
            self._update_ports(switch_addr)

        return port in self.local_port_list[switch_addr]

    def is_local(self, switch_addr, mac=None, ip=None):
        if not ip and not mac:
            raise KeyError("Either mac or ip must be non null!")
        vm = None
        if mac:
            vm = self.get_by_mac(mac)
        elif ip:
            vm = self.get_by_ip(ip)
        if vm and vm["host"] == switch_addr:
            return True
        return False

    def _arp_handler(self, msg, pkt):
        arpheader = pkt.get_protocol(arp.arp)
        if arpheader:
            logger.debug(" ARP packet, header=%s", arpheader)
            # get dst ip
            # is dst ip for the current application?
            vm = self.get_by_ip(arpheader.dst_ip)
            if vm:
                # is the ip local?
                switch_ip = self.switches[msg.datapath.id][0]
                if vm["host"] == switch_ip:
                    port = None
                    if vm["mac"]:
                        port = self._get_local_port(msg.datapath, vm["mac"])
                        logger.debug(" Local dst: vm_mac=%s, port=%s", vm['mac'], port)
                    elif vm["host"]:
                        port = self._get_gw_port(msg.datapath.id)
                        logger.debug(" Local dst: no_vm_mac, gw_port=%s", port)
                        # port = self._get_local_port(msg.datapath, vm["host"])
                    else:
                        logger.error(" No VM MAC or IP: nothing to search for!")

                    if not port:
                        logger.debug(" LOCAL Port not found for %s/%s", vm["mac"], vm["host"])
                        return None
                else:
                    port = self._get_remote_port(msg.datapath, vm["host"])

                    if not port:
                        logger.debug(" Remote port - port NOT FOUND: vm_host=%s, switch_ip=%s", vm["host"], switch_ip)
                        return None

                    if port < 0:
                        logger.debug(" Remote port - port NEGATIVE: vm_host=%s, switch_ip=%s", vm["host"], switch_ip)
                        return None

                parser = msg.datapath.ofproto_parser
                if vm["mac"]:
                    logger.debug(" ETHERNET fwd rule: mac=%s, ip=%s, oport=%s", vm["mac"], vm["ip"], port)
                    self.add_forwarding_flow(msg.datapath,
                                             parser.OFPMatch(
                                                 tunnel_id=self.app_tun_id,
                                                 eth_dst=vm["mac"]),
                                             [parser.OFPActionOutput(port)])

                match = parser.OFPMatch(tunnel_id=self.app_tun_id, eth_type=ETH_TYPE_ARP, arp_tpa=vm["ip"])
                logger.debug(" ARP fwd rule: port=%s", port)
                self.add_forwarding_flow(datapath=msg.datapath,
                                         match=match,
                                         actions=[parser.OFPActionOutput(port)])
                return [msg.datapath.ofproto_parser.OFPActionOutput(port)]

            else:
                logger.debug(" ARP handler :: IP does not belong to the app.")
                return None
        else:
            return None

    def _base_handler(self, msg, pkt):
        logger.warn("Not implemented yet!")
        return None
        eth = pkt.get_protocol(ethernet.ethernet)
        ipheader = pkt.get_protocol(ipv4.ipv4)
        vm = self.get_by_mac(eth.dst)
        key = eth.dst
        if not vm and ipheader:
            vm = self.get_by_ip(ipheader.dst)
            key = ipheader.dst

        logger.debug(" | Base handler - VM for %s: %s", key, vm)

        if vm:
            switch_ip = self.switches[msg.datapath.id][0]
            if vm["host"] == switch_ip:
                port = self._get_local_port(msg.datapath, key)

                if not port:
                    port = self._get_gw_port(msg.datapath.id)
                    logger.debug(" | Try local gateway: %s", port)

                if not port:
                    logger.debug(" | LOCAL Port not found for %s", key)
                    return None

                logger.debug(" | Will forward to port: %s", port)
            else:
                logger.debug(" | Dst ip is remote (%s, %s)!", vm["host"], switch_ip)
                port = self._get_remote_port(msg.datapath, vm["host"])

                if not port:
                    logger.debug(" | LOCAL Port not found for %s", vm["host"])
                    return None

            parser = msg.datapath.ofproto_parser
            dst_eth = vm["mac"] or eth.dst
            if dst_eth:
                logger.debug(" | --> ETHERNET fwd rule")
                logger.debug("dst_eth: %s", dst_eth)
                self.add_forwarding_flow(msg.datapath,
                                         parser.OFPMatch(
                                             tunnel_id=self.app_tun_id,
                                             eth_dst=dst_eth),
                                         [parser.OFPActionSetField(eth_dst=dst_eth), parser.OFPActionOutput(port)])

            match = parser.OFPMatch(tunnel_id=self.app_tun_id, eth_type=ETH_TYPE_ARP, arp_tpa=vm["ip"])
            logger.debug(" --> ARP fwd rule: %s", match)
            self.add_forwarding_flow(datapath=msg.datapath,
                                     match=match,
                                     actions=[parser.OFPActionOutput(port)])

            if port:
                return [msg.datapath.ofproto_parser.OFPActionOutput(port)]

        elif ipheader and ipheader.dst == "192.168.0.2":
            logger.debug(" | Special case: local access")
            # logger.debug("MAC2PORT: %s", self.ip_to_port)
            # if msg.datapath.id in self.ip_to_port and eth.dst in self.ip_to_port[msg.datapath.id]:
            #     port = self.ip_to_port[msg.datapath.id][eth.dst]
            # else:
            # tap_name = shorten_app_name("tapi-%s" % self.app_name, 15)
            tap_name = neth.create_tap_name(self.app_name)

            port = self._get_local_port_by_name(msg.datapath, tap_name)

            if port:
                parser = msg.datapath.ofproto_parser
                # if eth.dst !=
                dst_eth = eth.dst
                if dst_eth:
                    logger.debug(" | --> ETHERNET fwd rule")
                    self.add_forwarding_flow(msg.datapath,
                                             parser.OFPMatch(
                                                 tunnel_id=self.app_tun_id,
                                                 eth_dst=dst_eth),
                                             [parser.OFPActionOutput(port)])

                match = parser.OFPMatch(tunnel_id=self.app_tun_id, eth_type=ETH_TYPE_ARP, arp_tpa=ipheader.dst)
                logger.debug(" --> ARP fwd rule: %s", match)
                self.add_forwarding_flow(datapath=msg.datapath,
                                         match=match,
                                         actions=[parser.OFPActionOutput(port)])

            if port:
                return [msg.datapath.ofproto_parser.OFPActionOutput(port)]
        else:
            port = self._get_local_port(msg.datapath, eth.dst)
            if port:
                parser = msg.datapath.ofproto_parser
                # if eth.dst !=
                dst_eth = eth.dst
                if dst_eth:
                    logger.debug(" | --> ETHERNET fwd rule")
                    self.add_forwarding_flow(msg.datapath,
                                             parser.OFPMatch(
                                                 tunnel_id=self.app_tun_id,
                                                 eth_dst=dst_eth),
                                             [parser.OFPActionOutput(port)])

                return [msg.datapath.ofproto_parser.OFPActionOutput(port)]
            else:
                return None

    def _dhcp_handler(self, msg, pkt):
        udpheader = pkt.get_protocol(udp.udp)
        if udpheader and udpheader.dst_port == 67:
            logger.warn("Not implemented yet!")
            return None
            logger.debug(" | ")
            logger.debug(" | DHCP packet")
            # ipheader = pkt.get_protocol(ipv4.ipv4)

            switch_ip = self.switches[msg.datapath.id][0]
            logger.debug(" | Switch IP: %s, Agent IP: %s", switch_ip, self.agent_ip)
            if switch_ip == self.agent_ip:
                port = self._get_gw_port(msg.datapath.id)
                if not port:
                    logger.debug(" | LOCAL Port not found for %s", msg.datapath.id)
                    return None
            else:
                dhcp_info = self.get_by_ip(LamaSwitch.DHCPServerIP)
                logger.debug(" | DHCP info: %s", dhcp_info)
                if "host" not in dhcp_info:
                    logger.debug(" | No DHCP server found")
                    return None

                port = self._get_remote_port(msg.datapath, dhcp_info.get("host"))
                logger.debug(" | DHCP: %s", port)
                if not port:
                    logger.debug(" | REMOTE Port not found for %s", self.get_by_ip(LamaSwitch.DHCPServerIP)["host"])
                    return None

                if port < 0:
                    logger.debug(" | REMOTE Port NEGATIVE for %s", self.get_by_ip(LamaSwitch.DHCPServerIP)["host"])
                    return None

            parser = msg.datapath.ofproto_parser
            logger.debug(" | --> DHCP fwd rule")
            match = parser.OFPMatch(tunnel_id=self.app_tun_id, ip_proto=IPPROTO_UDP, udp_dst=67)
            self.add_forwarding_flow(msg.datapath, match, [parser.OFPActionOutput(port)])

            match = parser.OFPMatch(tunnel_id=self.app_tun_id, eth_type=ETH_TYPE_ARP, arp_tpa=LamaSwitch.DHCPServerIP)
            logger.debug(" | --> ARP fwd rule")
            self.add_forwarding_flow(datapath=msg.datapath,
                                     match=match,
                                     actions=[parser.OFPActionOutput(port)])

            logger.debug(" | Output port: %s", port)
            return [msg.datapath.ofproto_parser.OFPActionOutput(port)]

        return None

    @set_ev_cls(topo_event.EventSwitchEnter)
    def switch_enter_handler(self, ev):
        for sw in topo_api.get_all_switch(self):
            addr = sw.dp.socket.getpeername()
            logger.debug("Got ID for switch @ %s (id = %s, OF version = %s, %s)", addr,
                             sw.dp.id, sw.dp.ofproto.__name__, sw.dp.ofproto.OFP_VERSION)
            self.switches[sw.dp.id] = addr

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        msg = ev.msg
        logger.debug('OFPSwitchFeatures received: '
                          'datapath_id=0x%016x n_buffers=%d '
                          'n_tables=%d auxiliary_id=%d '
                          'capabilities=0x%08x',
                          msg.datapath_id, msg.n_buffers, msg.n_tables,
                          msg.auxiliary_id, msg.capabilities)

        logger.debug(msg.datapath.socket)
        datapath = msg.datapath
        parser = datapath.ofproto_parser
        ofproto = datapath.ofproto

        match = None
        instructions = [parser.OFPInstructionGotoTable(1)]
        logger.debug("Install resubmit action for ACL table")
        self.add_flow(datapath, 0, match, None, instructions=instructions, table_id=0)

        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        logger.debug("Install rule to receive events")
        self.add_flow(datapath, 0, match, actions, table_id=1)

    def _create_vxlan_tunnel(self, datapath, local_ip, remote_ip):
        # "tcp:10.1.1.8:6640"
        logger.debug("_create_vxlan_tunnel: %s, %s, %s", datapath, local_ip, remote_ip)
        ovsdb_addr = "tcp:%s:%d" % (local_ip, LamaSwitch.OvsdbPort)
        tunnel_name = "vt-%s-%s" % (self.app_tun_id, '.'.join(str(remote_ip).split('.')[-2:]))
        res = self._create_vxlan_local_vtep(datapath, ovsdb_addr, tunnel_name, local_ip, remote_ip)

        # if res:
        #     ovsdb_addr = "tcp:%s:%d" % (remote_ip, LamaSwitch.OvsdbPort)
        #     tunnel_name = "vt-%s-%s" % (self.app_tun_id,'.'.join(str(local_ip).split('.')[-2:]))
        #     res = self._create_vxlan_local_vtep(datapath, ovsdb_addr, tunnel_name, remote_ip, local_ip)
        # logger.debug("_create_vxlan_tunnel - result: %s", res)

        return res

    def _create_vxlan_local_vtep(self, datapath, ovsdb_addr, tunnel_name, tunnel_local_ip, tunnel_peer_ip):
        # "tcp:10.1.1.8:6640"
        c = LamaSwitch.OvsdbConf(ovsdb_timeout=10)
        bridge = OVSBridge(c, datapath.id, ovsdb_addr)
        # bridge.br_name = create_bridge_name(self.app_name)
        try:
            bridge.br_name = neth.create_bridge_name(self.app_name)
        except:
            logger.warn("Failed to create bridge. Traceback: %s", traceback.format_exc())
            return False

        try:
            logger.debug("name=%s, tunnel_type=%s, local_ip=%s, remote_ip=%s, key=%s", tunnel_name, "vxlan", tunnel_local_ip, tunnel_peer_ip, self.app_tun_id)
            bridge.add_tunnel_port(tunnel_name, "vxlan", tunnel_local_ip, tunnel_peer_ip, self.app_tun_id)
        except:
            logger.warn("Failed to add tunnel. Traceback: %s", traceback.format_exc())
            return False

        return True

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):

        logger.debug(" +-----  PACKET IN  -----")

        msg = ev.msg
        datapath = msg.datapath
        in_port = msg.match['in_port']
        ofproto = datapath.ofproto

        pkt = packet.Packet(ev.msg.data)

        dpid = datapath.id
        switch_ip = self.switches.get(dpid)

        logger.debug("Packet: %s", pkt)
        logger.debug("Protocols: %s", [str(p) for p in pkt.protocols])

        if self.is_local_port(switch_ip[0], in_port):
            self.mark_packet(datapath, in_port)

        # Identify and process packet
        actions = self._arp_handler(msg, pkt) or self._dhcp_handler(msg, pkt) or self._base_handler(msg, pkt)

        if actions:
            data = None
            if msg.buffer_id == ofproto.OFP_NO_BUFFER:
                data = msg.data

            parser = datapath.ofproto_parser
            out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                      in_port=in_port, actions=actions, data=data)
            datapath.send_msg(out)
        else:
            logger.debug(" No action")

        logger.debug(" +-----------------------")

    @set_ev_cls(ofp_event.EventOFPPortStatus, MAIN_DISPATCHER)
    def _port_status_handler(self, ev):
        msg = ev.msg
        reason = msg.reason
        port_no = msg.desc.port_no

        ofproto = msg.datapath.ofproto
        if reason == ofproto.OFPPR_ADD:
            logger.debug("port added %s", port_no)
        elif reason == ofproto.OFPPR_DELETE:
            logger.debug("port deleted %s", port_no)
            self.del_flows_by_port(msg.datapath, port=port_no)
        elif reason == ofproto.OFPPR_MODIFY:
            logger.debug("port modified %s", port_no)
        else:
            logger.debug("Illegal port state %s %s", port_no, reason)

