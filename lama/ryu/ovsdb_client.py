import logging
import socket
import json
import subprocess
from sys import argv
import traceback
from lama.ryu.ovs_vsctl import VSCtl
from lama.ryu.ovs_vsctl import VSCtlCommand
# import ryu.lib.ovs.vsctl as ovs_vsctl
from lama.utils.logging_lama import logger, LamaLogger

__author__ = 'tiago'


class OVSDbClient(object):

    OVSDB_PORT = 6640
    # CONTROLLER_PORT = 6633
    BUFFER_SIZE = 8192

    @staticmethod
    def _parse_bridge_ports(resp, bridge_name):
        bridge_ports = []
        if "result" in resp and resp["result"]:
            if "Bridge" in resp["result"]:
                for k, v in resp["result"]["Bridge"].items():
                    if "new" in v:
                        bdetails = v["new"]
                        if "name" in bdetails and bdetails["name"] == bridge_name:
                            if "ports" in bdetails:
                                if len(bdetails["ports"]) == 2 and bdetails["ports"][0] == "set":
                                    bridge_ports = [str(v[1]) for v in bdetails["ports"][1] if len(v) == 2 and v[0] == "uuid"]
        return bridge_ports

    @staticmethod
    def _parse_bridge_interfaces(resp, bridge_name):
        bridge_ports = OVSDbClient._parse_bridge_ports(resp, bridge_name)
        bridge_interfaces = []
        for port_id in bridge_ports:
            try:
                interface = resp["result"]["Port"][port_id]["new"]["interfaces"][1]
                bridge_interfaces.append(str(interface))
            except KeyError:
                continue

        return bridge_interfaces

    @staticmethod
    def _get_val_from_options(val, options):
        if options[0] == "map":
            for v in options[1]:
                if v[0] == val:
                    return v[1]
        else:
            raise NotImplementedError("val = %s, options = %s" % (val, options))

    @staticmethod
    def get_ports(bridge_name, ovs_ip="127.0.0.1", tap_prefix="ltp-"):
        # req = {"method": "list_dbs", "params": [], "id": 0}
        req = {
            "method": "monitor",
            "id": 0,
            "params": [
                "Open_vSwitch",
                None,
                {
                    "Port": {"columns": ["interfaces", "name", "tag", "trunks"]},
                    # "Manager": {"columns": ["is_connected", "target"]},
                    "Interface": {"columns": ["name", "options", "type", "external_ids", "ofport", "mac_in_use"]},
                    "Bridge": {"columns": ["controller", "name", "ports"]},
                    # "Controller": {"columns": ["is_connected", "target"]},
                    # "Open_vSwitch": {"columns": ["bridges", "cur_cfg", "manager_options", "ovs_version"]}
                }
            ]
        }

        resp = OVSDbClient.request(json.dumps(req), ovs_ip)
        # print(json.dumps(resp, sort_keys=True, indent=4, separators=(',', ': ')))
        # print(json.dumps(resp["result"]["Interface"], sort_keys=True, indent=4, separators=(',', ': ')))

        # get bridges
        # bridge -> ports
        # bridge_ports = OVSDbClient._parse_bridge_ports(resp, bridge_name)

        # ports -> bridges
        # print(resp)
        bridge_interfaces = OVSDbClient._parse_bridge_interfaces(resp, bridge_name)

        try:
            resp_interfaces = resp["result"]["Interface"]
        except KeyError:
            return None

        # interfaces = {}
        # for interface in bridge_interfaces:
        #     try:
        #         details = resp_interfaces[interface]["new"]
        #
        #         if details["type"] == "":
        #             interfaces[str(details["name"])] = {
        #                 "type": "local",
        #                 "ofport": details["ofport"],
        #                 "mac": OVSDbClient._get_val_from_options("attached-mac", details["external_ids"])
        #             }
        #         elif details["type"] == "vxlan":
        #             interfaces[str(details["name"])] = {
        #                 "type": "vxlan",
        #                 "ofport": details["ofport"],
        #                 "remote_ip": OVSDbClient._get_val_from_options("remote_ip", details["options"])
        #             }
        #     except:
        #         continue
        # print(resp_interfaces)
        # print(json.dumps(resp_interfaces, sort_keys=True, indent=4, separators=(',', ': ')))
        local = {}
        remote = {}
        gw = {}
        for interface in bridge_interfaces:
            try:
                details = resp_interfaces[interface]["new"]
                name = str(details["name"])
                if details["type"] == "" or details["type"] == "internal":
                    mac = str(OVSDbClient._get_val_from_options("attached-mac", details["external_ids"]))
                    if mac == "None":
                        mac = str(details.get("mac_in_use", "None"))

                    if mac != "None":
                        local[mac] = {
                            "name": name,
                            "type": "local",
                            "ofport": details["ofport"]
                        }
                    if name.startswith(tap_prefix):
                        gw = {
                            "name": name,
                            "ofport": details["ofport"]
                        }
                    else:
                        local[name] = {
                            "name": name,
                            "type": "local",
                            "ofport": details["ofport"]
                        }

                elif details["type"] == "vxlan":
                    remote[str(OVSDbClient._get_val_from_options("remote_ip", details["options"]))] = {
                        "name": name,
                        "type": "vxlan",
                        "ofport": details["ofport"]
                    }
            except:
                continue

        # print(local)
        # print(remote)
        return local, remote, gw

    @staticmethod
    def bridge_has_port(bridge_name, port, ovs_ip="127.0.0.1"):
        req = {
            "method": "monitor",
            "id": 0,
            "params": [
                "Open_vSwitch",
                None,
                {
                    "Port": {"columns": ["interfaces", "name", "tag", "trunks"]},
                    "Interface": {"columns": ["name", "options", "type", "external_ids", "ofport", "mac_in_use"]},
                    "Bridge": {"columns": ["controller", "name", "ports"]},
                }
            ]
        }
        resp = OVSDbClient.request(json.dumps(req), ovs_ip)
        # print(resp)
        if "result" not in resp or "Port" not in resp["result"]\
                or "Bridge" not in resp["result"]:
            raise Exception("Unable to retrieve ports")

        port_uuid = None
        for k, v in resp["result"]["Port"].items():
            # print("k")
            # print(k)
            # print("v")
            # print(v)
            try:
                if v["new"]["name"] == port:
                 port_uuid = k
            except:
                pass

        if port_uuid:
            for k, v in resp["result"]["Bridge"].items():
                try:
                    if v["new"]["name"] == bridge_name:
                        for p in v["new"]["ports"][1]:
                            if p[0] == "uuid" and p[1] == port_uuid:
                                return True
                except:
                    pass

        return False

    @staticmethod
    def request(req, ovs_ip, ovs_port=None):
        ovs_port = ovs_port or OVSDbClient.OVSDB_PORT

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ovs_ip, ovs_port))
        s.send(req.encode('utf-8'))

        got_json = False
        response = ""
        resp_json = None
        while not got_json:
            data = s.recv(OVSDbClient.BUFFER_SIZE)
            response += data.decode("utf-8")
            try:
                resp_json = json.loads(response)
                got_json = True
            except ValueError:
                pass
        s.close()

        return resp_json

    @staticmethod
    def bridge_exists(host, bridge):
        try:
            # TODO: on exception we will assume it does not exist.
            # Typically an exception can occur if the bridge exists but has no manager port specified.

            command = VSCtlCommand('list-br')
            commands = [command]
            ovsdb_addr = "tcp:%s:%d" % (host, OVSDbClient.OVSDB_PORT)
            vsctl = VSCtl(ovsdb_addr)
            logger.debug(commands)
            vsctl.run_command(commands)
            bridge_list = command.result
            return bridge in bridge_list
        except Exception as e:
            logger.warn("Received exception but return bridge does not exist: %s", e)
            return False


    @staticmethod
    def create_bridge(host, br_name, controller_ip=None, controller_port=None):
        commands = []

        if not OVSDbClient.bridge_exists(host, br_name):
            # 1)create bridge
            commands.append(VSCtlCommand('add-br', args=(br_name,)))
        # 2) sudo ovs-vsctl set bridge br0 protocols=OpenFlow10,OpenFlow12,OpenFlow13
        commands.append(
            VSCtlCommand('set', ('Bridge', br_name, 'protocols=OpenFlow13'))
        )

        if controller_ip and controller_port:
            # 4) sudo ovs-vsctl set-controller brlama-app1 tcp:<host>:20012
            commands.append(
                VSCtlCommand('set-controller', args=(br_name, 'tcp:%s:%d' % (controller_ip, controller_port))),
            )

        # 3) sudo ovs-vsctl set-manager ptcp:6640
        # VSCtlCommand('set-manager', args=('ptcp:6640',)),
        # 5) sudo ovs-vsctl add-port brlama-<app_name> veth-<app_name>-0
        # VSCtlCommand('add-port', args=(br_name, 'veth-%s-0' % app_name))

        ovsdb_addr = "tcp:%s:%d" % (host, OVSDbClient.OVSDB_PORT)
        vsctl = VSCtl(ovsdb_addr)
        logger.debug("Start commands: %s", commands)
        for command in commands:
            try:
                logger.debug("Run command: %s", command)
                vsctl.run_command([command])
                # logger.debug("Command: %s, Result: %s", command, command.result)
            except Exception as e:
                if command.command == 'add-br' and "already exists" in str(e):
                    logger.temp("Bridge already existed when trying to create.")
                    # Error caused by race condtion between bridge_exists and bridge_create
                    # TODO: check if all the details are proper... we should create without
                    # verifying and process the error to handle race conditions
                    return True
                else:
                    logger.error("Command failed: %s", command)
                    logger.error("Exception: %s", traceback.format_exc())
                return False
        return True

    @staticmethod
    def add_controller(host, br_name, controller_ip):
        commands = [
            VSCtlCommand('set-controller', args=(br_name, 'tcp:%s:%d' % (controller_ip, OVSDbClient.CONTROLLER_PORT))),
        ]
        ovsdb_addr = "tcp:%s:%d" % (host, OVSDbClient.OVSDB_PORT)
        vsctl = VSCtl(ovsdb_addr)
        for command in commands:
            try:
                vsctl.run_command([command])
                # logger.debug("Command: %s, Result: %s", command, command.result)
            except:
                logger.error("Command failed: %s", command)
                logger.error("Exception: %s", traceback.format_exc())
                return False
        return True

    @staticmethod
    def add_port_to_local_bridge(br_name, port_name):
        cmd = subprocess.Popen(["ovs-vsctl", "add-port", br_name, port_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = cmd.communicate()
        if err:
            logger.error("Error creating port: %s", err)
            return False
        return True

    @staticmethod
    def add_internal_port_to_local_bridge(br_name, port_name):
        args = ["ovs-vsctl", "add-port", br_name, port_name, "--", "set", "Interface", port_name, "type=internal"]
        cmd = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        out, err = cmd.communicate()
        if err:
            logger.error("Error creating port: %s", err)
            return False

        # if not OVSDbClient.bridge_exists(host, br_name):
        #     logger.error("Bridge does not exist.")
        #     print("Bridge does not exist.")
        #
        # command = VSCtlCommand('add-port', args=(br_name, port_name))
        # ovsdb_addr = "tcp:%s:%d" % (host, OVSDbClient.OVSDB_PORT)
        # vsctl = VSCtl(ovsdb_addr)
        # try:
        #     vsctl.run_command([command])
        #     logger.debug("Command: %s, Result: %s", command, command.result)
        #     print("Command: %s, Result: %s" % (command, command.result))
        # except:
        #     logger.error("Command failed: %s", command)
        #     logger.error("Exception: %s", traceback.format_exc())
        #     print("Command failed: %s" % command)
        #     print("Exception: %s" % traceback.format_exc())
        #     return False
        return True


if __name__ == "__main__":
    # if len(argv) > 1:
    #     OVSDbClient.request(argv[1], ovs_ip="10.1.1.8")
    # else:
    #     OVSDbClient.get_ports(bridge_name="brlama-app1", ovs_ip="10.1.1.8")
    # print(OVSDbClient.bridge_exists("10.1.1.9", "br-em1"))
    # print(OVSDbClient.bridge_exists("10.1.1.9", "no-bridge"))
    # print(OVSDbClient.bridge_exists("10.1.1.9", "test"))
    # logger = LamaLogger.get_logger("lama")
    LamaLogger.enable_stdout(logger)
    print(logger.level)
    OVSDbClient.create_bridge("10.1.1.9", "brlama-test", controller_ip="10.1.1.8", controller_port=6633)