# Copyright (c) 2009, 2010, 2011 Nicira, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re
import uuid
import sys
from lama.ryu.ovs import json as ovs_json
from lama.ryu.ovs import ovsuuid
from lama.ryu.ovs.db import parser as db_parser
from lama.ryu.ovs.db import error


class ComparableMixin(object):
    def _compare(self, other, method):
        try:
            return method(self._cmpkey(), other._cmpkey())
        except (AttributeError, TypeError):
            # _cmpkey not implemented, or return different type,
            # so I can't compare with "other".
            return NotImplemented

    def __lt__(self, other):
        return self._compare(other, lambda s, o: s < o)

    def __le__(self, other):
        return self._compare(other, lambda s, o: s <= o)

    def __eq__(self, other):
        return self._compare(other, lambda s, o: s == o)

    def __ge__(self, other):
        return self._compare(other, lambda s, o: s >= o)

    def __gt__(self, other):
        return self._compare(other, lambda s, o: s > o)

    def __ne__(self, other):
        return self._compare(other, lambda s, o: s != o)


class ConstraintViolation(error.Error):
    def __init__(self, msg, json=None):
        error.Error.__init__(self, msg, json, tag="constraint violation")


def escapeCString(src):
    dst = []
    for c in src:
        if c in "\\\"":
            dst.append("\\" + c)
        elif ord(c) < 32:
            if c == '\n':
                dst.append('\\n')
            elif c == '\r':
                dst.append('\\r')
            elif c == '\a':
                dst.append('\\a')
            elif c == '\b':
                dst.append('\\b')
            elif c == '\f':
                dst.append('\\f')
            elif c == '\t':
                dst.append('\\t')
            elif c == '\v':
                dst.append('\\v')
            else:
                dst.append('\\%03o' % ord(c))
        else:
            dst.append(c)
    return ''.join(dst)


def returnUnchanged(x):
    return x


class Atom(ComparableMixin):
    def __init__(self, type_, value=None):
        self.type = type_
        if value is not None:
            self.value = value
        else:
            self.value = type_.default_atom()

    def _cmpkey(self):
        return self.value

    # def __cmp__(self, other):
    #     if not isinstance(other, Atom) or self.type != other.type:
    #         return NotImplemented
    #     elif self.value < other.value:
    #         return -1
    #     elif self.value > other.value:
    #         return 1
    #     else:
    #         return 0

    def __hash__(self):
        return hash(self.value)

    @staticmethod
    def default(type_):
        """Returns the default value for the given type_, which must be an
        instance of AtomicType.

        The default value for each atomic type is;

          - 0, for integer or real atoms.

          - False, for a boolean atom.

          - "", for a string atom.

          - The all-zeros UUID, for a UUID atom."""
        return Atom(type_)

    def is_default(self):
        return self == self.default(self.type)

    @staticmethod
    def from_json(base, json, symtab=None):
        type_ = base.type
        json = db_parser.float_to_int(json)
        if ((type_ == IntegerType and type(json) in [int])
            or (type_ == RealType
                and type(json) in [int, float])
            or (type_ == BooleanType and type(json) == bool)
            or (type_ == StringType
                and type(json) in [str])):
            atom = Atom(type_, json)
        elif type_ == UuidType:
            atom = Atom(type_, ovsuuid.from_json(json, symtab))
        else:
            raise error.Error("expected %s" % type_.to_string(), json)
        atom.check_constraints(base)
        return atom

    @staticmethod
    def from_python(base, value):
        value = db_parser.float_to_int(value)
        if type(value) in base.type.python_types:
            atom = Atom(base.type, value)
        else:
            raise error.Error("expected %s, got %s" % (base.type, type(value)))
        atom.check_constraints(base)
        return atom

    def check_constraints(self, base):
        """Checks whether 'atom' meets the constraints (if any) defined in
        'base' and raises an ovs.db.error.Error if any constraint is violated.

        'base' and 'atom' must have the same type.
        Checking UUID constraints is deferred to transaction commit time, so
        this function does nothing for UUID constraints."""
        assert base.type == self.type
        if base.enum is not None and self not in base.enum:
            raise ConstraintViolation(
                "%s is not one of the allowed values (%s)"
                % (self.to_string(), base.enum.to_string()))
        elif base.type in [IntegerType, RealType]:
            if ((base.min is None or self.value >= base.min) and
                (base.max is None or self.value <= base.max)):
                pass
            elif base.min is not None and base.max is not None:
                raise ConstraintViolation(
                    "%s is not in the valid range %.15g to %.15g (inclusive)"
                    % (self.to_string(), base.min, base.max))
            elif base.min is not None:
                raise ConstraintViolation(
                    "%s is less than minimum allowed value %.15g"
                            % (self.to_string(), base.min))
            else:
                raise ConstraintViolation(
                    "%s is greater than maximum allowed value %.15g"
                    % (self.to_string(), base.max))
        elif base.type == StringType:
            # XXX The C version validates that the string is valid UTF-8 here.
            # Do we need to do that in Python too?
            s = self.value
            length = len(s)
            if length < base.min_length:
                raise ConstraintViolation(
                    '"%s" length %d is less than minimum allowed length %d'
                    % (s, length, base.min_length))
            elif length > base.max_length:
                raise ConstraintViolation(
                    '"%s" length %d is greater than maximum allowed '
                    'length %d' % (s, length, base.max_length))

    def to_json(self):
        if self.type == UuidType:
            return ovsuuid.to_json(self.value)
        else:
            return self.value

    def cInitAtom(self, var):
        if self.type == IntegerType:
            return ['%s.integer = %d;' % (var, self.value)]
        elif self.type == RealType:
            return ['%s.real = %.15g;' % (var, self.value)]
        elif self.type == BooleanType:
            if self.value:
                return ['%s.boolean = true;']
            else:
                return ['%s.boolean = false;']
        elif self.type == StringType:
            return ['%s.string = xstrdup("%s");'
                    % (var, escapeCString(self.value))]
        elif self.type == UuidType:
            return ovsuuid.to_c_assignment(self.value, var)

    def toEnglish(self, escapeLiteral=returnUnchanged):
        if self.type == IntegerType:
            return '%d' % self.value
        elif self.type == RealType:
            return '%.15g' % self.value
        elif self.type == BooleanType:
            if self.value:
                return 'true'
            else:
                return 'false'
        elif self.type == StringType:
            return escapeLiteral(self.value)
        elif self.type == UuidType:
            return self.value.value

    __need_quotes_re = re.compile("$|true|false|[^_a-zA-Z]|.*[^-._a-zA-Z]")

    @staticmethod
    def __string_needs_quotes(s):
        return Atom.__need_quotes_re.match(s)

    def to_string(self):
        if self.type == IntegerType:
            return '%d' % self.value
        elif self.type == RealType:
            return '%.15g' % self.value
        elif self.type == BooleanType:
            if self.value:
                return 'true'
            else:
                return 'false'
        elif self.type == StringType:
            if Atom.__string_needs_quotes(self.value):
                return ovs_json.to_string(self.value)
            else:
                return self.value
        elif self.type == UuidType:
            return str(self.value)

    @staticmethod
    def new(x):
        if type(x) in [int]:
            t = IntegerType
        elif type(x) == float:
            t = RealType
        elif x in [False, True]:
            t = BooleanType
        elif type(x) in [str]:
            t = StringType
        elif isinstance(x, uuid):
            t = UuidType
        else:
            raise TypeError
        return Atom(t, x)


class Datum(ComparableMixin):
    def __init__(self, type_, values={}):
        self.type = type_
        self.values = values

    def _cmpkey(self):
        return self.values


    # def __cmp__(self, other):
    #     if not isinstance(other, Datum):
    #         return NotImplemented
    #     elif self.values < other.values:
    #         return -1
    #     elif self.values > other.values:
    #         return 1
    #     else:
    #         return 0

    __hash__ = None

    def __contains__(self, item):
        return item in self.values

    def copy(self):
        return Datum(self.type, dict(self.values))

    @staticmethod
    def default(type_):
        if type_.n_min == 0:
            values = {}
        elif type_.is_map():
            values = {type_.key.default(): type_.value.default()}
        else:
            values = {type_.key.default(): None}
        return Datum(type_, values)

    def is_default(self):
        return self == Datum.default(self.type)

    def check_constraints(self):
        """Checks that each of the atoms in 'datum' conforms to the constraints
        specified by its 'type' and raises an ovs.db.error.Error.

        This function is not commonly useful because the most ordinary way to
        obtain a datum is ultimately via Datum.from_json() or Atom.from_json(),
        which check constraints themselves."""
        for keyAtom, valueAtom in self.values.items():
            keyAtom.check_constraints(self.type.key)
            if valueAtom is not None:
                valueAtom.check_constraints(self.type.value)

    @staticmethod
    def from_json(type_, json, symtab=None):
        """Parses 'json' as a datum of the type described by 'type'.  If
        successful, returns a new datum.  On failure, raises an
        ovs.db.error.Error.

        Violations of constraints expressed by 'type' are treated as errors.

        If 'symtab' is nonnull, then named UUIDs in 'symtab' are accepted.
        Refer to ovsdb/SPECS for information about this, and for the syntax
        that this function accepts."""
        is_map = type_.is_map()
        if (is_map or
            (type(json) == list and len(json) > 0 and json[0] == "set")):
            if is_map:
                class_ = "map"
            else:
                class_ = "set"

            inner = db_parser.unwrap_json(json, class_, [list, tuple],
                                              "array")
            n = len(inner)
            if n < type_.n_min or n > type_.n_max:
                raise error.Error("%s must have %d to %d members but %d are "
                                  "present" % (class_, type_.n_min,
                                               type_.n_max, n),
                                  json)

            values = {}
            for element in inner:
                if is_map:
                    key, value = db_parser.parse_json_pair(element)
                    keyAtom = Atom.from_json(type_.key, key, symtab)
                    valueAtom = Atom.from_json(type_.value, value, symtab)
                else:
                    keyAtom = Atom.from_json(type_.key, element, symtab)
                    valueAtom = None

                if keyAtom in values:
                    if is_map:
                        raise error.Error("map contains duplicate key")
                    else:
                        raise error.Error("set contains duplicate")

                values[keyAtom] = valueAtom

            return Datum(type_, values)
        else:
            keyAtom = Atom.from_json(type_.key, json, symtab)
            return Datum(type_, {keyAtom: None})

    def to_json(self):
        if self.type.is_map():
            return ["map", [[k.to_json(), v.to_json()]
                            for k, v in sorted(self.values.items())]]
        elif len(self.values) == 1:
            key = next(iter(self.values))
            return key.to_json()
        else:
            return ["set", [k.to_json() for k in sorted(self.values.keys())]]

    def to_string(self):
        head = tail = None
        if self.type.n_max > 1 or len(self.values) == 0:
            if self.type.is_map():
                head = "{"
                tail = "}"
            else:
                head = "["
                tail = "]"

        s = []
        if head:
            s.append(head)

        for i, key in enumerate(sorted(self.values)):
            if i:
                s.append(", ")

            s.append(key.to_string())
            if self.type.is_map():
                s.append("=")
                s.append(self.values[key].to_string())

        if tail:
            s.append(tail)
        return ''.join(s)

    def as_list(self):
        if self.type.is_map():
            return [[k.value, v.value] for k, v in self.values.items()]
        else:
            return [k.value for k in self.values.keys()]

    def as_dict(self):
        return dict(self.values)

    def as_scalar(self):
        if len(self.values) == 1:
            if self.type.is_map():
                k, v = self.values[next(iter(self.values))]
                return [k.value, v.value]
            else:
                return next(iter(self.values)).value
        else:
            return None

    def to_python(self, uuid_to_row):
        """Returns this datum's value converted into a natural Python
        representation of this datum's type, according to the following
        rules:

        - If the type has exactly one value and it is not a map (that is,
          self.type.is_scalar() returns True), then the value is:

            * An int or long, for an integer column.

            * An int or long or float, for a real column.

            * A bool, for a boolean column.

            * A str or unicode object, for a string column.

            * A uuid.UUID object, for a UUID column without a ref_table.

            * An object represented the referenced row, for a UUID column with
              a ref_table.  (For the Idl, this object will be an ovs.db.idl.Row
              object.)

          If some error occurs (e.g. the database server's idea of the column
          is different from the IDL's idea), then the default value for the
          scalar type is used (see Atom.default()).

        - Otherwise, if the type is not a map, then the value is a Python list
          whose elements have the types described above.

        - Otherwise, the type is a map, and the value is a Python dict that
          maps from key to value, with key and value types determined as
          described above.

        'uuid_to_row' must be a function that takes a value and an
        BaseType and translates UUIDs into row objects."""
        if self.type.is_scalar():
            value = uuid_to_row(self.as_scalar(), self.type.key)
            if value is None:
                return self.type.key.default()
            else:
                return value
        elif self.type.is_map():
            value = {}
            for k, v in self.values.items():
                dk = uuid_to_row(k.value, self.type.key)
                dv = uuid_to_row(v.value, self.type.value)
                if dk is not None and dv is not None:
                    value[dk] = dv
            return value
        else:
            s = set()
            for k in self.values:
                dk = uuid_to_row(k.value, self.type.key)
                if dk is not None:
                    s.add(dk)
            # return sorted(s)
            return list(s)

    @staticmethod
    def from_python(type_, value, row_to_uuid):
        """Returns a new Datum with the given Type 'type_'.  The
        new datum's value is taken from 'value', which must take the form
        described as a valid return value from Datum.to_python() for 'type'.

        Each scalar value within 'value' is initally passed through
        'row_to_uuid', which should convert objects that represent rows (if
        any) into uuid.UUID objects and return other data unchanged.

        Raises ovs.db.error.Error if 'value' is not in an appropriate form for
        'type_'."""
        d = {}
        if type(value) == dict:
            for k, v in value.items():
                ka = Atom.from_python(type_.key, row_to_uuid(k))
                va = Atom.from_python(type_.value, row_to_uuid(v))
                d[ka] = va
        elif type(value) in (list, tuple):
            for k in value:
                ka = Atom.from_python(type_.key, row_to_uuid(k))
                d[ka] = None
        else:
            ka = Atom.from_python(type_.key, row_to_uuid(value))
            d[ka] = None

        datum = Datum(type_, d)
        datum.check_constraints()
        if not datum.conforms_to_type():
            raise error.Error("%d values when type requires between %d and %d"
                              % (len(d), type_.n_min, type_.n_max))

        return datum

    def __getitem__(self, key):
        if not isinstance(key, Atom):
            key = Atom.new(key)
        if not self.type.is_map():
            raise IndexError
        elif key not in self.values:
            raise KeyError
        else:
            return self.values[key].value

    def get(self, key, default=None):
        if not isinstance(key, Atom):
            key = Atom.new(key)
        if key in self.values:
            return self.values[key].value
        else:
            return default

    def __str__(self):
        return self.to_string()

    def conforms_to_type(self):
        n = len(self.values)
        return self.type.n_min <= n <= self.type.n_max

    def cInitDatum(self, var):
        if len(self.values) == 0:
            return ["ovsdb_datum_init_empty(%s);" % var]

        s = ["%s->n = %d;" % (var, len(self.values))]
        s += ["%s->keys = xmalloc(%d * sizeof *%s->keys);"
              % (var, len(self.values), var)]

        for i, key in enumerate(sorted(self.values)):
            s += key.cInitAtom("%s->keys[%d]" % (var, i))

        if self.type.value:
            s += ["%s->values = xmalloc(%d * sizeof *%s->values);"
                  % (var, len(self.values), var)]
            for i, (key, value) in enumerate(sorted(self.values.items())):
                s += value.cInitAtom("%s->values[%d]" % (var, i))
        else:
            s += ["%s->values = NULL;" % var]

        if len(self.values) > 1:
            s += ["ovsdb_datum_sort_assert(%s, OVSDB_TYPE_%s);"
                  % (var, self.type.key.type.to_string().upper())]

        return s


class AtomicType(object):
    def __init__(self, name, default, python_types):
        self.name = name
        self.default = default
        self.python_types = python_types

    @staticmethod
    def from_string(s):
        if s != "void":
            for atomic_type in ATOMIC_TYPES:
                if s == atomic_type.name:
                    return atomic_type
        raise error.Error('"%s" is not an atomic-type' % s, s)

    @staticmethod
    def from_json(json):
        if type(json) not in [str]:
            raise error.Error("atomic-type expected", json)
        else:
            return AtomicType.from_string(json)

    def __str__(self):
        return self.name

    def to_string(self):
        return self.name

    def to_json(self):
        return self.name

    def default_atom(self):
        return Atom(self, self.default)

VoidType = AtomicType("void", None, ())
IntegerType = AtomicType("integer", 0, (int,))
RealType = AtomicType("real", 0.0, (int, float))
BooleanType = AtomicType("boolean", False, (bool,))
StringType = AtomicType("string", "", (str,))
UuidType = AtomicType("uuid", ovsuuid.zero(), (uuid.UUID,))

ATOMIC_TYPES = [VoidType, IntegerType, RealType, BooleanType, StringType,
                UuidType]


def escapeCString(src):
    dst = ""
    for c in src:
        if c in "\\\"":
            dst += "\\" + c
        elif ord(c) < 32:
            if c == '\n':
                dst += '\\n'
            elif c == '\r':
                dst += '\\r'
            elif c == '\a':
                dst += '\\a'
            elif c == '\b':
                dst += '\\b'
            elif c == '\f':
                dst += '\\f'
            elif c == '\t':
                dst += '\\t'
            elif c == '\v':
                dst += '\\v'
            else:
                dst += '\\%03o' % ord(c)
        else:
            dst += c
    return dst


def commafy(x):
    """Returns integer x formatted in decimal with thousands set off by
    commas."""
    return _commafy("%d" % x)


def _commafy(s):
    if s.startswith('-'):
        return '-' + _commafy(s[1:])
    elif len(s) <= 3:
        return s
    else:
        return _commafy(s[:-3]) + ',' + _commafy(s[-3:])


def returnUnchanged(x):
    return x


class BaseType(object):
    def __init__(self, type_, enum=None, min=None, max=None,
                 min_length=0, max_length=sys.maxsize, ref_table_name=None):
        assert isinstance(type_, AtomicType)
        self.type = type_
        self.enum = enum
        self.min = min
        self.max = max
        self.min_length = min_length
        self.max_length = max_length
        self.ref_table_name = ref_table_name
        if ref_table_name:
            self.ref_type = 'strong'
        else:
            self.ref_type = None
        self.ref_table = None

    def default(self):
        return Atom.default(self.type)

    def __eq__(self, other):
        if not isinstance(other, BaseType):
            return NotImplemented
        return (self.type == other.type and self.enum == other.enum and
                self.min == other.min and self.max == other.max and
                self.min_length == other.min_length and
                self.max_length == other.max_length and
                self.ref_table_name == other.ref_table_name)

    def __ne__(self, other):
        if not isinstance(other, BaseType):
            return NotImplemented
        else:
            return not (self == other)

    @staticmethod
    def __parse_uint(parser, name, default):
        value = parser.get_optional(name, [int])
        if value is None:
            value = default
        else:
            max_value = 2 ** 32 - 1
            if not (0 <= value <= max_value):
                raise error.Error("%s out of valid range 0 to %d"
                                  % (name, max_value), value)
        return value

    @staticmethod
    def from_json(json):
        if type(json) in [str]:
            return BaseType(AtomicType.from_json(json))

        parser = db_parser.Parser(json, "ovsdb type")
        atomic_type = AtomicType.from_json(parser.get("type", [str]))

        base = BaseType(atomic_type)

        enum = parser.get_optional("enum", [])
        if enum is not None:
            base.enum = Datum.from_json(
                    BaseType.get_enum_type(base.type), enum)
        elif base.type == IntegerType:
            base.min = parser.get_optional("minInteger", [int])
            base.max = parser.get_optional("maxInteger", [int])
            if (base.min is not None and base.max is not None
                    and base.min > base.max):
                raise error.Error("minInteger exceeds maxInteger", json)
        elif base.type == RealType:
            base.min = parser.get_optional("minReal", [int, float])
            base.max = parser.get_optional("maxReal", [int, float])
            if (base.min is not None and base.max is not None
                    and base.min > base.max):
                raise error.Error("minReal exceeds maxReal", json)
        elif base.type == StringType:
            base.min_length = BaseType.__parse_uint(parser, "minLength", 0)
            base.max_length = BaseType.__parse_uint(parser, "maxLength",
                                                    sys.maxsize)
            if base.min_length > base.max_length:
                raise error.Error("minLength exceeds maxLength", json)
        elif base.type == UuidType:
            base.ref_table_name = parser.get_optional("refTable", ['id'])
            if base.ref_table_name:
                base.ref_type = parser.get_optional("refType", [str],
                                                   "strong")
                if base.ref_type not in ['strong', 'weak']:
                    raise error.Error('refType must be "strong" or "weak" '
                                      '(not "%s")' % base.ref_type)
        parser.finish()

        return base

    def to_json(self):
        if not self.has_constraints():
            return self.type.to_json()

        json = {'type': self.type.to_json()}

        if self.enum:
            json['enum'] = self.enum.to_json()

        if self.type == IntegerType:
            if self.min is not None:
                json['minInteger'] = self.min
            if self.max is not None:
                json['maxInteger'] = self.max
        elif self.type == RealType:
            if self.min is not None:
                json['minReal'] = self.min
            if self.max is not None:
                json['maxReal'] = self.max
        elif self.type == StringType:
            if self.min_length != 0:
                json['minLength'] = self.min_length
            if self.max_length != sys.maxsize:
                json['maxLength'] = self.max_length
        elif self.type == UuidType:
            if self.ref_table_name:
                json['refTable'] = self.ref_table_name
                if self.ref_type != 'strong':
                    json['refType'] = self.ref_type
        return json

    def copy(self):
        base = BaseType(self.type, self.enum.copy(), self.min, self.max,
                        self.min_length, self.max_length, self.ref_table_name)
        base.ref_table = self.ref_table
        return base

    def is_valid(self):
        if self.type in (VoidType, BooleanType, UuidType):
            return True
        elif self.type in (IntegerType, RealType):
            return self.min is None or self.max is None or self.min <= self.max
        elif self.type == StringType:
            return self.min_length <= self.max_length
        else:
            return False

    def has_constraints(self):
        return (self.enum is not None or self.min is not None or
                self.max is not None or
                self.min_length != 0 or self.max_length != sys.maxsize or
                self.ref_table_name is not None)

    def without_constraints(self):
        return BaseType(self.type)

    @staticmethod
    def get_enum_type(atomic_type):
        """Returns the type of the 'enum' member for a BaseType whose
        'type' is 'atomic_type'."""
        return Type(BaseType(atomic_type), None, 1, sys.maxsize)

    def is_ref(self):
        return self.type == UuidType and self.ref_table_name is not None

    def is_strong_ref(self):
        return self.is_ref() and self.ref_type == 'strong'

    def is_weak_ref(self):
        return self.is_ref() and self.ref_type == 'weak'

    def toEnglish(self, escapeLiteral=returnUnchanged):
        if self.type == UuidType and self.ref_table_name:
            s = escapeLiteral(self.ref_table_name)
            if self.ref_type == 'weak':
                s = "weak reference to " + s
            return s
        else:
            return self.type.to_string()

    def constraintsToEnglish(self, escapeLiteral=returnUnchanged,
                             escapeNumber=returnUnchanged):
        if self.enum:
            literals = [value.toEnglish(escapeLiteral)
                        for value in self.enum.values]
            if len(literals) == 2:
                english = 'either %s or %s' % (literals[0], literals[1])
            else:
                english = 'one of %s, %s, or %s' % (literals[0],
                                                    ', '.join(literals[1:-1]),
                                                    literals[-1])
        elif self.min is not None and self.max is not None:
            if self.type == IntegerType:
                english = 'in range %s to %s' % (
                    escapeNumber(commafy(self.min)),
                    escapeNumber(commafy(self.max)))
            else:
                english = 'in range %s to %s' % (
                    escapeNumber("%g" % self.min),
                    escapeNumber("%g" % self.max))
        elif self.min is not None:
            if self.type == IntegerType:
                english = 'at least %s' % escapeNumber(commafy(self.min))
            else:
                english = 'at least %s' % escapeNumber("%g" % self.min)
        elif self.max is not None:
            if self.type == IntegerType:
                english = 'at most %s' % escapeNumber(commafy(self.max))
            else:
                english = 'at most %s' % escapeNumber("%g" % self.max)
        elif self.min_length != 0 and self.max_length != sys.maxsize:
            if self.min_length == self.max_length:
                english = ('exactly %s characters long'
                           % commafy(self.min_length))
            else:
                english = ('between %s and %s characters long'
                        % (commafy(self.min_length),
                           commafy(self.max_length)))
        elif self.min_length != 0:
            return 'at least %s characters long' % commafy(self.min_length)
        elif self.max_length != sys.maxsize:
            english = 'at most %s characters long' % commafy(self.max_length)
        else:
            english = ''

        return english

    def toCType(self, prefix):
        if self.ref_table_name:
            return "struct %s%s *" % (prefix, self.ref_table_name.lower())
        else:
            return {IntegerType: 'int64_t ',
                    RealType: 'double ',
                    UuidType: 'struct uuid ',
                    BooleanType: 'bool ',
                    StringType: 'char *'}[self.type]

    def toAtomicType(self):
        return "OVSDB_TYPE_%s" % self.type.to_string().upper()

    def copyCValue(self, dst, src):
        args = {'dst': dst, 'src': src}
        if self.ref_table_name:
            return ("%(dst)s = %(src)s->header_.uuid;") % args
        elif self.type == StringType:
            return "%(dst)s = xstrdup(%(src)s);" % args
        else:
            return "%(dst)s = %(src)s;" % args

    def initCDefault(self, var, is_optional):
        if self.ref_table_name:
            return "%s = NULL;" % var
        elif self.type == StringType and not is_optional:
            return '%s = "";' % var
        else:
            pattern = {IntegerType: '%s = 0;',
                       RealType: '%s = 0.0;',
                       UuidType: 'uuid_zero(&%s);',
                       BooleanType: '%s = false;',
                       StringType: '%s = NULL;'}[self.type]
            return pattern % var

    def cInitBaseType(self, indent, var):
        stmts = []
        stmts.append('ovsdb_base_type_init(&%s, %s);' % (
                var, self.toAtomicType()))
        if self.enum:
            stmts.append("%s.enum_ = xmalloc(sizeof *%s.enum_);"
                         % (var, var))
            stmts += self.enum.cInitDatum("%s.enum_" % var)
        if self.type == IntegerType:
            if self.min is not None:
                stmts.append('%s.u.integer.min = INT64_C(%d);'
                        % (var, self.min))
            if self.max is not None:
                stmts.append('%s.u.integer.max = INT64_C(%d);'
                        % (var, self.max))
        elif self.type == RealType:
            if self.min is not None:
                stmts.append('%s.u.real.min = %d;' % (var, self.min))
            if self.max is not None:
                stmts.append('%s.u.real.max = %d;' % (var, self.max))
        elif self.type == StringType:
            if self.min_length is not None:
                stmts.append('%s.u.string.minLen = %d;'
                        % (var, self.min_length))
            if self.max_length != sys.maxsize:
                stmts.append('%s.u.string.maxLen = %d;'
                        % (var, self.max_length))
        elif self.type == UuidType:
            if self.ref_table_name is not None:
                stmts.append('%s.u.uuid.refTableName = "%s";'
                        % (var, escapeCString(self.ref_table_name)))
                stmts.append('%s.u.uuid.refType = OVSDB_REF_%s;'
                        % (var, self.ref_type.upper()))
        return '\n'.join([indent + stmt for stmt in stmts])


class Type(object):
    DEFAULT_MIN = 1
    DEFAULT_MAX = 1

    def __init__(self, key, value=None, n_min=DEFAULT_MIN, n_max=DEFAULT_MAX):
        self.key = key
        self.value = value
        self.n_min = n_min
        self.n_max = n_max

    def copy(self):
        if self.value is None:
            value = None
        else:
            value = self.value.copy()
        return Type(self.key.copy(), value, self.n_min, self.n_max)

    def __eq__(self, other):
        if not isinstance(other, Type):
            return NotImplemented
        return (self.key == other.key and self.value == other.value and
                self.n_min == other.n_min and self.n_max == other.n_max)

    def __ne__(self, other):
        if not isinstance(other, Type):
            return NotImplemented
        else:
            return not (self == other)

    def is_valid(self):
        return (self.key.type != VoidType and self.key.is_valid() and
                (self.value is None or
                 (self.value.type != VoidType and self.value.is_valid())) and
                self.n_min <= 1 <= self.n_max)

    def is_scalar(self):
        return self.n_min == 1 and self.n_max == 1 and not self.value

    def is_optional(self):
        return self.n_min == 0 and self.n_max == 1

    def is_composite(self):
        return self.n_max > 1

    def is_set(self):
        return self.value is None and (self.n_min != 1 or self.n_max != 1)

    def is_map(self):
        return self.value is not None

    def is_smap(self):
        return (self.is_map()
                and self.key.type == StringType
                and self.value.type == StringType)

    def is_optional_pointer(self):
        return (self.is_optional() and not self.value
                and (self.key.type == StringType or self.key.ref_table_name))

    @staticmethod
    def __n_from_json(json, default):
        if json is None:
            return default
        elif type(json) == int and 0 <= json <= sys.maxsize:
            return json
        else:
            raise error.Error("bad min or max value", json)

    @staticmethod
    def from_json(json):
        if type(json) in [str]:
            return Type(BaseType.from_json(json))

        parser = db_parser.Parser(json, "ovsdb type")
        key_json = parser.get("key", [dict, str])
        value_json = parser.get_optional("value", [dict, str])
        min_json = parser.get_optional("min", [int])
        max_json = parser.get_optional("max", [int, str])
        parser.finish()

        key = BaseType.from_json(key_json)
        if value_json:
            value = BaseType.from_json(value_json)
        else:
            value = None

        n_min = Type.__n_from_json(min_json, Type.DEFAULT_MIN)

        if max_json == 'unlimited':
            n_max = sys.maxsize
        else:
            n_max = Type.__n_from_json(max_json, Type.DEFAULT_MAX)

        type_ = Type(key, value, n_min, n_max)
        if not type_.is_valid():
            raise error.Error("ovsdb type fails constraint checks", json)
        return type_

    def to_json(self):
        if self.is_scalar() and not self.key.has_constraints():
            return self.key.to_json()

        json = {"key": self.key.to_json()}
        if self.value is not None:
            json["value"] = self.value.to_json()
        if self.n_min != Type.DEFAULT_MIN:
            json["min"] = self.n_min
        if self.n_max == sys.maxsize:
            json["max"] = "unlimited"
        elif self.n_max != Type.DEFAULT_MAX:
            json["max"] = self.n_max
        return json

    def toEnglish(self, escapeLiteral=returnUnchanged):
        keyName = self.key.toEnglish(escapeLiteral)
        if self.value:
            valueName = self.value.toEnglish(escapeLiteral)

        if self.is_scalar():
            return keyName
        elif self.is_optional():
            if self.value:
                return "optional %s-%s pair" % (keyName, valueName)
            else:
                return "optional %s" % keyName
        else:
            if self.n_max == sys.maxsize:
                if self.n_min:
                    quantity = "%s or more " % commafy(self.n_min)
                else:
                    quantity = ""
            elif self.n_min:
                quantity = "%s to %s " % (commafy(self.n_min),
                                          commafy(self.n_max))
            else:
                quantity = "up to %s " % commafy(self.n_max)

            if self.value:
                return "map of %s%s-%s pairs" % (quantity, keyName, valueName)
            else:
                if keyName.endswith('s'):
                    plural = keyName + "es"
                else:
                    plural = keyName + "s"
                return "set of %s%s" % (quantity, plural)

    def constraintsToEnglish(self, escapeLiteral=returnUnchanged,
                             escapeNumber=returnUnchanged):
        constraints = []
        keyConstraints = self.key.constraintsToEnglish(escapeLiteral,
                                                       escapeNumber)
        if keyConstraints:
            if self.value:
                constraints.append('key %s' % keyConstraints)
            else:
                constraints.append(keyConstraints)

        if self.value:
            valueConstraints = self.value.constraintsToEnglish(escapeLiteral,
                                                               escapeNumber)
            if valueConstraints:
                constraints.append('value %s' % valueConstraints)

        return ', '.join(constraints)

    def cDeclComment(self):
        if self.n_min == 1 and self.n_max == 1 and self.key.type == StringType:
            return "\t/* Always nonnull. */"
        else:
            return ""

    def cInitType(self, indent, var):
        initKey = self.key.cInitBaseType(indent, "%s.key" % var)
        if self.value:
            initValue = self.value.cInitBaseType(indent, "%s.value" % var)
        else:
            initValue = ('%sovsdb_base_type_init(&%s.value, '
                         'OVSDB_TYPE_VOID);' % (indent, var))
        initMin = "%s%s.n_min = %s;" % (indent, var, self.n_min)
        if self.n_max == sys.maxsize:
            n_max = "UINT_MAX"
        else:
            n_max = self.n_max
        initMax = "%s%s.n_max = %s;" % (indent, var, n_max)
        return "\n".join((initKey, initValue, initMin, initMax))
