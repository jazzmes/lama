MYDIR=`pwd -P $0`
DIR=`dirname "$MYDIR/$0"`

trap "$DIR/kill_mon.sh" SIGHUP SIGINT SIGTERM

for i in 7 8 9 10 11 31 32
do
    echo "10.1.1.$i"
    ssh 10.1.1.$i -l lama "tail -n 1000 -f /var/log/lama/app_* /var/log/lama/pro_*" &
done
wait