#!/bin/bash
function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

# currently assumes that the interface is eth0
IP=$1
IFACE=${2:-eth0}
TFILE="/etc/network/interfaces"
if ! valid_ip $IP
then
    echo "Invalid IP: $IP"
    exit 1
fi

# remove possible old virtual address:
# NOTE: removes all statically assigned IP addresses
perl -i.backup -0pe 's/(\n)*iface \S* inet static.*address 192\.168\.\d{1,3}\.\d{1,3}.*netmask 255\.255\.255\.255/\n/igs' $TFILE
# appends new virtual address
echo -e "iface ${IFACE} inet static\n\taddress $IP\n\tnetmask 255.255.255.255" >> $TFILE

perl -i.backup -0pe 's/net.ipv4.conf.eth0.arp_ignore = \d+\n/\n/igs' /etc/sysctl.conf
perl -i.backup -0pe 's/net.ipv4.conf.eth0.arp_announce = \d+\n/\n/igs' /etc/sysctl.conf
if [[ ${IFACE} == "lo" ]]
then
    echo -e "net.ipv4.conf.eth0.arp_ignore = 1" >> /etc/sysctl.conf
    echo -e "net.ipv4.conf.eth0.arp_announce = 2" >> /etc/sysctl.conf
    sysctl -p

    ifdown lo
    ifup lo
fi

# restart interface
ifdown eth0
ifup eth0

