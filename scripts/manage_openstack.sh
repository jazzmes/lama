#!/usr/bin/env bash

if [ "$1" != "start" ] && [ "$1" != "stop" ]
then
    echo "Choose option: start or stop"
    exit 1
fi

for i in 1 2 3 4 6 8 9 13 14 16 17 18 19 26 27 31 38 41 42 44 45 49; do
    if [ "$1" == "start" ]
    then
        echo "Starting OpenStack @ 10.1.1.$i"
    else
        echo "Stopping OpenStack @ 10.1.1.$i"
    fi
    ssh lama@10.1.1.$i sudo systemctl $1 neutron-openvswitch-agent
    ssh lama@10.1.1.$i sudo systemctl $1 openstack-nova-compute
done
