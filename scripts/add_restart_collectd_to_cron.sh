#crontab -l | grep -q collectd
#if [ $? -eq 1 ]; then
#fi

echo "Adding periodical collectd restart"
crontab -l | grep -v collectd > /tmp/mycron
echo "0 1 * * * /home/lama/lama/check_and_restart_collectd.sh 2>&1 > /var/log/lama/collectd.restart.log" >> /tmp/mycron
crontab /tmp/mycron
rm /tmp/mycron
