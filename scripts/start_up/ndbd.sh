#!/bin/bash

# chkconfig: 345 99 30
# description: ndb_mgmd start/stop script

### BEGIN INIT INFO
# Provides:             ndbd
# Required-Start:       $local_fs $network $remote_fs
# Required-Stop:        $local_fs $network $remote_fs
# Default-Start:        3 4 5
# Default-Stop:         0 1 6
# Short-Description:    ndb_mgmd start/stop script
### END INIT INFO

basename=`basename $0`
LOG=/var/log/ndbd.log
DATA_DIR="/usr/local/mysql"
PID_FILE="$DATA_DIR/ndbd.pid"
TEE="/usr/bin/tee -a $LOG"
DATE=`date "+%Y-%m-%d %H:%M:%S"`
OPTIONS=""
PROG_NAME="ndbd"
LOCK_FILE=""

if [ -d /var/lock/subsys ]; then
  LOCK_FILE=/var/lock/subsys/$basename
else
  LOCK_FILE=/var/lock/$basename
fi

if [ ! command -v $PROG_NAME 2>/dev/null ]; then
  echo -e "$PROG_NAME executable not found"
  exit 1
fi

RETVAL=0
start() {
    echo -e "$DATE: Starting $PROG_NAME $OPTIONS !" | $TEE
    PID=`pgrep -x "$PROG_NAME"`
    if [ -n "$PID" ]; then
        echo -e "$PROG_NAME is already running with pids:$PID\n" | $TEE
        return 0
    else
      $PROG_NAME $OPTIONS | $TEE
      RETVAL=$?
      [ $RETVAL -eq 0 ] && /bin/touch $LOCK_FILE
      sleep 3
      PID=`pgrep -x "$PROG_NAME"`
      if [ "$PID" ]; then
        echo -e "OK: Success to start $PROG_NAME \n" | $TEE
          return 0
      else
        echo -e "Error: Failed to start $PROG_NAME \n" | $TEE
          return 1
      fi
     fi
}

stop() {
    echo -e "$DATE: Stopping $PROG_NAME : " | $TEE
    PIDS=`pgrep -x "$PROG_NAME"`
    if  [[ $PIDS ]]; then
        for PID in $PIDS; do
            kill $PID
	done
    else
        `rm -f ${PID_FILE}`
        `rm -f ${LOCK_FILE}`
        echo -e "Error: Failed $PROG_NAME not running !\n" | $TEE
        return 1
    fi
    i=1
    while [ `pgrep -x "$PROG_NAME"` ];
    do
       if [ $i -eq 60 ]; then
        echo -e "giving up after 60 secods\n"| $TEE
         return 0
       fi
       i=`expr $i + 1`
       sleep 1
    done
    PID=`pgrep -x '$PROG_NAME'`
    if [[ "$PID" ]]; then
      echo -e "Error: Failed $PROG_NAME not running\n" | $TEE
       return 1
    else
      `rm -f ${PID_FILE}`
      echo -e "OK: Success to stop $PROG_NAME\n" | $TEE
      return 0
    fi
}

case $1 in
  start)
        start
        RETVAL=$?
        ;;
  stop)
        stop
        RETVAL=$?
        ;;
  status)
        PIDS=`pgrep -x "$PROG_NAME"`
        RETVAL=$?
        if [ "$PIDS" ]; then
            echo -e "$PROG_NAME is running with pids:\n$PIDS"
            RETVAL=0
        else
            echo -e "$PROG_NAME is not running"
            RETVAL=1
	fi
	;;
  restart)
        stop
        start
        RETVAL=$?
        ;;
  *)
     echo "Usage: $basename {start|stop|restart|status}"
     exit 1
     ;;
esac
exit $RETVAL