#!/usr/bin/env bash

systemctl restart firewalld

# Base ports
firewall-cmd --zone=public --add-port=22/tcp --permanent
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --zone=public --add-port=111/tcp --permanent
firewall-cmd --zone=public --add-port=443/tcp --permanent

# VNC
firewall-cmd --zone=public --add-port=5900-5920/tcp --permanent

# CloudWatch
firewall-cmd --zone=public --add-port=5000/udp --permanent
firewall-cmd --zone=public --add-port=3000/udp --permanent

# LAMA
# dispatcher
firewall-cmd --zone=public --add-port=20001/tcp --permanent
# providers
firewall-cmd --zone=public --add-port=20010/tcp --permanent
firewall-cmd --zone=public --add-port=20010/udp --permanent
firewall-cmd --zone=public --add-port=20011/tcp --permanent
# REDIS
firewall-cmd --zone=public --add-port=6379/tcp --permanent
# SDN Controller
firewall-cmd --zone=public --add-port=16633/tcp --permanent
# OpenVSwitch
firewall-cmd --zone=public --add-port=6640/tcp --permanent
# Experiment server
firewall-cmd --zone=public --add-port=58228/tcp --permanent
# Anomaly server
firewall-cmd --zone=public --add-port=8717/tcp --permanent

systemctl restart firewalld