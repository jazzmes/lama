IP_LAST_OCTET=$1
APP=${2:-"rubbos-001"}
PAST_LINES=${3:-100}

ssh lama@10.1.1.$IP_LAST_OCTET tail -n $PAST_LINES -F /var/log/lama/app_$APP.log
