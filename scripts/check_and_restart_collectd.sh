echo "Checking collectd crontab job"
res=`/sbin/pidof -x collectd`

if [ $? -eq 0 ]; then
    echo "Collectd is running with pid $res :: Restart to reset memory usage"
    systemctl restart collectd
fi
