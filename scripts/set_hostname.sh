#!/bin/sh
### BEGIN INIT INFO
# Provides:          	set_hostname
# Required-Start:	$remote_fs $syslog
# Required-Stop:	$remote_fs $syslog
# Default-Start:	2 3 4 5
# Default-Stop: 	0 1 6
# Short-Description: 	Set hostname based on the ip of eth0
# Description:       	Get IP of eth0 and request the correspondin name.
### END INIT INFO

do_start () {
    OLD_HOSTNAME=$(hostname)
    IP=`ifconfig eth0 2>/dev/null|awk '/inet addr:/ {print $2}'|sed 's/addr://'`
    HOSTNAME=$(host $IP | cut -d ' ' -f 5 | sed -r 's/((.*)[^\.])\.?/\1/g' )
    echo $HOSTNAME > /etc/hostname
    hostname $HOSTNAME
    echo "Hostname set to $HOSTNAME"
}

do_status () {
	HOSTNAME=$(hostname)
	if [ "$HOSTNAME" ] ; then
	    echo "Hostname set: $HOSTNAME"
		return 0
	else
		return 4
	fi
}

case "$1" in
  start|"")
	do_start
	;;
  restart|reload|force-reload)
	echo "Error: argument '$1' not supported" >&2
	exit 3
	;;
  stop)
	# No-op
	;;
  status)
	do_status
	exit $?
	;;
  *)
	echo "Usage: hostname.sh [start|stop]" >&2
	exit 3
	;;
esac

: